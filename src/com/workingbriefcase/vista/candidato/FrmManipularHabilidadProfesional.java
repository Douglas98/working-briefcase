/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.workingbriefcase.vista.candidato;

import com.workingbriefcase.persistencia.controladores.ArealaboralJpaController;
import com.workingbriefcase.persistencia.controladores.CurriculumHabilidadJpaController;
import com.workingbriefcase.persistencia.controladores.CurriculumProfesionJpaController;
import com.workingbriefcase.persistencia.controladores.HabilidadprofesionalJpaController;
import com.workingbriefcase.persistencia.controladores.ProfesionJpaController;
import com.workingbriefcase.persistencia.entities.Arealaboral;
import com.workingbriefcase.persistencia.entities.Curriculum;
import com.workingbriefcase.persistencia.entities.CurriculumHabilidad;
import com.workingbriefcase.persistencia.entities.CurriculumProfesion;
import com.workingbriefcase.persistencia.entities.Habilidadprofesional;
import com.workingbriefcase.persistencia.entities.Profesion;
import com.workingbriefcase.utilidades.ComboBoxItem;
import com.workingbriefcase.utilidades.JTableCell;
import com.workingbriefcase.utilidades.SwingFormUtilities;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComponent;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.table.DefaultTableModel;

/**
 * Nombre de la clase: FrmManipularExperienciaLaboral
 * Fecha Creación: 11-09-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class FrmManipularHabilidadProfesional extends javax.swing.JInternalFrame {
    //<editor-fold defaultstate="collapsed" desc="variables globales">
    //tipo de manipulación con la que abrio el formulario
    TipoManipulacion tipoManipulacionActual;
    
    //instancia de la habilidad del profesional 
    CurriculumHabilidad habilidadManipulando;
    
    //curriculum al que pertenece la experiencia laboral
    Curriculum curriculumPertenece;
    
    //instancia al manupulador de curriculum que ha invocado a este form
    FrmManipularCurriculum frmInvocador;
    
    //instancia de clases dao
    CurriculumHabilidadJpaController daoCurriculumHabilidad;
    ArealaboralJpaController daoAreaLaboral;
    ProfesionJpaController daoProfesion;
    HabilidadprofesionalJpaController daoHabilidadprofesional;
    
    //lista de controles del formulario
    JComponent [] frmExperienciaCntrls;
    
    //opciones del cmbNivelDominio
    String [] cmbNivelDominioItems;
    
    
    //otras instancias
    SwingFormUtilities sfutil;
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="constructores">
    /** Creates new form FrmManipularExperienciaLaboral */
    public FrmManipularHabilidadProfesional(CurriculumHabilidad habilidadManipulando, Curriculum pertenece
            , TipoManipulacion tipoManipulacion, FrmManipularCurriculum frmInvocador) {
        initComponents();
        
        //inicializar variables de instancia locales
        tipoManipulacionActual = tipoManipulacion;
        this.habilidadManipulando = habilidadManipulando;
        this.frmInvocador = frmInvocador;
        this.curriculumPertenece = pertenece;
        
        //inicializar otros aspectos
        initialize();
    }
    
    private void initialize(){
        //inicializar otras instancias
        sfutil = SwingFormUtilities.getInstance();
        
        //inicializar instancias dao
        daoCurriculumHabilidad = new CurriculumHabilidadJpaController();
        daoAreaLaboral = new ArealaboralJpaController();
        daoProfesion = new ProfesionJpaController();
        daoHabilidadprofesional = new HabilidadprofesionalJpaController();
        
        //inicializar arreglo de controles del formulario
        frmExperienciaCntrls = new JComponent[]{
            txtCorrelativo, cmbAreaLaboral, cmbHabilidad, cmbNivelDominio
        };
        
        //inicializar las opciones del cmbNivelDominio
        
        //solo se aceptará un id inferior a 1 cuando el formulario
        //haya sido abierto para otra cosa que no sea inserción y el id
        //no es válido
        if(tipoManipulacionActual != TipoManipulacion.Insertar){
            if(habilidadManipulando == null){
                //avisar de mal parametro y cerrar el formulario
                sfutil.mostrarMensajeFallo(this, "La Habilidad profesional que desea manipular"
                        + "no es válida");
                this.dispose();
            }
        }
        
        //si fue abierto para visualización solamente
        if(tipoManipulacionActual == TipoManipulacion.Visualizar){
            sfutil.alternarControlesActivos(frmExperienciaCntrls, false);
            btnGuardarCambios.setVisible(false);
        }
        //si fue abierto para insersión
        else if(tipoManipulacionActual == TipoManipulacion.Insertar){
            sfutil.alternarControlesActivos(frmExperienciaCntrls, true);
        }
        //si fue abierto para modificación
        else if(tipoManipulacionActual == TipoManipulacion.Modificar){
            sfutil.alternarControlesActivos(frmExperienciaCntrls, true);
        }
        //sino, salir del formulario
        else{
            sfutil.mostrarMensajeFallo(this, "No se pudo abrir el formulario");
            this.dispose();
        }
        
        //añadiendo escuchador de eventos de ventana a este internal frame
        this.addInternalFrameListener(new InternalFrameAdapter() {
            /**
             * Realizará una actualización del curriculum siendo modificado cuando
             * este formulario sea cerrado
             * @param e 
             */
            @Override
            public void internalFrameClosed(InternalFrameEvent e) {
                frmInvocador.llenarFormularioCurriculum(); //esta línea lo hará
                
                super.internalFrameClosed(e);
            }
            
        });
        
        //llenar cmbs correspondientes
        llenarCmbAreaLaboral();
        llenarCmbHabilidadProfesional();
        llenarCmbNivelDominio();
        
        //llenar formulario si la operación no es insertar
        if(tipoManipulacionActual != TipoManipulacion.Insertar){
            llenarFormularioHabilidadProfesional();
        }
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="tipos anidados">
    public static enum TipoManipulacion{
        Visualizar,
        Insertar,
        Modificar,
    }
    
    
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones CRUD Habilidad profesional">
    private void insertarHabilidadProfesional(){
        try {
            CurriculumHabilidad objetivo;

            objetivo = obtenerDatosFromFormulario();
            
            objetivo.setCurriculum(curriculumPertenece);

            daoCurriculumHabilidad.create(objetivo);
            
            sfutil.mostrarMensajeExito(this, "La operación se ha realizado con exito");
        } catch (Exception e) {
            sfutil.mostrarMensajeExcepcion(this, "Ha ocurrido un error inesperado", e);
        }
    }
    
    private void modificarHabilidadProfesional(){
        try {
            CurriculumHabilidad objetivo;
            CurriculumHabilidad tmp;

            objetivo = daoCurriculumHabilidad.findCurriculumHabilidad(habilidadManipulando.getId());

            tmp = obtenerDatosFromFormulario();

            objetivo.setCorrelativo(tmp.getCorrelativo());
            objetivo.setHabilidadProfesional(tmp.getHabilidadProfesional());
            objetivo.setNivelDominio(tmp.getNivelDominio());

            daoCurriculumHabilidad.edit(objetivo);
            
            sfutil.mostrarMensajeExito(this, "La operación se ha realizado con exito");
        } catch (Exception e) {
            sfutil.mostrarMensajeExcepcion(this, "Ha ocurrido un error inesperado", e);
        }
    }
    
    private void llenarFormularioHabilidadProfesional(){
        CurriculumHabilidad objetivo;
        ComboBoxItem ci;
        
        objetivo = habilidadManipulando;
        
        txtCorrelativo.setText(String.valueOf(objetivo.getCorrelativo()));
        
        ci = new ComboBoxItem();
        
        ci.setValor(objetivo.getHabilidadProfesional().getAreaLaboral().getId());
        
        sfutil.setJComboBoxSelectedItem(cmbAreaLaboral, ci);
        
        ci = new ComboBoxItem();
        
        ci.setValor(objetivo.getHabilidadProfesional().getId());
        
        sfutil.setJComboBoxSelectedItem(cmbHabilidad, ci);
        
        cmbNivelDominio.setSelectedItem(objetivo.getNivelDominio());
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones CRUD Area Laboral">
    private void llenarCmbAreaLaboral(){
        DefaultComboBoxModel dcmbm;
        
        dcmbm = new DefaultComboBoxModel();
        
        
        for(Arealaboral areaLaboral : daoAreaLaboral.findArealaboralEntities()){
            ComboBoxItem ci;
            
            ci = new ComboBoxItem(areaLaboral.getId(), areaLaboral.getNombre());
            
            dcmbm.addElement(ci);
        }
        
        cmbAreaLaboral.setModel(dcmbm);
    }
        
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones CRUD Profesion">
    private void llenarCmbHabilidadProfesional(){
        DefaultComboBoxModel dcmbm;
        
        dcmbm = new DefaultComboBoxModel();
        
        
        for(Profesion pofesion : daoAreaLaboral.findArealaboral((
                (ComboBoxItem) cmbAreaLaboral.getSelectedItem()).getValor()).getProfesionList()){
            ComboBoxItem ci;
            
            ci = new ComboBoxItem(pofesion.getId(), pofesion.getNombre());
            
            dcmbm.addElement(ci);
        }
        
        cmbHabilidad.setModel(dcmbm);
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones formulario Habilidad profesional">
    private void llenarCmbNivelDominio(){
        DefaultComboBoxModel dcmbm;
        
        cmbNivelDominioItems  = new String[]{
            "Básico", "Intermedio", "Avanzado", "Experto"
        };
        
        dcmbm = new DefaultComboBoxModel(cmbNivelDominioItems);
        
        cmbNivelDominio.setModel(dcmbm);
    }
    
    private CurriculumHabilidad obtenerDatosFromFormulario(){
        CurriculumHabilidad salida;
        
        salida = new CurriculumHabilidad();
        
        salida.setCorrelativo(Integer.parseInt(txtCorrelativo.getText()));
        salida.setHabilidadProfesional(daoHabilidadprofesional.findHabilidadprofesional(((ComboBoxItem) cmbHabilidad.getSelectedItem()).getValor()));
        salida.setNivelDominio(cmbNivelDominio.getSelectedItem().toString());
        
        return salida;
    }
//</editor-fold>
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtCorrelativo = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        cmbAreaLaboral = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        cmbHabilidad = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnGuardarCambios = new javax.swing.JButton();
        cmbNivelDominio = new javax.swing.JComboBox<>();

        setClosable(true);

        txtCorrelativo.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel3.setText("Area Laboral");

        cmbAreaLaboral.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        cmbAreaLaboral.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbAreaLaboral.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbAreaLaboralItemStateChanged(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel4.setText("Habilidad");

        cmbHabilidad.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        cmbHabilidad.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel1.setText("Habilidad Profesional");

        jLabel5.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel5.setText("Nivel de dominio");

        jLabel2.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel2.setText("correlativo");

        btnGuardarCambios.setText("Guardar Cambios");
        btnGuardarCambios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnGuardarCambiosMousePressed(evt);
            }
        });

        cmbNivelDominio.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        cmbNivelDominio.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(144, 144, 144)
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addGap(44, 44, 44)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbNivelDominio, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbHabilidad, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbAreaLaboral, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtCorrelativo, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(228, 228, 228)
                .addComponent(btnGuardarCambios)
                .addContainerGap(241, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtCorrelativo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cmbAreaLaboral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cmbHabilidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(cmbNivelDominio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(btnGuardarCambios)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardarCambiosMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGuardarCambiosMousePressed
        if(tipoManipulacionActual == TipoManipulacion.Modificar){
            modificarHabilidadProfesional();
        }else if(tipoManipulacionActual == TipoManipulacion.Insertar){
            insertarHabilidadProfesional();
        }
    }//GEN-LAST:event_btnGuardarCambiosMousePressed

    private void cmbAreaLaboralItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbAreaLaboralItemStateChanged
        llenarCmbHabilidadProfesional();
    }//GEN-LAST:event_cmbAreaLaboralItemStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGuardarCambios;
    private javax.swing.JComboBox<String> cmbAreaLaboral;
    private javax.swing.JComboBox<String> cmbHabilidad;
    private javax.swing.JComboBox<String> cmbNivelDominio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JTextField txtCorrelativo;
    // End of variables declaration//GEN-END:variables

}
