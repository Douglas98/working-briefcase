/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.workingbriefcase.vista.candidato;

import com.workingbriefcase.persistencia.controladores.ArealaboralJpaController;
import com.workingbriefcase.persistencia.controladores.CurriculumProfesionJpaController;
import com.workingbriefcase.persistencia.controladores.ProfesionJpaController;
import com.workingbriefcase.persistencia.entities.Arealaboral;
import com.workingbriefcase.persistencia.entities.Curriculum;
import com.workingbriefcase.persistencia.entities.CurriculumProfesion;
import com.workingbriefcase.persistencia.entities.Profesion;
import com.workingbriefcase.utilidades.ComboBoxItem;
import com.workingbriefcase.utilidades.JTableCell;
import com.workingbriefcase.utilidades.SwingFormUtilities;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComponent;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.table.DefaultTableModel;

/**
 * Nombre de la clase: FrmManipularExperienciaLaboral
 * Fecha Creación: 11-09-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class FrmManipularExperienciaLaboral extends javax.swing.JInternalFrame {
//<editor-fold defaultstate="collapsed" desc="variables globales">
    //tipo de manipulación con la que abrio el formulario
    TipoManipulacion tipoManipulacionActual;
    
    //identidad de la experiencia siendo manipulada
    int idExperienciaEnProfesionManipulando;
    
    //curriculum al que pertenece la experiencia laboral
    Curriculum curriculumPertenece;
    
    //instancia al manupulador de curriculum que ha invocado a este form
    FrmManipularCurriculum frmInvocador;
    
    //instancia de clases dao
    CurriculumProfesionJpaController daoCurriculumProfesion;
    ArealaboralJpaController daoAreaLaboral;
    ProfesionJpaController daoProfesion;
    
    //lista de controles del formulario
    JComponent [] frmExperienciaCntrls;
    
    
    //otras instancias
    SwingFormUtilities sfutil;
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="constructores">
    /** Creates new form FrmManipularExperienciaLaboral */
    public FrmManipularExperienciaLaboral(int idExperienciaEnProfesionManipulando, Curriculum pertenece
            , TipoManipulacion tipoManipulacion, FrmManipularCurriculum frmInvocador) {
        initComponents();
        
        //inicializar variables de instancia locales
        tipoManipulacionActual = tipoManipulacion;
        this.idExperienciaEnProfesionManipulando = idExperienciaEnProfesionManipulando;
        this.frmInvocador = frmInvocador;
        this.curriculumPertenece = pertenece;
        
        //inicializar otros aspectos
        initialize();
    }
    
    private void initialize(){
        //inicializar otras instancias
        sfutil = SwingFormUtilities.getInstance();
        
        //inicializar instancias dao
        daoCurriculumProfesion = new CurriculumProfesionJpaController();
        daoAreaLaboral = new ArealaboralJpaController();
        daoProfesion = new ProfesionJpaController();
        
        //inicializar arreglo de controles del formulario
        frmExperienciaCntrls = new JComponent[]{
            txtCorrelativo, CmbAreaLaboral, cmbProfesion, spnAnnosExperiencia
        };
        
        //solo se aceptará un id inferior a 1 cuando el formulario
        //haya sido abierto para otra cosa que no sea inserción y el id
        //no es válido
        if(tipoManipulacionActual != TipoManipulacion.Insertar){
            if(idExperienciaEnProfesionManipulando < 1){
                //avisar de mal parametro y cerrar el formulario
                sfutil.mostrarMensajeFallo(this, "La Experiencia laboral que desea manipular"
                        + "no es válida");
                this.dispose();
            }
        }
        
        //si fue abierto para visualización solamente
        if(tipoManipulacionActual == TipoManipulacion.Visualizar){
            sfutil.alternarControlesActivos(frmExperienciaCntrls, false);
            btnGuardarCambios.setVisible(false);
        }
        //si fue abierto para insersión
        else if(tipoManipulacionActual == TipoManipulacion.Insertar){
            sfutil.alternarControlesActivos(frmExperienciaCntrls, true);
        }
        //si fue abierto para modificación
        else if(tipoManipulacionActual == TipoManipulacion.Modificar){
            sfutil.alternarControlesActivos(frmExperienciaCntrls, true);
        }
        //sino, salir del formulario
        else{
            sfutil.mostrarMensajeFallo(this, "No se pudo abrir el formulario");
            this.dispose();
        }
        
        //añadiendo escuchador de eventos de ventana a este internal frame
        this.addInternalFrameListener(new InternalFrameAdapter() {
            /**
             * Realizará una actualización del curriculum siendo modificado cuando
             * este formulario sea cerrado
             * @param e 
             */
            @Override
            public void internalFrameClosed(InternalFrameEvent e) {
                frmInvocador.llenarFormularioCurriculum(); //esta línea lo hará
                
                super.internalFrameClosed(e);
            }
            
        });
        
        //llenar cmbs correspondientes
        llenarCmbAreaLaboral();
        llenarCmbProfesion();
        
        //llenar formulario si la operación no es insertar
        if(tipoManipulacionActual != TipoManipulacion.Insertar){
            llenarFormularioExperiencia();
        }
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="tipos anidados">
    public static enum TipoManipulacion{
        Visualizar,
        Insertar,
        Modificar,
    }
    
    
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones CRUD Experiencia Laboral">
    private void insertarExperienciaLaboral(){
        try {
            CurriculumProfesion objetivo;

            objetivo = obtenerDatosFromFormulario();
            
            objetivo.setCurriculum(curriculumPertenece);

            daoCurriculumProfesion.create(objetivo);
            
            sfutil.mostrarMensajeExito(this, "La operación se ha realizado con exito");
        } catch (Exception e) {
            sfutil.mostrarMensajeExcepcion(this, "Ha ocurrido un error inesperado", e);
        }
    }
    
    private void modificarExperienciaLaboral(){
        try {
            CurriculumProfesion objetivo;
            CurriculumProfesion tmp;

            objetivo = daoCurriculumProfesion.findCurriculumProfesion(idExperienciaEnProfesionManipulando);

            tmp = obtenerDatosFromFormulario();

            objetivo.setCorrelativo(tmp.getCorrelativo());
            objetivo.setProfesion(tmp.getProfesion());
            objetivo.setAnnosExperiencia(tmp.getAnnosExperiencia());

            daoCurriculumProfesion.edit(objetivo);
            
            sfutil.mostrarMensajeExito(this, "La operación se ha realizado con exito");
        } catch (Exception e) {
            sfutil.mostrarMensajeExcepcion(this, "Ha ocurrido un error inesperado", e);
        }
    }
    
    private void llenarFormularioExperiencia(){
        CurriculumProfesion objetivo;
        ComboBoxItem ci;
        
        objetivo = daoCurriculumProfesion.findCurriculumProfesion(idExperienciaEnProfesionManipulando);
        
        txtCorrelativo.setText(String.valueOf(objetivo.getCorrelativo()));
        
        ci = new ComboBoxItem();
        
        ci.setValor(objetivo.getProfesion().getAreaLaboral().getId());
        
        sfutil.setJComboBoxSelectedItem(CmbAreaLaboral, ci);
        
        ci = new ComboBoxItem();
        
        ci.setValor(objetivo.getProfesion().getId());
        
        sfutil.setJComboBoxSelectedItem(cmbProfesion, ci);
        
        spnAnnosExperiencia.setValue(objetivo.getAnnosExperiencia());
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones CRUD Area Laboral">
    private void llenarCmbAreaLaboral(){
        DefaultComboBoxModel dcmbm;
        
        dcmbm = new DefaultComboBoxModel();
        
        
        for(Arealaboral areaLaboral : daoAreaLaboral.findArealaboralEntities()){
            ComboBoxItem ci;
            
            ci = new ComboBoxItem(areaLaboral.getId(), areaLaboral.getNombre());
            
            dcmbm.addElement(ci);
        }
        
        CmbAreaLaboral.setModel(dcmbm);
    }
        
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones CRUD Profesion">
    private void llenarCmbProfesion(){
        DefaultComboBoxModel dcmbm;
        
        dcmbm = new DefaultComboBoxModel();
        
        
        for(Profesion pofesion : daoAreaLaboral.findArealaboral((
                (ComboBoxItem) CmbAreaLaboral.getSelectedItem()).getValor()).getProfesionList()){
            ComboBoxItem ci;
            
            ci = new ComboBoxItem(pofesion.getId(), pofesion.getNombre());
            
            dcmbm.addElement(ci);
        }
        
        cmbProfesion.setModel(dcmbm);
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones formulario Experiencia Laboral">
    private CurriculumProfesion obtenerDatosFromFormulario(){
        CurriculumProfesion salida;
        
        salida = new CurriculumProfesion();
        
        salida.setCorrelativo(Integer.parseInt(txtCorrelativo.getText()));
        salida.setProfesion(new Profesion());
        salida.getProfesion().setId(((ComboBoxItem) cmbProfesion.getSelectedItem()).getValor());
        salida.getProfesion().setAreaLaboral(new Arealaboral());
        salida.getProfesion().getAreaLaboral().setId(((ComboBoxItem) CmbAreaLaboral.getSelectedItem()).getValor());
        salida.setAnnosExperiencia((int) spnAnnosExperiencia.getValue());
        
        return salida;
    }
//</editor-fold>
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtCorrelativo = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        CmbAreaLaboral = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        cmbProfesion = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnGuardarCambios = new javax.swing.JButton();
        spnAnnosExperiencia = new javax.swing.JSpinner();

        setClosable(true);

        txtCorrelativo.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel3.setText("Area Laboral");

        CmbAreaLaboral.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        CmbAreaLaboral.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        CmbAreaLaboral.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbAreaLaboralItemStateChanged(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel4.setText("Profesión");

        cmbProfesion.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        cmbProfesion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel1.setText("Experiencia Profesional");

        jLabel5.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel5.setText("Años de Experiencia");

        jLabel2.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel2.setText("correlativo");

        btnGuardarCambios.setText("Guardar Cambios");
        btnGuardarCambios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnGuardarCambiosMousePressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(144, 144, 144)
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel4))
                                .addGap(65, 65, 65)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cmbProfesion, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(CmbAreaLaboral, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtCorrelativo, javax.swing.GroupLayout.Alignment.TRAILING)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(18, 18, 18)
                                .addComponent(spnAnnosExperiencia)))))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(228, 228, 228)
                .addComponent(btnGuardarCambios)
                .addContainerGap(241, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtCorrelativo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(CmbAreaLaboral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cmbProfesion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(spnAnnosExperiencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(btnGuardarCambios)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardarCambiosMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGuardarCambiosMousePressed
        if(tipoManipulacionActual == TipoManipulacion.Modificar){
            modificarExperienciaLaboral();
        }else if(tipoManipulacionActual == TipoManipulacion.Insertar){
            insertarExperienciaLaboral();
        }
    }//GEN-LAST:event_btnGuardarCambiosMousePressed

    private void CmbAreaLaboralItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbAreaLaboralItemStateChanged
        llenarCmbProfesion();
    }//GEN-LAST:event_CmbAreaLaboralItemStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> CmbAreaLaboral;
    private javax.swing.JButton btnGuardarCambios;
    private javax.swing.JComboBox<String> cmbProfesion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JSpinner spnAnnosExperiencia;
    private javax.swing.JTextField txtCorrelativo;
    // End of variables declaration//GEN-END:variables

}
