/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.workingbriefcase.vista.candidato;

import com.workingbriefcase.persistencia.controladores.ArealaboralJpaController;
import com.workingbriefcase.persistencia.controladores.CurriculumIdiomaJpaController;
import com.workingbriefcase.persistencia.controladores.CurriculumProfesionJpaController;
import com.workingbriefcase.persistencia.controladores.IdiomaJpaController;
import com.workingbriefcase.persistencia.controladores.ProfesionJpaController;
import com.workingbriefcase.persistencia.entities.Arealaboral;
import com.workingbriefcase.persistencia.entities.Curriculum;
import com.workingbriefcase.persistencia.entities.CurriculumIdioma;
import com.workingbriefcase.persistencia.entities.CurriculumProfesion;
import com.workingbriefcase.persistencia.entities.Idioma;
import com.workingbriefcase.persistencia.entities.Profesion;
import com.workingbriefcase.utilidades.ComboBoxItem;
import com.workingbriefcase.utilidades.JTableCell;
import com.workingbriefcase.utilidades.SwingFormUtilities;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComponent;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.table.DefaultTableModel;

/**
 * Nombre de la clase: FrmManipularExperienciaLaboral
 * Fecha Creación: 11-09-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class FrmManipularManejoIdioma extends javax.swing.JInternalFrame {
    //<editor-fold defaultstate="collapsed" desc="variables globales">
    //tipo de manipulación con la que abrio el formulario
    private TipoManipulacion tipoManipulacionActual;
    
    //identidad del idioma siendo manipulad
    private int idManejoIdiomaManipulando;
    
    //curriculum al que pertenece la experiencia laboral
    private Curriculum curriculumPertenece;
    
    //instancia al manupulador de curriculum que ha invocado a este form
    private FrmManipularCurriculum frmInvocador;
    
    //instancia de clases dao
    private CurriculumIdiomaJpaController daoCurriculumIdioma;
    private IdiomaJpaController daoIdioma;
    
    //lista de controles del formulario
    private JComponent [] frmExperienciaCntrls;
    
    
    //otras instancias
    private SwingFormUtilities sfutil;
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="constructores">
    /** Creates new form FrmManipularExperienciaLaboral */
    public FrmManipularManejoIdioma(int idManejoIdiomaManipulando, Curriculum pertenece
            , TipoManipulacion tipoManipulacion, FrmManipularCurriculum frmInvocador) {
        initComponents();
        
        //inicializar variables de instancia locales
        tipoManipulacionActual = tipoManipulacion;
        this.idManejoIdiomaManipulando = idManejoIdiomaManipulando;
        this.frmInvocador = frmInvocador;
        this.curriculumPertenece = pertenece;
        
        //inicializar otros aspectos
        initialize();
    }
    
    private void initialize(){
        //inicializar otras instancias
        sfutil = SwingFormUtilities.getInstance();
        
        //inicializar instancias dao
        daoCurriculumIdioma = new CurriculumIdiomaJpaController();
        daoIdioma = new IdiomaJpaController();
        
        //inicializar arreglo de controles del formulario
        frmExperienciaCntrls = new JComponent[]{
            txtCorrelativo, CmbIdioma, cmbNivelDominio
        };
        
        //solo se aceptará un id inferior a 1 cuando el formulario
        //haya sido abierto para otra cosa que no sea inserción y el id
        //no es válido
        if(tipoManipulacionActual != TipoManipulacion.Insertar){
            if(idManejoIdiomaManipulando < 1){
                //avisar de mal parametro y cerrar el formulario
                sfutil.mostrarMensajeFallo(this, "El idioma que desea manipular"
                        + "no es válido");
                this.dispose();
            }
        }
        
        //si fue abierto para visualización solamente
        if(tipoManipulacionActual == TipoManipulacion.Visualizar){
            sfutil.alternarControlesActivos(frmExperienciaCntrls, false);
            btnGuardarCambios.setVisible(false);
        }
        //si fue abierto para insersión
        else if(tipoManipulacionActual == TipoManipulacion.Insertar){
            sfutil.alternarControlesActivos(frmExperienciaCntrls, true);
        }
        //si fue abierto para modificación
        else if(tipoManipulacionActual == TipoManipulacion.Modificar){
            sfutil.alternarControlesActivos(frmExperienciaCntrls, true);
        }
        //sino, salir del formulario
        else{
            sfutil.mostrarMensajeFallo(this, "No se pudo abrir el formulario");
            this.dispose();
        }
        
        //añadiendo escuchador de eventos de ventana a este internal frame
        this.addInternalFrameListener(new InternalFrameAdapter() {
            /**
             * Realizará una actualización del curriculum siendo modificado cuando
             * este formulario sea cerrado
             * @param e 
             */
            @Override
            public void internalFrameClosed(InternalFrameEvent e) {
                frmInvocador.llenarFormularioCurriculum(); //esta línea lo hará
                
                super.internalFrameClosed(e);
            }
            
        });
        
        //llenar cmbs correspondientes
        llenarCmbIdioma();
        
        //llenar formulario si la operación no es insertar
        if(tipoManipulacionActual != TipoManipulacion.Insertar){
            llenarFormularioManejoIdioma();
        }
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="tipos anidados">
    public static enum TipoManipulacion{
        Visualizar,
        Insertar,
        Modificar,
    }
    
    
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones CRUD Manejo de idioma">
    private void insertarManejoIdioma(){
        try {
            CurriculumIdioma objetivo;

            objetivo = obtenerDatosFromFormulario();
            
            objetivo.setCurriculum(curriculumPertenece);

            daoCurriculumIdioma.create(objetivo);
            
            sfutil.mostrarMensajeExito(this, "La operación se ha realizado con exito");
        } catch (Exception e) {
            sfutil.mostrarMensajeExcepcion(this, "Ha ocurrido un error inesperado", e);
        }
    }
    
    private void modificarManejoIdioma(){
        try {
            CurriculumIdioma objetivo;
            CurriculumIdioma tmp;

            objetivo = daoCurriculumIdioma.findCurriculumIdioma(idManejoIdiomaManipulando);

            tmp = obtenerDatosFromFormulario();

            objetivo.setCorrelativo(tmp.getCorrelativo());
            objetivo.setIdioma(tmp.getIdioma());
            objetivo.setNivelDominio(tmp.getNivelDominio());

            daoCurriculumIdioma.edit(objetivo);
            
            sfutil.mostrarMensajeExito(this, "La operación se ha realizado con exito");
        } catch (Exception e) {
            sfutil.mostrarMensajeExcepcion(this, "Ha ocurrido un error inesperado", e);
        }
    }
    
    private void llenarFormularioManejoIdioma(){
        CurriculumIdioma objetivo;
        ComboBoxItem ci;
        
        objetivo = daoCurriculumIdioma.findCurriculumIdioma(idManejoIdiomaManipulando);
        
        txtCorrelativo.setText(String.valueOf(objetivo.getCorrelativo()));
        
        ci = new ComboBoxItem();
        
        ci.setValor(objetivo.getIdioma().getId());
        
        sfutil.setJComboBoxSelectedItem(CmbIdioma, ci);
        
        cmbNivelDominio.setSelectedItem(objetivo.getNivelDominio());
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones CRUD Idioma">
    private void llenarCmbIdioma(){
        DefaultComboBoxModel dcmbm;
        
        dcmbm = new DefaultComboBoxModel();
        
        
        for(Idioma idioma : daoIdioma.findIdiomaEntities()){
            ComboBoxItem ci;
            
            ci = new ComboBoxItem(idioma.getId(), idioma.getNombre());
            
            dcmbm.addElement(ci);
        }
        
        CmbIdioma.setModel(dcmbm);
    }
        
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones formulario Experiencia Laboral">
    private CurriculumIdioma obtenerDatosFromFormulario(){
        CurriculumIdioma salida;
        
        salida = new CurriculumIdioma();
        
        salida.setIdioma(new Idioma());
        
        salida.setCorrelativo(Integer.parseInt(txtCorrelativo.getText()));
        salida.getIdioma().setId(((ComboBoxItem)CmbIdioma.getSelectedItem()).getValor());
        salida.setNivelDominio(cmbNivelDominio.getSelectedItem().toString());
        
        return salida;
    }
//</editor-fold>
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtCorrelativo = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        CmbIdioma = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        cmbNivelDominio = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnGuardarCambios = new javax.swing.JButton();

        setClosable(true);

        txtCorrelativo.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel3.setText("Idioma");

        CmbIdioma.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        CmbIdioma.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        CmbIdioma.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbIdiomaItemStateChanged(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel4.setText("Nivel Dominio");

        cmbNivelDominio.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        cmbNivelDominio.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel1.setText("Manejo Idioma");

        jLabel2.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel2.setText("correlativo");

        btnGuardarCambios.setText("Guardar Cambios");
        btnGuardarCambios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnGuardarCambiosMousePressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(CmbIdioma, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbNivelDominio, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtCorrelativo)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(228, 228, 228)
                                .addComponent(btnGuardarCambios))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(222, 222, 222)
                                .addComponent(jLabel1)))
                        .addGap(0, 231, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtCorrelativo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(CmbIdioma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cmbNivelDominio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(53, 53, 53)
                .addComponent(btnGuardarCambios)
                .addContainerGap(25, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardarCambiosMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGuardarCambiosMousePressed
        if(tipoManipulacionActual == TipoManipulacion.Modificar){
            modificarManejoIdioma();
        }else if(tipoManipulacionActual == TipoManipulacion.Insertar){
            insertarManejoIdioma();
        }
    }//GEN-LAST:event_btnGuardarCambiosMousePressed

    private void CmbIdiomaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbIdiomaItemStateChanged
        
    }//GEN-LAST:event_CmbIdiomaItemStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> CmbIdioma;
    private javax.swing.JButton btnGuardarCambios;
    private javax.swing.JComboBox<String> cmbNivelDominio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JTextField txtCorrelativo;
    // End of variables declaration//GEN-END:variables

}
