/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.workingbriefcase.vista.candidato;

import com.workingbriefcase.persistencia.controladores.ArealaboralJpaController;
import com.workingbriefcase.persistencia.controladores.CurriculumCursoJpaController;
import com.workingbriefcase.persistencia.controladores.CurriculumProfesionJpaController;
import com.workingbriefcase.persistencia.controladores.CursoJpaController;
import com.workingbriefcase.persistencia.controladores.ProfesionJpaController;
import com.workingbriefcase.persistencia.entities.Arealaboral;
import com.workingbriefcase.persistencia.entities.Curriculum;
import com.workingbriefcase.persistencia.entities.CurriculumCurso;
import com.workingbriefcase.persistencia.entities.CurriculumProfesion;
import com.workingbriefcase.persistencia.entities.Curso;
import com.workingbriefcase.persistencia.entities.Profesion;
import com.workingbriefcase.utilidades.ComboBoxItem;
import com.workingbriefcase.utilidades.JTableCell;
import com.workingbriefcase.utilidades.SwingFormUtilities;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComponent;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.table.DefaultTableModel;

/**
 * Nombre de la clase: FrmManipularExperienciaLaboral
 * Fecha Creación: 11-09-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class FrmManipularCursosCurriculum extends javax.swing.JInternalFrame {
    //<editor-fold defaultstate="collapsed" desc="variables globales">
    //tipo de manipulación con la que abrio el formulario
    TipoManipulacion tipoManipulacionActual;
    
    //identidad del curso manipulado
    int idCursoManipulado;
    
    //curriculum al que pertenece la experiencia laboral
    Curriculum curriculumPertenece;
    
    //instancia al manupulador de curriculum que ha invocado a este form
    FrmManipularCurriculum frmInvocador;
    
    //instancia de clases dao
    CurriculumCursoJpaController daoCurriculumCurso;
    CursoJpaController daoCurso;
    
    //lista de controles del formulario
    JComponent [] frmExperienciaCntrls;
    
    
    //otras instancias
    SwingFormUtilities sfutil;
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="constructores">
    /** Creates new form FrmManipularExperienciaLaboral */
    public FrmManipularCursosCurriculum(int idCursoManipulado, Curriculum pertenece
            , TipoManipulacion tipoManipulacion, FrmManipularCurriculum frmInvocador) {
        initComponents();
        
        //inicializar variables de instancia locales
        tipoManipulacionActual = tipoManipulacion;
        this.idCursoManipulado = idCursoManipulado;
        this.frmInvocador = frmInvocador;
        this.curriculumPertenece = pertenece;
        
        //inicializar otros aspectos
        initialize();
    }
    
    private void initialize(){
        //inicializar otras instancias
        sfutil = SwingFormUtilities.getInstance();
        
        //inicializar instancias dao
        daoCurriculumCurso = new CurriculumCursoJpaController();
        daoCurso = new CursoJpaController();
        
        //inicializar arreglo de controles del formulario
        frmExperienciaCntrls = new JComponent[]{
            txtCorrelativo, cmbCurso
        };
        
        //solo se aceptará un id inferior a 1 cuando el formulario
        //haya sido abierto para otra cosa que no sea inserción y el id
        //no es válido
        if(tipoManipulacionActual != TipoManipulacion.Insertar){
            if(idCursoManipulado < 1){
                //avisar de mal parametro y cerrar el formulario
                sfutil.mostrarMensajeFallo(this, "La Experiencia laboral que desea manipular"
                        + "no es válida");
                this.dispose();
            }
        }
        
        //si fue abierto para visualización solamente
        if(tipoManipulacionActual == TipoManipulacion.Visualizar){
            sfutil.alternarControlesActivos(frmExperienciaCntrls, false);
            btnGuardarCambios.setVisible(false);
        }
        //si fue abierto para insersión
        else if(tipoManipulacionActual == TipoManipulacion.Insertar){
            sfutil.alternarControlesActivos(frmExperienciaCntrls, true);
        }
        //si fue abierto para modificación
        else if(tipoManipulacionActual == TipoManipulacion.Modificar){
            sfutil.alternarControlesActivos(frmExperienciaCntrls, true);
        }
        //sino, salir del formulario
        else{
            sfutil.mostrarMensajeFallo(this, "No se pudo abrir el formulario");
            this.dispose();
        }
        
        //añadiendo escuchador de eventos de ventana a este internal frame
        this.addInternalFrameListener(new InternalFrameAdapter() {
            /**
             * Realizará una actualización del curriculum siendo modificado cuando
             * este formulario sea cerrado
             * @param e 
             */
            @Override
            public void internalFrameClosed(InternalFrameEvent e) {
                frmInvocador.llenarFormularioCurriculum(); //esta línea lo hará
                
                super.internalFrameClosed(e);
            }
            
        });
        
        //llenar cmbs correspondientes
        llenarCmbCursos();
        
        //llenar formulario si la operación no es insertar
        if(tipoManipulacionActual != TipoManipulacion.Insertar){
            llenarFormularioCursos();
        }
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="tipos anidados">
    public static enum TipoManipulacion{
        Visualizar,
        Insertar,
        Modificar,
    }
    
    
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones CRUD Curso">
    private void insertarCurso(){
        try {
            CurriculumCurso objetivo;

            objetivo = obtenerDatosFromFormulario();
            
            objetivo.setCurriculum(curriculumPertenece);

            daoCurriculumCurso.create(objetivo);
            
            sfutil.mostrarMensajeExito(this, "La operación se ha realizado con exito");
        } catch (Exception e) {
            sfutil.mostrarMensajeExcepcion(this, "Ha ocurrido un error inesperado", e);
        }
    }
    
    private void modificarCurso(){
        try {
            CurriculumCurso objetivo;
            CurriculumCurso tmp;

            objetivo = daoCurriculumCurso.findCurriculumCurso(idCursoManipulado);

            tmp = obtenerDatosFromFormulario();

            objetivo.setCorrelativo(tmp.getCorrelativo());
            objetivo.setCurso(tmp.getCurso());

            daoCurriculumCurso.edit(objetivo);
            
            sfutil.mostrarMensajeExito(this, "La operación se ha realizado con exito");
        } catch (Exception e) {
            sfutil.mostrarMensajeExcepcion(this, "Ha ocurrido un error inesperado", e);
        }
    }
    
    private void llenarFormularioCursos(){
        CurriculumCurso objetivo;
        ComboBoxItem ci;
        
        objetivo = daoCurriculumCurso.findCurriculumCurso(idCursoManipulado);
        
        txtCorrelativo.setText(String.valueOf(objetivo.getCorrelativo()));
        
        ci = new ComboBoxItem();
        
        ci.setValor(objetivo.getCurso().getId());
        
        sfutil.setJComboBoxSelectedItem(cmbCurso, ci);

        
    }
    
    private void llenarCmbCursos(){
        DefaultComboBoxModel dcmbm;
        
        dcmbm = new DefaultComboBoxModel();
        
        
        for(Curso curso : daoCurso.findCursoEntities()){
            ComboBoxItem ci;
            
            ci = new ComboBoxItem(curso.getId(), curso.getNombre());
            
            dcmbm.addElement(ci);
        }
        
        cmbCurso.setModel(dcmbm);
    }
        
//</editor-fold>
    
    
    //<editor-fold defaultstate="collapsed" desc="funciones formulario Cursos">
    private CurriculumCurso obtenerDatosFromFormulario(){
        CurriculumCurso salida;
        
        salida = new CurriculumCurso();
        
        salida.setCorrelativo(Integer.parseInt(txtCorrelativo.getText()));
        salida.setCurso(new Curso());
        salida.getCurso().setId(((ComboBoxItem) cmbCurso.getSelectedItem()).getValor());
        
        return salida;
    }
//</editor-fold>
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cmbCurso = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        btnGuardarCambios = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        txtCorrelativo = new javax.swing.JTextField();

        setClosable(true);

        cmbCurso.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        cmbCurso.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbCurso.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbCursoItemStateChanged(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel1.setText("Cursos");

        jLabel5.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel5.setText("Curso");

        btnGuardarCambios.setText("Guardar Cambios");
        btnGuardarCambios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnGuardarCambiosMousePressed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel4.setText("Correlativo");

        txtCorrelativo.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(147, 147, 147)
                        .addComponent(txtCorrelativo))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(147, 147, 147)
                        .addComponent(cmbCurso, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(261, 261, 261)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(239, Short.MAX_VALUE)
                .addComponent(btnGuardarCambios)
                .addGap(230, 230, 230))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtCorrelativo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(cmbCurso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addComponent(btnGuardarCambios)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardarCambiosMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGuardarCambiosMousePressed
        if(tipoManipulacionActual == TipoManipulacion.Modificar){
            modificarCurso();
        }else if(tipoManipulacionActual == TipoManipulacion.Insertar){
            insertarCurso();
        }
    }//GEN-LAST:event_btnGuardarCambiosMousePressed

    private void cmbCursoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbCursoItemStateChanged
        
    }//GEN-LAST:event_cmbCursoItemStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGuardarCambios;
    private javax.swing.JComboBox<String> cmbCurso;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JTextField txtCorrelativo;
    // End of variables declaration//GEN-END:variables

}
