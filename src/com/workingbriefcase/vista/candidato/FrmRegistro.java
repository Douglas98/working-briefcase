/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.workingbriefcase.vista.candidato;

import com.workingbriefcase.persistencia.controladores.CandidatoJpaController;
import com.workingbriefcase.persistencia.controladores.DepartamentoJpaController;
import com.workingbriefcase.persistencia.controladores.MunicipioJpaController;
import com.workingbriefcase.persistencia.controladores.PaisJpaController;
import com.workingbriefcase.persistencia.controladores.RolJpaController;
import com.workingbriefcase.persistencia.entities.Candidato;
import com.workingbriefcase.persistencia.entities.Departamento;
import com.workingbriefcase.persistencia.entities.Municipio;
import com.workingbriefcase.persistencia.entities.Pais;
import com.workingbriefcase.persistencia.entities.Rol;
import com.workingbriefcase.persistencia.entities.Usuario;
import com.workingbriefcase.utilidades.ComboBoxItem;
import com.workingbriefcase.utilidades.SwingFormUtilities;
import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Date;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
/**
 * Nombre de la clase: FrmRegistro
 * Fecha Creación: 10-16-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class FrmRegistro extends javax.swing.JFrame {
    //<editor-fold defaultstate="collapsed" desc="variables globales">
    //objetos de clases dao
    private CandidatoJpaController daoCandidato;
    private RolJpaController daoRol;
    private PaisJpaController daoPais;
    private DepartamentoJpaController daoDepartamento;
    private MunicipioJpaController daoMunicipio;
    //objeto de clase de utilidades
    private SwingFormUtilities sfutil;
    //definir los roles de usuario que manejará el formulario;
    private Rol rolCandidatoFormulario;
    
    //definir instancias a formularios de respuesta según entrada del candidato
    private FrmLogin frmLogin;
    
    //ruta de la imagen
    String rutaImagen;
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="constructores">
    /** Creates new form FrmRegistro */
    public FrmRegistro() {
        initComponents();
        initialize();
    }
    
    /**
     * Realiza acciones de inicialización para ser llamado en el constructor
     */
    private void initialize(){
        //llenar los combo box
        llenarCmbNacionalidad();
        llenarCmbDepartamento();
        llenarCmbMunicipio();
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="encapsulamiento">
    private CandidatoJpaController getDaoCandidato() {
        if(daoCandidato == null){
            daoCandidato = new CandidatoJpaController();
        }
        return daoCandidato;
    }

    private void setDaoCandidato(CandidatoJpaController daoJpa) {
        this.daoCandidato = daoJpa;
    }

    private SwingFormUtilities getSfutil() {
        if(sfutil == null){
            sfutil = SwingFormUtilities.getInstance();
        }
        return sfutil;
    }

    private void setSfutil(SwingFormUtilities sfutil) {
        this.sfutil = sfutil;
    }

    private RolJpaController getDaoRol() {
        if(daoRol == null){
            daoRol = new RolJpaController();
        }
        return daoRol;
    }

    private void setDaoRol(RolJpaController daoRol) {
        this.daoRol = daoRol;
    }

    private Rol getRolCandidatoFormulario() {
        if(rolCandidatoFormulario == null){
            //definir el rol que este formulario utilizará para registrar a
            //candidato que en la base es el id = 1
            rolCandidatoFormulario = getDaoRol().findRol(3);
        }
        return rolCandidatoFormulario;
    }

    private void setRolCandidatoFormulario(Rol rolCandidato) {
        this.rolCandidatoFormulario = rolCandidato;
    }

    private PaisJpaController getDaoPais() {
        if(daoPais == null){
            daoPais = new PaisJpaController();
        }
        return daoPais;
    }

    private void setDaoPais(PaisJpaController daoPais) {
        this.daoPais = daoPais;
    }

    private DepartamentoJpaController getDaoDepartamento() {
        if(daoDepartamento == null){
            daoDepartamento = new DepartamentoJpaController();
        }
        return daoDepartamento;
    }

    private void setDaoDepartamento(DepartamentoJpaController daoDepartamento) {
        this.daoDepartamento = daoDepartamento;
    }

    private MunicipioJpaController getDaoMunicipio() {
        if(daoMunicipio == null){
            daoMunicipio = new MunicipioJpaController();
        }
        return daoMunicipio;
    }

    private void setDaoMunicipio(MunicipioJpaController daoMunicipio) {
        this.daoMunicipio = daoMunicipio;
    }

    public FrmLogin getFrmLogin() {
        return frmLogin;
    }

    public void setFrmLogin(FrmLogin frmLogin) {
        this.frmLogin = frmLogin;
    }
    
    
    
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones CRUD candidato">
    /**
     * Intenta registra al candidato, si lo logra, vuelve al login para candidato<br>
     * sino entonces muestra un mensaje con la exceptión en cuestion y permanece<br>
     * en el registro hasta que el usuario lo cancele manualemnte
     */
    private void registrarCandidato(){
        try {
            getDaoCandidato().create(obtenerDatosCandidatoFormulario());
            getSfutil().mostrarMensajeExito(jPanel1, "¡Se ha registrado con exito!"
                    + "\n¡Bienvenido a WorkingBriefcase!");
            setFrmLogin(new FrmLogin());
            getFrmLogin().setVisible(true);
        } catch (Exception e) {
            getSfutil().mostrarMensajeExcepcion(jPanel1, "Ha ocurrido un error"
                    + " inesperado al registrarse. Intentelo de nuevo más tarde", e);
        }
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones formulario para candidato">
    /**
     * Obtienen los datos del formulario de registro. Para completar algunos de los<br>
     * requerimientos de jpa, se obtienen entidades de tipo Nacionalidad, Municipio<br>
     *  y Departamento mediante los daos correspondientes, de ahí que sea posible<br>
     * que se lance una excepción
     * @return instancia "detached" de la entidad candidato llena con los datos<br>
     * introducidos por el candidato
     * @throws Exception Alguna Excepción al momento de conectarse a la base por<br>
     * por medio de los controladores
     */
    private Candidato obtenerDatosCandidatoFormulario() throws Exception{
        Candidato salida;
        
        salida = new Candidato();
        salida.setUsuario(new Usuario());
        salida.getUsuario().setNombre(txtNombreUsuario.getText());   
        salida.getUsuario().setCorreoElectronico(txtCorreo.getText());
        salida.getUsuario().setContrasenna(txtContrasenna.getText());
        salida.getUsuario().setCorreoElectronico(txtCorreo.getText());
        salida.getUsuario().setUltimaConexion(new Date());
        salida.getUsuario().setBorrado(false);
        salida.getUsuario().setRol(getRolCandidatoFormulario());
        salida.setDui(txtDui.getText());
        salida.setNit(txtNit.getText());
        salida.setNombre(txtNombre.getText());
        salida.setApellido(txtApellidos.getText());
        salida.setGenero(rdbFemenino.isSelected() 
            ? rdbFemenino.getText() : rdbMasculino.getText());
        salida.setFechaNacimiento((Date) spnFechaNacimiento.getValue());
        //salida.setFoto(//obtener la foto);
        salida.setDireccion(txaDireccion.getText());
        salida.setAutoDescripcion(txaAutoDescripcion.getText());
        salida.setTrabajando(chkTrabajando.isSelected());
        
        /*
        File file = new File(rutaImagen);
        FileInputStream fi;
                        //luego un arreglo de tipo byte
        byte[] bArray = new byte[(int) file.length()];
                         //se agrega el archivo al fileinpustream
        fi = new FileInputStream(file);
                         //se grava el file input a arreglo byte
        fi.read(bArray);
                         //se sierra el proceso
        fi.close();
                         //se envia el arreglo
                         
        salida.setFoto(bArray);*/

        //por defecto la privacidad del perfil será publica
        salida.setPrivacidadPerfil(0);
        salida.setMunicipioActual(getDaoMunicipio()
                .findMunicipio(((ComboBoxItem) cmbMunicipio.getSelectedItem()).getValor()));
        salida.setNacionalidad(getDaoPais()
                .findPais(((ComboBoxItem) cmbNacionalidad.getSelectedItem()).getValor()));
        
        return salida;
    }
    
    /**
     * Pide confirmación al usuario para salir del formulario y regresar al logín cuando<br>
     * el usuario decide cancelar el registro manualmente
     */
    private void salir(){
        if(getSfutil().mostrarConfirmacionSimple(jPanel1, "Realmente desea cancelar el registro?")
                == 0){
            getSfutil().mostrarMensajeCancelacion(jPanel1, "Ha cancelado su registro como candidato!");
            setFrmLogin(new FrmLogin());
            getFrmLogin().setVisible(true);
            this.dispose();
        }
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones CRUD pais">
    private void llenarCmbNacionalidad(){
        DefaultComboBoxModel dcmbm;
        
        dcmbm = new DefaultComboBoxModel();
        
        for(Pais p : getDaoPais().findPaisEntities()){
            ComboBoxItem ci;
            
            ci = new ComboBoxItem(p.getId(), p.getNombre());
            
            dcmbm.addElement(ci);
        }
        
        cmbNacionalidad.setModel(dcmbm);
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones CRUD departamento">
    private void llenarCmbDepartamento(){
        DefaultComboBoxModel dcmbm;
        
        dcmbm = new DefaultComboBoxModel();
        
        for(Departamento d : getDaoDepartamento().findDepartamentoEntities()){
            ComboBoxItem ci;
            
            ci = new ComboBoxItem(d.getId(), d.getNombre());
            
            dcmbm.addElement(ci);
        }
        
        cmbDepartamento.setModel(dcmbm);
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones CRUD municipio">
    private void llenarCmbMunicipio(){
        DefaultComboBoxModel dcmbm;
        ComboBoxItem selectedItem;
        
        dcmbm = new DefaultComboBoxModel();
        
        selectedItem = (ComboBoxItem) cmbDepartamento.getSelectedItem();
        for(Municipio m : getDaoDepartamento().findDepartamento(selectedItem.getValor())
                .getMunicipioList()){
            ComboBoxItem ci;
            
            ci = new ComboBoxItem(m.getId(), m.getNombre());
            
            dcmbm.addElement(ci);
        }
        
        cmbMunicipio.setModel(dcmbm);
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="otras funciones">
    void abrirImagen(){
        JFileChooser j = new JFileChooser();
        j.setCurrentDirectory(new File("Imagenes/"));
        int ap = j.showOpenDialog(this);
        
        if(ap == JFileChooser.APPROVE_OPTION){
            rutaImagen = j.getSelectedFile().getAbsolutePath();
            lblFotoPerfilPerview.setIcon(new ImageIcon(rutaImagen));
            //lblRuta.setText(ruta);
        }
    }
//</editor-fold>
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btgGenero = new javax.swing.ButtonGroup();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        txtCorreo = new javax.swing.JFormattedTextField();
        txtNombreUsuario = new javax.swing.JTextField();
        txtContrasennaConf = new javax.swing.JPasswordField();
        txtContrasenna = new javax.swing.JPasswordField();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        lblDui = new javax.swing.JLabel();
        lblNit = new javax.swing.JLabel();
        lblNombre = new javax.swing.JLabel();
        lblApellidos = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        btnRegistrarse = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        txtDui = new javax.swing.JFormattedTextField();
        txtNit = new javax.swing.JFormattedTextField();
        txtNombre = new javax.swing.JTextField();
        txtApellidos = new javax.swing.JTextField();
        rdbFemenino = new javax.swing.JRadioButton();
        rdbMasculino = new javax.swing.JRadioButton();
        spnFechaNacimiento = new javax.swing.JSpinner();
        cmbNacionalidad = new javax.swing.JComboBox<>();
        jPanel6 = new javax.swing.JPanel();
        btnSeleccionarFoto = new javax.swing.JButton();
        lblFotoPerfilPerview = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txaDireccion = new javax.swing.JTextArea();
        cmbDepartamento = new javax.swing.JComboBox<>();
        cmbMunicipio = new javax.swing.JComboBox<>();
        jScrollPane3 = new javax.swing.JScrollPane();
        txaAutoDescripcion = new javax.swing.JTextArea();
        chkTrabajando = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Registro para candidatos");
        setResizable(false);

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos del usuario"));

        jPanel5.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N

        txtCorreo.setToolTipText("");
        txtCorreo.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        txtNombreUsuario.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        txtContrasennaConf.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        txtContrasenna.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel7.setText("Correo Electrónico*:");

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel6.setText("Confirme contraseña*:");

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel5.setText("Contraseña*:");

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel4.setText("Nombre de usuario*:");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jLabel6)
                    .addComponent(jLabel5)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtNombreUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, 264, Short.MAX_VALUE)
                    .addComponent(txtContrasenna, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtContrasennaConf, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtCorreo, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNombreUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(13, 13, 13)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtContrasenna, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtContrasennaConf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(0, 13, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos Personales"));

        lblDui.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lblDui.setText("DUI*:");

        lblNit.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lblNit.setText("NIT:");

        lblNombre.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lblNombre.setText("Nombre*:");

        lblApellidos.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lblApellidos.setText("Apellidos*:");

        jLabel12.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel12.setText("Genero*:");

        jLabel13.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel13.setText("Fecha de Nacimiento*:");

        jLabel15.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel15.setText("Direccion*:");

        jLabel16.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel16.setText("Nacionalidad*:");

        jLabel17.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel17.setText("Departamento*:");

        jLabel18.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel18.setText("Municipio*:");

        jLabel19.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel19.setText("¡Digale al mundo cómo es usted!, añada una auto descripción.");

        btnRegistrarse.setFont(new java.awt.Font("Arial Black", 0, 12)); // NOI18N
        btnRegistrarse.setText("Registrarse");
        btnRegistrarse.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnRegistrarseMousePressed(evt);
            }
        });

        btnCancelar.setFont(new java.awt.Font("Arial Black", 0, 12)); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnCancelarMousePressed(evt);
            }
        });

        txtDui.setToolTipText("");
        txtDui.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        txtNit.setToolTipText("");
        txtNit.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        txtNombre.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        txtApellidos.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        btgGenero.add(rdbFemenino);
        rdbFemenino.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        rdbFemenino.setText("Femenino");

        btgGenero.add(rdbMasculino);
        rdbMasculino.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        rdbMasculino.setText("Masculino");

        spnFechaNacimiento.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        spnFechaNacimiento.setModel(new javax.swing.SpinnerDateModel(new java.util.Date(-288682860000L), new java.util.Date(-288682920000L), new java.util.Date(1573150782962L), java.util.Calendar.DAY_OF_MONTH));

        cmbNacionalidad.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        cmbNacionalidad.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        btnSeleccionarFoto.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        btnSeleccionarFoto.setText("Seleccionar Foto");
        btnSeleccionarFoto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnSeleccionarFotoMousePressed(evt);
            }
        });

        lblFotoPerfilPerview.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/workingbriefcase/imagenes/user_profile_default.jpg"))); // NOI18N

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(btnSeleccionarFoto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblFotoPerfilPerview))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblFotoPerfilPerview)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(btnSeleccionarFoto))
        );

        jLabel14.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel14.setText("Foto:");

        txaDireccion.setColumns(20);
        txaDireccion.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        txaDireccion.setRows(5);
        jScrollPane2.setViewportView(txaDireccion);

        cmbDepartamento.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        cmbDepartamento.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        cmbMunicipio.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        cmbMunicipio.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        txaAutoDescripcion.setColumns(20);
        txaAutoDescripcion.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        txaAutoDescripcion.setRows(5);
        jScrollPane3.setViewportView(txaAutoDescripcion);

        chkTrabajando.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        chkTrabajando.setText("Me encuentro laborando activamente*");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addComponent(lblDui)
                        .addGap(142, 142, 142)
                        .addComponent(txtDui))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addComponent(lblNit)
                        .addGap(149, 149, 149)
                        .addComponent(txtNit))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addComponent(lblNombre)
                        .addGap(120, 120, 120)
                        .addComponent(txtNombre))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addComponent(lblApellidos)
                        .addGap(115, 115, 115)
                        .addComponent(txtApellidos))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12)
                            .addComponent(jLabel13))
                        .addGap(47, 47, 47)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(rdbFemenino)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(rdbMasculino))
                            .addComponent(spnFechaNacimiento)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel16)
                        .addGap(94, 94, 94)
                        .addComponent(cmbNacionalidad, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel14)
                            .addComponent(jLabel15))
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(111, 111, 111)
                                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addGap(86, 86, 86)
                        .addComponent(cmbDepartamento, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addGap(109, 109, 109)
                        .addComponent(cmbMunicipio, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel19)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(chkTrabajando)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(btnRegistrarse)
                                .addGap(43, 43, 43)
                                .addComponent(btnCancelar)
                                .addGap(104, 104, 104)))))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDui)
                    .addComponent(txtDui, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNit)
                    .addComponent(txtNit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNombre)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblApellidos)
                    .addComponent(txtApellidos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(rdbFemenino)
                    .addComponent(rdbMasculino))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(spnFechaNacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(cmbNacionalidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel14)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel17)
                    .addComponent(cmbDepartamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel18)
                    .addComponent(cmbMunicipio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(chkTrabajando)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRegistrarse)
                    .addComponent(btnCancelar))
                .addContainerGap())
        );

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("<html>\nInsertar logo de <br>empresa aquí\n</html>");

        jLabel2.setFont(new java.awt.Font("Arial Narrow", 1, 24)); // NOI18N
        jLabel2.setText("Registro para Candidatos");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel3.setText("<html>\n<center><b>¡Registrese a Working Briefcase, la interfaz de empleo más completa de el Salvador</b></center><br>\n<b><center>y disfrute del trabajo de sus sueños en segundos!</center></b>\n</html>");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(132, 132, 132)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(107, 107, 107)
                        .addComponent(jLabel2)))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(614, 614, 614))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jScrollPane1.setViewportView(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //<editor-fold defaultstate="collapsed" desc="manejadores de evento">
    private void btnRegistrarseMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRegistrarseMousePressed
        registrarCandidato();
    }//GEN-LAST:event_btnRegistrarseMousePressed

    private void btnCancelarMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancelarMousePressed
        salir();
    }//GEN-LAST:event_btnCancelarMousePressed

    private void btnSeleccionarFotoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSeleccionarFotoMousePressed
        abrirImagen();
    }//GEN-LAST:event_btnSeleccionarFotoMousePressed
//</editor-fold>
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmRegistro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmRegistro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmRegistro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmRegistro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmRegistro().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup btgGenero;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnRegistrarse;
    private javax.swing.JButton btnSeleccionarFoto;
    private javax.swing.JCheckBox chkTrabajando;
    private javax.swing.JComboBox<String> cmbDepartamento;
    private javax.swing.JComboBox<String> cmbMunicipio;
    private javax.swing.JComboBox<String> cmbNacionalidad;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblApellidos;
    private javax.swing.JLabel lblDui;
    private javax.swing.JLabel lblFotoPerfilPerview;
    private javax.swing.JLabel lblNit;
    private javax.swing.JLabel lblNombre;
    private javax.swing.JRadioButton rdbFemenino;
    private javax.swing.JRadioButton rdbMasculino;
    private javax.swing.JSpinner spnFechaNacimiento;
    private javax.swing.JTextArea txaAutoDescripcion;
    private javax.swing.JTextArea txaDireccion;
    private javax.swing.JTextField txtApellidos;
    private javax.swing.JPasswordField txtContrasenna;
    private javax.swing.JPasswordField txtContrasennaConf;
    private javax.swing.JFormattedTextField txtCorreo;
    private javax.swing.JFormattedTextField txtDui;
    private javax.swing.JFormattedTextField txtNit;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtNombreUsuario;
    // End of variables declaration//GEN-END:variables

}
