/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.workingbriefcase.vista.candidato;

import com.workingbriefcase.persistencia.controladores.CurriculumJpaController;
import com.workingbriefcase.persistencia.controladores.HabilidadprofesionalJpaController;
import com.workingbriefcase.persistencia.controladores.IdiomaJpaController;
import com.workingbriefcase.persistencia.controladores.ProfesionJpaController;
import com.workingbriefcase.persistencia.entities.Curriculum;
import com.workingbriefcase.persistencia.entities.CurriculumCurso;
import com.workingbriefcase.persistencia.entities.CurriculumHabilidad;
import com.workingbriefcase.persistencia.entities.CurriculumIdioma;
import com.workingbriefcase.persistencia.entities.CurriculumProfesion;
import com.workingbriefcase.persistencia.entities.Habilidadprofesional;
import com.workingbriefcase.persistencia.entities.Profesion;
import com.workingbriefcase.utilidades.JTableCell;
import com.workingbriefcase.utilidades.StringAndCharUtilities;
import com.workingbriefcase.utilidades.SwingFormUtilities;
import javax.swing.JInternalFrame;
import javax.swing.table.DefaultTableModel;

/**
 * Nombre de la clase: FrmVisualizarCurriculum
 Fecha Creación: 11-07-2019
 Version: 1.0
 Copyright: ITCA-FEPADE
 Descripción: 
 * 
 * @author Daniel Angel
 */
public class FrmVisualizarCurriculum extends javax.swing.JInternalFrame {
    //<editor-fold defaultstate="collapsed" desc="variables globales">
    //instancias a controladores jpa
    private CurriculumJpaController daoCurriculum;
    private IdiomaJpaController daoIdioma;
    private ProfesionJpaController daoProfesion;
    private HabilidadprofesionalJpaController daoHabilidadProfesional;
    
    private int idCurriculumActual;
    private JInternalFrame frmInvocador;
    
    //lista de nombres de columnas de las tablas
    Object [] tbExperienciaProfesionalColumns;
    Object [] tbHabilidadesProfesionalesColumns;
    Object [] tbManejoIdiomasColumns;
    Object [] tbCursosColumns;
    
    
    
    //otras instancias
    SwingFormUtilities sfutil;
    StringAndCharUtilities sycutil;
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="constructores">
    /** Creates new form FrmCurriculum */
    public FrmVisualizarCurriculum(int idCurriculumSeleccionado, JInternalFrame frmInvocador) {
        initComponents();
        
        //inicializando el curriculum actual al que se halla seleccionado
        idCurriculumActual = idCurriculumSeleccionado;
        //inicializando el frame que ha invocado a este
        this.frmInvocador = frmInvocador;
        
        initialize();
        
       
    }
    
    private void initialize(){
        //inicializar instancias dao
        daoCurriculum = new CurriculumJpaController();
        daoIdioma = new IdiomaJpaController();
        daoProfesion = new ProfesionJpaController();
        daoHabilidadProfesional = new HabilidadprofesionalJpaController();
        
        //inicializar los nombres de las columnas de las tablas
        tbExperienciaProfesionalColumns = new Object[]{
            "Correlativo", "Area laboral", "Prefesión", "Años de Experiencia"
        };
        tbHabilidadesProfesionalesColumns = new Object[]{
            "Correlativo", "Area laboral", "Habilidad", "Nivel de Dominio"
        };
        tbManejoIdiomasColumns = new Object[]{
            "Correlativo", "Idioma", "Nivel de Dominio"
        };
        tbCursosColumns = new Object[]{
            "Correlativo", "Tipo", "Nombre", "Fecha de Inicio", "Fecha de Finalización"
        };
        
        
        //inicializando otras instancias
        sfutil = SwingFormUtilities.getInstance();
        sycutil = StringAndCharUtilities.getInstance();
        
        //llenado el formulario de curriculum
        llenarFormularioCurriculum();
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="encapsulamiento">

    public JInternalFrame getFrmInvocador() {
        return frmInvocador;
    }

    private void setFrmInvocador(JInternalFrame frmInvocador) {
        this.frmInvocador = frmInvocador;
    }
    
    
    
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones Crud Curriculum">
    private void llenarFormularioCurriculum(){
        boolean haFallado; //determina si la obtencion del curriculum ha fallado
        
        haFallado = true;
        try {
            Curriculum actual;
        
            actual = daoCurriculum.findCurriculum(idCurriculumActual);
        
            if(actual != null){
                
                haFallado = false;
                
                //llenar el formulario
                lblCorrelativo.setText(String.valueOf(actual.getCorrelativoActual()));
                lblFechaPublicado.setText(actual.getFechaPublicado().toString());
                lblDescripcion.setText(sycutil.formtearTextoParaEncajar(Double
                        .valueOf(lblDescripcion.getPreferredSize().getWidth()).intValue()
                        , actual.getDescripcionCurriculum()));
                
                //llenar tablas del formulario
                llenarTbExperienciaProfesional(actual);
                llenarTbHabilidadesProfesionales(actual);
                llenarTbManejoIdiomas(actual);
                llenarTbCursos(actual);
            }else{
                sfutil.mostrarMensajeFallo(this, "No ha sido posible encontrar el curriculum");
            }
        } catch (Exception e) {
            sfutil.mostrarMensajeExcepcion(this, "ha ocurrido un error inesperado", e);
        }
        
        if(haFallado){
            this.dispose();
        }
    }
    
    private void llenarTbExperienciaProfesional(Curriculum actual){
        DefaultTableModel dtbm;
        
        dtbm = new DefaultTableModel(tbExperienciaProfesionalColumns, 0);
        
        //"Correlativo", "Area laboral", "Prefesión", "Años de Experiencia"
        for(CurriculumProfesion currProfesion : actual.getCurriculumProfesionList()){
            Object [] row;

            row = new Object[]{
                new JTableCell<CurriculumProfesion>(currProfesion
                        , String.valueOf(currProfesion.getCorrelativo()))
                , new JTableCell<CurriculumProfesion>(currProfesion
                        , currProfesion.getProfesion().getAreaLaboral().getNombre())
                , new JTableCell<CurriculumProfesion>(currProfesion
                        , currProfesion.getProfesion().getNombre())
                , new JTableCell<CurriculumProfesion>(currProfesion
                        , String.valueOf(currProfesion.getAnnosExperiencia()))
            };
            
            dtbm.addRow(row);
        }
        
        tbExperienciaProfesional.setModel(dtbm);
    }
    
    private void llenarTbHabilidadesProfesionales(Curriculum actual){
        DefaultTableModel dtbm;
        
        dtbm = new DefaultTableModel(tbHabilidadesProfesionalesColumns, 0);
        
        //"Correlativo", "Area laboral", "Prefesión", "Años de Experiencia"
        for(CurriculumHabilidad curriculumHabilidad : actual.getCurriculumHabilidadList()){
            Object [] row;
            //"Correlativo", "Area laboral", "Habilidad", "Nivel de Dominio"
            row = new Object[]{
                new JTableCell<CurriculumHabilidad>(curriculumHabilidad
                        , String.valueOf(curriculumHabilidad.getCorrelativo()))
                , new JTableCell<CurriculumHabilidad>(curriculumHabilidad
                        , curriculumHabilidad.getHabilidadProfesional().getAreaLaboral().getNombre())
                , new JTableCell<CurriculumHabilidad>(curriculumHabilidad
                        , curriculumHabilidad.getHabilidadProfesional().getNombre())
                , new JTableCell<CurriculumHabilidad>(curriculumHabilidad
                        , String.valueOf(curriculumHabilidad.getNivelDominio()))
            };
            
            dtbm.addRow(row);
        }
        
        tbHabilidadesProfesionales.setModel(dtbm);
    }
    
    private void llenarTbManejoIdiomas(Curriculum actual){
        DefaultTableModel dtbm;
        
        dtbm = new DefaultTableModel(tbHabilidadesProfesionalesColumns, 0);
        
        //"Correlativo", "Idioma", "Nivel de Dominio"
        for(CurriculumIdioma curriculumIdioma : actual.getCurriculumIdiomaList()){
            Object [] row;
            row = new Object[]{
                new JTableCell<CurriculumIdioma>(curriculumIdioma
                        , String.valueOf(curriculumIdioma.getCorrelativo()))
                , new JTableCell<CurriculumIdioma>(curriculumIdioma
                        , curriculumIdioma.getIdioma().getNombre())
                , new JTableCell<CurriculumIdioma>(curriculumIdioma
                        , String.valueOf(curriculumIdioma.getNivelDominio()))
            };
            
            dtbm.addRow(row);
        }
        
        tbManejoIdiomas.setModel(dtbm);
    }
    
    private void llenarTbCursos(Curriculum actual){
        DefaultTableModel dtbm;
        
        dtbm = new DefaultTableModel(tbHabilidadesProfesionalesColumns, 0);
        
        //"Correlativo", "Tipo", "Nombre", "Fecha de Inicio", "Fecha de Finalización"
        for(CurriculumCurso curriculumCurso : actual.getCurriculumCursoList()){
            Object [] row;
            row = new Object[]{
                new JTableCell<CurriculumCurso>(curriculumCurso
                        , String.valueOf(curriculumCurso.getCorrelativo()))
                , new JTableCell<CurriculumCurso>(curriculumCurso
                        , curriculumCurso.getCurso().getTipo() == 0 ? "Curso" : "Capacitación")
                , new JTableCell<CurriculumCurso>(curriculumCurso
                        , curriculumCurso.getCurso().getNombre())
                , new JTableCell<CurriculumCurso>(curriculumCurso
                        , String.valueOf(curriculumCurso.getCurso().getFechaInicio()))
            };
            
            dtbm.addRow(row);
        }
        
        tbCursos.setModel(dtbm);
    }
//</editor-fold>
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        lblCorrelativo = new javax.swing.JLabel();
        lblFechaPublicado = new javax.swing.JLabel();
        pExperienciaLaboral = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbExperienciaProfesional = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbHabilidadesProfesionales = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbManejoIdiomas = new javax.swing.JTable();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tbCursos = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        lblDescripcion = new javax.swing.JLabel();

        setClosable(true);

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jScrollPane1.setHorizontalScrollBar(null);

        lblCorrelativo.setFont(new java.awt.Font("Century Schoolbook", 1, 36)); // NOI18N
        lblCorrelativo.setText("Place holder");

        lblFechaPublicado.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        lblFechaPublicado.setText("Place holder");

        pExperienciaLaboral.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Experiencia Profesional", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 14))); // NOI18N

        tbExperienciaProfesional.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbExperienciaProfesional.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        jScrollPane2.setViewportView(tbExperienciaProfesional);

        javax.swing.GroupLayout pExperienciaLaboralLayout = new javax.swing.GroupLayout(pExperienciaLaboral);
        pExperienciaLaboral.setLayout(pExperienciaLaboralLayout);
        pExperienciaLaboralLayout.setHorizontalGroup(
            pExperienciaLaboralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pExperienciaLaboralLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );
        pExperienciaLaboralLayout.setVerticalGroup(
            pExperienciaLaboralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pExperienciaLaboralLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Habilidades Profesionales", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 14))); // NOI18N

        tbHabilidadesProfesionales.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tbHabilidadesProfesionales);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Manejo de Idiomas", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 14))); // NOI18N

        tbManejoIdiomas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(tbManejoIdiomas);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Cursos y Capacitaciones", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 14))); // NOI18N

        tbCursos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane5.setViewportView(tbCursos);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 700, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel6.setFont(new java.awt.Font("Century Schoolbook", 1, 36)); // NOI18N
        jLabel6.setText("Curriculum #");

        jLabel7.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel7.setText("Fecha Publicado");

        jLabel8.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel8.setText("Descripcion Curriculum");

        lblDescripcion.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        lblDescripcion.setText("Place holder");
        lblDescripcion.setMaximumSize(new java.awt.Dimension(722, 120));
        lblDescripcion.setPreferredSize(new java.awt.Dimension(722, 120));

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblDescripcion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addGap(154, 154, 154)
                        .addComponent(jLabel6)
                        .addGap(18, 18, 18)
                        .addComponent(lblCorrelativo))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(62, 62, 62)
                                .addComponent(lblFechaPublicado))))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pExperienciaLaboral, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(17, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(lblCorrelativo))
                .addGap(28, 28, 28)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblFechaPublicado)
                    .addComponent(jLabel7))
                .addGap(18, 18, 18)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(pExperienciaLaboral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 124, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jScrollPane1.setViewportView(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 784, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 571, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    //<editor-fold defaultstate="collapsed" desc="overrides">
    @Override
    public void dispose(){
        super.dispose();
    }
//</editor-fold>

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JLabel lblCorrelativo;
    private javax.swing.JLabel lblDescripcion;
    private javax.swing.JLabel lblFechaPublicado;
    private javax.swing.JPanel pExperienciaLaboral;
    private javax.swing.JTable tbCursos;
    private javax.swing.JTable tbExperienciaProfesional;
    private javax.swing.JTable tbHabilidadesProfesionales;
    private javax.swing.JTable tbManejoIdiomas;
    // End of variables declaration//GEN-END:variables

}
