/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.workingbriefcase.vista.candidato;

import com.workingbriefcase.persistencia.controladores.CandidatoJpaController;
import com.workingbriefcase.persistencia.controladores.DepartamentoJpaController;
import com.workingbriefcase.persistencia.controladores.MunicipioJpaController;
import com.workingbriefcase.persistencia.controladores.PaisJpaController;
import com.workingbriefcase.persistencia.controladores.RolJpaController;
import com.workingbriefcase.persistencia.entities.Candidato;
import com.workingbriefcase.persistencia.entities.Curriculum;
import com.workingbriefcase.persistencia.entities.Usuario;
import com.workingbriefcase.utilidades.JTableCell;
import com.workingbriefcase.utilidades.SwingFormUtilities;
import java.util.Date;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.table.DefaultTableModel;

/**
 * Nombre de la clase: FrmPerfil
 * Fecha Creación: 11-07-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class FrmPerfil extends javax.swing.JInternalFrame {
    //<editor-fold defaultstate="collapsed" desc="varaibles globales">
    //objetos de clases dao
    private CandidatoJpaController daoCandidato;
    private RolJpaController daoRol;
    private PaisJpaController daoPais;
    private DepartamentoJpaController daoDepartamento;
    private MunicipioJpaController daoMunicipio;
    //objeto de clase de utilidades
    private SwingFormUtilities sfutil;
    
    //id del candidato dueño del perfil
    private int idCandidatoDelPerfil;
    
    //columnas de tbCurriculums
    Object [] tbCorriculumsColumns;
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="constructores">
    /**
     * Crear el formulario que muestra el perfil del usuario
     * @param idCandidatoLogeado id del usuario coyos datos ser vaciarán en el formulario
     */
    public FrmPerfil(int idCandidatoLogeado) {
        initComponents();
        
        //establecer la variable de instacia equivalente al parametro
        idCandidatoDelPerfil = idCandidatoLogeado;
        
        initialize(); 
       
    }
    
    /**
     * Realiza las acciones de inicialización que sean posibles sin utilizar<br>
     * los parametros del constructor
     */
    private void initialize(){
        //inicializar instancias de clases dao
        daoCandidato = new CandidatoJpaController();
        daoRol = new RolJpaController();
        daoPais = new PaisJpaController();
        daoDepartamento = new DepartamentoJpaController();
        daoMunicipio = new MunicipioJpaController();
        sfutil = SwingFormUtilities.getInstance();
        
        //definir las columnas por defecto del tbCurriculums
        tbCorriculumsColumns = new Object[]{
            "Correlativo", "Fecha de Publicación", "Privacidad", "Profesion Principal"
        };
        
        //llenar el formulario con los datos correspondientes
        llenarFormularioPerfil();
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="encapsulamiento">
    
    
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones CRUD Candidato"> 
    private void llenarFormularioPerfil(){
        try {
            Candidato candidatoDelPerfil;
            
            candidatoDelPerfil = daoCandidato.findCandidato(idCandidatoDelPerfil);
            
            lblUsuario.setText(candidatoDelPerfil.getUsuario().getNombre());
            lblNombre.setText(candidatoDelPerfil.getNombre());
            lblApellidos.setText(candidatoDelPerfil.getApellido());
            lblGenero.setText(candidatoDelPerfil.getGenero());
            lblCorreo.setText(candidatoDelPerfil.getUsuario().getCorreoElectronico());
            lblFechaNacimiento.setText(candidatoDelPerfil.getFechaNacimiento().toString());
            lblDepartamento.setText(candidatoDelPerfil.getMunicipioActual().getDepartamentoPertenece().getNombre());
            lblMunicipio.setText(candidatoDelPerfil.getMunicipioActual().getNombre());
            lblUltimaConexion.setText(candidatoDelPerfil.getUsuario().getUltimaConexion().toString());
            lblDireccion.setText(candidatoDelPerfil.getDireccion());
            lblAutoDescripcion.setText(candidatoDelPerfil.getAutoDescripcion());
            
            //llenar el grid de los curriculums
            llenarTbCurriculumns(candidatoDelPerfil);
        } catch (Exception e) {
            sfutil.mostrarMensajeExcepcion(this, "Ha ocurrido un error al intentar"
                    + "\nObtener los datos del Perfil", e);
            //cerrar el fomulario
            this.dispose();
        }
    }
    
    private void llenarTbCurriculumns(Candidato candidatoDelPerfil){
        DefaultTableModel dtbm;     
        List<Curriculum> curriculums;
        
        
                
        //"Correlativo", "Fecha de Publicación", "Privacidad", "Profesion Principal"
        curriculums = candidatoDelPerfil.getCurriculumList();
        if(curriculums.size() > 0 ){
            dtbm = new DefaultTableModel(tbCorriculumsColumns, 0);
            
            for(Curriculum c : curriculums){
                Object [] row;

                row = new Object[]{
                    new JTableCell<Curriculum>(c, String.valueOf(c.getCorrelativoActual())),
                    new JTableCell<Curriculum>(c, c.getFechaPublicado().toString()),
                    new JTableCell<Curriculum>(c, c.getPrivacidadCurriculum() == 0 ? "Privado" : "Publico"),
                    (c.getCurriculumProfesionList().size() > 0)
                        ? new JTableCell<Curriculum>(c, c.getCurriculumProfesionList().get(0).getProfesion().getNombre()) : ""
                };

                dtbm.addRow(row);
            }
            
            tbCurriculums.setEnabled(true);
        }else{
            dtbm = new DefaultTableModel(new Object[] {"No hay curriculums que mostrar"}, 0);
            tbCurriculums.setEnabled(false);
        }
          
        tbCurriculums.setModel(dtbm);
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones formulario candidato">
    public void abrirVistaDeCurriculum(){
        int selRow;
        
        selRow = tbCurriculums.getSelectedRow();
        
        //"Correlativo", "Fecha de Publicación", "Privacidad", "Profesion Principal"
        if(selRow > -1){       
            FrmVisualizarCurriculum vistaCurriculum;
            
            vistaCurriculum = new FrmVisualizarCurriculum(((JTableCell<Curriculum>)sfutil
                    .JTableGetValueAt(tbCurriculums, selRow, "Correlativo")).getValor().getId(), this);          
            
            this.getParent().add(vistaCurriculum);
            vistaCurriculum.show();  
        }
    }
    
    public void abrirManipuladorDeCurriculum(){
        int selRow;
        
        selRow = tbCurriculums.getSelectedRow();
        
        //"Correlativo", "Fecha de Publicación", "Privacidad", "Profesion Principal"
        if(selRow > -1){       
            FrmManipularCurriculum manipularCurriculum;
            
            manipularCurriculum = new FrmManipularCurriculum(((JTableCell<Curriculum>)sfutil
                    .JTableGetValueAt(tbCurriculums, selRow, "Correlativo")).getValor().getId(), this);          
            
            this.getParent().add(manipularCurriculum);
            manipularCurriculum.show();  
        }
    }
    
    public void abrirAgregarCurriculum(){
        try {
            Candidato objetivo;
            Curriculum nuevo;

            objetivo = daoCandidato.findCandidato(idCandidatoDelPerfil);

            nuevo = new Curriculum();
            objetivo.getCurriculumList().add(nuevo);

            nuevo.setCorrelativoActual(1);
            nuevo.setFechaPublicado(new Date());
            nuevo.setDescripcionCurriculum("");
            nuevo.setBorrado(false);
            nuevo.setCandidatoPertenece(objetivo);

            daoCandidato.edit(objetivo);
            
            llenarFormularioPerfil();
        } catch (Exception e) {
            sfutil.mostrarMensajeExcepcion(jPanel1, "Ha ocurrido un error inesperado", e);
        }
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones CRUD Pais">
    
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones CRUD Departamento">
    
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funcioens CRUD Municipio">
    
//</editor-fold>
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel8 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        lblUsuario = new javax.swing.JLabel();
        lblNombre = new javax.swing.JLabel();
        lblApellidos = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        lblCorreo = new javax.swing.JLabel();
        lblFoto = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        lblAutoDescripcion = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        lblPaisOrigen = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        lblGenero = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lblFechaNacimiento = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        lblDepartamento = new javax.swing.JLabel();
        lblMunicipio = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        lblUltimaConexion = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        lblDireccion = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbCurriculums = new javax.swing.JTable();
        jToolBar1 = new javax.swing.JToolBar();
        btnVisualizarCurriculum = new javax.swing.JButton();
        btnAgreagarCurriculum = new javax.swing.JButton();
        btnModificarCurriculum = new javax.swing.JButton();
        btnEliminarCurriculum = new javax.swing.JButton();

        setClosable(true);

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("Perfil de");

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel2.setText("Nombre Real");

        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel3.setText("Apellidos");

        lblUsuario.setFont(new java.awt.Font("Castellar", 2, 24)); // NOI18N
        lblUsuario.setText("Place holder");

        lblNombre.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lblNombre.setText("Place holder");

        lblApellidos.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lblApellidos.setText("Place holder");

        jLabel19.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel19.setText("Correo Electrónico");

        lblCorreo.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lblCorreo.setText("Place holder");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel19))
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblCorreo)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(lblUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, 308, Short.MAX_VALUE)
                    .addComponent(lblNombre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblApellidos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(lblUsuario))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(lblNombre))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(lblApellidos))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(lblCorreo)))
        );

        lblFoto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/workingbriefcase/imagenes/user_profile_default.jpg"))); // NOI18N
        lblFoto.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "AutoDescripción", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Vivaldi", 0, 18))); // NOI18N

        lblAutoDescripcion.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblAutoDescripcion.setText("Este es un place holder");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblAutoDescripcion, javax.swing.GroupLayout.DEFAULT_SIZE, 657, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblAutoDescripcion, javax.swing.GroupLayout.DEFAULT_SIZE, 78, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Otros Datos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 0, 14))); // NOI18N

        jLabel12.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel12.setText("Pais de origen");

        lblPaisOrigen.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lblPaisOrigen.setText("Place holder");

        jLabel20.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel20.setText("Genero");

        lblGenero.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lblGenero.setText("Place holder");

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel7.setText("Fecha de Nacimiento");

        lblFechaNacimiento.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lblFechaNacimiento.setText("Place holder");

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Dirección Actual"));
        jPanel4.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N

        jLabel14.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel14.setText("Municipio");

        jLabel13.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel13.setText("Departamento");

        lblDepartamento.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lblDepartamento.setText("Place holder");

        lblMunicipio.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lblMunicipio.setText("Place holder");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13)
                    .addComponent(jLabel14))
                .addGap(50, 50, 50)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDepartamento, javax.swing.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
                    .addComponent(lblMunicipio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(lblDepartamento))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(lblMunicipio))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jLabel20)
                    .addComponent(jLabel12))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblPaisOrigen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblGenero, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblFechaNacimiento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(lblFechaNacimiento))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(lblGenero))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(lblPaisOrigen))
                .addGap(18, 18, 18)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jLabel18.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel18.setText("Ultima Conexión");

        lblUltimaConexion.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lblUltimaConexion.setText("Place holder");

        jLabel9.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel9.setText("Dirección");

        lblDireccion.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lblDireccion.setText("Place holder");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel18)
                    .addComponent(jLabel9))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblUltimaConexion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblDireccion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(lblUltimaConexion))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(lblDireccion))
                .addContainerGap(150, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder("Curriculums"));

        tbCurriculums.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbCurriculums.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tbCurriculumsMousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(tbCurriculums);

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        btnVisualizarCurriculum.setText("Visualizar Curriculum");
        btnVisualizarCurriculum.setFocusable(false);
        btnVisualizarCurriculum.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnVisualizarCurriculum.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnVisualizarCurriculum.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnVisualizarCurriculumMousePressed(evt);
            }
        });
        jToolBar1.add(btnVisualizarCurriculum);

        btnAgreagarCurriculum.setText("Agregar Curriculum");
        btnAgreagarCurriculum.setFocusable(false);
        btnAgreagarCurriculum.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAgreagarCurriculum.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAgreagarCurriculum.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnAgreagarCurriculumMousePressed(evt);
            }
        });
        jToolBar1.add(btnAgreagarCurriculum);

        btnModificarCurriculum.setText("Modificar Curriculum");
        btnModificarCurriculum.setFocusable(false);
        btnModificarCurriculum.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnModificarCurriculum.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnModificarCurriculum.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnModificarCurriculumMousePressed(evt);
            }
        });
        jToolBar1.add(btnModificarCurriculum);

        btnEliminarCurriculum.setText("Eliminar Curriculum");
        btnEliminarCurriculum.setFocusable(false);
        btnEliminarCurriculum.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEliminarCurriculum.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar1.add(btnEliminarCurriculum);

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 422, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(lblFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 38, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addGap(0, 11, Short.MAX_VALUE)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jScrollPane1.setViewportView(jPanel8);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 732, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 579, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //<editor-fold defaultstate="collapsed" desc="escuchadores de evento">
    private void tbCurriculumsMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbCurriculumsMousePressed
        
    }//GEN-LAST:event_tbCurriculumsMousePressed

    private void btnVisualizarCurriculumMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnVisualizarCurriculumMousePressed
        abrirVistaDeCurriculum();
    }//GEN-LAST:event_btnVisualizarCurriculumMousePressed

    private void btnModificarCurriculumMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnModificarCurriculumMousePressed
        abrirManipuladorDeCurriculum();
    }//GEN-LAST:event_btnModificarCurriculumMousePressed

    private void btnAgreagarCurriculumMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAgreagarCurriculumMousePressed
        abrirAgregarCurriculum();
    }//GEN-LAST:event_btnAgreagarCurriculumMousePressed
//</editor-fold>

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgreagarCurriculum;
    private javax.swing.JButton btnEliminarCurriculum;
    private javax.swing.JButton btnModificarCurriculum;
    private javax.swing.JButton btnVisualizarCurriculum;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JLabel lblApellidos;
    private javax.swing.JLabel lblAutoDescripcion;
    private javax.swing.JLabel lblCorreo;
    private javax.swing.JLabel lblDepartamento;
    private javax.swing.JLabel lblDireccion;
    private javax.swing.JLabel lblFechaNacimiento;
    private javax.swing.JLabel lblFoto;
    private javax.swing.JLabel lblGenero;
    private javax.swing.JLabel lblMunicipio;
    private javax.swing.JLabel lblNombre;
    private javax.swing.JLabel lblPaisOrigen;
    private javax.swing.JLabel lblUltimaConexion;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JTable tbCurriculums;
    // End of variables declaration//GEN-END:variables

}
