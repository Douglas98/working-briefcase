/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.workingbriefcase.vista.administrador;

import com.workingbriefcase.persistencia.controladores.ArealaboralJpaController;
import com.workingbriefcase.persistencia.controladores.HabilidadprofesionalJpaController;
import com.workingbriefcase.persistencia.entities.Arealaboral;
import com.workingbriefcase.persistencia.entities.Habilidadprofesional;
import com.workingbriefcase.utilidades.ComboBoxItem;
import com.workingbriefcase.utilidades.MetodosUtil;
import com.workingbriefcase.utilidades.SwingFormUtilities;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Erick Reyes
 */
public class FrmHabilidadProfesional extends javax.swing.JInternalFrame {

    Habilidadprofesional hp = new Habilidadprofesional();
    HabilidadprofesionalJpaController daoH = new HabilidadprofesionalJpaController();
    ArealaboralJpaController daoA = new ArealaboralJpaController();
    private SwingFormUtilities sfutil;
    MetodosUtil mu = new MetodosUtil();
    int cod = 0;
    int codigoA = 0;
    
    public FrmHabilidadProfesional() {
        initComponents();
        cargarDatos();
        llenarCombo();
    }
           public SwingFormUtilities getSfutil() {

             if(sfutil == null){
            sfutil = SwingFormUtilities.getInstance();
             }
        
        return sfutil;
    }
    public void limpiar(){
        txtNombre.setText("");
    }
    boolean cajasVacias(){
         boolean estado  = true;
         if(txtNombre.getText().isEmpty()){
             estado = false;
         }
         return estado;
     }
    Habilidadprofesional llenar()
    {
        hp.setId(cod);
        hp.setNombre(this.txtNombre.getText());
        hp.setBorrado(true);
        Arealaboral al = new Arealaboral();
        al.setId(codigoA);
        hp.setAreaLaboral(al);
        return hp;
    }
     void llenarCombo(){
        try {
            DefaultComboBoxModel modelo = new DefaultComboBoxModel();
            modelo.addElement("Seleccionar");
            List<Arealaboral> lista = daoA.findArealaboralEntities();
            for (Arealaboral item : lista) {
                modelo.addElement(new ComboBoxItem(item.getId(),item.getNombre()));
            }
            this.cmbArea.setModel(modelo);
        } catch (Exception e) {
            this.sfutil.mostrarMensajeFallo(null, "Error al llenar los roles" + e.toString());
        }
    }
    private void cargarDatos()
    {
        String titulos[] = {"Código","Habilidad_Profesional","Área Laboral","Código Área"};
        Object obj[] = new Object[4];
        DefaultTableModel tabla = new DefaultTableModel(null, titulos);
        List<Habilidadprofesional> ls;
        
        try 
        {
            ls = daoH.findHabilidadprofesionalEntities();
            for (int i = 0; i < ls.size(); i++) {
                 hp = (Habilidadprofesional)ls.get(i);
                 obj[0] = hp.getId();
                 obj[1] = hp.getNombre();
                 obj[2] = hp.getAreaLaboral().getNombre();
                 obj[3] = hp.getAreaLaboral().getId();
                 tabla.addRow(obj);
            }
            this.tblHabilidad.setModel(tabla);
            
        } catch (Exception e) 
        {
             JOptionPane.showMessageDialog(null,e.getMessage());
        }
        
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblHabilidad = new javax.swing.JTable();
        btnInsertar = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        cmbArea = new javax.swing.JComboBox<>();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Habilidad Profesional");

        jLabel2.setText("Nombre:");

        tblHabilidad.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblHabilidad.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblHabilidadMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblHabilidad);

        btnInsertar.setText("Insertar");
        btnInsertar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInsertarActionPerformed(evt);
            }
        });

        btnModificar.setText("Modificar");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        jLabel3.setText("Área Laboral:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 481, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(180, 180, 180)
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(62, 62, 62)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel2)
                    .addComponent(btnInsertar))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(btnModificar)
                        .addGap(29, 29, 29)
                        .addComponent(btnEliminar)
                        .addGap(18, 18, 18)
                        .addComponent(btnCancelar))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtNombre, javax.swing.GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE)
                            .addComponent(cmbArea, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cmbArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnInsertar)
                    .addComponent(btnModificar)
                    .addComponent(btnEliminar)
                    .addComponent(btnCancelar))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(12, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnInsertarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInsertarActionPerformed
        try {
            if (cajasVacias() == true)
            {
                daoH.create(llenar());
                JOptionPane.showMessageDialog(null,"Datos ingresados correctamente.");
                cargarDatos();
                limpiar();
            }
            else
            {
                  JOptionPane.showMessageDialog(null, "Debe llenar los campos.");
            }
        } catch (Exception e) {
        }
    }//GEN-LAST:event_btnInsertarActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
       try {
            if (cajasVacias() == true)
            {
                daoH.edit(llenar());
                JOptionPane.showMessageDialog(null,"Datos modificados correctamente.");
                cargarDatos();
                limpiar();
            }
            else
            {
                  JOptionPane.showMessageDialog(null, "Debe llenar los campos.");
            }
        } catch (Exception e) {
        }
    }//GEN-LAST:event_btnModificarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
      try {
               if(cajasVacias() == true)
               if(JOptionPane.showConfirmDialog(null, "Desea eliminar los datos?","Modificar",JOptionPane.YES_NO_OPTION) == 0){
                    daoH.destroy(llenar().getId());
                    JOptionPane.showMessageDialog(null,"Datos eliminar correctamente.");
                    cargarDatos();
                    limpiar();
               }
               else
                   JOptionPane.showMessageDialog(null, "Debe seleccionar un registro.");
          
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Error al eliminar los datos." + e.toString());
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void tblHabilidadMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblHabilidadMouseClicked
        int fila = this.tblHabilidad.getSelectedRow();
        cod = Integer.parseInt(String.valueOf(this.tblHabilidad.getValueAt(fila, 0)));
        txtNombre.setText(String.valueOf(this.tblHabilidad.getValueAt(fila, 1)));
        codigoA = Integer.parseInt(String.valueOf(this.tblHabilidad.getValueAt(fila, 3)));
        int index = 1;
        List<Arealaboral>ls = daoA.findArealaboralEntities();
        for (Arealaboral l : ls) 
        {
            if(codigoA == hp.getAreaLaboral().getId())
            {
                break;
            }
            index++;
        }
        this.cmbArea.setSelectedIndex(index);
    }//GEN-LAST:event_tblHabilidadMouseClicked

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
       if(JOptionPane.showConfirmDialog(null, "Desea cancelar la operacion?","Cancelar",JOptionPane.YES_NO_OPTION)==0){
           limpiar();
       }
    }//GEN-LAST:event_btnCancelarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnInsertar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JComboBox<String> cmbArea;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblHabilidad;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}
