/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.workingbriefcase.vista.administrador;


import com.workingbriefcase.persistencia.controladores.CargoJpaController;
import com.workingbriefcase.persistencia.controladores.EmpleadoasalariadoJpaController;
import com.workingbriefcase.persistencia.controladores.MunicipioJpaController;
import com.workingbriefcase.persistencia.controladores.PaisJpaController;
import com.workingbriefcase.persistencia.controladores.ProfesionJpaController;
import com.workingbriefcase.persistencia.controladores.UsuarioJpaController;
import com.workingbriefcase.persistencia.entities.Encargado;
import com.workingbriefcase.persistencia.entities.Usuario;

import com.workingbriefcase.utilidades.SwingFormUtilities;


/**
 *
 * @author hola
 */
public class FrmPerfilEmpleado extends javax.swing.JInternalFrame {
        int codigoMunicipio = 0;
        int codigoProfesion =0;
        int condigoCargo =0;
        int codigoPais = 0;
        int codigoEmpleado =0;
        int codigoUsuario =0;

        Usuario us = new Usuario();
        EmpleadoasalariadoJpaController emj = new EmpleadoasalariadoJpaController();
        MunicipioJpaController munj = new MunicipioJpaController();
        ProfesionJpaController poj = new ProfesionJpaController();
        CargoJpaController carj = new CargoJpaController();
        PaisJpaController paj = new PaisJpaController();
        UsuarioJpaController usj= new UsuarioJpaController();
        
        
      
      //variables globales
       
        Encargado en = new Encargado();
      private SwingFormUtilities sfultil;
        String ruta = null;

       //constructor
    public SwingFormUtilities getSfultil() {
         if(sfultil == null){

            sfultil = SwingFormUtilities.getInstance();

             }
        return sfultil;
    }
      
    /**
     * Creates new form FrmPerfilEmpresa
     * @param codigoUs
     */
    public FrmPerfilEmpleado(int codigoUs) {
        initComponents();
        codigoUsuario = codigoUs;
        llenarDatos();       
//        comboDepartamento();
//        comboNacionalidadEmpresa();
//        comboSectorEmpresarial();
//        comboAreaLaboral();
//        comboCargo();
//        comboNacionalidad();
        
    }
    
        public FrmPerfilEmpleado() {
        initComponents();
//        comboDepartamento();
//        comboNacionalidadEmpresa();
//        comboSectorEmpresarial();
//        comboAreaLaboral();
//        comboCargo();
//        comboNacionalidad();
        
    }
        
        void llenarDatos()
        {
            us=usj.findUsuario(codigoUsuario);
            
            txtNombreEmpresa.setText(us.getNombre());
            
            
        }
 //llenar combos
//      
//      public void comboDepartamento(){
//          try {
//                  DefaultComboBoxModel modelo = new DefaultComboBoxModel();
//              modelo.addElement("Seleccionar");
//              List<Departamento> lista = departamentoControlador.findDepartamentoEntities();
//              for (Departamento departamento : lista) {
//                  modelo.addElement(new ComboBoxItem(departamento.getId(),departamento.getNombre()));
//              }
//              this.cmbDepartamento.setModel(modelo);
//          } catch (Exception e) {
//              getSfultil().mostrarMensajeFallo(null, "Error al mostra departamentos");
//              e.printStackTrace();
//          }
//      }
//      public void comboMunicipio(int codigoDepartamento){
//          try {
//                  DefaultComboBoxModel modelo = new DefaultComboBoxModel();
//              this.cmbMunicipio.removeAllItems();
//              modelo.addElement("Seleccionar");
//              List<Municipio> lista = municipioControlador.findMunicipioEntitiesByDepartamento(codigoDepartamento);
//              for (Municipio municipio : lista) {
//                  modelo.addElement(new ComboBoxItem(municipio.getId(),municipio.getNombre()));
//              }
//              this.cmbMunicipio.setModel(modelo);
//          } catch (Exception e) {
//              getSfultil().mostrarMensajeFallo(null, "Error al mostra Municipios");
//              e.printStackTrace();
//          }
//      }
//      public void comboNacionalidadEmpresa(){
//              try {
//                 DefaultComboBoxModel modelo = new DefaultComboBoxModel();
//              modelo.addElement("Seleccionar");
//              List<Pais> lista = nacionalidadControlador.findPaisEntities();
//              for (Pais pais : lista) {
//                  modelo.addElement(new ComboBoxItem(pais.getId(),pais.getNombre()));
//              }
//              this.cmbNacionalidadEmpresa.setModel(modelo);
//          } catch (Exception e) {
//              getSfultil().mostrarMensajeFallo(null, "Error al mostra pais");
//              e.printStackTrace();
//          }
//      }
//      public void comboSectorEmpresarial(){
//                try {
//                 DefaultComboBoxModel modelo = new DefaultComboBoxModel();
//              modelo.addElement("Seleccionar");
//              List<Sectorempresarial> lista = sectorEmpresarialcontrolador.findSectorempresarialEntities();
//              for (Sectorempresarial sectorempresarial : lista) {
//                  modelo.addElement(new ComboBoxItem(sectorempresarial.getId(),sectorempresarial.getNombre()));
//              }
//              this.cmbSectorEmpresarial.setModel(modelo);
//          } catch (Exception e) {
//              getSfultil().mostrarMensajeFallo(null, "Error al mostra Sector empresarial");
//              e.printStackTrace();
//          }
//      }
//      public void comboAreaLaboral(){
//          try{
//            DefaultComboBoxModel modelo = new DefaultComboBoxModel();
//              modelo.addElement("Seleccionar");
//              List<Arealaboral> lista = areaLaboralControlador.findArealaboralEntities();
//              for (Arealaboral arealaboral : lista) {
//                  modelo.addElement(new ComboBoxItem(arealaboral.getId(),arealaboral.getNombre()));
//              }
//              this.cmbAreaLaboral.setModel(modelo);
//          } catch (Exception e) {
//              getSfultil().mostrarMensajeFallo(null, "Error al mostra area laboral");
//              e.printStackTrace();
//          }
//      }
//      public void comboProfesion(int codigoAreaLaboral){
//            try{
//                this.cmbProfesion.removeAllItems();
//            DefaultComboBoxModel modelo = new DefaultComboBoxModel();
//              modelo.addElement("Seleccionar");
//              List<Profesion> lista = profesionControlador.findProfesionByAreaLaboral(codigoAreaLaboral);
//              for (Profesion profesion : lista) {
//                  modelo.addElement(new ComboBoxItem(profesion.getId(),profesion.getNombre()));
//              }
//              this.cmbProfesion.setModel(modelo);
//          } catch (Exception e) {
//              getSfultil().mostrarMensajeFallo(null, "Error al mostra profesion");
//              e.printStackTrace();
//          }
//      }
//      public void comboCargo(){
//     try{
//            DefaultComboBoxModel modelo = new DefaultComboBoxModel();
//              modelo.addElement("Seleccionar");
//              List<Cargo> lista = cargoControlador.findCargoEntities();
//              for (Cargo cargo : lista) {
//                  modelo.addElement(new ComboBoxItem(cargo.getId(),cargo.getNombre()));
//              }
//              this.cmbCargo.setModel(modelo);
//          } catch (Exception e) {
//              getSfultil().mostrarMensajeFallo(null, "Error al mostra cargo");
//              e.printStackTrace();
//          }
//    }
//      public void comboNacionalidad(){
//              try {
//                 DefaultComboBoxModel modelo = new DefaultComboBoxModel();
//              modelo.addElement("Seleccionar");
//              List<Pais> lista = nacionalidadControlador.findPaisEntities();
//              for (Pais pais : lista) {
//                  modelo.addElement(new ComboBoxItem(pais.getId(),pais.getNombre()));
//              }
//              this.cmbNacionalidad.setModel(modelo);
//          } catch (Exception e) {
//              getSfultil().mostrarMensajeFallo(null, "Error al mostra pais encargado");
//              e.printStackTrace();
//          }
//      }
//      
//       
//    
//     void abrirImagen(){
//            JFileChooser j = new JFileChooser();
//        j.setCurrentDirectory(new File("Imagenes/"));
//        int ap = j.showOpenDialog(this);
//        
//        if(ap == JFileChooser.APPROVE_OPTION){
//            ruta = j.getSelectedFile().getAbsolutePath();
//            lblimagen.setIcon(new ImageIcon(ruta));
//            lblRuta.setText(ruta);
//        }
//     }
//     Encargado llenarEncargado(){
//       
//         en.setNombre(txtNombre.getText());
//         en.setApellido(txtApellido.getText());
//         Profesion pe = new Profesion();
//         pe.setId(codigoProfesion);
//         en.setProfesionEjercida(pe);
//         Cargo car = new Cargo();
//         car.setId(codigoCargo);
//         en.setCargoDesepennado(car);
//         Pais p = new Pais();
//         p.setId(codigoNacionalidadEncargado);
//         en.setNacionalidad(p);
//         return en;
//     }
//     Empresa llenar(){
//                Empresa em = new Empresa();
//                FileInputStream fi = null;
//              int codigoEncargado = 0;
//            try {
//            
//         em.setNit(txtNit.getText());
//         em.setNombre(txtNombreEmpresa.getText());
//         em.setRazonSocial(txtRazonSocial.getText());
//         em.setDireccion(txtDireccion.getText());
//         em.setTipologia(txtTipologia.getText());
//         em.setDireccionPaginaWeb(txtPaginaWeb.getText());
//               File file = new File(ruta);
//               byte[] bArray = new byte[(int) file.length()];
//               fi = new FileInputStream(file);
//               fi.read(bArray);
//               fi.close();
//          em.setFoto(bArray);
//          em.setMaxNumeroTrabajadores(Integer.parseInt(jsMax.getValue().toString()));
//          em.setMinNumeroTrabajadores(Integer.parseInt(this.jsMin.getValue().toString()));
//          Municipio mu = new Municipio();
//          mu.setId(codigoMunicipio);
//          em.setMunicipioActual(mu);
//          Sectorempresarial sec = new Sectorempresarial();
//          sec.setId(codigoSectorEmpresarial);
//          em.setSectorEmpresarial(sec);
//          codigoEncargado = encargadoControlador.lastID();
//          en.setId(codigoEncargado);
//          em.getEncargado().setId(codigoEncargado);
//          em.setEncargado(en);
//          Pais p = new Pais();
//          p.setId(codigoNacionalidadEmpresa);
//          em.setNacionalidad(p);
//          Usuario us = new Usuario();
//          us.setId(codigoUs);
//          em.setUsuario(us);
//         } catch (Exception e) {
//                eliminarEncargado(codigoEncargado);
//              getSfultil().mostrarMensajeFallo(null, "Error al llenar " + e.toString());
//              e.printStackTrace();
//         }
//   
//         return em;
//     }
//     void eliminarEncargado(int codigoEncargado){
//         try {
//             encargadoControlador.destroy(codigoEncargado);
//         } catch (Exception e) {
//             e.printStackTrace();
//         }
//     }
//     
//     
//     void insertar(){
//         try {
//             encargadoControlador.create(llenarEncargado());
//             empresaControlador.create(llenar());
//             getSfultil().mostrarMensajeInformativo(null,"Datos ingresados correctamente.","Exito");
//         } catch (Exception e) {
//             getSfultil().mostrarMensajeFallo(null, "Error al ingresar los datos." + e.toString());
//             e.printStackTrace();
//         }
//     }
      
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     * @param args
     */
 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtNit = new javax.swing.JFormattedTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDireccion = new javax.swing.JTextArea();
        lblimagen = new javax.swing.JLabel();
        btnAgregarFoto = new javax.swing.JButton();
        btnAgregar = new javax.swing.JButton();
        lblRuta = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtNombreEmpresa = new javax.swing.JTextField();
        txtApellido = new javax.swing.JTextField();
        txtDui = new javax.swing.JFormattedTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        Masculino = new javax.swing.JRadioButton();
        Femenino = new javax.swing.JRadioButton();
        jLabel9 = new javax.swing.JLabel();
        txtSalario = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        cmbCargoDesempenado = new javax.swing.JComboBox<>();
        jLabel11 = new javax.swing.JLabel();
        cmbMunicipioActual = new javax.swing.JComboBox<>();
        jLabel12 = new javax.swing.JLabel();
        cmbNacionalidad = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        cmbProfesion = new javax.swing.JComboBox<>();

        setClosable(true);

        jScrollPane2.setAutoscrolls(true);

        jPanel1.setToolTipText("");

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jLabel1.setText("Datos Personales Empleado");

        jLabel2.setText("Apellido:");

        try {
            txtNit.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####-######-###-#")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel3.setText("Dui:");

        jLabel4.setText("Direccion:");

        txtDireccion.setColumns(20);
        txtDireccion.setRows(5);
        jScrollPane1.setViewportView(txtDireccion);

        lblimagen.setText("Foto:");

        btnAgregarFoto.setText("Explorar...");
        btnAgregarFoto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarFotoActionPerformed(evt);
            }
        });

        btnAgregar.setText("Agregar");
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });

        jLabel5.setText("Nombre:");

        try {
            txtDui.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("########-#")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel6.setText("Nit:");

        jLabel7.setText("Fecha Nacimiento:");

        jLabel8.setText("Genero:");

        Masculino.setText("Masculino");

        Femenino.setText("Femenino");

        jLabel9.setText("Salario");

        jLabel10.setText("Cargo Desenpeñado:");

        jLabel11.setText("Municipio Actual:");

        jLabel12.setText("Nacionalidad:");

        jLabel13.setText("Profesion Ejercida:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel9)
                .addGap(109, 109, 109))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Masculino)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Femenino)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 98, Short.MAX_VALUE)
                        .addComponent(txtSalario, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(51, 51, 51))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11)
                            .addComponent(jLabel12)
                            .addComponent(jLabel13))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cmbCargoDesempenado, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbMunicipioActual, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbNacionalidad, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbProfesion, 0, 194, Short.MAX_VALUE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(Masculino)
                    .addComponent(Femenino)
                    .addComponent(txtSalario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(51, 51, 51)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(cmbCargoDesempenado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(41, 41, 41)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11)
                    .addComponent(cmbMunicipioActual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(cmbNacionalidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(cmbProfesion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(122, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(330, 330, 330)
                        .addComponent(lblRuta)
                        .addGap(99, 99, 99))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblimagen, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(68, 68, 68)
                                .addComponent(btnAgregarFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel3)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(txtDui, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(txtNit, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel5)
                                            .addComponent(jLabel4))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtNombreEmpresa, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(35, 35, 35)))))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(150, 150, 150))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(351, 351, 351)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(txtNombreEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(45, 45, 45)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(32, 32, 32)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(txtDui, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(44, 44, 44)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblRuta))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(txtNit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblimagen, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAgregarFoto)))
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addComponent(jLabel7)
                .addGap(27, 27, 27)
                .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(94, Short.MAX_VALUE))
        );

        jScrollPane2.setViewportView(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
 
    private void btnAgregarFotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarFotoActionPerformed
//        abrirImagen();
    }//GEN-LAST:event_btnAgregarFotoActionPerformed

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
//        insertar();
//        this.setVisible(false);
    }//GEN-LAST:event_btnAgregarActionPerformed

    
      public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmLoginAdminEm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmLoginAdminEm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmLoginAdminEm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmLoginAdminEm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
         
            public void run() {
               new FrmPerfilEmpleado().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton Femenino;
    private javax.swing.JRadioButton Masculino;
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnAgregarFoto;
    private javax.swing.JComboBox<String> cmbCargoDesempenado;
    private javax.swing.JComboBox<String> cmbMunicipioActual;
    private javax.swing.JComboBox<String> cmbNacionalidad;
    private javax.swing.JComboBox<String> cmbProfesion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblRuta;
    private javax.swing.JLabel lblimagen;
    private javax.swing.JTextField txtApellido;
    private javax.swing.JTextArea txtDireccion;
    private javax.swing.JFormattedTextField txtDui;
    private javax.swing.JFormattedTextField txtNit;
    private javax.swing.JTextField txtNombreEmpresa;
    private javax.swing.JTextField txtSalario;
    // End of variables declaration//GEN-END:variables
}
