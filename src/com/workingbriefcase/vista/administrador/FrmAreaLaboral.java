/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.workingbriefcase.vista.administrador;

import com.workingbriefcase.persistencia.controladores.ArealaboralJpaController;
import com.workingbriefcase.persistencia.entities.Arealaboral;
import com.workingbriefcase.utilidades.MetodosUtil;
import com.workingbriefcase.utilidades.SwingFormUtilities;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Erick Reyes
 */
public class FrmAreaLaboral extends javax.swing.JInternalFrame {

    Arealaboral al = new Arealaboral();
    ArealaboralJpaController daoL = new ArealaboralJpaController();
    private SwingFormUtilities sfutil;
    MetodosUtil mu = new MetodosUtil();
    int cod = 0;
    
    public FrmAreaLaboral() {
        initComponents();
        cargarDatos();
    }
        public SwingFormUtilities getSfutil() {

             if(sfutil == null){
            sfutil = SwingFormUtilities.getInstance();
             }
        
        return sfutil;
    }
    
    public void limpiar(){
        txtNombre.setText("");
    }
    boolean cajasVacias(){
         boolean estado  = true;
         if(txtNombre.getText().isEmpty()){
             estado = false;
         }
         return estado;
     }
    Arealaboral llenar()
    {
        al.setId(cod);
        al.setNombre(this.txtNombre.getText());
        al.setBorrado(true);
        return al;
    }
    
    private void cargarDatos()
    {
        String titulos[] = {"Código","Habilidad Profesional"};
        Object obj[] = new Object[2];
        DefaultTableModel tabla = new DefaultTableModel(null, titulos);
        List<Arealaboral> ls;
        
        try 
        {
            ls = daoL.findArealaboralEntities();
            for (int i = 0; i < ls.size(); i++) {
                 al = (Arealaboral)ls.get(i);
                 obj[0] = al.getId();
                 obj[1] = al.getNombre();
                 tabla.addRow(obj);
            }
            this.tblLaboral.setModel(tabla);
            
        } catch (Exception e) 
        {
             JOptionPane.showMessageDialog(null,e.getMessage());
        }
        
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblLaboral = new javax.swing.JTable();
        txtNombre = new javax.swing.JTextField();
        btnModificar = new javax.swing.JButton();
        btnInsertar = new javax.swing.JButton();
        btnElimina = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();

        jTextField1.setText("jTextField1");

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Área Laboral");

        jLabel2.setText("Nombre:");

        tblLaboral.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblLaboral.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblLaboralMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblLaboral);

        btnModificar.setText("Modificar");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        btnInsertar.setText("Insertar");
        btnInsertar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInsertarActionPerformed(evt);
            }
        });

        btnElimina.setText("Eliminar");
        btnElimina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminaActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 21, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(185, 185, 185)
                                .addComponent(jLabel1))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(77, 77, 77)
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(55, 55, 55)
                .addComponent(btnInsertar)
                .addGap(18, 18, 18)
                .addComponent(btnModificar)
                .addGap(18, 18, 18)
                .addComponent(btnElimina)
                .addGap(18, 18, 18)
                .addComponent(btnCancelar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jLabel1)
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 44, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnModificar)
                    .addComponent(btnInsertar)
                    .addComponent(btnElimina)
                    .addComponent(btnCancelar))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnInsertarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInsertarActionPerformed
        try {
            if (cajasVacias() == true)
            {
                daoL.create(llenar());
                JOptionPane.showMessageDialog(null,"Datos ingresados correctamente.");
                cargarDatos();
                limpiar();
            }
            else
            {
                  JOptionPane.showMessageDialog(null, "Debe llenar los campos.");
            }
        } catch (Exception e) {
        }
    }//GEN-LAST:event_btnInsertarActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
       try {
            if (cajasVacias() == true)
            {
                daoL.edit(llenar());
                JOptionPane.showMessageDialog(null,"Datos modificados correctamente.");
                cargarDatos();
                limpiar();
            }
            else
            {
                  JOptionPane.showMessageDialog(null, "Debe llenar los campos.");
            }
        } catch (Exception e) {
        }
    }//GEN-LAST:event_btnModificarActionPerformed

    private void btnEliminaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminaActionPerformed
         try {
               if(cajasVacias() == true)
               if(JOptionPane.showConfirmDialog(null, "Desea eliminar los datos?","Modificar",JOptionPane.YES_NO_OPTION) == 0){
                    daoL.destroy(llenar().getId());
                    JOptionPane.showMessageDialog(null,"Datos eliminar correctamente.");
                    cargarDatos();
                    limpiar();
               }
               else
                   JOptionPane.showMessageDialog(null, "Debe seleccionar un registro.");
          
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Error al eliminar los datos." + e.toString());
        }
    }//GEN-LAST:event_btnEliminaActionPerformed

    private void tblLaboralMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblLaboralMouseClicked
         int fila = this.tblLaboral.getSelectedRow();
        cod = Integer.parseInt(String.valueOf(this.tblLaboral.getValueAt(fila, 0)));
        txtNombre.setText(String.valueOf(this.tblLaboral.getValueAt(fila, 1)));
    }//GEN-LAST:event_tblLaboralMouseClicked

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
       if(JOptionPane.showConfirmDialog(null, "Desea cancelar la operacion?","Cancelar",JOptionPane.YES_NO_OPTION)==0){
           limpiar();
       }
    }//GEN-LAST:event_btnCancelarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnElimina;
    private javax.swing.JButton btnInsertar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTable tblLaboral;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}
