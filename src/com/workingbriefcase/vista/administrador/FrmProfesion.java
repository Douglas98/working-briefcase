/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.workingbriefcase.vista.administrador;

import com.workingbriefcase.persistencia.controladores.ArealaboralJpaController;
import com.workingbriefcase.persistencia.controladores.ProfesionJpaController;
import com.workingbriefcase.persistencia.entities.Arealaboral;
import com.workingbriefcase.persistencia.entities.Profesion;
import com.workingbriefcase.utilidades.ComboBoxItem;
import com.workingbriefcase.utilidades.SwingFormUtilities;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author hola
 */
public class FrmProfesion extends javax.swing.JInternalFrame {
        ProfesionJpaController pc = new ProfesionJpaController();
        ArealaboralJpaController ap = new ArealaboralJpaController();
        Profesion p = new Profesion();
        int codigo = 0;
        int codigoAreaLaboral = 0;
        private SwingFormUtilities sfutil;

    public SwingFormUtilities getSfutil() {

             if(sfutil == null){
            sfutil = SwingFormUtilities.getInstance();
             }
        
        return sfutil;
    }
        

        
    /**
     * Creates new form FrmProfesion
     */
    public FrmProfesion() {
        initComponents();
        llenarCombo();
        tablaE();
        
    }
    
        boolean cajasVacias(int flag){
        boolean estado = true;
        


        if(flag == 1){
            
        if(txtNombre.getText().isEmpty()){
            estado = false;
        }

        if(this.cmbAreaLaboral.getSelectedItem().equals(0)){
            estado = false;
        }
        return estado;
     }

        return estado;
    }
    
    void tablaE()
    {
        String [] headers = {"Codig","Nombre","Area Laboral"};
        Object[] obj = new Object[3]; 
        DefaultTableModel modelo = new DefaultTableModel(null, headers);
        List ls;
        
        ls = pc.findProfesionEntities();
        
        for (int i = 0; i < ls.size(); i++) {
            p = (Profesion) ls.get(i);
            obj[0] = p.getId();
            obj[1] = p.getNombre();
            obj[2] = p.getAreaLaboral().getId();
            modelo.addRow(obj);
        }
        this.jtablaDatos.setModel(modelo);
    }

    Profesion llenar(){
        
        p.setNombre(this.txtNombre.getText());
        Arealaboral al = new Arealaboral();
        al.setId(codigoAreaLaboral+1);
        p.setAreaLaboral(al);
        return p;
    }
    
    
    void insertar(){
        try {
            if(cajasVacias(1)){
                        pc.create(llenar());
                        JOptionPane.showMessageDialog(null, "Datos ingresados correctamente");
                         tablaE();
                         limpiar();
            }else{
                JOptionPane.showMessageDialog(null, "Debe completar todos los campos");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Error al insertar." + e.toString());
        }
    }
    
    Profesion Modificar()
    {
        try 
        {
        codigo = p.getId();
        p.setNombre(this.txtNombre.getText());
        Arealaboral al = new Arealaboral();
        al.setId(this.cmbAreaLaboral.getSelectedIndex());
        p.setAreaLaboral(al);
        
        int respuesta = JOptionPane.showConfirmDialog(null, "Desea Modificar el usuario ?","Confirmacion",0,JOptionPane.YES_NO_OPTION);
        
            if(respuesta==0)
            {

                JOptionPane.showMessageDialog(null, "Datos Modificados Correctamente");
                pc.edit(p);
                 tablaE();
                 limpiar();
            } 
        }
        catch (Exception e) 
        {
         JOptionPane.showMessageDialog(null, "Error al modificar");
        }
        
        return p;
    }
    
    
    void modificar(){
        try {
            if(cajasVacias(0)){
                
                    if(JOptionPane.showConfirmDialog(null, "Desea modificar los datos?","Modificar",JOptionPane.YES_NO_OPTION)==0){
                        pc.edit(llenar());
                        JOptionPane.showMessageDialog(null, "Datos modificados correctamente");
                         tablaE();
                         limpiar();
                    }
               
            }else{
                JOptionPane.showMessageDialog(null, "Debe completar todos los campos");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Error al modificar." + e.toString());
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    Profesion Eliminar(){
        
         try 
        {
           codigo = p.getId();
     
        
        int respuesta = JOptionPane.showConfirmDialog(null, "Desea Eliminar el usuario ?","Confirmacion",0,JOptionPane.YES_NO_OPTION);
        
            if(respuesta==0)
            {

                JOptionPane.showMessageDialog(null, "Datos Eliminados Correctamente");
                pc.destroy(codigo);
                 tablaE();
                 limpiar();
            } 
        }
        catch (Exception e) 
        {
         JOptionPane.showMessageDialog(null, "Error al modificar");
        }
        
        return p;
    }
    
    void limpiar(){
        this.txtNombre.setText("");
        this.cmbAreaLaboral.setSelectedIndex(0);
    }
    boolean cajasVacias(){
        boolean estado = true;
        if(this.txtNombre.getText().isEmpty()){
            
        }
        return estado;
    }
    void llenarCombo(){
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        List<Arealaboral> lista = ap.findArealaboralEntities();
        
        for (Arealaboral arealaboral : lista) {
            modelo.addElement(new ComboBoxItem(arealaboral.getId(),arealaboral.getNombre()));
            
        }
        this.cmbAreaLaboral.setModel(modelo);
    }
    
    void ModificarTabla()
    {
        int fila = this.jtablaDatos.getSelectedRow();
        codigo = Integer.parseInt(String.valueOf(this.jtablaDatos.getValueAt(fila, 0)));
        txtNombre.setText(String.valueOf(this.jtablaDatos.getValueAt(fila, 1)));
        cmbAreaLaboral.setSelectedIndex(Integer.parseInt(String.valueOf(this.jtablaDatos.getValueAt(fila, 2))));
        
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        cmbAreaLaboral = new javax.swing.JComboBox<>();
        btnInsertar = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtablaDatos = new javax.swing.JTable();

        jLabel1.setText("Profesion");

        jLabel2.setText("Nombre:");

        jLabel3.setText("Area Laboral:");

        cmbAreaLaboral.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbAreaLaboralItemStateChanged(evt);
            }
        });

        btnInsertar.setText("Insertar");
        btnInsertar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnInsertarMouseClicked(evt);
            }
        });

        btnModificar.setText("Modificar");
        btnModificar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnModificarMouseClicked(evt);
            }
        });

        btnEliminar.setText("Eliminar");
        btnEliminar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnEliminarMouseClicked(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnCancelarMouseClicked(evt);
            }
        });

        jtablaDatos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jtablaDatos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtablaDatosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jtablaDatos);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(277, 277, 277)
                                .addComponent(jLabel1))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(133, 133, 133)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel3)
                                            .addComponent(jLabel2))
                                        .addGap(27, 27, 27))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(btnInsertar)
                                        .addGap(36, 36, 36)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(txtNombre)
                                        .addComponent(cmbAreaLaboral, 0, 216, Short.MAX_VALUE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(btnModificar)
                                        .addGap(38, 38, 38)
                                        .addComponent(btnEliminar)
                                        .addGap(28, 28, 28)
                                        .addComponent(btnCancelar)))))
                        .addGap(0, 147, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cmbAreaLaboral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(53, 53, 53)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnInsertar)
                    .addComponent(btnModificar)
                    .addComponent(btnEliminar)
                    .addComponent(btnCancelar))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jtablaDatosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtablaDatosMouseClicked
        ModificarTabla();
    }//GEN-LAST:event_jtablaDatosMouseClicked

    private void btnInsertarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnInsertarMouseClicked
        insertar();
    }//GEN-LAST:event_btnInsertarMouseClicked

    private void btnModificarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnModificarMouseClicked
        modificar();
    }//GEN-LAST:event_btnModificarMouseClicked

    private void btnEliminarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEliminarMouseClicked
        Eliminar();
    }//GEN-LAST:event_btnEliminarMouseClicked

    private void btnCancelarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancelarMouseClicked
       limpiar();
    }//GEN-LAST:event_btnCancelarMouseClicked

    private void cmbAreaLaboralItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbAreaLaboralItemStateChanged
      ComboBoxItem cmb = (ComboBoxItem)this.cmbAreaLaboral.getSelectedItem();
       codigoAreaLaboral = cmb.getValor();
    }//GEN-LAST:event_cmbAreaLaboralItemStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnInsertar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JComboBox<ComboBoxItem> cmbAreaLaboral;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jtablaDatos;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}
