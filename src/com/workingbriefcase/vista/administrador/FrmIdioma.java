package com.workingbriefcase.vista.administrador;

import com.workingbriefcase.persistencia.controladores.IdiomaJpaController;
import com.workingbriefcase.persistencia.controladores.exceptions.IllegalOrphanException;
import com.workingbriefcase.persistencia.controladores.exceptions.NonexistentEntityException;
import com.workingbriefcase.persistencia.entities.Idioma;
import com.workingbriefcase.utilidades.MetodosUtil;
import com.workingbriefcase.utilidades.SwingFormUtilities;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 Nombre de la JInternalFrame: Conexion
 Fecha: /2019
 CopyRight: ASSS
 Version: 1.0
 Autor: Angel Saravia
 * Itca123!
 */
public class FrmIdioma extends javax.swing.JInternalFrame 
{
    Idioma i = new Idioma();
    IdiomaJpaController ijc = new IdiomaJpaController();
    private SwingFormUtilities sfu;
    MetodosUtil mu = new MetodosUtil();
    int codigo = 0;
    
    public FrmIdioma() 
    {
        initComponents();
        cargarDatos();
        this.lblCodigo.setVisible(false);
    }

    private void cargarDatos()
    {
        String[] header = {"Código","Nombre de País","Estado"};
        Object[] obj = new Object[3];
        DefaultTableModel tabla = new DefaultTableModel(null,header);
        List<Idioma> ListaIdiomas;
        try 
        {
            ListaIdiomas = ijc.findIdiomaEntities();
            for (int j = 0; j < ListaIdiomas.size(); j++) 
            {
                i = (Idioma)ListaIdiomas.get(j);
                obj[0]= i.getId();
                obj[1] = i.getNombre();
                obj[2] = i.getBorrado();
                tabla.addRow(obj);
            }
            tbIdiomas.setModel(tabla);
        } 
        catch (Exception e)
        {
            sfu.mostrarMensajeFallo(null,"Error al mostrar los dato: ");
        }
    }
    
    public void limpiar()
    {
        this.lblCodigo.setText("");
        this.txtNombre.setText("");
    }
    
    public void insertar() throws Exception
    {
        i.setId(codigo);
        i.setNombre(this.txtNombre.getText());
        i.setBorrado(true);
        int op = sfu.mostrarConfirmacionSimple(null,"¿Desea ingresar el idioma?");
        if(op == 0)
            ijc.create(i);
        else
            limpiar();
    }
    public void editar() throws Exception
    {
        i.setId(Integer.parseInt(this.lblCodigo.getText()));
        i.setNombre(this.txtNombre.getText());
        i.setBorrado(true);
        int op = sfu.mostrarConfirmacionSimple(null,"¿Desea modificar el idioma?");
        if(op == 0)
            ijc.edit(i);
        else
            limpiar();
    }
    public void eliminar() throws NonexistentEntityException, IllegalOrphanException
    {
        i.setId(Integer.parseInt(this.lblCodigo.getText()));
        int op = sfu.mostrarConfirmacionSimple(null,"¿Desea eliminar el idioma?");
        if(op == 0)
            ijc.destroy(i.getId());
        else
            limpiar();
    }
    public void eventoClic()
    {
        int fila = this.tbIdiomas.getSelectedRow();
        this.lblCodigo.setVisible(true);
        this.lblCodigo.setText(String.valueOf(this.tbIdiomas.getValueAt(fila,0)));
        this.txtNombre.setText(String.valueOf(this.tbIdiomas.getValueAt(fila,1)));
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        lblCodigo = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        btnGuardar = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbIdiomas = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Idiomas");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Codigo:");

        lblCodigo.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lblCodigo.setText("jLabel3");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Idioma:");

        txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreKeyTyped(evt);
            }
        });

        btnGuardar.setText("Insertar");
        btnGuardar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnGuardarMouseClicked(evt);
            }
        });

        btnEditar.setText("Editar");
        btnEditar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnEditarMouseClicked(evt);
            }
        });

        btnEliminar.setText("Eliminar");
        btnEliminar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnEliminarMouseClicked(evt);
            }
        });

        btnLimpiar.setText("Limpiar");
        btnLimpiar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnLimpiarMouseClicked(evt);
            }
        });

        tbIdiomas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbIdiomas.setAutoscrolls(false);
        tbIdiomas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbIdiomasMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbIdiomas);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setText("Listado de idiomas");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                                .addComponent(btnEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnLimpiar))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(134, 134, 134)
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 259, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(43, 43, 43))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(18, 18, 18)
                                .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(lblCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(60, 60, 60))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(lblCodigo))
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGuardar)
                    .addComponent(btnEditar)
                    .addComponent(btnEliminar)
                    .addComponent(btnLimpiar))
                .addGap(25, 25, 25)
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGuardarMouseClicked
        try
        {
            insertar();
            cargarDatos();            
            limpiar();
        }
        catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null,"Error al insertar: " + ex.getMessage());
        }
    }//GEN-LAST:event_btnGuardarMouseClicked

    private void btnEditarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEditarMouseClicked
        try 
        {
            editar();
            limpiar();
            cargarDatos();
        }
        catch (Exception e)
        {
            Logger.getLogger(FrmIdioma.class.getName()).log(Level.SEVERE,null,e);
        }
    }//GEN-LAST:event_btnEditarMouseClicked

    private void btnEliminarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEliminarMouseClicked
        try 
        {
            eliminar();
            cargarDatos();
            limpiar();
        } catch (IllegalOrphanException | NonexistentEntityException e) 
        {
            Logger.getLogger(FrmIdioma.class.getName()).log(Level.SEVERE,null,e);
        }
    }//GEN-LAST:event_btnEliminarMouseClicked

    private void btnLimpiarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLimpiarMouseClicked
        limpiar();
    }//GEN-LAST:event_btnLimpiarMouseClicked

    private void tbIdiomasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbIdiomasMouseClicked
        eventoClic();
    }//GEN-LAST:event_tbIdiomasMouseClicked

    private void txtNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyTyped
        mu.soloLetras(evt);
    }//GEN-LAST:event_txtNombreKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblCodigo;
    private javax.swing.JTable tbIdiomas;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}