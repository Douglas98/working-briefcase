/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.workingbriefcase.vista.administrador;

import com.workingbriefcase.persistencia.controladores.CargoJpaController;
import com.workingbriefcase.persistencia.controladores.EmpleadoasalariadoJpaController;
import com.workingbriefcase.persistencia.controladores.MunicipioJpaController;
import com.workingbriefcase.persistencia.controladores.PaisJpaController;
import com.workingbriefcase.persistencia.controladores.ProfesionJpaController;
import com.workingbriefcase.persistencia.controladores.UsuarioJpaController;
import com.workingbriefcase.persistencia.entities.Cargo;
import com.workingbriefcase.persistencia.entities.Empleadoasalariado;
import com.workingbriefcase.persistencia.entities.Encargado;
import com.workingbriefcase.persistencia.entities.Municipio;
import com.workingbriefcase.persistencia.entities.Pais;
import com.workingbriefcase.persistencia.entities.Profesion;
import com.workingbriefcase.persistencia.entities.Usuario;
import com.workingbriefcase.utilidades.ComboBoxItem;
import com.workingbriefcase.utilidades.SwingFormUtilities;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author douglas
 */
public class FrmEmpleadosPerfil extends javax.swing.JFrame {

    /**
     * Creates new form FrmEmpleadosPerfil
     */
        int codigoMunicipio = 0;
        int codigoProfesion =0;
        int condigoCargo =0;
        int codigoPais = 0;
        int codigoEmpleado =0;
        int codigoUsuario =0;

        Empleadoasalariado e = new Empleadoasalariado();
        Usuario us = new Usuario();
        EmpleadoasalariadoJpaController emj = new EmpleadoasalariadoJpaController();
        MunicipioJpaController munj = new MunicipioJpaController();
        ProfesionJpaController poj = new ProfesionJpaController();
        CargoJpaController carj = new CargoJpaController();
        PaisJpaController paj = new PaisJpaController();
        UsuarioJpaController usj= new UsuarioJpaController();
    
    
           
        Encargado en = new Encargado();
      private SwingFormUtilities sfultil;
        String ruta = null;

       //constructor
    public SwingFormUtilities getSfultil() {
         if(sfultil == null){

            sfultil = SwingFormUtilities.getInstance();

             }
        return sfultil;
    }
    public FrmEmpleadosPerfil() {
        initComponents();
        comboCargo();
        comboMunicipio();
        comboNacionalidad();
        comboProfesion();
        
    }
    public FrmEmpleadosPerfil(int codigoUs) {
        initComponents();
        codigoUsuario = codigoUs;
        llenarDatos();       
        comboCargo();
        comboMunicipio();
        comboNacionalidad();
        comboProfesion();
        
    }
    
        void llenarDatos()
        {
            us=usj.findUsuario(codigoUsuario);
            txtNombre.setText(us.getNombre());
            
        }
           
        public void comboCargo()
        {
          try {
                  DefaultComboBoxModel modelo = new DefaultComboBoxModel();
              modelo.addElement("Seleccionar");
              List<Cargo> lista = carj.findCargoEntities();
              for (Cargo cargo : lista) {
                  modelo.addElement(new ComboBoxItem(cargo.getId(),cargo.getNombre()));
              }
              this.cmbCargoD.setModel(modelo);
          } catch (Exception e) {
              getSfultil().mostrarMensajeFallo(null, "Error al mostra Cargo Desempeniado");
              e.printStackTrace();
          }
        }
        
        
        public void comboMunicipio()
        {
          try {
              DefaultComboBoxModel modelo = new DefaultComboBoxModel();
              modelo.addElement("Seleccionar");
              List<Municipio> lista = munj.findMunicipioEntities();
              for (Municipio mun : lista) {
                  modelo.addElement(new ComboBoxItem(mun.getId(),mun.getNombre()));
              }
              this.cmbMunicipio.setModel(modelo);
          } catch (Exception e) {
              getSfultil().mostrarMensajeFallo(null, "Error al mostra Municipio");
              e.printStackTrace();
          }
        }
        
        public void comboNacionalidad()
        {
          try {
              DefaultComboBoxModel modelo = new DefaultComboBoxModel();
              modelo.addElement("Seleccionar");
              List<Pais> lista = paj.findPaisEntities();
              for (Pais p : lista) {
                  modelo.addElement(new ComboBoxItem(p.getId(),p.getNombre()));
              }
              this.cmbNacionalidad.setModel(modelo);
          } catch (Exception e) {
              getSfultil().mostrarMensajeFallo(null, "Error al mostra Municipio");
              e.printStackTrace();
          }
        }
        
        
        public void comboProfesion()
        {
          try {
              DefaultComboBoxModel modelo = new DefaultComboBoxModel();
              modelo.addElement("Seleccionar");
              List<Profesion> lista = poj.findProfesionEntities();
              for (Profesion p : lista) {
                  modelo.addElement(new ComboBoxItem(p.getId(),p.getNombre()));
              }
              this.cmbProfesion.setModel(modelo);
          } catch (Exception e) {
              getSfultil().mostrarMensajeFallo(null, "Error al mostra Municipio");
              e.printStackTrace();
          }
        }
        
        void llenar() throws FileNotFoundException, IOException
        {
            FileInputStream fi = null;
           e.setNombre(this.txtNombre.getText());
           e.setApellido(this.txtApellido.getText());
           e.setDui(this.txtDui.getText());
           e.setNit(this.txtNit.getText());
           String genero ="";
           if(Masculino.isSelected())
           {
               genero+="Masculino";
           }else{
               genero+="Femenino";
           }
           e.setGenero(genero);
           e.setDireccion(this.txtDireccion.getText());
            String fechaInicios = fecha.getValue().toString();
            Date fechainicio = new Date(fechaInicios);
           e.setFechaNacimiento(fechainicio);
           
               File file = new File(ruta);
               byte[] bArray = new byte[(int) file.length()];
               fi = new FileInputStream(file);
               fi.read(bArray);
               fi.close();
          e.setFoto(bArray);
          e.setSalario((long) Double.parseDouble(this.txtSalario.getText()));
          Cargo c = new Cargo();
          c.setId(condigoCargo);
          e.setCargoDesempennado(c);
          Municipio m = new Municipio();
          m.setId(codigoMunicipio);
          e.setMunicipioActual(m);
          Pais ps = new Pais();
          ps.setId(codigoPais);
          e.setNacionalidad(ps);
          Profesion pro = new Profesion();
          pro.setId(codigoProfesion);
          e.setProfesionEjercida(pro);
            JOptionPane.showMessageDialog(null, "Datos Insertados Correctamente");
            Limpiar();
          emj.create(e);
          
        }
           
           
        void abrirImagen(){
            JFileChooser j = new JFileChooser();
        j.setCurrentDirectory(new File("Imagenes/"));
        int ap = j.showOpenDialog(this);
        
        if(ap == JFileChooser.APPROVE_OPTION){
            ruta = j.getSelectedFile().getAbsolutePath();
            lblimagen.setIcon(new ImageIcon(ruta));
            lblRuta.setText(ruta);
        }
     }
           
        
      void Limpiar()
      {
          txtNombre.setText("");
          txtApellido.setText("");
          txtDui.setText("");
          txtNit.setText("");
          txtDireccion.setText("");
          txtSalario.setText("");
          buttonGroup1.clearSelection();
          cmbCargoD.setSelectedIndex(0);
          cmbMunicipio.setSelectedIndex(0);
          cmbNacionalidad.setSelectedIndex(0);
          cmbProfesion.setSelectedIndex(0);
                  
      }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtApellido = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtDui = new javax.swing.JFormattedTextField();
        jLabel5 = new javax.swing.JLabel();
        txtNit = new javax.swing.JFormattedTextField();
        jLabel6 = new javax.swing.JLabel();
        Masculino = new javax.swing.JRadioButton();
        Femenino = new javax.swing.JRadioButton();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDireccion = new javax.swing.JTextArea();
        jLabel8 = new javax.swing.JLabel();
        lblimagen = new javax.swing.JLabel();
        btnExplorar = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        txtSalario = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        cmbCargoD = new javax.swing.JComboBox<>();
        jLabel12 = new javax.swing.JLabel();
        cmbMunicipio = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        cmbNacionalidad = new javax.swing.JComboBox<>();
        jLabel14 = new javax.swing.JLabel();
        cmbProfesion = new javax.swing.JComboBox<>();
        btnAgregar = new javax.swing.JButton();
        fecha = new javax.swing.JSpinner();
        lblRuta = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Purisa", 2, 24)); // NOI18N
        jLabel1.setText("Complete Los Datos del Empleado");

        jLabel2.setText("Nombre:");

        jLabel3.setText("Apellido:");

        jLabel4.setText("Dui:");

        try {
            txtDui.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("########-#")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel5.setText("Nit:");

        try {
            txtNit.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####-#####-###-#")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel6.setText("Genero:");

        buttonGroup1.add(Masculino);
        Masculino.setText("Masculino");

        buttonGroup1.add(Femenino);
        Femenino.setText("Femenino");

        jLabel7.setText("Direccion:");

        txtDireccion.setColumns(20);
        txtDireccion.setRows(5);
        jScrollPane1.setViewportView(txtDireccion);

        jLabel8.setText("Fecha Nacimiento:");

        lblimagen.setText("Foto:");

        btnExplorar.setText("Explorar");
        btnExplorar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnExplorarMouseClicked(evt);
            }
        });
        btnExplorar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExplorarActionPerformed(evt);
            }
        });

        jLabel10.setText("Salario:");

        jLabel11.setText("Cargo Desempeñado:");

        cmbCargoD.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbCargoDItemStateChanged(evt);
            }
        });

        jLabel12.setText("Municipo Actual:");

        cmbMunicipio.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbMunicipioItemStateChanged(evt);
            }
        });

        jLabel13.setText("Nacionalidad:");

        cmbNacionalidad.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbNacionalidadItemStateChanged(evt);
            }
        });

        jLabel14.setText("Profesion Ejercida:");

        cmbProfesion.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProfesionItemStateChanged(evt);
            }
        });

        btnAgregar.setText("Agregar");
        btnAgregar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAgregarMouseClicked(evt);
            }
        });

        fecha.setModel(new javax.swing.SpinnerDateModel());

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(271, 271, 271)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8)
                            .addComponent(lblimagen, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblRuta))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(52, 52, 52)
                                .addComponent(btnExplorar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtNombre)
                                    .addComponent(txtApellido)
                                    .addComponent(txtDui)
                                    .addComponent(txtNit)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(Masculino)
                                        .addGap(18, 18, 18)
                                        .addComponent(Femenino))
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE)
                                    .addComponent(fecha))))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(157, 157, 157)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel10)
                                        .addGap(80, 80, 80)
                                        .addComponent(txtSalario, javax.swing.GroupLayout.PREFERRED_SIZE, 259, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel11)
                                            .addComponent(jLabel12)
                                            .addComponent(jLabel13)
                                            .addComponent(jLabel14))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(cmbCargoD, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(cmbMunicipio, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(cmbNacionalidad, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(cmbProfesion, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(137, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel1)
                .addGap(45, 45, 45)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(txtSalario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(cmbCargoD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel12)
                                .addGap(3, 3, 3)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(txtDui, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(24, 24, 24)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel5)
                                            .addComponent(txtNit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(30, 30, 30)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel13)
                                            .addComponent(cmbNacionalidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(11, 11, 11)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel6)
                                    .addComponent(Masculino)
                                    .addComponent(Femenino))
                                .addGap(8, 8, 8)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel14)
                                    .addComponent(cmbProfesion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(3, 3, 3)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(29, 29, 29)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel8)
                                    .addComponent(fecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(cmbMunicipio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblimagen, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnExplorar, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblRuta)
                        .addGap(34, 34, 34))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(274, 274, 274)
                        .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAgregarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAgregarMouseClicked
                try {
                    llenar();
                    Limpiar();
                } catch (IOException ex) {
                    Logger.getLogger(FrmEmpleadosPerfil.class.getName()).log(Level.SEVERE, null, ex);
                }
    }//GEN-LAST:event_btnAgregarMouseClicked

    private void btnExplorarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExplorarActionPerformed
        abrirImagen();
    }//GEN-LAST:event_btnExplorarActionPerformed

    private void cmbCargoDItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbCargoDItemStateChanged
         if(cmbCargoD.getSelectedIndex() > 0) 
         {  
            ComboBoxItem cmb = (ComboBoxItem)this.cmbCargoD.getSelectedItem();
            condigoCargo = cmb.getValor();
         }
    }//GEN-LAST:event_cmbCargoDItemStateChanged

    private void cmbMunicipioItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbMunicipioItemStateChanged
        if(cmbMunicipio.getSelectedIndex() > 0) 
         {  
            ComboBoxItem cmb = (ComboBoxItem)this.cmbMunicipio.getSelectedItem();
            codigoMunicipio = cmb.getValor();
         }
    }//GEN-LAST:event_cmbMunicipioItemStateChanged

    private void cmbNacionalidadItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbNacionalidadItemStateChanged
        if(cmbNacionalidad.getSelectedIndex() > 0) 
         {  
            ComboBoxItem cmb = (ComboBoxItem)this.cmbNacionalidad.getSelectedItem();
            codigoPais = cmb.getValor();
         }
    }//GEN-LAST:event_cmbNacionalidadItemStateChanged

    private void cmbProfesionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProfesionItemStateChanged
        if(cmbProfesion.getSelectedIndex() > 0) 
         {  
            ComboBoxItem cmb = (ComboBoxItem)this.cmbProfesion.getSelectedItem();
            codigoProfesion = cmb.getValor();
         }
    }//GEN-LAST:event_cmbProfesionItemStateChanged

    private void btnExplorarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnExplorarMouseClicked
       abrirImagen();
    }//GEN-LAST:event_btnExplorarMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmEmpleadosPerfil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmEmpleadosPerfil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmEmpleadosPerfil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmEmpleadosPerfil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmEmpleadosPerfil().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton Femenino;
    private javax.swing.JRadioButton Masculino;
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnExplorar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<ComboBoxItem> cmbCargoD;
    private javax.swing.JComboBox<ComboBoxItem> cmbMunicipio;
    private javax.swing.JComboBox<ComboBoxItem> cmbNacionalidad;
    private javax.swing.JComboBox<ComboBoxItem> cmbProfesion;
    private javax.swing.JSpinner fecha;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblRuta;
    private javax.swing.JLabel lblimagen;
    private javax.swing.JTextField txtApellido;
    private javax.swing.JTextArea txtDireccion;
    private javax.swing.JFormattedTextField txtDui;
    private javax.swing.JFormattedTextField txtNit;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtSalario;
    // End of variables declaration//GEN-END:variables
}
