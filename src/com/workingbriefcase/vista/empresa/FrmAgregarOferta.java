/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.workingbriefcase.vista.empresa;

import com.workingbriefcase.persistencia.controladores.ArealaboralJpaController;
import com.workingbriefcase.persistencia.controladores.CargoJpaController;
import com.workingbriefcase.persistencia.controladores.DepartamentoJpaController;
import com.workingbriefcase.persistencia.controladores.EmpresaJpaController;
import com.workingbriefcase.persistencia.controladores.MunicipioJpaController;
import com.workingbriefcase.persistencia.controladores.OfertalaboralJpaController;
import com.workingbriefcase.persistencia.controladores.ProfesionJpaController;
import com.workingbriefcase.persistencia.entities.Arealaboral;
import com.workingbriefcase.persistencia.entities.Cargo;
import com.workingbriefcase.persistencia.entities.Departamento;
import com.workingbriefcase.persistencia.entities.Empresa;
import com.workingbriefcase.persistencia.entities.Municipio;
import com.workingbriefcase.persistencia.entities.Ofertalaboral;
import com.workingbriefcase.persistencia.entities.Profesion;
import com.workingbriefcase.utilidades.ComboBoxItem;
import com.workingbriefcase.utilidades.MetodosUtil;
import com.workingbriefcase.utilidades.SwingFormUtilities;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author hola
 */
public class FrmAgregarOferta extends javax.swing.JInternalFrame {
    //controladores
      CargoJpaController cargoControlador = new CargoJpaController();
      ProfesionJpaController profesionControlador = new ProfesionJpaController();
      ArealaboralJpaController areaLaboralControlador = new ArealaboralJpaController();
      DepartamentoJpaController departamentoControlador = new DepartamentoJpaController();
      MunicipioJpaController municipioControlador = new MunicipioJpaController();
      EmpresaJpaController empresaControlador = new EmpresaJpaController();
      OfertalaboralJpaController ofertaControlador = new OfertalaboralJpaController();
      Empresa em = new Empresa();
      //variables globales
      int codigoUs = 0;
      int codigoMunicipio = 0;
      int codigoAreaLaboral = 0;
      int codigoProfesion = 0;
      int codigoDepartamento = 0;
      int codigoCargo = 0;
      int codigoEmpresa = 0;
         Date fecha = new Date();
        final String FORMATO_FECHA = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(FORMATO_FECHA);
        private SwingFormUtilities sfultil;
        MetodosUtil mu = new MetodosUtil();
      
        public SwingFormUtilities getSfultil() {
         if(sfultil == null){

            sfultil = SwingFormUtilities.getInstance();

             }
        return sfultil;
    }
    /**
     * Creates new form FrmAgregarOferta
     */
    public FrmAgregarOferta(int codigoUsuario) {
        initComponents();
        codigoUs = codigoUsuario;
        em = (Empresa)empresaControlador.obtenerEmpresaByIdUsuario(codigoUsuario);
        if(em != null){
        codigoEmpresa = em.getId();
        }
        comboAreaLaboral();
        comboCargo();
        comboDepartamento();
    }
  //llenar combos
    public void comboAreaLaboral() {
        try {
          
            DefaultComboBoxModel modelo = new DefaultComboBoxModel();
            modelo.addElement("Seleccionar");
            List<Arealaboral> lista = areaLaboralControlador.findArealaboralEntities();
            for (Arealaboral arealaboral : lista) {
                modelo.addElement(new ComboBoxItem(arealaboral.getId(), arealaboral.getNombre()));
            }
            this.cmbAreaLaboral.setModel(modelo);
            
        } catch (Exception e) {
            getSfultil().mostrarMensajeFallo(null, "Error al mostra area laboral");
            e.printStackTrace();
        }
    }
 boolean cajasVacias(){
     boolean estado = true;
     if (txtTipoEmpleo.getText().isEmpty()) {
         estado = false;
     }
     if (txtPaga.getText().isEmpty()) {
         estado = false;
     }
     if (txtDireccion.getText().isEmpty()) {
         estado = false;
     }
     if (cmbAreaLaboral.getSelectedIndex() == 0) {
         estado = false;
     }
     if (cmbDepartamento.getSelectedIndex() == 0) {
         estado = false;
     }
     if (cmbCargo.getSelectedIndex() == 0) {
         estado = false;
     }

     if (cmbMunicipio.getSelectedIndex() == 0) {
         estado = false;
     }
     if (cmbProfesion.getSelectedIndex() == 0) {
         estado = false;
     }

     return estado;
 }
    public void comboProfesion(int codigoAreaLaboral) {
        try {
            this.cmbProfesion.removeAllItems();
            DefaultComboBoxModel modelo = new DefaultComboBoxModel();
            modelo.addElement("Seleccionar");
            List<Profesion> lista = profesionControlador.findProfesionByAreaLaboral(codigoAreaLaboral);
            for (Profesion profesion : lista) {
                modelo.addElement(new ComboBoxItem(profesion.getId(), profesion.getNombre()));
            }
            this.cmbProfesion.setModel(modelo);
        } catch (Exception e) {
            getSfultil().mostrarMensajeFallo(null, "Error al mostra profesion");
            e.printStackTrace();
        }
    }

    public void comboCargo() {
        try {
            DefaultComboBoxModel modelo = new DefaultComboBoxModel();
            modelo.addElement("Seleccionar");
            List<Cargo> lista = cargoControlador.findCargoEntities();
            for (Cargo cargo : lista) {
                modelo.addElement(new ComboBoxItem(cargo.getId(), cargo.getNombre()));
            }
            this.cmbCargo.setModel(modelo);
        } catch (Exception e) {
            getSfultil().mostrarMensajeFallo(null, "Error al mostra cargo");
            e.printStackTrace();
        }
    }

    public void comboDepartamento() {
        try {
            DefaultComboBoxModel modelo = new DefaultComboBoxModel();
            modelo.addElement("Seleccionar");
            List<Departamento> lista = departamentoControlador.findDepartamentoEntities();
            for (Departamento departamento : lista) {
                modelo.addElement(new ComboBoxItem(departamento.getId(), departamento.getNombre()));
            }
            this.cmbDepartamento.setModel(modelo);
        } catch (Exception e) {
            getSfultil().mostrarMensajeFallo(null, "Error al mostra departamentos");
            e.printStackTrace();
        }
    }

    public void comboMunicipio(int codigoDepartamento) {
        try {
            DefaultComboBoxModel modelo = new DefaultComboBoxModel();
            this.cmbMunicipio.removeAllItems();
            modelo.addElement("Seleccionar");
            List<Municipio> lista = municipioControlador.findMunicipioEntitiesByDepartamento(codigoDepartamento);
            for (Municipio municipio : lista) {
                modelo.addElement(new ComboBoxItem(municipio.getId(), municipio.getNombre()));
            }
            this.cmbMunicipio.setModel(modelo);
        } catch (Exception e) {
            getSfultil().mostrarMensajeFallo(null, "Error al mostra Municipios");
            e.printStackTrace();
        }
    }
    
    Ofertalaboral llenar(){
            Ofertalaboral of = new Ofertalaboral();
        try {
     
            of.setCorrelativo(Integer.parseInt(jsCorrelativo.getValue().toString()));
            of.setTipoEmpleo(txtTipoEmpleo.getText());
            of.setPaga(Long.decode(txtPaga.getText()));
            of.setHorasMes(Integer.parseInt(jsHorasMes.getValue().toString()));
            of.setDireccion(txtDireccion.getText());
            of.setFechaCreacionOferta(fecha);
            String fechaInicios = jsFechaInicio.getValue().toString();
            Date fechainicio = new Date(fechaInicios);
            of.setFechaInicioOferta(fechainicio);
            String fechaIniciof = jsFechaFin.getValue().toString();
            Date fechafin = new Date(fechaIniciof);
            of.setFechaFinOferta(fechafin);
            of.setActivo(true);
            of.setBorrado(true);
            Empresa em = new Empresa();
            em.setId(codigoEmpresa);
            of.setEmpresaPublicadora(em); 
    
            Profesion pro = new Profesion();
            pro.setId(codigoProfesion);
            of.setProfesionRequerida(pro);
      
            Cargo car = new Cargo();
            car.setId(codigoCargo);
            of.setCargoSolicitado(car);
    
            Municipio mu = new Municipio();
            mu.setId(codigoMunicipio);
            of.setMunicipioEmpleo(mu);
        
        } catch (Exception e) {
            e.printStackTrace();
        }
       
        return of;
    }
    
    void insertar(){
        try {
            Empresa em = new Empresa();
            em = empresaControlador.obtenerEmpresaByIdUsuario(codigoUs);
            if(cajasVacias()){
                if(em != null){
            ofertaControlador.create(llenar());
            getSfultil().mostrarMensajeExito(null, "Se creo la oferta correctamente.");
                }else{
                    getSfultil().mostrarMensajeAdvertencia(null, "No existe un perfil de empresa para crear una oferta.");
                }
            this.setVisible(false);
            }else{
                getSfultil().mostrarMensajeAdvertencia(null, "Aun quedan datos que llenar.");
            }
        } catch (Exception e) {
            getSfultil().mostrarMensajeFallo(null, "Error al insertar. " + e.toString());
            e.printStackTrace();
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jsCorrelativo = new javax.swing.JSpinner();
        jLabel2 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jsFechaFin = new javax.swing.JSpinner();
        jLabel1 = new javax.swing.JLabel();
        jsFechaInicio = new javax.swing.JSpinner();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDireccion = new javax.swing.JTextArea();
        jLabel5 = new javax.swing.JLabel();
        txtPaga = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtTipoEmpleo = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        cmbAreaLaboral = new javax.swing.JComboBox<>();
        cmbProfesion = new javax.swing.JComboBox<>();
        jLabel16 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        cmbCargo = new javax.swing.JComboBox<>();
        jLabel17 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        cmbDepartamento = new javax.swing.JComboBox<>();
        jLabel11 = new javax.swing.JLabel();
        cmbMunicipio = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jsHorasMes = new javax.swing.JSpinner();

        setClosable(true);

        jLabel2.setText("Correlativo:");

        jLabel7.setText("Fecha fin:");

        jLabel3.setText("Tipo Empleado:");

        jsFechaFin.setModel(new javax.swing.SpinnerDateModel());

        jLabel1.setText("Oferta Laboral");

        jsFechaInicio.setModel(new javax.swing.SpinnerDateModel());

        jLabel6.setText("Fecha Incio:");

        txtDireccion.setColumns(20);
        txtDireccion.setRows(5);
        jScrollPane1.setViewportView(txtDireccion);

        jLabel5.setText("Direccion:");

        txtPaga.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPagaKeyTyped(evt);
            }
        });

        jLabel4.setText("Paga:");

        txtTipoEmpleo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTipoEmpleoKeyTyped(evt);
            }
        });

        jLabel15.setText("Area laboral:");

        cmbAreaLaboral.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbAreaLaboralItemStateChanged(evt);
            }
        });

        cmbProfesion.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProfesionItemStateChanged(evt);
            }
        });

        jLabel16.setText("Profesion:");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        cmbCargo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbCargoItemStateChanged(evt);
            }
        });

        jLabel17.setText("Cargo:");

        jLabel10.setText("Departamento:");

        cmbDepartamento.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbDepartamentoItemStateChanged(evt);
            }
        });

        jLabel11.setText("Municipio:");

        cmbMunicipio.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbMunicipioItemStateChanged(evt);
            }
        });

        jButton1.setText("Agregar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(67, 67, 67)
                        .addComponent(jLabel17)))
                .addGap(45, 45, 45)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cmbDepartamento, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmbMunicipio, 0, 244, Short.MAX_VALUE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbCargo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(cmbCargo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(cmbDepartamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(cmbMunicipio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(65, 65, 65)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(193, Short.MAX_VALUE))
        );

        jLabel8.setText("Horas Mes:");

        jsHorasMes.setModel(new javax.swing.SpinnerNumberModel(1, 1, null, 1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel2))
                                .addGap(48, 48, 48)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jsFechaInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jsFechaFin, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtTipoEmpleo, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jsCorrelativo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(14, 14, 14)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel16)
                                    .addComponent(jLabel15))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cmbAreaLaboral, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cmbProfesion, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(44, 44, 44))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtPaga, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel5))
                                .addGap(41, 41, 41)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jScrollPane1)
                                    .addComponent(jsHorasMes))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(26, 26, 26)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(jsCorrelativo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtTipoEmpleo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txtPaga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(jsHorasMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(jsFechaInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(jsFechaFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15)
                            .addComponent(cmbAreaLaboral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(22, 22, 22)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel16)
                            .addComponent(cmbProfesion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmbAreaLaboralItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbAreaLaboralItemStateChanged
        if(cmbAreaLaboral.getSelectedIndex() > 0) {
            ComboBoxItem cmb = (ComboBoxItem)this.cmbAreaLaboral.getSelectedItem();
            codigoAreaLaboral = cmb.getValor();
            comboProfesion(codigoAreaLaboral);
        }
    }//GEN-LAST:event_cmbAreaLaboralItemStateChanged

    private void cmbProfesionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProfesionItemStateChanged
        if(cmbProfesion.getSelectedIndex() > 0) {
            ComboBoxItem cmb = (ComboBoxItem)this.cmbProfesion.getSelectedItem();
            codigoProfesion = cmb.getValor();
        }
    }//GEN-LAST:event_cmbProfesionItemStateChanged

    private void cmbCargoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbCargoItemStateChanged
        // TODO add your handling code here:
        if(cmbCargo.getSelectedIndex() > 0) {
            ComboBoxItem cmb = (ComboBoxItem)this.cmbCargo.getSelectedItem();
            codigoCargo = cmb.getValor();
        }
    }//GEN-LAST:event_cmbCargoItemStateChanged

    private void cmbDepartamentoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbDepartamentoItemStateChanged
        if(cmbDepartamento.getSelectedIndex() > 0) {
            ComboBoxItem cmb = (ComboBoxItem)this.cmbDepartamento.getSelectedItem();
            codigoDepartamento = cmb.getValor();
            comboMunicipio(codigoDepartamento);
        }
    }//GEN-LAST:event_cmbDepartamentoItemStateChanged

    private void cmbMunicipioItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbMunicipioItemStateChanged
        if(cmbMunicipio.getSelectedIndex() > 0) {
            ComboBoxItem cmb = (ComboBoxItem)this.cmbMunicipio.getSelectedItem();
            codigoMunicipio = cmb.getValor();
        }
    }//GEN-LAST:event_cmbMunicipioItemStateChanged

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        insertar();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtTipoEmpleoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTipoEmpleoKeyTyped
       mu.soloLetras(evt);
    }//GEN-LAST:event_txtTipoEmpleoKeyTyped

    private void txtPagaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPagaKeyTyped
        mu.soloDecimales(txtPaga, evt);
    }//GEN-LAST:event_txtPagaKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<ComboBoxItem> cmbAreaLaboral;
    private javax.swing.JComboBox<ComboBoxItem> cmbCargo;
    private javax.swing.JComboBox<ComboBoxItem> cmbDepartamento;
    private javax.swing.JComboBox<ComboBoxItem> cmbMunicipio;
    private javax.swing.JComboBox<ComboBoxItem> cmbProfesion;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSpinner jsCorrelativo;
    private javax.swing.JSpinner jsFechaFin;
    private javax.swing.JSpinner jsFechaInicio;
    private javax.swing.JSpinner jsHorasMes;
    private javax.swing.JTextArea txtDireccion;
    private javax.swing.JTextField txtPaga;
    private javax.swing.JTextField txtTipoEmpleo;
    // End of variables declaration//GEN-END:variables
}
