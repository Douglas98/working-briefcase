/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.workingbriefcase.vista.empresa;

import com.workingbriefcase.persistencia.controladores.CandidatoJpaController;
import com.workingbriefcase.persistencia.controladores.CurriculumJpaController;
import com.workingbriefcase.persistencia.controladores.EmpresaJpaController;
import com.workingbriefcase.persistencia.controladores.OfertalaboralJpaController;
import com.workingbriefcase.persistencia.controladores.PropuestatrabajoCurriculumJpaController;
import com.workingbriefcase.persistencia.controladores.PropuestatrabajoJpaController;
import com.workingbriefcase.persistencia.controladores.UsuarioJpaController;
import com.workingbriefcase.persistencia.entities.Candidato;
import com.workingbriefcase.persistencia.entities.Curriculum;
import com.workingbriefcase.persistencia.entities.Empresa;
import com.workingbriefcase.persistencia.entities.Ofertalaboral;
import com.workingbriefcase.persistencia.entities.Propuestatrabajo;
import com.workingbriefcase.persistencia.entities.PropuestatrabajoCurriculum;
import com.workingbriefcase.persistencia.entities.Usuario;
import com.workingbriefcase.utilidades.ComboBoxItem;
import com.workingbriefcase.utilidades.LlamarReportes;
import com.workingbriefcase.utilidades.MetodosUtil;
import com.workingbriefcase.utilidades.SwingFormUtilities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;

/**
 *
 * @author hola
 */
public class FrmFormularioPropuesta extends javax.swing.JInternalFrame {
    int codigoCandidato = 0;
    int codigoUsuario = 0;
    int ofertaLaboralid = 0;
    int idEmpresa = 0;
    int idCurriculum = 0;
    int idPropuesta = 0;
     private SwingFormUtilities sfultil;
         
    final String FORMATO_FECHA = "dd/MM/yyyy";
    SimpleDateFormat sdf = new SimpleDateFormat(FORMATO_FECHA);
    MetodosUtil mu = new MetodosUtil();
    CandidatoJpaController candidatoControlador = new CandidatoJpaController();
    EmpresaJpaController empresaControlador = new EmpresaJpaController();
    OfertalaboralJpaController ofertaControlador = new OfertalaboralJpaController();
    CurriculumJpaController controladorCurriculum = new CurriculumJpaController();
    PropuestatrabajoJpaController controladorPropuesta = new PropuestatrabajoJpaController();
    PropuestatrabajoCurriculumJpaController controladorPc = new PropuestatrabajoCurriculumJpaController();
    UsuarioJpaController controladorUsuario = new UsuarioJpaController();
    LlamarReportes lr = new LlamarReportes();
        public SwingFormUtilities getSfultil() {
         if(sfultil == null){

            sfultil = SwingFormUtilities.getInstance();

             }
        return sfultil;
    }
    /**
     * Creates new form FrmFormularioPropuesta
     */
    public FrmFormularioPropuesta(int codigoUs, int idCandidato) {
        initComponents();
        codigoCandidato = idCandidato;
        codigoUsuario = codigoUs;
        llenarDatosCandidato();
        idEmpresa();
        comboOferta();
        idCurriculum();
    }
    void idEmpresa(){
        Empresa em = new Empresa();
        em = empresaControlador.obtenerEmpresaByIdUsuario(codigoUsuario);
        idEmpresa = em.getId();
    }
    void idCurriculum(){
        Curriculum cu = new Curriculum();
        String sql = "SELECT c.id FROM curriculum as c where c.candidatoPertenece = " + codigoCandidato + ";";
        idCurriculum = mu.retornarIntByparametro(sql);
    }
    void idPropuesta(){
        idPropuesta = controladorPropuesta.lastID();
    }
    Usuario informacionUsuario(){
        Usuario us = new Usuario();
        us = controladorUsuario.findUsuario(codigoUsuario);
        
        return us;
    }
    Candidato correoCandidato(){
        Candidato car = new Candidato();
        car = candidatoControlador.findCandidato(codigoCandidato);
        return car;
    }
    void enviarMail(){
        try {
            mu.SendMail(informacionUsuario().getCorreoElectronico(), 
                        String.valueOf(txtContrasenia.getPassword()), 
                        correoCandidato().getUsuario().getCorreoElectronico(), 
                        txtAsunto.getText(), 
                        txtMensaje.getText());
        } catch (Exception e) {
            getSfultil().mostrarMensajeFallo(null, "Error al enviar el mensaje");
            e.printStackTrace();
        }
    }
    
    void imprimirCurriculum(){
        try {
            Map parametros = new HashMap();
            parametros.put("id", codigoCandidato);
            lr.ReporteconParamentros(parametros, "reporteCurriculum");
        } catch (Exception e) {
            getSfultil().mostrarMensajeFallo(null, "Error al imprimir el reporte.");
            e.printStackTrace();
        }
    }
    
    void comboOferta(){
        try {
             DefaultComboBoxModel modelo = new DefaultComboBoxModel();
           Iterator lista  =  ofertaControlador.obtenerByEmpresa(idEmpresa).iterator();
           modelo.addElement("Seleccionar");
        while(lista.hasNext()) {
            Object[] obj = (Object[])lista.next();
            modelo.addElement(new ComboBoxItem(Integer.parseInt(obj[0].toString()), obj[1].toString()));
        }
        this.cmbPropuesta.setModel(modelo);
        } catch (Exception e) {
            getSfultil().mostrarMensajeFallo(null, "Error al mostrar ofertas");
            e.printStackTrace();
        }    
    }
    void llenarDatosCandidato(){
        Candidato car = new Candidato();
        car = candidatoControlador.findCandidato(codigoCandidato);
        lblDui.setText(car.getDui());
        lblNIt.setText(car.getNit());
        lblNombre.setText(car.getNombre());
        lblApellido.setText(car.getApellido());
        lblDireccion.setText(car.getDireccion());
        lblMunicipio.setText(car.getMunicipioActual().getNombre());
        lblDepartamento.setText(car.getMunicipioActual().getDepartamentoPertenece().getNombre());
        lblAutoDescription.setText(car.getAutoDescripcion());
        if(car.getFoto() == null){
            lblFoto.setText("No tiene foto.");
        }else{
            lblFoto.setIcon(new ImageIcon(car.getFoto()));
        }
    }

    Propuestatrabajo llenar(){
         Propuestatrabajo p = new Propuestatrabajo();
        try {
            
            p.setCorrelativo(Integer.parseInt(jsCorrelativo.getValue().toString()));
            String fecha1 = jsFechaInicio.getValue().toString();
            Date fechaInicio = new Date(fecha1);
            p.setFechaInicioPropuesta(fechaInicio);
            String fecha2 = jsFechaFin.getValue().toString();
            Date fechaFin = new Date(fecha2);
            p.setFechaFinPropuesta(fechaFin);
            p.setActivo(true);
            p.setBorrado(true);
            Ofertalaboral f = new Ofertalaboral();
                      f.setId(ofertaLaboralid);
            p.setOfertaLaboral(f);
  
        } catch (Exception e) {
            getSfultil().mostrarMensajeFallo(null, "Error al llenarPropuesta" + e.toString());
            e.printStackTrace();
        }
        return p;
    }
    PropuestatrabajoCurriculum llenarPropuestaCu(){
          PropuestatrabajoCurriculum pc = new  PropuestatrabajoCurriculum();
   
        try {
      
            pc.setCorrelativo(Integer.parseInt(jsCorrelativo.getValue().toString()));
            String fecha3 = jsFechaOfresido.getValue().toString();
            Date fechaOfrecido = new Date(fecha3);
            pc.setFechaOfrecido(fechaOfrecido);
            pc.setBorrado(false);
            Propuestatrabajo p = new Propuestatrabajo();
            pc.setPropuestaTrabajo(p);
            idPropuesta();
            p.setId(idPropuesta);
            pc.getPropuestaTrabajo().setId(idPropuesta);

            Curriculum cu = new Curriculum();
            pc.setCurriculum(cu);
            cu.setId(idCurriculum);
            pc.getCurriculum().setId(idCurriculum);
     
        
        } catch (Exception e) {
             getSfultil().mostrarMensajeFallo(null, "Error al llenarPropuesta" + e.toString());
            e.printStackTrace();
        }
    return pc;
    }
    void insertar(){
        try {
            if(getSfultil().mostrarConfirmacionSimple(null, "Desea realizar propuesta?")==0){
                controladorPropuesta.create(llenar());
                controladorPc.create(llenarPropuestaCu());
                enviarMail();
            }
        } catch (Exception e) {
                getSfultil().mostrarMensajeFallo(null, "Error al realizar propuesta." + e.toString());
            e.printStackTrace();
        }
    }
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jsCorrelativo = new javax.swing.JSpinner();
        cmbPropuesta = new javax.swing.JComboBox<>();
        btnAgregar = new javax.swing.JButton();
        jsFechaInicio = new javax.swing.JSpinner();
        jsFechaFin = new javax.swing.JSpinner();
        jsFechaOfresido = new javax.swing.JSpinner();
        lblDui = new javax.swing.JLabel();
        lblNIt = new javax.swing.JLabel();
        lblNombre = new javax.swing.JLabel();
        lblApellido = new javax.swing.JLabel();
        lblDireccion = new javax.swing.JLabel();
        lblMunicipio = new javax.swing.JLabel();
        lblDepartamento = new javax.swing.JLabel();
        lblAutoDescription = new javax.swing.JLabel();
        lblFoto = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtContrasenia = new javax.swing.JPasswordField();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtAsunto = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtMensaje = new javax.swing.JTextArea();
        jLabel14 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        btnImprimirReporte = new javax.swing.JButton();

        setClosable(true);

        jLabel1.setText("Informacion empleado");

        jLabel2.setText("Dui:");

        jLabel3.setText("NIT:");

        jLabel4.setText("nombre:");

        jLabel5.setText("Apellido:");

        jLabel6.setText("Direccion:");

        jLabel7.setText("Municipio:");

        jLabel8.setText("Departamento:");

        jLabel9.setText("AutoDescripcion:");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel10.setText("Realizar propuesta");

        jLabel19.setText("Correlativo:");

        jLabel20.setText("Fecha Inicio:");

        jLabel21.setText("Fecha fin:");

        jLabel22.setText("FechaOfresido:");

        jLabel23.setText("Oferta laboral:");

        cmbPropuesta.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbPropuestaItemStateChanged(evt);
            }
        });

        btnAgregar.setText("Agregar y enviar mensaje");
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });

        jsFechaInicio.setModel(new javax.swing.SpinnerDateModel());

        jsFechaFin.setModel(new javax.swing.SpinnerDateModel());

        jsFechaOfresido.setModel(new javax.swing.SpinnerDateModel());

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel20)
                    .addComponent(jLabel19)
                    .addComponent(jLabel21)
                    .addComponent(jLabel22)
                    .addComponent(jLabel23))
                .addGap(43, 43, 43)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jsCorrelativo)
                    .addComponent(jsFechaInicio)
                    .addComponent(jsFechaFin)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jsFechaOfresido, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbPropuesta, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(91, 91, 91))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(74, 74, 74))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel19)
                            .addComponent(jsCorrelativo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel20)
                            .addComponent(jsFechaInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(33, 33, 33)
                        .addComponent(jLabel21))
                    .addComponent(jsFechaFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(jsFechaOfresido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(cmbPropuesta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70))
        );

        lblDui.setText("Dui:");

        lblNIt.setText("Dui:");

        lblNombre.setText("Dui:");

        lblApellido.setText("Dui:");

        lblDireccion.setText("Dui:");

        lblMunicipio.setText("Dui:");

        lblDepartamento.setText("Dui:");

        lblAutoDescription.setText("Dui:");

        lblFoto.setText("Dui:");

        jLabel11.setText("Enviar Mensaje:");

        jLabel12.setText("Contrasenia:");

        jLabel13.setText("Asunto:");

        txtMensaje.setColumns(20);
        txtMensaje.setRows(5);
        jScrollPane1.setViewportView(txtMensaje);

        jLabel14.setText("Mensaje:");

        btnImprimirReporte.setText("Imprimir Curriculum");
        btnImprimirReporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirReporteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(14, 14, 14)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3))))
                        .addGap(51, 51, 51)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(48, 48, 48)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblDui)
                                    .addComponent(lblNIt)
                                    .addComponent(lblNombre))))
                        .addGap(83, 83, 83)
                        .addComponent(lblFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(42, 42, 42)
                        .addComponent(jLabel5)
                        .addGap(99, 99, 99)
                        .addComponent(lblApellido))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(jLabel6)
                        .addGap(99, 99, 99)
                        .addComponent(lblDireccion))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addComponent(jLabel7)
                        .addGap(99, 99, 99)
                        .addComponent(lblMunicipio))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(jLabel8)
                        .addGap(99, 99, 99)
                        .addComponent(lblDepartamento))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(99, 99, 99)
                        .addComponent(lblAutoDescription)
                        .addGap(118, 118, 118)
                        .addComponent(btnImprimirReporte))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(185, 185, 185)
                        .addComponent(jLabel11))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addComponent(jLabel12)
                        .addGap(18, 18, 18)
                        .addComponent(txtContrasenia, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(72, 72, 72)
                        .addComponent(jLabel13)
                        .addGap(18, 18, 18)
                        .addComponent(txtAsunto, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(67, 67, 67)
                        .addComponent(jLabel14)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 378, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(35, 35, 35)
                                .addComponent(jLabel2)
                                .addGap(12, 12, 12)
                                .addComponent(jLabel3)
                                .addGap(12, 12, 12)
                                .addComponent(jLabel4))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(20, 20, 20)
                                .addComponent(lblDui)
                                .addGap(12, 12, 12)
                                .addComponent(lblNIt)
                                .addGap(12, 12, 12)
                                .addComponent(lblNombre))
                            .addComponent(lblFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(lblApellido))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(lblDireccion))
                        .addGap(22, 22, 22)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(lblMunicipio))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(lblDepartamento))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnImprimirReporte)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9)
                                    .addComponent(lblAutoDescription))))
                        .addGap(6, 6, 6)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(jLabel11)
                        .addGap(12, 12, 12)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(jLabel12))
                            .addComponent(txtContrasenia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(12, 12, 12)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(jLabel13))
                            .addComponent(txtAsunto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addComponent(jLabel14))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 18, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmbPropuestaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbPropuestaItemStateChanged
        if(cmbPropuesta.getSelectedIndex() > 0){
              ComboBoxItem cmb = (ComboBoxItem)cmbPropuesta.getSelectedItem();
        ofertaLaboralid = cmb.getValor();
        }
      
    }//GEN-LAST:event_cmbPropuestaItemStateChanged

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
       insertar();
    }//GEN-LAST:event_btnAgregarActionPerformed

    private void btnImprimirReporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirReporteActionPerformed
        imprimirCurriculum();
    }//GEN-LAST:event_btnImprimirReporteActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnImprimirReporte;
    private javax.swing.JComboBox<String> cmbPropuesta;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSpinner jsCorrelativo;
    private javax.swing.JSpinner jsFechaFin;
    private javax.swing.JSpinner jsFechaInicio;
    private javax.swing.JSpinner jsFechaOfresido;
    private javax.swing.JLabel lblApellido;
    private javax.swing.JLabel lblAutoDescription;
    private javax.swing.JLabel lblDepartamento;
    private javax.swing.JLabel lblDireccion;
    private javax.swing.JLabel lblDui;
    private javax.swing.JLabel lblFoto;
    private javax.swing.JLabel lblMunicipio;
    private javax.swing.JLabel lblNIt;
    private javax.swing.JLabel lblNombre;
    private javax.swing.JTextField txtAsunto;
    private javax.swing.JPasswordField txtContrasenia;
    private javax.swing.JTextArea txtMensaje;
    // End of variables declaration//GEN-END:variables
}
