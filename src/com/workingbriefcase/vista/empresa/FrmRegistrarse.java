/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.workingbriefcase.vista.empresa;
  
import com.workingbriefcase.persistencia.controladores.UsuarioJpaController;
import com.workingbriefcase.persistencia.entities.Rol;
import com.workingbriefcase.persistencia.entities.Usuario;
import com.workingbriefcase.utilidades.MetodosUtil;
import com.workingbriefcase.utilidades.SwingFormUtilities;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

/**
 *  Nombre de la clase: FrmRegistrarse
 *  Version: 1.0
 *  
 * @author Oscar caceres
 */
public class FrmRegistrarse extends javax.swing.JFrame {
        UsuarioJpaController uc = new UsuarioJpaController();
            private SwingFormUtilities sfutil;
      MetodosUtil mu = new MetodosUtil();
            Date fecha = new Date();
        final String FORMATO_FECHA = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(FORMATO_FECHA);
          int codigo = 0;

    public SwingFormUtilities getSfutil() {
        if(sfutil == null){
            sfutil = SwingFormUtilities.getInstance();
        }
        return sfutil;
    }
          
    /**
     * Creates new form FrmRegistrarse
     */
    public FrmRegistrarse() {
        initComponents();
        this.setLocationRelativeTo(null);
    }
        boolean contraseniasIguales(){
        boolean estado = false;
        if(String.valueOf(jpContrasenia.getPassword()).equals(String.valueOf(jpRepetirContrasenia.getPassword()))){
            estado = true;
        }
        return estado;
    }
    boolean cajasVacias(int flag){
        boolean estado = true;
        
        if(txtNombre.getText().isEmpty()){
            estado = false;
        }
        if(txtCorreo.getText().isEmpty()){
            estado = false;
        }
       

            if (String.valueOf(jpContrasenia.getPassword()).isEmpty()) {
                estado = false;
            }
            if (String.valueOf(jpContrasenia.getPassword()).isEmpty()) {
                estado = false;
        
   
        return estado;
         }

        return estado;
    }
    boolean espaciosVacios(){
        boolean estado = true;
            if(txtNombre.getText().contains(" ")){
                estado = false;
            }
            if(String.valueOf(jpRepetirContrasenia.getPassword()).contains(" ")){
                estado = false;
            }
            if(txtCorreo.getText().contains(" ")){
                estado = false;
            }
        return estado;
    }
  boolean correoCorrecto(){
        boolean estado = true;
        // Patrón para validar el email
        Pattern pattern = Pattern
                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
 
        // El email a validar
        String email = txtCorreo.getText();
 
        Matcher mather = pattern.matcher(email);
 
        if (mather.find() != true) {
            estado = false;
        } 
        return estado;
    }
              
    void limpiar(){
        txtNombre.setText("");
        txtCorreo.setText("");
        jpContrasenia.setText("");
        jpRepetirContrasenia.setText("");
    }
                  
     Usuario llenar(){
        Usuario us = new Usuario();
         us.setId(codigo);
         us.setNombre(txtNombre.getText());
         us.setCorreoElectronico(txtCorreo.getText());
         us.setUltimaConexion(fecha);
         us.setBorrado(true);
         us.setContrasenna(String.valueOf(jpRepetirContrasenia.getPassword()));
         Rol r = new Rol();
         r.setId(4);
         us.setRol(r);
        return us;
    }
       void insertar(){
        try {
            if(cajasVacias(1)){
                if(espaciosVacios()){
                if(correoCorrecto()){
                    if(contraseniasIguales()){
                        uc.create(llenar());
                        JOptionPane.showMessageDialog(null, "Datos ingresados correctamente");
                         limpiar();
                         this.hide();
                    }else{
                        JOptionPane.showMessageDialog(null, "Las contrasenias son incorrectas.");
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Su correo no es correcto");
                }
                }else{
                    getSfutil().mostrarMensajeAdvertencia(null, "la informacion no debe llevar espacios en blanco.");
                }
            }else{
                JOptionPane.showMessageDialog(null, "Debe completar todos los campos");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Error al insertar." + e.toString());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtCorreo = new javax.swing.JTextField();
        jpContrasenia = new javax.swing.JPasswordField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jpRepetirContrasenia = new javax.swing.JPasswordField();
        jLabel2 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        btnRegistrarme = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Registrarme");

        jLabel4.setText("Contrasenia:");

        jLabel5.setText("Repetir contrasenia:");

        jLabel2.setText("Nombre:");

        jLabel3.setText("Correo electronico:");

        btnRegistrarme.setText("Registrarme");
        btnRegistrarme.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnRegistrarmeMouseClicked(evt);
            }
        });
        btnRegistrarme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarmeActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        jLabel6.setText("ejemplo:");

        jLabel7.setText("info@programacionextrema.com");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(123, 123, 123)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2))
                        .addGap(47, 47, 47)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(txtNombre, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
                                .addComponent(txtCorreo, javax.swing.GroupLayout.Alignment.LEADING))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jLabel7))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addGap(47, 47, 47)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jpContrasenia, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jpRepetirContrasenia, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnRegistrarme, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(51, 51, 51)
                        .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 113, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(257, 257, 257)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(176, 176, 176)
                        .addComponent(jLabel6)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jpContrasenia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jpRepetirContrasenia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRegistrarme, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRegistrarmeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarmeActionPerformed
       insertar();
    }//GEN-LAST:event_btnRegistrarmeActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
          if(JOptionPane.showConfirmDialog(null, "Desea cancelar la accion?","Cancelar",JOptionPane.YES_NO_OPTION)==0){
                         limpiar();
                         this.dispose();
                    }
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnRegistrarmeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRegistrarmeMouseClicked
        
    }//GEN-LAST:event_btnRegistrarmeMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmRegistrarse.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmRegistrarse.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmRegistrarse.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmRegistrarse.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmRegistrarse().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnRegistrarme;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPasswordField jpContrasenia;
    private javax.swing.JPasswordField jpRepetirContrasenia;
    private javax.swing.JTextField txtCorreo;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}
