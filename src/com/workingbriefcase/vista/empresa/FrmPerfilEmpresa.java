/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.workingbriefcase.vista.empresa;

import com.workingbriefcase.persistencia.controladores.ArealaboralJpaController;
import com.workingbriefcase.persistencia.controladores.CargoJpaController;
import com.workingbriefcase.persistencia.controladores.DepartamentoJpaController;
import com.workingbriefcase.persistencia.controladores.EmpresaJpaController;
import com.workingbriefcase.persistencia.controladores.EncargadoJpaController;
import com.workingbriefcase.persistencia.controladores.MunicipioJpaController;
import com.workingbriefcase.persistencia.controladores.PaisJpaController;
import com.workingbriefcase.persistencia.controladores.ProfesionJpaController;
import com.workingbriefcase.persistencia.controladores.SectorempresarialJpaController;
import com.workingbriefcase.persistencia.entities.Arealaboral;
import com.workingbriefcase.persistencia.entities.Cargo;
import com.workingbriefcase.persistencia.entities.Departamento;
import com.workingbriefcase.persistencia.entities.Empresa;
import com.workingbriefcase.persistencia.entities.Encargado;
import com.workingbriefcase.persistencia.entities.Municipio;
import com.workingbriefcase.persistencia.entities.Pais;
import com.workingbriefcase.persistencia.entities.Profesion;
import com.workingbriefcase.persistencia.entities.Sectorempresarial;
import com.workingbriefcase.persistencia.entities.Usuario;
import com.workingbriefcase.utilidades.ComboBoxItem;
import com.workingbriefcase.utilidades.MetodosUtil;
import com.workingbriefcase.utilidades.SwingFormUtilities;
import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;

/**
 *
 * @author hola
 */
public class FrmPerfilEmpresa extends javax.swing.JInternalFrame {
        //Controladores
        // <editor-fold defaultstate="collapsed" desc="Controladores">  
          EncargadoJpaController encargadoControlador = new EncargadoJpaController();
    DepartamentoJpaController departamentoControlador = new DepartamentoJpaController();
    MunicipioJpaController municipioControlador = new MunicipioJpaController();
    EmpresaJpaController empresaControlador = new EmpresaJpaController();
    PaisJpaController nacionalidadControlador = new PaisJpaController();
    CargoJpaController cargoControlador = new CargoJpaController();
    ProfesionJpaController profesionControlador = new ProfesionJpaController();
    ArealaboralJpaController areaLaboralControlador = new ArealaboralJpaController();
    SectorempresarialJpaController sectorEmpresarialcontrolador = new SectorempresarialJpaController();

    // </editor-fold>  
      
         // <editor-fold defaultstate="collapsed" desc="Variables globales y constructor">  
      //variables globales
      int codigoUs = 0;
      int codigoMunicipio = 0;
      int codigoNacionalidadEmpresa = 0;
      int codigoSectorEmpresarial = 0;
      int codigoAreaLaboral = 0;
      int codigoProfesion = 0;
      int codigoDepartamento = 0;
      int codigoNacionalidadEncargado = 0;
      int codigoCargo = 0;
      MetodosUtil mu = new MetodosUtil();
     
      private SwingFormUtilities sfultil;
        String ruta = null;

       //constructor
    public SwingFormUtilities getSfultil() {
         if(sfultil == null){

            sfultil = SwingFormUtilities.getInstance();

             }
        return sfultil;
    }
    // </editor-fold>  
      
    /**
     * Creates new form FrmPerfilEmpresa
     */
    public FrmPerfilEmpresa(int codigoUsuario) {
        initComponents();
        codigoUs = codigoUsuario;
        comboDepartamento();
        comboNacionalidadEmpresa();
        comboSectorEmpresarial();
        comboAreaLaboral();
        comboCargo();
        comboNacionalidad();
        
    }
       
     // <editor-fold defaultstate="collapsed" desc="Codigo para llenar los combos">  
       
      public void comboDepartamento(){
          try {
                  DefaultComboBoxModel modelo = new DefaultComboBoxModel();
              modelo.addElement("Seleccionar");
              List<Departamento> lista = departamentoControlador.findDepartamentoEntities();
              for (Departamento departamento : lista) {
                  modelo.addElement(new ComboBoxItem(departamento.getId(),departamento.getNombre()));
              }
              this.cmbDepartamento.setModel(modelo);
          } catch (Exception e) {
              getSfultil().mostrarMensajeFallo(null, "Error al mostra departamentos");
              e.printStackTrace();
          }
      }
      public void comboMunicipio(int codigoDepartamento){
          try {
                  DefaultComboBoxModel modelo = new DefaultComboBoxModel();
              this.cmbMunicipio.removeAllItems();
              modelo.addElement("Seleccionar");
              List<Municipio> lista = municipioControlador.findMunicipioEntitiesByDepartamento(codigoDepartamento);
              for (Municipio municipio : lista) {
                  modelo.addElement(new ComboBoxItem(municipio.getId(),municipio.getNombre()));
              }
              this.cmbMunicipio.setModel(modelo);
          } catch (Exception e) {
              getSfultil().mostrarMensajeFallo(null, "Error al mostra Municipios");
              e.printStackTrace();
          }
      }
      public void comboNacionalidadEmpresa(){
              try {
                 DefaultComboBoxModel modelo = new DefaultComboBoxModel();
              modelo.addElement("Seleccionar");
              List<Pais> lista = nacionalidadControlador.findPaisEntities();
              for (Pais pais : lista) {
                  modelo.addElement(new ComboBoxItem(pais.getId(),pais.getNombre()));
              }
              this.cmbNacionalidadEmpresa.setModel(modelo);
          } catch (Exception e) {
              getSfultil().mostrarMensajeFallo(null, "Error al mostra pais");
              e.printStackTrace();
          }
      }
      public void comboSectorEmpresarial(){
                try {
                 DefaultComboBoxModel modelo = new DefaultComboBoxModel();
              modelo.addElement("Seleccionar");
              List<Sectorempresarial> lista = sectorEmpresarialcontrolador.findSectorempresarialEntities();
              for (Sectorempresarial sectorempresarial : lista) {
                  modelo.addElement(new ComboBoxItem(sectorempresarial.getId(),sectorempresarial.getNombre()));
              }
              this.cmbSectorEmpresarial.setModel(modelo);
          } catch (Exception e) {
              getSfultil().mostrarMensajeFallo(null, "Error al mostra Sector empresarial");
              e.printStackTrace();
          }
      }
      public void comboAreaLaboral(){
          try{
            DefaultComboBoxModel modelo = new DefaultComboBoxModel();
              modelo.addElement("Seleccionar");
              List<Arealaboral> lista = areaLaboralControlador.findArealaboralEntities();
              for (Arealaboral arealaboral : lista) {
                  modelo.addElement(new ComboBoxItem(arealaboral.getId(),arealaboral.getNombre()));
              }
              this.cmbAreaLaboral.setModel(modelo);
          } catch (Exception e) {
              getSfultil().mostrarMensajeFallo(null, "Error al mostra area laboral");
              e.printStackTrace();
          }
      }
      public void comboProfesion(int codigoAreaLaboral){
            try{
                this.cmbProfesion.removeAllItems();
            DefaultComboBoxModel modelo = new DefaultComboBoxModel();
              modelo.addElement("Seleccionar");
              List<Profesion> lista = profesionControlador.findProfesionByAreaLaboral(codigoAreaLaboral);
              for (Profesion profesion : lista) {
                  modelo.addElement(new ComboBoxItem(profesion.getId(),profesion.getNombre()));
              }
              this.cmbProfesion.setModel(modelo);
          } catch (Exception e) {
              getSfultil().mostrarMensajeFallo(null, "Error al mostra profesion");
              e.printStackTrace();
          }
      }
      public void comboCargo(){
     try{
            DefaultComboBoxModel modelo = new DefaultComboBoxModel();
              modelo.addElement("Seleccionar");
              List<Cargo> lista = cargoControlador.findCargoEntities();
              for (Cargo cargo : lista) {
                  modelo.addElement(new ComboBoxItem(cargo.getId(),cargo.getNombre()));
              }
              this.cmbCargo.setModel(modelo);
          } catch (Exception e) {
              getSfultil().mostrarMensajeFallo(null, "Error al mostra cargo");
              e.printStackTrace();
          }
    }
      public void comboNacionalidad(){
              try {
                 DefaultComboBoxModel modelo = new DefaultComboBoxModel();
              modelo.addElement("Seleccionar");
              List<Pais> lista = nacionalidadControlador.findPaisEntities();
              for (Pais pais : lista) {
                  modelo.addElement(new ComboBoxItem(pais.getId(),pais.getNombre()));
              }
              this.cmbNacionalidad.setModel(modelo);
          } catch (Exception e) {
              getSfultil().mostrarMensajeFallo(null, "Error al mostra pais encargado");
              e.printStackTrace();
          }
      }
    // </editor-fold>  
 
     // <editor-fold defaultstate="collapsed" desc="Metodos personalizados">  
void abrirImagen(){
            JFileChooser j = new JFileChooser();
        j.setCurrentDirectory(new File("Imagenes/"));
        int ap = j.showOpenDialog(this);
        
        if(ap == JFileChooser.APPROVE_OPTION){
            ruta = j.getSelectedFile().getAbsolutePath();
            lblimagen.setIcon(new ImageIcon(ruta));
            lblRuta.setText(ruta);
        }
     }
     Encargado llenarEncargado(){
         Encargado en = new Encargado();
         en.setNombre(txtNombre.getText());
         en.setApellido(txtApellido.getText());
         Profesion pe = new Profesion();
         pe.setId(codigoProfesion);
         en.setProfesionEjercida(pe);
         Cargo car = new Cargo();
         car.setId(codigoCargo);
         en.setCargoDesepennado(car);
         Pais p = new Pais();
         p.setId(codigoNacionalidadEncargado);
         en.setNacionalidad(p);
         return en;
     }
     Empresa llenar(){
                Empresa em = new Empresa();
                FileInputStream fi = null;
              int codigoEncargado = 0;
            try {
            
                em.setNit(txtNit.getText());
                em.setNombre(txtNombreEmpresa.getText());
                em.setRazonSocial(txtRazonSocial.getText());
                em.setDireccion(txtDireccion.getText());
                em.setTipologia(txtTipologia.getText());
                em.setDireccionPaginaWeb(txtPaginaWeb.getText());
                File file = new File(ruta);
                byte[] bArray = new byte[(int) file.length()];
                fi = new FileInputStream(file);
                fi.read(bArray);
                fi.close();
                em.setFoto(bArray);
                em.setMaxNumeroTrabajadores(Integer.parseInt(jsMax.getValue().toString()));
                em.setMinNumeroTrabajadores(Integer.parseInt(this.jsMin.getValue().toString()));
                Municipio mu = new Municipio();
                mu.setId(codigoMunicipio);
                em.setMunicipioActual(mu);
                Sectorempresarial sec = new Sectorempresarial();
                sec.setId(codigoSectorEmpresarial);
                em.setSectorEmpresarial(sec);
                codigoEncargado = encargadoControlador.lastID();
                Encargado en = new Encargado();
                en.setId(codigoEncargado);
                em.setEncargado(en);
                Pais p = new Pais();
                p.setId(codigoNacionalidadEmpresa);
                em.setNacionalidad(p);
                Usuario us = new Usuario();
                us.setId(codigoUs);
                em.setUsuario(us);
         } catch (Exception e) {
                eliminarEncargado(codigoEncargado);
              getSfultil().mostrarMensajeFallo(null, "Error al llenar " + e.toString());
              e.printStackTrace();
         }
   
         return em;
     }
     void eliminarEncargado(int codigoEncargado){
         try {
             encargadoControlador.destroy(codigoEncargado);
         } catch (Exception e) {
             e.printStackTrace();
         }
     }    
     void insertar(){
             Empresa em = new Empresa();
                        em = empresaControlador.obtenerEmpresaByIdUsuario(codigoUs);
         try {
             if(cajasVacias()){
             
                    if(em != null){
                     encargadoControlador.create(llenarEncargado());
             empresaControlador.create(llenar());
             getSfultil().mostrarMensajeInformativo(null,"Datos ingresados correctamente.","Exito");
                     this.setVisible(false);
                    }else{
                        getSfultil().mostrarMensajeAdvertencia(null, "Este usuario ya contiene un perfil de empresa.");
                    }
             }else{
                 getSfultil().mostrarMensajeAdvertencia(null, "Aun quedan datos por completar");
             }
         
         } catch (Exception e) {
             getSfultil().mostrarMensajeFallo(null, "Error al ingresar los datos." + e.toString());
             e.printStackTrace();
         }
     }
     boolean cajasVacias(){
         boolean estado = true;
         if(txtNombre.getText().isEmpty()){
             estado = false;
         }
         if(txtNit.getText().isEmpty()){
             estado = false;
         }
         if(txtRazonSocial.getText().isEmpty()){
             estado = false;
         }
         if(txtDireccion.getText().isEmpty()){
             estado = false;
         }
         if(txtPaginaWeb.getText().isEmpty()){
             estado = false;
         }
         if(txtTipologia.getText().isEmpty()){
             estado = false;
         }
         if(cmbMunicipio.getSelectedIndex() == 0){
             estado = false;
         }
         if(cmbDepartamento.getSelectedIndex() == 0){
             estado = false;
         }
         if(cmbNacionalidadEmpresa.getSelectedIndex() == 0){
             estado = false;
         }
         if(cmbSectorEmpresarial.getSelectedIndex() == 0){
             estado = false;
         }
         if(cmbAreaLaboral.getSelectedIndex() == 0){
             estado = false;
         }
         if(cmbProfesion.getSelectedIndex() == 0){
             estado = false;
         }
         if(cmbCargo.getSelectedIndex() == 0){
             estado = false;
         }
         if(cmbNacionalidadEmpresa.getSelectedIndex() == 0){
             estado = false;
         }
         if(txtNombre.getText().isEmpty()){
             estado = false;
         }
         if(txtApellido.getText().isEmpty()){
             estado = false;
         }
         return estado;
     }
// </editor-fold> 
     
      
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtNit = new javax.swing.JFormattedTextField();
        jLabel3 = new javax.swing.JLabel();
        txtRazonSocial = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDireccion = new javax.swing.JTextArea();
        lblimagen = new javax.swing.JLabel();
        btnAgregarFoto = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        txtPaginaWeb = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtTipologia = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jsMin = new javax.swing.JSpinner();
        jLabel9 = new javax.swing.JLabel();
        jsMax = new javax.swing.JSpinner();
        jLabel10 = new javax.swing.JLabel();
        cmbDepartamento = new javax.swing.JComboBox<>();
        jLabel11 = new javax.swing.JLabel();
        cmbMunicipio = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txtApellido = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        cmbAreaLaboral = new javax.swing.JComboBox<>();
        jLabel16 = new javax.swing.JLabel();
        cmbProfesion = new javax.swing.JComboBox<>();
        jLabel17 = new javax.swing.JLabel();
        cmbCargo = new javax.swing.JComboBox<>();
        jLabel18 = new javax.swing.JLabel();
        cmbNacionalidad = new javax.swing.JComboBox<>();
        jLabel19 = new javax.swing.JLabel();
        cmbNacionalidadEmpresa = new javax.swing.JComboBox<>();
        jLabel20 = new javax.swing.JLabel();
        cmbSectorEmpresarial = new javax.swing.JComboBox<>();
        btnAgregar = new javax.swing.JButton();
        lblRuta = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtNombreEmpresa = new javax.swing.JTextField();

        setClosable(true);

        jScrollPane2.setAutoscrolls(true);

        jPanel1.setToolTipText("");

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jLabel1.setText("Empresa");

        jLabel2.setText("Nit:");

        try {
            txtNit.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####-######-###-#")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel3.setText("Razon social:");

        jLabel4.setText("Direccion:");

        txtDireccion.setColumns(20);
        txtDireccion.setRows(5);
        jScrollPane1.setViewportView(txtDireccion);

        lblimagen.setText("Foto:");

        btnAgregarFoto.setText("Explorar...");
        btnAgregarFoto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarFotoActionPerformed(evt);
            }
        });

        jLabel6.setText("pagina web:");

        jLabel7.setText("Tipologia:");

        txtTipologia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTipologiaKeyTyped(evt);
            }
        });

        jLabel8.setText("Min numero trabajadores:");

        jsMin.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10000, 1));

        jLabel9.setText("Max numero trabajadores:");

        jsMax.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10000, 1));

        jLabel10.setText("Departamento:");

        cmbDepartamento.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbDepartamentoItemStateChanged(evt);
            }
        });

        jLabel11.setText("Municipio:");

        cmbMunicipio.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbMunicipioItemStateChanged(evt);
            }
        });

        jPanel2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel12.setText("Encargado");

        txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreKeyTyped(evt);
            }
        });

        jLabel13.setText("Nombre:");

        jLabel14.setText("Apellido:");

        txtApellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtApellidoKeyTyped(evt);
            }
        });

        jLabel15.setText("Area laboral:");

        cmbAreaLaboral.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbAreaLaboralItemStateChanged(evt);
            }
        });

        jLabel16.setText("Profesion:");

        cmbProfesion.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProfesionItemStateChanged(evt);
            }
        });

        jLabel17.setText("Cargo:");

        cmbCargo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbCargoItemStateChanged(evt);
            }
        });

        jLabel18.setText("Nacionalidad:");

        cmbNacionalidad.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbNacionalidadItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(190, 190, 190)
                        .addComponent(jLabel12))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(58, 58, 58)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel13)
                                .addComponent(jLabel14))
                            .addComponent(jLabel18)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel17)
                                .addGap(16, 16, 16))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel16)
                                .addComponent(jLabel15)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtApellido)
                                .addComponent(txtNombre)
                                .addComponent(cmbAreaLaboral, 0, 202, Short.MAX_VALUE)
                                .addComponent(cmbProfesion, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cmbNacionalidad, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(cmbCargo, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(88, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel12)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(cmbAreaLaboral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(cmbProfesion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel18)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(cmbNacionalidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel17)
                        .addComponent(cmbCargo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel19.setText("Nacionalidad Empresa:");

        cmbNacionalidadEmpresa.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbNacionalidadEmpresaItemStateChanged(evt);
            }
        });

        jLabel20.setText("SectorEmpresarial:");

        cmbSectorEmpresarial.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbSectorEmpresarialItemStateChanged(evt);
            }
        });

        btnAgregar.setText("Agregar");
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });

        jLabel5.setText("Nombre:");

        txtNombreEmpresa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreEmpresaKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel20)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(143, 143, 143)
                                .addComponent(jLabel19)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbNacionalidadEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbSectorEmpresarial, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(78, 78, 78))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(17, 17, 17)
                                .addComponent(lblimagen, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(lblRuta))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(66, 66, 66)
                                        .addComponent(btnAgregarFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel10)
                                    .addComponent(jLabel11))
                                .addGap(62, 62, 62)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtPaginaWeb)
                                    .addComponent(txtTipologia)
                                    .addComponent(jsMin, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jsMax, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cmbDepartamento, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cmbMunicipio, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(50, 50, 50)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel3)
                                        .addComponent(jLabel4))
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel5))
                                .addGap(65, 65, 65)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(txtNit)
                                        .addComponent(txtRazonSocial)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(txtNombreEmpresa, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 59, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(150, 150, 150))))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel19)
                            .addComponent(cmbNacionalidadEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel20)
                            .addComponent(cmbSectorEmpresarial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(23, 23, 23))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(txtNombreEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtRazonSocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(29, 29, 29)
                                .addComponent(jLabel4))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(20, 20, 20)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblimagen, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(btnAgregarFoto)
                                .addGap(29, 29, 29)
                                .addComponent(lblRuta)
                                .addGap(20, 20, 20)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(txtPaginaWeb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(txtTipologia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(jsMin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jsMax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(cmbDepartamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(cmbMunicipio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(46, Short.MAX_VALUE))
        );

        jScrollPane2.setViewportView(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmbMunicipioItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbMunicipioItemStateChanged
        if(cmbMunicipio.getSelectedIndex() > 0) { 
        ComboBoxItem cmb = (ComboBoxItem)this.cmbMunicipio.getSelectedItem();
            codigoMunicipio = cmb.getValor();
    }

    }//GEN-LAST:event_cmbMunicipioItemStateChanged

    private void cmbDepartamentoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbDepartamentoItemStateChanged
          if(cmbDepartamento.getSelectedIndex() > 0) { 
        ComboBoxItem cmb = (ComboBoxItem)this.cmbDepartamento.getSelectedItem();
        codigoDepartamento = cmb.getValor();
        comboMunicipio(codigoDepartamento);
          }
    }//GEN-LAST:event_cmbDepartamentoItemStateChanged

    private void cmbAreaLaboralItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbAreaLaboralItemStateChanged
        if(cmbAreaLaboral.getSelectedIndex() > 0) {  
        ComboBoxItem cmb = (ComboBoxItem)this.cmbAreaLaboral.getSelectedItem();
        codigoAreaLaboral = cmb.getValor();
        comboProfesion(codigoAreaLaboral);
        }
    }//GEN-LAST:event_cmbAreaLaboralItemStateChanged

    private void cmbNacionalidadEmpresaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbNacionalidadEmpresaItemStateChanged
           if(cmbNacionalidadEmpresa.getSelectedIndex() > 0) {  
        ComboBoxItem cmb = (ComboBoxItem)this.cmbNacionalidadEmpresa.getSelectedItem();
        codigoNacionalidadEmpresa = cmb.getValor();
        }
    }//GEN-LAST:event_cmbNacionalidadEmpresaItemStateChanged

    private void cmbSectorEmpresarialItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbSectorEmpresarialItemStateChanged
         if(cmbSectorEmpresarial.getSelectedIndex() > 0) {  
        ComboBoxItem cmb = (ComboBoxItem)this.cmbSectorEmpresarial.getSelectedItem();
        codigoSectorEmpresarial = cmb.getValor();
        }
    }//GEN-LAST:event_cmbSectorEmpresarialItemStateChanged

    private void cmbProfesionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProfesionItemStateChanged
           if(cmbProfesion.getSelectedIndex() > 0) {  
        ComboBoxItem cmb = (ComboBoxItem)this.cmbProfesion.getSelectedItem();
        codigoProfesion = cmb.getValor();
        }
    }//GEN-LAST:event_cmbProfesionItemStateChanged

    private void cmbCargoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbCargoItemStateChanged
        // TODO add your handling code here:
           if(cmbCargo.getSelectedIndex() > 0) {  
        ComboBoxItem cmb = (ComboBoxItem)this.cmbCargo.getSelectedItem();
        codigoCargo = cmb.getValor();
        }
    }//GEN-LAST:event_cmbCargoItemStateChanged

    private void cmbNacionalidadItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbNacionalidadItemStateChanged
        // TODO add your handling code here:
            if(cmbNacionalidad.getSelectedIndex() > 0) {  
        ComboBoxItem cmb = (ComboBoxItem)this.cmbNacionalidad.getSelectedItem();
        codigoNacionalidadEncargado = cmb.getValor();
        }
    }//GEN-LAST:event_cmbNacionalidadItemStateChanged

    private void btnAgregarFotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarFotoActionPerformed
        abrirImagen();
    }//GEN-LAST:event_btnAgregarFotoActionPerformed

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
        insertar();

    }//GEN-LAST:event_btnAgregarActionPerformed

    private void txtNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyTyped
        mu.soloLetras(evt);
    }//GEN-LAST:event_txtNombreKeyTyped

    private void txtApellidoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtApellidoKeyTyped
       mu.soloLetras(evt);
    }//GEN-LAST:event_txtApellidoKeyTyped

    private void txtTipologiaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTipologiaKeyTyped
        mu.soloLetras(evt);
    }//GEN-LAST:event_txtTipologiaKeyTyped

    private void txtNombreEmpresaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreEmpresaKeyTyped
         
    }//GEN-LAST:event_txtNombreEmpresaKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnAgregarFoto;
    private javax.swing.JComboBox<ComboBoxItem> cmbAreaLaboral;
    private javax.swing.JComboBox<ComboBoxItem> cmbCargo;
    private javax.swing.JComboBox<ComboBoxItem> cmbDepartamento;
    private javax.swing.JComboBox<ComboBoxItem> cmbMunicipio;
    private javax.swing.JComboBox<ComboBoxItem> cmbNacionalidad;
    private javax.swing.JComboBox<ComboBoxItem> cmbNacionalidadEmpresa;
    private javax.swing.JComboBox<ComboBoxItem> cmbProfesion;
    private javax.swing.JComboBox<ComboBoxItem> cmbSectorEmpresarial;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSpinner jsMax;
    private javax.swing.JSpinner jsMin;
    private javax.swing.JLabel lblRuta;
    private javax.swing.JLabel lblimagen;
    private javax.swing.JTextField txtApellido;
    private javax.swing.JTextArea txtDireccion;
    private javax.swing.JFormattedTextField txtNit;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtNombreEmpresa;
    private javax.swing.JTextField txtPaginaWeb;
    private javax.swing.JTextField txtRazonSocial;
    private javax.swing.JTextField txtTipologia;
    // End of variables declaration//GEN-END:variables
}
