package com.workingbriefcase.utilidades;

/**
 * Nombre de la clase: StringAndCharUtilities
 * Fecha Creación: 10-16-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class StringAndCharUtilities {
    //<editor-fold defaultstate="collapsed" desc="variables generales">
    private static StringAndCharUtilities instance;
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="constructores">
    private StringAndCharUtilities(){
        
    }
    
    public static StringAndCharUtilities getInstance(){
        if(instance == null){
            instance = new StringAndCharUtilities();
        }
        
        return instance;
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="fucniones char">
    public boolean esLetraOrEspacio(Character c){
        return Character.isLetter(c) || Character.isSpaceChar(0) ? true: false;
    }
    
    public boolean esDigitoOrPunto(Character c){
        return Character.isDigit(c) || c == '.' ? true: false;
    }
    
    public boolean esDigitoSinPunto(Character c){
        return Character.isDigit(c) ? true: false;
    }
    
    
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones string">
    /**
     * Da formato al texto para que este encaje en el ancho especificado
     * @param ancho ancho en el que el texto debe encajas
     * @param texto texto a formatear
     * @return el texto formateado para encajar
     */
    public String formtearTextoParaEncajar(int ancho, String texto){
        return String.format("<html><div style = \"width:%dpx;\">%s</div></html>", ancho, texto);
    }
//</editor-fold>
}
