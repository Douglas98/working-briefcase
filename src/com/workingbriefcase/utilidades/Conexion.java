package com.workingbriefcase.utilidades;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 * Nombre de la clase: Conexion
 * Fecha: /2019
 * CopyRight: ASSS
 * Version: 1.0
 * Autor: Angel Saravia
 */

public class Conexion 
{
    private Connection con;

    public Connection getCon() {
        return con;
    }
    
    public void conectar()
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/vacantesprofesionales?zeroDateTimeBehavior=convertToNull", "root","");
        }
        catch (ClassNotFoundException | SQLException e)
        {
            JOptionPane.showMessageDialog(null,"Error al conectar: " + e);
        }
    }
    
    public void desconectar()
    {
        try 
        {
            if(con.isClosed() == false)
            {
                if(con != null)
                {
                    con.close();
                }
            }
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(null,"Error al desconectar: " + e);
        }
    }
}
