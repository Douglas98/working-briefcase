package com.workingbriefcase.utilidades;

/**
 * Nombre de la clase: JTableCell
 * Fecha Creación: 11-08-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class JTableCell<T> {
    private T valor;
    private String muestra;
    
    public JTableCell(){
        
    }

    public JTableCell(T valor, String muestra) {
        this.valor = valor;
        this.muestra = muestra;
    }
    
    public T getValor() {
        return valor;
    }

    public void setValor(T valor) {
        this.valor = valor;
    }

    public String getMuestra() {
        return muestra;
    }

    public void setMuestra(String muestra) {
        this.muestra = muestra;
    }

    @Override
    public String toString() {
        return getMuestra();
    }
    
    
}
