package com.workingbriefcase.utilidades;

import java.util.Map;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

/**
 * Nombre de la clase: LlamarReportes
 * Fecha: /2019
 * CopyRight: ASSS
 * Version: 1.0
 * Autor: Angel Saravia
 */

public class LlamarReportes extends Conexion
{
    JasperReport reportes;
    public void ReporteSinParamentros(String name)
    {
        try 
        {
            this.conectar();
            reportes = JasperCompileManager.compileReport("src/com/workingbriefcase/reportes/" + name + ".jrxml");
            JasperPrint jp = JasperFillManager.fillReport(reportes,null,this.getCon());
            JasperViewer.viewReport(jp,false);
        }
        catch (JRException e)
        {
            JOptionPane.showMessageDialog(null,"Error al mostrar reporte: " + e.getMessage());
        }
        finally
        {
            this.desconectar();
        }
    }
    
    public void ReporteconParamentros(Map parametros,String name)
    {
        try 
        {
            this.conectar();
            reportes = JasperCompileManager.compileReport("src/com/workingbriefcase/reportes/" + name + ".jrxml");
            JasperPrint jp = JasperFillManager.fillReport(reportes,parametros,this.getCon());
            JasperViewer.viewReport(jp,false);
        }
        catch (JRException e)
        {
            JOptionPane.showMessageDialog(null,"Error al mostrar reporte: " + e.getMessage());
        }
        finally
        {
            this.desconectar();
        }
    }
}