
package com.workingbriefcase.utilidades;

import javax.swing.AbstractSpinnerModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.text.JTextComponent;

/**
 * Nombre de la clase: SwingFormUtilities
 * Fecha de creación: 5 oct. 2019
 * Version: 1.0
 * CopyRight: ITCA-FEPADE  
 * @author Daniel Ángel
 */
public class SwingFormUtilities {
    //<editor-fold defaultstate="collapsed" desc="variables globales">
    private static SwingFormUtilities instance;
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="constructores">
    private SwingFormUtilities() {
    }
    
    public static SwingFormUtilities getInstance(){
        if(instance == null){
            instance = new SwingFormUtilities();
        }
        
        return instance;
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones para controles generales">
    /**
     * establece el atributo "enabled" de un arreglo de jComponents
     * @param cntrls arreglo de controles a los que se les será establecido "enabled"
     * @param estados estados a los que los atrubutos "enabled" de los contrles serán establecidos (debe ser del mismo tamaño que el arreglo de controles)
     */
    public void establecerControlesActivos(JComponent [] cntrls, boolean [] estados){
        for(int i = 0; i < cntrls.length; i++){
            cntrls[i].setEnabled(estados[i]);
        }
    }
    
    /**
     * establece el atributo "enabled" de un arreglo de jComponents
     * @param cntrls arreglo de controles a los que les será establecido "enabled"
     * @param estado estádo al que los atributos "enabled" de los controles serán establecidos 
     */
    public void alternarControlesActivos(JComponent [] cntrls, boolean estado){
        for(int i = 0; i < cntrls.length; i++){
            cntrls[i].setEnabled(estado);
        }
    }
    
    /**
     * <b>!Este método está incompleto!</b><br>
     * Este método pretende restablecer los valores por defecto de los controles swing de forma general
     * únicamente se necesita pasar un arreglo de JComponents que contenga los controles que se desea
     * restablecer a su estádo por defecto (limpiarlos).<br>
     * <i>ya que esté método no se encuentra finalizado y se quiere utilizar para algún otro control
     * de <b>javax.swing</b>, puede añadirse el código sin mutar los códigos para limpieza
     * de otros JComponents ya establecidos.
     * @param cntrls arreglo de JComponent con los controles que se quiera "limpiar"
     */
    public void restablecerValoresPorDefecto(JComponent [] cntrls){
        for(JComponent c : cntrls){
            //limpiar componentes contenedores de texto
            if(JTextComponent.class.isAssignableFrom(c.getClass())){
                ((JTextComponent) c).setText("");
            }
            //limpiar comboBoxes
            else if(c.getClass().isAssignableFrom(JComboBox.class)){
                ((JComboBox) c).setSelectedIndex(0);
            }
            //limpiar Spinners
            else if(c.getClass().isAssignableFrom(JSpinner.class)){
                JSpinner spn = ((JSpinner) c);
                SpinnerModel spmodel= spn.getModel();
                
                if(spmodel.getClass().isAssignableFrom(SpinnerNumberModel.class)){
                    SpinnerNumberModel model = (SpinnerNumberModel) spmodel;
                    
                    model.setValue(model.getMinimum());
                }
                
                if(spmodel.getClass().isAssignableFrom(SpinnerDateModel.class)){
                    SpinnerDateModel model = (SpinnerDateModel) spmodel;
                    
                    model.setValue(model.getStart());
                }
            }
        }
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones para tablas">
    /**
     * Devuelve el valor de la celda usando el indice de la fila
     * y el nombre de la columna.<br>
     * <i>¿Por qué utilizar esta función?</i><br>
     * Porque permite re-ordenar las columnas de la tabla sin tener el problema
     * de adquirir un valor erroneo a la hora de hacer referencia al indice de la
     * columna.
     * @param tb Tabla objetivo
     * @param rowIndex indice de la fila
     * @param columName nombre de la columna
     * @return Object valor de la celda
     */
    public Object JTableGetValueAt(JTable tb, int rowIndex, String columName){
        return tb.getValueAt(rowIndex, tb.getColumnModel().getColumnIndex(columName));
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones para combo box">
    /**
     * "Facilita" el establecimiento del item deseado en un combo box cuyo contenido
     * sea de la clase <b>com.workingbriefcase.utilidades.ComboBoxItem</b>
     * @param cmb comboBox objetivo
     * @param ci ComboBoxItem que será el valor seleccionado
     */
    public void setJComboBoxSelectedItem(JComboBox cmb, ComboBoxItem ci){
        for (int i = 0; i < cmb.getItemCount(); i++) {
            ComboBoxItem ciTmp;
            
            ciTmp = (ComboBoxItem) cmb.getItemAt(i);
            
            if(ci.getValor() == ciTmp.getValor()){
                cmb.setSelectedIndex(i);
            }
        }
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funciones para validación">
    
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="funcones JOptionPane 'hornedas'">
    public void mostrarMensajeInformativo(JComponent parent ,String mensaje, String titulo){
        JOptionPane.showMessageDialog(parent, mensaje, titulo, JOptionPane.INFORMATION_MESSAGE);
    }
    
    public void mostrarMensajeExito(JComponent parent ,String mensaje){
        JOptionPane.showMessageDialog(parent, mensaje, "Exito", JOptionPane.INFORMATION_MESSAGE);
    }
    
    public void mostrarMensajeFallo(JComponent parent ,String mensaje){
        JOptionPane.showMessageDialog(parent, mensaje, "Fallo", JOptionPane.ERROR_MESSAGE);
    }
    
    public void mostrarMensajeAdvertencia(JComponent parent ,String mensaje){
        JOptionPane.showMessageDialog(parent, mensaje, "Precaución!!!", JOptionPane.WARNING_MESSAGE);
    }
    
    public int mostrarConfirmacionSimple(JComponent parent ,String mensaje){
        return JOptionPane.showConfirmDialog(parent, mensaje, "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
    }
    
    public void mostrarMensajeCancelacion(JComponent parent ,String mensaje){
        JOptionPane.showMessageDialog(parent, mensaje, "Cancelado", JOptionPane.INFORMATION_MESSAGE);
    }
    
    public void mostrarMensajeExcepcion(JComponent parent, String mensaje, Exception e){
        JOptionPane.showMessageDialog(parent, mensaje 
                + "\nInformación: " + e.getMessage()
                , "Fallo", JOptionPane.ERROR_MESSAGE);
        e.printStackTrace();
    }
//</editor-fold>
}
