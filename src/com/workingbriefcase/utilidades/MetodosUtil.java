/**
 * Nombre de la clase: MetodosUtil;
 * Copyright: EnclaveStudio
 * Fecha: 05/10/2019;
 * Version: 1.0
 * @author Oscar caceres
 */
package com.workingbriefcase.utilidades;


import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Properties;
import javax.imageio.ImageIO;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;


public class MetodosUtil extends Conexion{
  
    //enviar correos
    public void SendMail(String usuario, String contrasenia, String para, String asunto, String mensaje) {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(usuario, contrasenia);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(para));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(para));
            message.setSubject(asunto);
            message.setText(mensaje);

            Transport.send(message);
            JOptionPane.showMessageDialog(null, "Su mensaje ha sido enviado.");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
    
    
     public void llenarGridCandidatos(JTable tabla, String sql, String[] headers){
        DefaultTableModel modelo = new DefaultTableModel(null, headers);
        ResultSet res;
          tabla.setDefaultRenderer(Object.class, new TablaImagen());
        try {
            this.conectar();
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            res = pre.executeQuery();
 
            while(res.next()){
                         Object[] obj = new Object[9];
                obj[0] = res.getInt(1);
                obj[1] = res.getString(2);
                obj[2] = res.getString(3);
                obj[3] = res.getString(4);
                boolean trabajando = res.getBoolean(5);
                if(trabajando){
                      obj[4]  = "Trabajando";
                }else{
                      obj[4]  = "Desempleado";
                }
                
                obj[5] = res.getInt(6);
                obj[6] = res.getInt(7);
                obj[7] = res.getString(8);
                Blob blob = res.getBlob(9);
                 if(blob != null){
                   try{
                        byte[] data = blob.getBytes(1, (int)blob.length());
                        BufferedImage img = null;
                        try{
                        img = ImageIO.read(new ByteArrayInputStream(data));
                        }catch(Exception ex){
                        System.out.println(ex.getMessage());
                        }
                    ImageIcon icono = new ImageIcon(img);
                    obj[8] = new JLabel(icono);
                        }catch(Exception ex){
                            obj[8] = "No Imagen";
                        }
                }
                else{
                    obj[8] = "No Imagen";
                }
                                         
                modelo.addRow(obj);
            }
            tabla.setModel(modelo);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al mostrar los datos: " + e.toString());
        }finally{
            this.desconectar();
        }
    }
     public int retornarIntByparametro(String sql){
         int resultado = 0;
           ResultSet res;
           try {
               this.conectar();
              PreparedStatement pre = this.getCon().prepareStatement(sql);
            res = pre.executeQuery();
            while(res.next()){
                resultado = res.getInt("id");
            }
            return resultado;
         } catch (Exception e) {
             e.printStackTrace();
         }finally{
            this.desconectar();
        }
           return resultado;
     }
     public void llenarGrid(JTable tabla, String sql, String[] headers){
        DefaultTableModel modelo = new DefaultTableModel(null, headers);
        ResultSet res;
        ResultSetMetaData rem;
        try {
            this.conectar();
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            res = pre.executeQuery();
            rem = res.getMetaData();
            int filas = rem.getColumnCount();
            while(res.next()){
                Object[] obj = new Object[filas];
                for (int i = 0; i < filas; i++) {
                    obj[i] = res.getObject(i+1);
                }
                modelo.addRow(obj);
            }
            tabla.setModel(modelo);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al mostrar los datos: " + e.toString());
        }finally{
            this.desconectar();
        }
    }
  
    
    public void soloNumeros(KeyEvent evt){
        Character c = evt.getKeyChar();
        if(!Character.isDigit(c)){
            evt.consume();
        }
    }
    
    public void soloLetras(KeyEvent evt){
        Character c = evt.getKeyChar();
        if(!Character.isLetter(c) && c != KeyEvent.VK_SPACE)
        {
            evt.consume();
        }
    }
    
    public void soloDecimales(JTextField caja, KeyEvent evt){
        Character c = evt.getKeyChar();
        if(!Character.isDigit(c) && c != '.'){
            evt.consume();
        }
        if(c == '.' && caja.getText().contains(".")){
            evt.consume();
        }
    }
    public void centrarFormularios(JDesktopPane desktop, JInternalFrame frame){
          Dimension desktopSize = desktop.getSize();
         Dimension jInternalFrameSize = frame.getSize();
          frame.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
          (desktopSize.height- jInternalFrameSize.height)/2);
    }
}
