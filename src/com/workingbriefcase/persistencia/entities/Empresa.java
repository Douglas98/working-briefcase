package com.workingbriefcase.persistencia.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Nombre de la clase: Empresa
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
@Entity
@Table(name = "empresa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Empresa.findAll", query = "SELECT e FROM Empresa e")
    , @NamedQuery(name = "Empresa.findById", query = "SELECT e FROM Empresa e WHERE e.id = :id")
    , @NamedQuery(name = "Empresa.findByNit", query = "SELECT e FROM Empresa e WHERE e.nit = :nit")
    , @NamedQuery(name = "Empresa.findByNombre", query = "SELECT e FROM Empresa e WHERE e.nombre = :nombre")
    , @NamedQuery(name = "Empresa.findByRazonSocial", query = "SELECT e FROM Empresa e WHERE e.razonSocial = :razonSocial")
    , @NamedQuery(name = "Empresa.findByDireccion", query = "SELECT e FROM Empresa e WHERE e.direccion = :direccion")
    , @NamedQuery(name = "Empresa.findByDireccionPaginaWeb", query = "SELECT e FROM Empresa e WHERE e.direccionPaginaWeb = :direccionPaginaWeb")
    , @NamedQuery(name = "Empresa.findByTipologia", query = "SELECT e FROM Empresa e WHERE e.tipologia = :tipologia")
    , @NamedQuery(name = "Empresa.findByMinNumeroTrabajadores", query = "SELECT e FROM Empresa e WHERE e.minNumeroTrabajadores = :minNumeroTrabajadores")
    , @NamedQuery(name = "Empresa.findByMaxNumeroTrabajadores", query = "SELECT e FROM Empresa e WHERE e.maxNumeroTrabajadores = :maxNumeroTrabajadores")
    , @NamedQuery(name = "Empresa.findEmpresaIdByUsuarioId", query = "SELECT e FROM Empresa e INNER JOIN e.usuario u WHERE u.id = :UsuarioId")})
public class Empresa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "nit")
    private String nit;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "razonSocial")
    private String razonSocial;
    @Basic(optional = false)
    @Column(name = "direccion")
    private String direccion;
    @Lob
    @Column(name = "foto")
    private byte[] foto;
    @Column(name = "direccionPaginaWeb")
    private String direccionPaginaWeb;
    @Basic(optional = false)
    @Column(name = "tipologia")
    private String tipologia;
    @Basic(optional = false)
    @Column(name = "minNumeroTrabajadores")
    private int minNumeroTrabajadores;
    @Basic(optional = false)
    @Column(name = "maxNumeroTrabajadores")
    private int maxNumeroTrabajadores;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "empresaPublicadora")
    private List<Ofertalaboral> ofertalaboralList;
    @JoinColumn(name = "encargado", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Encargado encargado;
    @JoinColumn(name = "municipioActual", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Municipio municipioActual;
    @JoinColumn(name = "nacionalidad", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Pais nacionalidad;
    @JoinColumn(name = "sectorEmpresarial", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Sectorempresarial sectorEmpresarial;
    @JoinColumn(name = "usuario", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Usuario usuario;

    public Empresa() {
    }

    public Empresa(Integer id) {
        this.id = id;
    }

    public Empresa(Integer id, String nombre, String razonSocial, String direccion, String tipologia, int minNumeroTrabajadores, int maxNumeroTrabajadores) {
        this.id = id;
        this.nombre = nombre;
        this.razonSocial = razonSocial;
        this.direccion = direccion;
        this.tipologia = tipologia;
        this.minNumeroTrabajadores = minNumeroTrabajadores;
        this.maxNumeroTrabajadores = maxNumeroTrabajadores;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public String getDireccionPaginaWeb() {
        return direccionPaginaWeb;
    }

    public void setDireccionPaginaWeb(String direccionPaginaWeb) {
        this.direccionPaginaWeb = direccionPaginaWeb;
    }

    public String getTipologia() {
        return tipologia;
    }

    public void setTipologia(String tipologia) {
        this.tipologia = tipologia;
    }

    public int getMinNumeroTrabajadores() {
        return minNumeroTrabajadores;
    }

    public void setMinNumeroTrabajadores(int minNumeroTrabajadores) {
        this.minNumeroTrabajadores = minNumeroTrabajadores;
    }

    public int getMaxNumeroTrabajadores() {
        return maxNumeroTrabajadores;
    }

    public void setMaxNumeroTrabajadores(int maxNumeroTrabajadores) {
        this.maxNumeroTrabajadores = maxNumeroTrabajadores;
    }

    @XmlTransient
    public List<Ofertalaboral> getOfertalaboralList() {
        return ofertalaboralList;
    }

    public void setOfertalaboralList(List<Ofertalaboral> ofertalaboralList) {
        this.ofertalaboralList = ofertalaboralList;
    }

    public Encargado getEncargado() {
        return encargado;
    }

    public void setEncargado(Encargado encargado) {
        this.encargado = encargado;
    }

    public Municipio getMunicipioActual() {
        return municipioActual;
    }

    public void setMunicipioActual(Municipio municipioActual) {
        this.municipioActual = municipioActual;
    }

    public Pais getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(Pais nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public Sectorempresarial getSectorEmpresarial() {
        return sectorEmpresarial;
    }

    public void setSectorEmpresarial(Sectorempresarial sectorEmpresarial) {
        this.sectorEmpresarial = sectorEmpresarial;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empresa)) {
            return false;
        }
        Empresa other = (Empresa) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.workingbriefcase.persistencia.entities.Empresa[ id=" + id + " ]";
    }

}
