package com.workingbriefcase.persistencia.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Nombre de la clase: Idioma
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
@Entity
@Table(name = "idioma")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Idioma.findAll", query = "SELECT i FROM Idioma i")
    , @NamedQuery(name = "Idioma.findById", query = "SELECT i FROM Idioma i WHERE i.id = :id")
    , @NamedQuery(name = "Idioma.findByNombre", query = "SELECT i FROM Idioma i WHERE i.nombre = :nombre")
    , @NamedQuery(name = "Idioma.findByBorrado", query = "SELECT i FROM Idioma i WHERE i.borrado = :borrado")})
public class Idioma implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "borrado")
    private boolean borrado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idioma")
    private List<CurriculumIdioma> curriculumIdiomaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idioma")
    private List<OfertalaboralIdioma> ofertalaboralIdiomaList;

    public Idioma() {
    }

    public Idioma(Integer id) {
        this.id = id;
    }

    public Idioma(Integer id, boolean borrado) {
        this.id = id;
        this.borrado = borrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(boolean borrado) {
        this.borrado = borrado;
    }

    @XmlTransient
    public List<CurriculumIdioma> getCurriculumIdiomaList() {
        return curriculumIdiomaList;
    }

    public void setCurriculumIdiomaList(List<CurriculumIdioma> curriculumIdiomaList) {
        this.curriculumIdiomaList = curriculumIdiomaList;
    }

    @XmlTransient
    public List<OfertalaboralIdioma> getOfertalaboralIdiomaList() {
        return ofertalaboralIdiomaList;
    }

    public void setOfertalaboralIdiomaList(List<OfertalaboralIdioma> ofertalaboralIdiomaList) {
        this.ofertalaboralIdiomaList = ofertalaboralIdiomaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Idioma)) {
            return false;
        }
        Idioma other = (Idioma) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.workingbriefcase.persistencia.entities.Idioma[ id=" + id + " ]";
    }

}
