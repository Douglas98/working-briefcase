package com.workingbriefcase.persistencia.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Nombre de la clase: CurriculumHabilidad
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
@Entity
@Table(name = "curriculum_habilidad")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CurriculumHabilidad.findAll", query = "SELECT c FROM CurriculumHabilidad c")
    , @NamedQuery(name = "CurriculumHabilidad.findById", query = "SELECT c FROM CurriculumHabilidad c WHERE c.id = :id")
    , @NamedQuery(name = "CurriculumHabilidad.findByCorrelativo", query = "SELECT c FROM CurriculumHabilidad c WHERE c.correlativo = :correlativo")
    , @NamedQuery(name = "CurriculumHabilidad.findByNivelDominio", query = "SELECT c FROM CurriculumHabilidad c WHERE c.nivelDominio = :nivelDominio")
    , @NamedQuery(name = "CurriculumHabilidad.findByBorrado", query = "SELECT c FROM CurriculumHabilidad c WHERE c.borrado = :borrado")})
public class CurriculumHabilidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "correlativo")
    private int correlativo;
    @Basic(optional = false)
    @Column(name = "nivelDominio")
    private String nivelDominio;
    @Column(name = "borrado")
    private Boolean borrado;
    @JoinColumn(name = "curriculum", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Curriculum curriculum;
    @JoinColumn(name = "habilidadProfesional", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Habilidadprofesional habilidadProfesional;

    public CurriculumHabilidad() {
    }

    public CurriculumHabilidad(Integer id) {
        this.id = id;
    }

    public CurriculumHabilidad(Integer id, int correlativo, String nivelDominio) {
        this.id = id;
        this.correlativo = correlativo;
        this.nivelDominio = nivelDominio;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(int correlativo) {
        this.correlativo = correlativo;
    }

    public String getNivelDominio() {
        return nivelDominio;
    }

    public void setNivelDominio(String nivelDominio) {
        this.nivelDominio = nivelDominio;
    }

    public Boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(Boolean borrado) {
        this.borrado = borrado;
    }

    public Curriculum getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(Curriculum curriculum) {
        this.curriculum = curriculum;
    }

    public Habilidadprofesional getHabilidadProfesional() {
        return habilidadProfesional;
    }

    public void setHabilidadProfesional(Habilidadprofesional habilidadProfesional) {
        this.habilidadProfesional = habilidadProfesional;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CurriculumHabilidad)) {
            return false;
        }
        CurriculumHabilidad other = (CurriculumHabilidad) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.workingbriefcase.persistencia.entities.CurriculumHabilidad[ id=" + id + " ]";
    }

}
