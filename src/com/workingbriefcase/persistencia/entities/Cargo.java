package com.workingbriefcase.persistencia.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Nombre de la clase: Cargo
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
@Entity
@Table(name = "cargo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cargo.findAll", query = "SELECT c FROM Cargo c")
    , @NamedQuery(name = "Cargo.findById", query = "SELECT c FROM Cargo c WHERE c.id = :id")
    , @NamedQuery(name = "Cargo.findByNombre", query = "SELECT c FROM Cargo c WHERE c.nombre = :nombre")
    , @NamedQuery(name = "Cargo.findByBorrado", query = "SELECT c FROM Cargo c WHERE c.borrado = :borrado")})
public class Cargo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = true)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "borrado")
    private boolean borrado;
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "cargoDesempennado")
    private List<Empleadoasalariado> empleadoasalariadoList;
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "cargoSolicitado")
    private List<Ofertalaboral> ofertalaboralList;
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "cargoDesepennado")
    private List<Encargado> encargadoList;
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "cargoDesempennado")
    private List<Empleadohora> empleadohoraList;

    public Cargo() {
    }

    public Cargo(Integer id) {
        this.id = id;
    }

    public Cargo(Integer id, String nombre, boolean borrado) {
        this.id = id;
        this.nombre = nombre;
        this.borrado = borrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(boolean borrado) {
        this.borrado = borrado;
    }

    @XmlTransient
    public List<Empleadoasalariado> getEmpleadoasalariadoList() {
        return empleadoasalariadoList;
    }

    public void setEmpleadoasalariadoList(List<Empleadoasalariado> empleadoasalariadoList) {
        this.empleadoasalariadoList = empleadoasalariadoList;
    }

    @XmlTransient
    public List<Ofertalaboral> getOfertalaboralList() {
        return ofertalaboralList;
    }

    public void setOfertalaboralList(List<Ofertalaboral> ofertalaboralList) {
        this.ofertalaboralList = ofertalaboralList;
    }

    @XmlTransient
    public List<Encargado> getEncargadoList() {
        return encargadoList;
    }

    public void setEncargadoList(List<Encargado> encargadoList) {
        this.encargadoList = encargadoList;
    }

    @XmlTransient
    public List<Empleadohora> getEmpleadohoraList() {
        return empleadohoraList;
    }

    public void setEmpleadohoraList(List<Empleadohora> empleadohoraList) {
        this.empleadohoraList = empleadohoraList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cargo)) {
            return false;
        }
        Cargo other = (Cargo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.workingbriefcase.persistencia.entities.Cargo[ id=" + id + " ]";
    }

}
