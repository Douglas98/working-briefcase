package com.workingbriefcase.persistencia.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Nombre de la clase: Profesion
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
@Entity
@Table(name = "profesion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Profesion.findAll", query = "SELECT p FROM Profesion p")
    , @NamedQuery(name = "Profesion.findById", query = "SELECT p FROM Profesion p WHERE p.id = :id")
    , @NamedQuery(name = "Profesion.findByNombre", query = "SELECT p FROM Profesion p WHERE p.nombre = :nombre")
    , @NamedQuery(name = "Profesion.findByBorrado", query = "SELECT p FROM Profesion p WHERE p.borrado = :borrado")
    , @NamedQuery(name = "Profesion.findByAreaLaboral", query = "SELECT p From Profesion p Where p.areaLaboral.id = :idAreaLaboral")})
public class Profesion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "borrado")
    private boolean borrado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "profesionEjercida")
    private List<Empleadoasalariado> empleadoasalariadoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "profesion")
    private List<CurriculumProfesion> curriculumProfesionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "profesionRequerida")
    private List<Ofertalaboral> ofertalaboralList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "profesionEjercida")
    private List<Encargado> encargadoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "profesionEjercida")
    private List<Empleadohora> empleadohoraList;
    @JoinColumn(name = "areaLaboral", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Arealaboral areaLaboral;

    public Profesion() {
    }

    public Profesion(Integer id) {
        this.id = id;
    }

    public Profesion(Integer id, String nombre, boolean borrado) {
        this.id = id;
        this.nombre = nombre;
        this.borrado = borrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(boolean borrado) {
        this.borrado = borrado;
    }

    @XmlTransient
    public List<Empleadoasalariado> getEmpleadoasalariadoList() {
        return empleadoasalariadoList;
    }

    public void setEmpleadoasalariadoList(List<Empleadoasalariado> empleadoasalariadoList) {
        this.empleadoasalariadoList = empleadoasalariadoList;
    }

    @XmlTransient
    public List<CurriculumProfesion> getCurriculumProfesionList() {
        return curriculumProfesionList;
    }

    public void setCurriculumProfesionList(List<CurriculumProfesion> curriculumProfesionList) {
        this.curriculumProfesionList = curriculumProfesionList;
    }

    @XmlTransient
    public List<Ofertalaboral> getOfertalaboralList() {
        return ofertalaboralList;
    }

    public void setOfertalaboralList(List<Ofertalaboral> ofertalaboralList) {
        this.ofertalaboralList = ofertalaboralList;
    }

    @XmlTransient
    public List<Encargado> getEncargadoList() {
        return encargadoList;
    }

    public void setEncargadoList(List<Encargado> encargadoList) {
        this.encargadoList = encargadoList;
    }

    @XmlTransient
    public List<Empleadohora> getEmpleadohoraList() {
        return empleadohoraList;
    }

    public void setEmpleadohoraList(List<Empleadohora> empleadohoraList) {
        this.empleadohoraList = empleadohoraList;
    }

    public Arealaboral getAreaLaboral() {
        return areaLaboral;
    }

    public void setAreaLaboral(Arealaboral areaLaboral) {
        this.areaLaboral = areaLaboral;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Profesion)) {
            return false;
        }
        Profesion other = (Profesion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.workingbriefcase.persistencia.entities.Profesion[ id=" + id + " ]";
    }

}
