package com.workingbriefcase.persistencia.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Nombre de la clase: Curriculum
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
@Entity
@Table(name = "curriculum")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Curriculum.findAll", query = "SELECT c FROM Curriculum c")
    , @NamedQuery(name = "Curriculum.findById", query = "SELECT c FROM Curriculum c WHERE c.id = :id")
    , @NamedQuery(name = "Curriculum.findByCorrelativoActual", query = "SELECT c FROM Curriculum c WHERE c.correlativoActual = :correlativoActual")
    , @NamedQuery(name = "Curriculum.findByFechaPublicado", query = "SELECT c FROM Curriculum c WHERE c.fechaPublicado = :fechaPublicado")
    , @NamedQuery(name = "Curriculum.findByPrivacidadCurriculum", query = "SELECT c FROM Curriculum c WHERE c.privacidadCurriculum = :privacidadCurriculum")
    , @NamedQuery(name = "Curriculum.findByBorrado", query = "SELECT c FROM Curriculum c WHERE c.borrado = :borrado")
    , @NamedQuery(name = "Curriculum.findByCandidatos", query = "SELECT c FROM Curriculum c WHERE c.candidatoPertenece.id = :idCandidato")
  })
public class Curriculum implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "correlativoActual")
    private int correlativoActual;
    @Basic(optional = false)
    @Column(name = "fechaPublicado")
    @Temporal(TemporalType.DATE)
    private Date fechaPublicado;
    @Basic(optional = false)
    @Lob
    @Column(name = "descripcionCurriculum")
    private String descripcionCurriculum;
    @Basic(optional = false)
    @Column(name = "privacidadCurriculum")
    private int privacidadCurriculum;
    @Basic(optional = false)
    @Column(name = "borrado")
    private boolean borrado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "curriculum", orphanRemoval = true)
    private List<PropuestatrabajoCurriculum> propuestatrabajoCurriculumList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "curriculum", orphanRemoval = true)
    private List<CurriculumProfesion> curriculumProfesionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "curriculum", orphanRemoval = true)
    private List<CurriculumCurso> curriculumCursoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "curriculum", orphanRemoval = true)
    private List<CurriculumHabilidad> curriculumHabilidadList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "curriculum", orphanRemoval = true)
    private List<CurriculumIdioma> curriculumIdiomaList;
    @JoinColumn(name = "candidatoPertenece", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Candidato candidatoPertenece;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "curriculum", orphanRemoval = true)
    private List<OfertalaboralCurriculum> ofertalaboralCurriculumList;

    public Curriculum() {
    }

    public Curriculum(Integer id) {
        this.id = id;
    }

    public Curriculum(Integer id, int correlativoActual, Date fechaPublicado, String descripcionCurriculum, int privacidadCurriculum, boolean borrado) {
        this.id = id;
        this.correlativoActual = correlativoActual;
        this.fechaPublicado = fechaPublicado;
        this.descripcionCurriculum = descripcionCurriculum;
        this.privacidadCurriculum = privacidadCurriculum;
        this.borrado = borrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCorrelativoActual() {
        return correlativoActual;
    }

    public void setCorrelativoActual(int correlativoActual) {
        this.correlativoActual = correlativoActual;
    }

    public Date getFechaPublicado() {
        return fechaPublicado;
    }

    public void setFechaPublicado(Date fechaPublicado) {
        this.fechaPublicado = fechaPublicado;
    }

    public String getDescripcionCurriculum() {
        return descripcionCurriculum;
    }

    public void setDescripcionCurriculum(String descripcionCurriculum) {
        this.descripcionCurriculum = descripcionCurriculum;
    }

    public int getPrivacidadCurriculum() {
        return privacidadCurriculum;
    }

    public void setPrivacidadCurriculum(int privacidadCurriculum) {
        this.privacidadCurriculum = privacidadCurriculum;
    }

    public boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(boolean borrado) {
        this.borrado = borrado;
    }

    @XmlTransient
    public List<PropuestatrabajoCurriculum> getPropuestatrabajoCurriculumList() {
        return propuestatrabajoCurriculumList;
    }

    public void setPropuestatrabajoCurriculumList(List<PropuestatrabajoCurriculum> propuestatrabajoCurriculumList) {
        this.propuestatrabajoCurriculumList = propuestatrabajoCurriculumList;
    }

    @XmlTransient
    public List<CurriculumProfesion> getCurriculumProfesionList() {
        return curriculumProfesionList;
    }

    public void setCurriculumProfesionList(List<CurriculumProfesion> curriculumProfesionList) {
        this.curriculumProfesionList = curriculumProfesionList;
    }

    @XmlTransient
    public List<CurriculumCurso> getCurriculumCursoList() {
        return curriculumCursoList;
    }

    public void setCurriculumCursoList(List<CurriculumCurso> curriculumCursoList) {
        this.curriculumCursoList = curriculumCursoList;
    }

    @XmlTransient
    public List<CurriculumHabilidad> getCurriculumHabilidadList() {
        return curriculumHabilidadList;
    }

    public void setCurriculumHabilidadList(List<CurriculumHabilidad> curriculumHabilidadList) {
        this.curriculumHabilidadList = curriculumHabilidadList;
    }

    @XmlTransient
    public List<CurriculumIdioma> getCurriculumIdiomaList() {
        return curriculumIdiomaList;
    }

    public void setCurriculumIdiomaList(List<CurriculumIdioma> curriculumIdiomaList) {
        this.curriculumIdiomaList = curriculumIdiomaList;
    }

    public Candidato getCandidatoPertenece() {
        return candidatoPertenece;
    }

    public void setCandidatoPertenece(Candidato candidatoPertenece) {
        this.candidatoPertenece = candidatoPertenece;
    }

    @XmlTransient
    public List<OfertalaboralCurriculum> getOfertalaboralCurriculumList() {
        return ofertalaboralCurriculumList;
    }

    public void setOfertalaboralCurriculumList(List<OfertalaboralCurriculum> ofertalaboralCurriculumList) {
        this.ofertalaboralCurriculumList = ofertalaboralCurriculumList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Curriculum)) {
            return false;
        }
        Curriculum other = (Curriculum) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.workingbriefcase.persistencia.entities.Curriculum[ id=" + id + " ]";
    }

}
