package com.workingbriefcase.persistencia.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Nombre de la clase: Propuestatrabajo
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
@Entity
@Table(name = "propuestatrabajo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Propuestatrabajo.findAll", query = "SELECT p FROM Propuestatrabajo p")
    , @NamedQuery(name = "Propuestatrabajo.findById", query = "SELECT p FROM Propuestatrabajo p WHERE p.id = :id")
    , @NamedQuery(name = "Propuestatrabajo.findByCorrelativo", query = "SELECT p FROM Propuestatrabajo p WHERE p.correlativo = :correlativo")
    , @NamedQuery(name = "Propuestatrabajo.findByFechaInicioPropuesta", query = "SELECT p FROM Propuestatrabajo p WHERE p.fechaInicioPropuesta = :fechaInicioPropuesta")
    , @NamedQuery(name = "Propuestatrabajo.findByFechaFinPropuesta", query = "SELECT p FROM Propuestatrabajo p WHERE p.fechaFinPropuesta = :fechaFinPropuesta")
    , @NamedQuery(name = "Propuestatrabajo.findByActivo", query = "SELECT p FROM Propuestatrabajo p WHERE p.activo = :activo")
    , @NamedQuery(name = "Propuestatrabajo.findByBorrado", query = "SELECT p FROM Propuestatrabajo p WHERE p.borrado = :borrado")
    , @NamedQuery(name = "Propuestatrabajo.lastId", query = "SELECT MAX(p.id) FROM Propuestatrabajo p")})
public class Propuestatrabajo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "correlativo")
    private int correlativo;
    @Basic(optional = false)
    @Column(name = "fechaInicioPropuesta")
    @Temporal(TemporalType.DATE)
    private Date fechaInicioPropuesta;
    @Basic(optional = false)
    @Column(name = "fechaFinPropuesta")
    @Temporal(TemporalType.DATE)
    private Date fechaFinPropuesta;
    @Basic(optional = false)
    @Column(name = "activo")
    private boolean activo;
    @Basic(optional = false)
    @Column(name = "borrado")
    private boolean borrado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "propuestaTrabajo")
    private List<PropuestatrabajoCurriculum> propuestatrabajoCurriculumList;
    @JoinColumn(name = "ofertaLaboral", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Ofertalaboral ofertaLaboral;

    public Propuestatrabajo() {
    }

    public Propuestatrabajo(Integer id) {
        this.id = id;
    }

    public Propuestatrabajo(Integer id, int correlativo, Date fechaInicioPropuesta, Date fechaFinPropuesta, boolean activo, boolean borrado) {
        this.id = id;
        this.correlativo = correlativo;
        this.fechaInicioPropuesta = fechaInicioPropuesta;
        this.fechaFinPropuesta = fechaFinPropuesta;
        this.activo = activo;
        this.borrado = borrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(int correlativo) {
        this.correlativo = correlativo;
    }

    public Date getFechaInicioPropuesta() {
        return fechaInicioPropuesta;
    }

    public void setFechaInicioPropuesta(Date fechaInicioPropuesta) {
        this.fechaInicioPropuesta = fechaInicioPropuesta;
    }

    public Date getFechaFinPropuesta() {
        return fechaFinPropuesta;
    }

    public void setFechaFinPropuesta(Date fechaFinPropuesta) {
        this.fechaFinPropuesta = fechaFinPropuesta;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(boolean borrado) {
        this.borrado = borrado;
    }

    @XmlTransient
    public List<PropuestatrabajoCurriculum> getPropuestatrabajoCurriculumList() {
        return propuestatrabajoCurriculumList;
    }

    public void setPropuestatrabajoCurriculumList(List<PropuestatrabajoCurriculum> propuestatrabajoCurriculumList) {
        this.propuestatrabajoCurriculumList = propuestatrabajoCurriculumList;
    }

    public Ofertalaboral getOfertaLaboral() {
        return ofertaLaboral;
    }

    public void setOfertaLaboral(Ofertalaboral ofertaLaboral) {
        this.ofertaLaboral = ofertaLaboral;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Propuestatrabajo)) {
            return false;
        }
        Propuestatrabajo other = (Propuestatrabajo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.workingbriefcase.persistencia.entities.Propuestatrabajo[ id=" + id + " ]";
    }

}
