package com.workingbriefcase.persistencia.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Nombre de la clase: Ofertalaboral
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
@Entity
@Table(name = "ofertalaboral")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ofertalaboral.findAll", query = "SELECT o FROM Ofertalaboral o")
    , @NamedQuery(name = "Ofertalaboral.findById", query = "SELECT o FROM Ofertalaboral o WHERE o.id = :id")
    , @NamedQuery(name = "Ofertalaboral.findByCorrelativo", query = "SELECT o FROM Ofertalaboral o WHERE o.correlativo = :correlativo")
    , @NamedQuery(name = "Ofertalaboral.findByTipoEmpleo", query = "SELECT o FROM Ofertalaboral o WHERE o.tipoEmpleo = :tipoEmpleo")
    , @NamedQuery(name = "Ofertalaboral.findByPaga", query = "SELECT o FROM Ofertalaboral o WHERE o.paga = :paga")
    , @NamedQuery(name = "Ofertalaboral.findByHorasMes", query = "SELECT o FROM Ofertalaboral o WHERE o.horasMes = :horasMes")
    , @NamedQuery(name = "Ofertalaboral.findByDireccion", query = "SELECT o FROM Ofertalaboral o WHERE o.direccion = :direccion")
    , @NamedQuery(name = "Ofertalaboral.findByFechaCreacionOferta", query = "SELECT o FROM Ofertalaboral o WHERE o.fechaCreacionOferta = :fechaCreacionOferta")
    , @NamedQuery(name = "Ofertalaboral.findByFechaInicioOferta", query = "SELECT o FROM Ofertalaboral o WHERE o.fechaInicioOferta = :fechaInicioOferta")
    , @NamedQuery(name = "Ofertalaboral.findByFechaFinOferta", query = "SELECT o FROM Ofertalaboral o WHERE o.fechaFinOferta = :fechaFinOferta")
    , @NamedQuery(name = "Ofertalaboral.findByActivo", query = "SELECT o FROM Ofertalaboral o WHERE o.activo = :activo")
    , @NamedQuery(name = "Ofertalaboral.findByBorrado", query = "SELECT o FROM Ofertalaboral o WHERE o.borrado = :borrado")
    , @NamedQuery(name = "Ofertalaboral.findByEmpresa", query = "SELECT o.id, o.tipoEmpleo FROM Ofertalaboral o WHERE o.empresaPublicadora.id = :idEmpresa")})
public class Ofertalaboral implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "correlativo")
    private int correlativo;
    @Basic(optional = false)
    @Column(name = "tipoEmpleo")
    private String tipoEmpleo;
    @Basic(optional = false)
    @Column(name = "paga")
    private long paga;
    @Basic(optional = false)
    @Column(name = "horasMes")
    private int horasMes;
    @Basic(optional = false)
    @Column(name = "direccion")
    private String direccion;
    @Basic(optional = false)
    @Column(name = "fechaCreacionOferta")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacionOferta;
    @Basic(optional = false)
    @Column(name = "fechaInicioOferta")
    @Temporal(TemporalType.DATE)
    private Date fechaInicioOferta;
    @Basic(optional = false)
    @Column(name = "fechaFinOferta")
    @Temporal(TemporalType.DATE)
    private Date fechaFinOferta;
    @Basic(optional = false)
    @Column(name = "activo")
    private boolean activo;
    @Basic(optional = false)
    @Column(name = "borrado")
    private boolean borrado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ofertaLaboral")
    private List<OfertalaboralHabilidad> ofertalaboralHabilidadList;
    @JoinColumn(name = "cargoSolicitado", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Cargo cargoSolicitado;
    @JoinColumn(name = "empresaPublicadora", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Empresa empresaPublicadora;
    @JoinColumn(name = "municipioEmpleo", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Municipio municipioEmpleo;
    @JoinColumn(name = "profesionRequerida", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Profesion profesionRequerida;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ofertaLaboral")
    private List<Propuestatrabajo> propuestatrabajoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ofertaLaboral")
    private List<OfertalaboralIdioma> ofertalaboralIdiomaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ofertaLaboral")
    private List<OfertalaboralCurriculum> ofertalaboralCurriculumList;

    public Ofertalaboral() {
    }

    public Ofertalaboral(Integer id) {
        this.id = id;
    }

    public Ofertalaboral(Integer id, int correlativo, String tipoEmpleo, long paga, int horasMes, String direccion, Date fechaCreacionOferta, Date fechaInicioOferta, Date fechaFinOferta, boolean activo, boolean borrado) {
        this.id = id;
        this.correlativo = correlativo;
        this.tipoEmpleo = tipoEmpleo;
        this.paga = paga;
        this.horasMes = horasMes;
        this.direccion = direccion;
        this.fechaCreacionOferta = fechaCreacionOferta;
        this.fechaInicioOferta = fechaInicioOferta;
        this.fechaFinOferta = fechaFinOferta;
        this.activo = activo;
        this.borrado = borrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(int correlativo) {
        this.correlativo = correlativo;
    }

    public String getTipoEmpleo() {
        return tipoEmpleo;
    }

    public void setTipoEmpleo(String tipoEmpleo) {
        this.tipoEmpleo = tipoEmpleo;
    }

    public long getPaga() {
        return paga;
    }

    public void setPaga(long paga) {
        this.paga = paga;
    }

    public int getHorasMes() {
        return horasMes;
    }

    public void setHorasMes(int horasMes) {
        this.horasMes = horasMes;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Date getFechaCreacionOferta() {
        return fechaCreacionOferta;
    }

    public void setFechaCreacionOferta(Date fechaCreacionOferta) {
        this.fechaCreacionOferta = fechaCreacionOferta;
    }

    public Date getFechaInicioOferta() {
        return fechaInicioOferta;
    }

    public void setFechaInicioOferta(Date fechaInicioOferta) {
        this.fechaInicioOferta = fechaInicioOferta;
    }

    public Date getFechaFinOferta() {
        return fechaFinOferta;
    }

    public void setFechaFinOferta(Date fechaFinOferta) {
        this.fechaFinOferta = fechaFinOferta;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(boolean borrado) {
        this.borrado = borrado;
    }

    @XmlTransient
    public List<OfertalaboralHabilidad> getOfertalaboralHabilidadList() {
        return ofertalaboralHabilidadList;
    }

    public void setOfertalaboralHabilidadList(List<OfertalaboralHabilidad> ofertalaboralHabilidadList) {
        this.ofertalaboralHabilidadList = ofertalaboralHabilidadList;
    }

    public Cargo getCargoSolicitado() {
        return cargoSolicitado;
    }

    public void setCargoSolicitado(Cargo cargoSolicitado) {
        this.cargoSolicitado = cargoSolicitado;
    }

    public Empresa getEmpresaPublicadora() {
        return empresaPublicadora;
    }

    public void setEmpresaPublicadora(Empresa empresaPublicadora) {
        this.empresaPublicadora = empresaPublicadora;
    }

    public Municipio getMunicipioEmpleo() {
        return municipioEmpleo;
    }

    public void setMunicipioEmpleo(Municipio municipioEmpleo) {
        this.municipioEmpleo = municipioEmpleo;
    }

    public Profesion getProfesionRequerida() {
        return profesionRequerida;
    }

    public void setProfesionRequerida(Profesion profesionRequerida) {
        this.profesionRequerida = profesionRequerida;
    }

    @XmlTransient
    public List<Propuestatrabajo> getPropuestatrabajoList() {
        return propuestatrabajoList;
    }

    public void setPropuestatrabajoList(List<Propuestatrabajo> propuestatrabajoList) {
        this.propuestatrabajoList = propuestatrabajoList;
    }

    @XmlTransient
    public List<OfertalaboralIdioma> getOfertalaboralIdiomaList() {
        return ofertalaboralIdiomaList;
    }

    public void setOfertalaboralIdiomaList(List<OfertalaboralIdioma> ofertalaboralIdiomaList) {
        this.ofertalaboralIdiomaList = ofertalaboralIdiomaList;
    }

    @XmlTransient
    public List<OfertalaboralCurriculum> getOfertalaboralCurriculumList() {
        return ofertalaboralCurriculumList;
    }

    public void setOfertalaboralCurriculumList(List<OfertalaboralCurriculum> ofertalaboralCurriculumList) {
        this.ofertalaboralCurriculumList = ofertalaboralCurriculumList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ofertalaboral)) {
            return false;
        }
        Ofertalaboral other = (Ofertalaboral) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.workingbriefcase.persistencia.entities.Ofertalaboral[ id=" + id + " ]";
    }

}
