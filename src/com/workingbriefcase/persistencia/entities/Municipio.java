package com.workingbriefcase.persistencia.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Nombre de la clase: Municipio
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
@Entity
@Table(name = "municipio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Municipio.findAll", query = "SELECT m FROM Municipio m")
    , @NamedQuery(name = "Municipio.findById", query = "SELECT m FROM Municipio m WHERE m.id = :id")
    , @NamedQuery(name = "Municipio.findByNombre", query = "SELECT m FROM Municipio m WHERE m.nombre = :nombre")
    , @NamedQuery(name = "Municipio.findByBorrado", query = "SELECT m FROM Municipio m WHERE m.borrado = :borrado")
    , @NamedQuery(name = "Municipio.findByDepartamento", query = "SELECT m FROM Municipio m WHERE m.departamentoPertenece.id = :id")})
public class Municipio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "borrado")
    private boolean borrado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "municipioActual")
    private List<Empleadoasalariado> empleadoasalariadoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "municipioEmpleo")
    private List<Ofertalaboral> ofertalaboralList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "municipioActual")
    private List<Candidato> candidatoList;
    @JoinColumn(name = "departamentoPertenece", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Departamento departamentoPertenece;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "municipioActual")
    private List<Empleadohora> empleadohoraList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "municipioActual")
    private List<Empresa> empresaList;

    public Municipio() {
    }

    public Municipio(Integer id) {
        this.id = id;
    }

    public Municipio(Integer id, String nombre, boolean borrado) {
        this.id = id;
        this.nombre = nombre;
        this.borrado = borrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(boolean borrado) {
        this.borrado = borrado;
    }

    @XmlTransient
    public List<Empleadoasalariado> getEmpleadoasalariadoList() {
        return empleadoasalariadoList;
    }

    public void setEmpleadoasalariadoList(List<Empleadoasalariado> empleadoasalariadoList) {
        this.empleadoasalariadoList = empleadoasalariadoList;
    }

    @XmlTransient
    public List<Ofertalaboral> getOfertalaboralList() {
        return ofertalaboralList;
    }

    public void setOfertalaboralList(List<Ofertalaboral> ofertalaboralList) {
        this.ofertalaboralList = ofertalaboralList;
    }

    @XmlTransient
    public List<Candidato> getCandidatoList() {
        return candidatoList;
    }

    public void setCandidatoList(List<Candidato> candidatoList) {
        this.candidatoList = candidatoList;
    }

    public Departamento getDepartamentoPertenece() {
        return departamentoPertenece;
    }

    public void setDepartamentoPertenece(Departamento departamentoPertenece) {
        this.departamentoPertenece = departamentoPertenece;
    }

    @XmlTransient
    public List<Empleadohora> getEmpleadohoraList() {
        return empleadohoraList;
    }

    public void setEmpleadohoraList(List<Empleadohora> empleadohoraList) {
        this.empleadohoraList = empleadohoraList;
    }

    @XmlTransient
    public List<Empresa> getEmpresaList() {
        return empresaList;
    }

    public void setEmpresaList(List<Empresa> empresaList) {
        this.empresaList = empresaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Municipio)) {
            return false;
        }
        Municipio other = (Municipio) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.workingbriefcase.persistencia.entities.Municipio[ id=" + id + " ]";
    }

}
