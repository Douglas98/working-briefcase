package com.workingbriefcase.persistencia.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Nombre de la clase: PropuestatrabajoCurriculum
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
@Entity
@Table(name = "propuestatrabajo_curriculum")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PropuestatrabajoCurriculum.findAll", query = "SELECT p FROM PropuestatrabajoCurriculum p")
    , @NamedQuery(name = "PropuestatrabajoCurriculum.findById", query = "SELECT p FROM PropuestatrabajoCurriculum p WHERE p.id = :id")
    , @NamedQuery(name = "PropuestatrabajoCurriculum.findByCorrelativo", query = "SELECT p FROM PropuestatrabajoCurriculum p WHERE p.correlativo = :correlativo")
    , @NamedQuery(name = "PropuestatrabajoCurriculum.findByFechaOfrecido", query = "SELECT p FROM PropuestatrabajoCurriculum p WHERE p.fechaOfrecido = :fechaOfrecido")
    , @NamedQuery(name = "PropuestatrabajoCurriculum.findByBorrado", query = "SELECT p FROM PropuestatrabajoCurriculum p WHERE p.borrado = :borrado")})
public class PropuestatrabajoCurriculum implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "correlativo")
    private int correlativo;
    @Column(name = "fechaOfrecido")
    @Temporal(TemporalType.DATE)
    private Date fechaOfrecido;
    @Column(name = "borrado")
    private Boolean borrado;
    @JoinColumn(name = "curriculum", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Curriculum curriculum;
    @JoinColumn(name = "propuestaTrabajo", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Propuestatrabajo propuestaTrabajo;

    public PropuestatrabajoCurriculum() {
    }

    public PropuestatrabajoCurriculum(Integer id) {
        this.id = id;
    }

    public PropuestatrabajoCurriculum(Integer id, int correlativo) {
        this.id = id;
        this.correlativo = correlativo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(int correlativo) {
        this.correlativo = correlativo;
    }

    public Date getFechaOfrecido() {
        return fechaOfrecido;
    }

    public void setFechaOfrecido(Date fechaOfrecido) {
        this.fechaOfrecido = fechaOfrecido;
    }

    public Boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(Boolean borrado) {
        this.borrado = borrado;
    }

    public Curriculum getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(Curriculum curriculum) {
        this.curriculum = curriculum;
    }

    public Propuestatrabajo getPropuestaTrabajo() {
        return propuestaTrabajo;
    }

    public void setPropuestaTrabajo(Propuestatrabajo propuestaTrabajo) {
        this.propuestaTrabajo = propuestaTrabajo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PropuestatrabajoCurriculum)) {
            return false;
        }
        PropuestatrabajoCurriculum other = (PropuestatrabajoCurriculum) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.workingbriefcase.persistencia.entities.PropuestatrabajoCurriculum[ id=" + id + " ]";
    }

}
