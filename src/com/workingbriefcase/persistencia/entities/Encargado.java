package com.workingbriefcase.persistencia.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Nombre de la clase: Encargado
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
@Entity
@Table(name = "encargado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Encargado.findAll", query = "SELECT e FROM Encargado e")
    , @NamedQuery(name = "Encargado.findById", query = "SELECT e FROM Encargado e WHERE e.id = :id")
    , @NamedQuery(name = "Encargado.findByNombre", query = "SELECT e FROM Encargado e WHERE e.nombre = :nombre")
    , @NamedQuery(name = "Encargado.findByApellido", query = "SELECT e FROM Encargado e WHERE e.apellido = :apellido")
    , @NamedQuery(name = "Encargado.LastId", query = "SELECT MAX(e.id) From Encargado e")   
})
public class Encargado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "apellido")
    private String apellido;
    @JoinColumn(name = "cargoDesepennado", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Cargo cargoDesepennado;
    @JoinColumn(name = "nacionalidad", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Pais nacionalidad;
    @JoinColumn(name = "profesionEjercida", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Profesion profesionEjercida;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "encargado")
    private List<Empresa> empresaList;

    public Encargado() {
    }

    public Encargado(Integer id) {
        this.id = id;
    }

    public Encargado(Integer id, String nombre, String apellido) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Cargo getCargoDesepennado() {
        return cargoDesepennado;
    }

    public void setCargoDesepennado(Cargo cargoDesepennado) {
        this.cargoDesepennado = cargoDesepennado;
    }

    public Pais getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(Pais nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public Profesion getProfesionEjercida() {
        return profesionEjercida;
    }

    public void setProfesionEjercida(Profesion profesionEjercida) {
        this.profesionEjercida = profesionEjercida;
    }

    @XmlTransient
    public List<Empresa> getEmpresaList() {
        return empresaList;
    }

    public void setEmpresaList(List<Empresa> empresaList) {
        this.empresaList = empresaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Encargado)) {
            return false;
        }
        Encargado other = (Encargado) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.workingbriefcase.persistencia.entities.Encargado[ id=" + id + " ]";
    }

}
