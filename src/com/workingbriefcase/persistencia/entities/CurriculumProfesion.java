package com.workingbriefcase.persistencia.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Nombre de la clase: CurriculumProfesion
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
@Entity
@Table(name = "curriculum_profesion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CurriculumProfesion.findAll", query = "SELECT c FROM CurriculumProfesion c")
    , @NamedQuery(name = "CurriculumProfesion.findById", query = "SELECT c FROM CurriculumProfesion c WHERE c.id = :id")
    , @NamedQuery(name = "CurriculumProfesion.findByCorrelativo", query = "SELECT c FROM CurriculumProfesion c WHERE c.correlativo = :correlativo")
    , @NamedQuery(name = "CurriculumProfesion.findByAnnosExperiencia", query = "SELECT c FROM CurriculumProfesion c WHERE c.annosExperiencia = :annosExperiencia")
    , @NamedQuery(name = "CurriculumProfesion.findByBorrado", query = "SELECT c FROM CurriculumProfesion c WHERE c.borrado = :borrado")
    , @NamedQuery(name = "CurriculumProfesion.findByAniosAndAreaLaboral", 
            
 query = "SELECT c FROM CurriculumProfesion c  JOIN c.curriculum cu  JOIN cu.candidatoPertenece ca  JOIN c.profesion p JOIN p.areaLaboral a WHERE c.annosExperiencia > :anios AND a.id = :idAreaLaboral"
            
                                                                                  
      )})
public class CurriculumProfesion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "correlativo")
    private int correlativo;
    @Basic(optional = false)
    @Column(name = "annosExperiencia")
    private int annosExperiencia;
    @Basic(optional = false)
    @Column(name = "borrado")
    private boolean borrado;
    @JoinColumn(name = "curriculum", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Curriculum curriculum;
    @JoinColumn(name = "profesion", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Profesion profesion;

    public CurriculumProfesion() {
    }

    public CurriculumProfesion(Integer id) {
        this.id = id;
    }

    public CurriculumProfesion(Integer id, int correlativo, int annosExperiencia, boolean borrado) {
        this.id = id;
        this.correlativo = correlativo;
        this.annosExperiencia = annosExperiencia;
        this.borrado = borrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(int correlativo) {
        this.correlativo = correlativo;
    }

    public int getAnnosExperiencia() {
        return annosExperiencia;
    }

    public void setAnnosExperiencia(int annosExperiencia) {
        this.annosExperiencia = annosExperiencia;
    }

    public boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(boolean borrado) {
        this.borrado = borrado;
    }

    public Curriculum getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(Curriculum curriculum) {
        this.curriculum = curriculum;
    }

    public Profesion getProfesion() {
        return profesion;
    }

    public void setProfesion(Profesion profesion) {
        this.profesion = profesion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CurriculumProfesion)) {
            return false;
        }
        CurriculumProfesion other = (CurriculumProfesion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.workingbriefcase.persistencia.entities.CurriculumProfesion[ id=" + id + " ]";
    }

}
