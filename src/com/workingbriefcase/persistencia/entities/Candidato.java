package com.workingbriefcase.persistencia.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Nombre de la clase: Candidato
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
@Entity
@Table(name = "candidato")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Candidato.findAll", query = "SELECT c FROM Candidato c")
    , @NamedQuery(name = "Candidato.findById", query = "SELECT c FROM Candidato c WHERE c.id = :id")
    , @NamedQuery(name = "Candidato.findByDui", query = "SELECT c FROM Candidato c WHERE c.dui = :dui")
    , @NamedQuery(name = "Candidato.findByNit", query = "SELECT c FROM Candidato c WHERE c.nit = :nit")
    , @NamedQuery(name = "Candidato.findByNombre", query = "SELECT c FROM Candidato c WHERE c.nombre = :nombre")
    , @NamedQuery(name = "Candidato.findByApellido", query = "SELECT c FROM Candidato c WHERE c.apellido = :apellido")
    , @NamedQuery(name = "Candidato.findByGenero", query = "SELECT c FROM Candidato c WHERE c.genero = :genero")
    , @NamedQuery(name = "Candidato.findByFechaNacimiento", query = "SELECT c FROM Candidato c WHERE c.fechaNacimiento = :fechaNacimiento")
    , @NamedQuery(name = "Candidato.findByDireccion", query = "SELECT c FROM Candidato c WHERE c.direccion = :direccion")
    , @NamedQuery(name = "Candidato.findByAutoDescripcion", query = "SELECT c FROM Candidato c WHERE c.autoDescripcion = :autoDescripcion")
    , @NamedQuery(name = "Candidato.findByTrabajando", query = "SELECT c FROM Candidato c WHERE c.trabajando = :trabajando")
    , @NamedQuery(name = "Candidato.findByPrivacidadPerfil", query = "SELECT c FROM Candidato c WHERE c.privacidadPerfil = :privacidadPerfil")
    , @NamedQuery(name = "Candidato.logIn" , query = "SELECT c FROM Candidato c INNER JOIN FETCH c.usuario WHERE c.usuario.nombre = :nombreUsuario AND c.usuario.contrasenna = :contrasenna")
})
public class Candidato implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "dui")
    private String dui;
    @Column(name = "nit")
    private String nit;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "apellido")
    private String apellido;
    @Basic(optional = false)
    @Column(name = "genero")
    private String genero;
    @Basic(optional = false)
    @Column(name = "fechaNacimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaNacimiento;
    @Lob
    @Column(name = "foto")
    private byte[] foto;
    @Column(name = "direccion")
    private String direccion;
    @Column(name = "autoDescripcion")
    private String autoDescripcion;
    @Basic(optional = false)
    @Column(name = "trabajando")
    private boolean trabajando;
    @Basic(optional = false)
    @Column(name = "privacidadPerfil")
    private int privacidadPerfil;
    @JoinColumn(name = "municipioActual", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Municipio municipioActual;
    @JoinColumn(name = "nacionalidad", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Pais nacionalidad;
    @JoinColumn(name = "usuario", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Usuario usuario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "candidatoPertenece", orphanRemoval = true)
    private List<Curriculum> curriculumList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "candidatoPertenece", orphanRemoval = true)
    private List<Curso> cursoList;

    public Candidato() {
    }

    public Candidato(Integer id) {
        this.id = id;
    }

    public Candidato(Integer id, String nombre, String apellido, String genero, Date fechaNacimiento, boolean trabajando, int privacidadPerfil) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.genero = genero;
        this.fechaNacimiento = fechaNacimiento;
        this.trabajando = trabajando;
        this.privacidadPerfil = privacidadPerfil;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDui() {
        return dui;
    }

    public void setDui(String dui) {
        this.dui = dui;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getAutoDescripcion() {
        return autoDescripcion;
    }

    public void setAutoDescripcion(String autoDescripcion) {
        this.autoDescripcion = autoDescripcion;
    }

    public boolean getTrabajando() {
        return trabajando;
    }

    public void setTrabajando(boolean trabajando) {
        this.trabajando = trabajando;
    }

    public int getPrivacidadPerfil() {
        return privacidadPerfil;
    }

    public void setPrivacidadPerfil(int privacidadPerfil) {
        this.privacidadPerfil = privacidadPerfil;
    }

    public Municipio getMunicipioActual() {
        return municipioActual;
    }

    public void setMunicipioActual(Municipio municipioActual) {
        this.municipioActual = municipioActual;
    }

    public Pais getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(Pais nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @XmlTransient
    public List<Curriculum> getCurriculumList() {
        return curriculumList;
    }

    public void setCurriculumList(List<Curriculum> curriculumList) {
        this.curriculumList = curriculumList;
    }

    @XmlTransient
    public List<Curso> getCursoList() {
        return cursoList;
    }

    public void setCursoList(List<Curso> cursoList) {
        this.cursoList = cursoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Candidato)) {
            return false;
        }
        Candidato other = (Candidato) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.workingbriefcase.persistencia.entities.Candidato[ id=" + id + " ]";
    }

}
