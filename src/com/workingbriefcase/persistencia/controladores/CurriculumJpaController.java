package com.workingbriefcase.persistencia.controladores;

import com.workingbriefcase.persistencia.controladores.exceptions.IllegalOrphanException;
import com.workingbriefcase.persistencia.controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.workingbriefcase.persistencia.entities.Candidato;
import com.workingbriefcase.persistencia.entities.Curriculum;
import com.workingbriefcase.persistencia.entities.PropuestatrabajoCurriculum;
import java.util.ArrayList;
import java.util.List;
import com.workingbriefcase.persistencia.entities.CurriculumProfesion;
import com.workingbriefcase.persistencia.entities.CurriculumCurso;
import com.workingbriefcase.persistencia.entities.CurriculumHabilidad;
import com.workingbriefcase.persistencia.entities.CurriculumIdioma;
import com.workingbriefcase.persistencia.entities.OfertalaboralCurriculum;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Nombre de la clase: CurriculumJpaController
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class CurriculumJpaController implements Serializable {

     public CurriculumJpaController() {
        this.emf = Persistence.createEntityManagerFactory("Working-BriefcasePU");
    }
    public CurriculumJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public Curriculum obtenerByCandidato(int candidato){
        Curriculum salida;
        EntityManager em;
        Query query;
        List resultado;
        
        salida = null;
        em = null;
        try {
            em = getEntityManager();
            
            //creando consulta
            query =em.createNamedQuery("Curriculum.findByCandidatos");
            query.setParameter("idCandidato", candidato);
            
            //"ejecutando" query
            resultado = query.getResultList();
            
            //solo debería haber 1 item en el resultado si tuvo exito sino 0
            if(resultado.size() > 0){
                salida = (Curriculum) resultado.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();      
        }finally{
            //cerrar el EntityManger
            if (em != null) {
                em.close();
            }
        }
        
        return salida;
    }
    
    
    public void create(Curriculum curriculum) {
        if (curriculum.getPropuestatrabajoCurriculumList() == null) {
            curriculum.setPropuestatrabajoCurriculumList(new ArrayList<PropuestatrabajoCurriculum>());
        }
        if (curriculum.getCurriculumProfesionList() == null) {
            curriculum.setCurriculumProfesionList(new ArrayList<CurriculumProfesion>());
        }
        if (curriculum.getCurriculumCursoList() == null) {
            curriculum.setCurriculumCursoList(new ArrayList<CurriculumCurso>());
        }
        if (curriculum.getCurriculumHabilidadList() == null) {
            curriculum.setCurriculumHabilidadList(new ArrayList<CurriculumHabilidad>());
        }
        if (curriculum.getCurriculumIdiomaList() == null) {
            curriculum.setCurriculumIdiomaList(new ArrayList<CurriculumIdioma>());
        }
        if (curriculum.getOfertalaboralCurriculumList() == null) {
            curriculum.setOfertalaboralCurriculumList(new ArrayList<OfertalaboralCurriculum>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Candidato candidatoPertenece = curriculum.getCandidatoPertenece();
            if (candidatoPertenece != null) {
                candidatoPertenece = em.getReference(candidatoPertenece.getClass(), candidatoPertenece.getId());
                curriculum.setCandidatoPertenece(candidatoPertenece);
            }
            List<PropuestatrabajoCurriculum> attachedPropuestatrabajoCurriculumList = new ArrayList<PropuestatrabajoCurriculum>();
            for (PropuestatrabajoCurriculum propuestatrabajoCurriculumListPropuestatrabajoCurriculumToAttach : curriculum.getPropuestatrabajoCurriculumList()) {
                propuestatrabajoCurriculumListPropuestatrabajoCurriculumToAttach = em.getReference(propuestatrabajoCurriculumListPropuestatrabajoCurriculumToAttach.getClass(), propuestatrabajoCurriculumListPropuestatrabajoCurriculumToAttach.getId());
                attachedPropuestatrabajoCurriculumList.add(propuestatrabajoCurriculumListPropuestatrabajoCurriculumToAttach);
            }
            curriculum.setPropuestatrabajoCurriculumList(attachedPropuestatrabajoCurriculumList);
            List<CurriculumProfesion> attachedCurriculumProfesionList = new ArrayList<CurriculumProfesion>();
            for (CurriculumProfesion curriculumProfesionListCurriculumProfesionToAttach : curriculum.getCurriculumProfesionList()) {
                curriculumProfesionListCurriculumProfesionToAttach = em.getReference(curriculumProfesionListCurriculumProfesionToAttach.getClass(), curriculumProfesionListCurriculumProfesionToAttach.getId());
                attachedCurriculumProfesionList.add(curriculumProfesionListCurriculumProfesionToAttach);
            }
            curriculum.setCurriculumProfesionList(attachedCurriculumProfesionList);
            List<CurriculumCurso> attachedCurriculumCursoList = new ArrayList<CurriculumCurso>();
            for (CurriculumCurso curriculumCursoListCurriculumCursoToAttach : curriculum.getCurriculumCursoList()) {
                curriculumCursoListCurriculumCursoToAttach = em.getReference(curriculumCursoListCurriculumCursoToAttach.getClass(), curriculumCursoListCurriculumCursoToAttach.getId());
                attachedCurriculumCursoList.add(curriculumCursoListCurriculumCursoToAttach);
            }
            curriculum.setCurriculumCursoList(attachedCurriculumCursoList);
            List<CurriculumHabilidad> attachedCurriculumHabilidadList = new ArrayList<CurriculumHabilidad>();
            for (CurriculumHabilidad curriculumHabilidadListCurriculumHabilidadToAttach : curriculum.getCurriculumHabilidadList()) {
                curriculumHabilidadListCurriculumHabilidadToAttach = em.getReference(curriculumHabilidadListCurriculumHabilidadToAttach.getClass(), curriculumHabilidadListCurriculumHabilidadToAttach.getId());
                attachedCurriculumHabilidadList.add(curriculumHabilidadListCurriculumHabilidadToAttach);
            }
            curriculum.setCurriculumHabilidadList(attachedCurriculumHabilidadList);
            List<CurriculumIdioma> attachedCurriculumIdiomaList = new ArrayList<CurriculumIdioma>();
            for (CurriculumIdioma curriculumIdiomaListCurriculumIdiomaToAttach : curriculum.getCurriculumIdiomaList()) {
                curriculumIdiomaListCurriculumIdiomaToAttach = em.getReference(curriculumIdiomaListCurriculumIdiomaToAttach.getClass(), curriculumIdiomaListCurriculumIdiomaToAttach.getId());
                attachedCurriculumIdiomaList.add(curriculumIdiomaListCurriculumIdiomaToAttach);
            }
            curriculum.setCurriculumIdiomaList(attachedCurriculumIdiomaList);
            List<OfertalaboralCurriculum> attachedOfertalaboralCurriculumList = new ArrayList<OfertalaboralCurriculum>();
            for (OfertalaboralCurriculum ofertalaboralCurriculumListOfertalaboralCurriculumToAttach : curriculum.getOfertalaboralCurriculumList()) {
                ofertalaboralCurriculumListOfertalaboralCurriculumToAttach = em.getReference(ofertalaboralCurriculumListOfertalaboralCurriculumToAttach.getClass(), ofertalaboralCurriculumListOfertalaboralCurriculumToAttach.getId());
                attachedOfertalaboralCurriculumList.add(ofertalaboralCurriculumListOfertalaboralCurriculumToAttach);
            }
            curriculum.setOfertalaboralCurriculumList(attachedOfertalaboralCurriculumList);
            em.persist(curriculum);
            if (candidatoPertenece != null) {
                candidatoPertenece.getCurriculumList().add(curriculum);
                candidatoPertenece = em.merge(candidatoPertenece);
            }
            for (PropuestatrabajoCurriculum propuestatrabajoCurriculumListPropuestatrabajoCurriculum : curriculum.getPropuestatrabajoCurriculumList()) {
                Curriculum oldCurriculumOfPropuestatrabajoCurriculumListPropuestatrabajoCurriculum = propuestatrabajoCurriculumListPropuestatrabajoCurriculum.getCurriculum();
                propuestatrabajoCurriculumListPropuestatrabajoCurriculum.setCurriculum(curriculum);
                propuestatrabajoCurriculumListPropuestatrabajoCurriculum = em.merge(propuestatrabajoCurriculumListPropuestatrabajoCurriculum);
                if (oldCurriculumOfPropuestatrabajoCurriculumListPropuestatrabajoCurriculum != null) {
                    oldCurriculumOfPropuestatrabajoCurriculumListPropuestatrabajoCurriculum.getPropuestatrabajoCurriculumList().remove(propuestatrabajoCurriculumListPropuestatrabajoCurriculum);
                    oldCurriculumOfPropuestatrabajoCurriculumListPropuestatrabajoCurriculum = em.merge(oldCurriculumOfPropuestatrabajoCurriculumListPropuestatrabajoCurriculum);
                }
            }
            for (CurriculumProfesion curriculumProfesionListCurriculumProfesion : curriculum.getCurriculumProfesionList()) {
                Curriculum oldCurriculumOfCurriculumProfesionListCurriculumProfesion = curriculumProfesionListCurriculumProfesion.getCurriculum();
                curriculumProfesionListCurriculumProfesion.setCurriculum(curriculum);
                curriculumProfesionListCurriculumProfesion = em.merge(curriculumProfesionListCurriculumProfesion);
                if (oldCurriculumOfCurriculumProfesionListCurriculumProfesion != null) {
                    oldCurriculumOfCurriculumProfesionListCurriculumProfesion.getCurriculumProfesionList().remove(curriculumProfesionListCurriculumProfesion);
                    oldCurriculumOfCurriculumProfesionListCurriculumProfesion = em.merge(oldCurriculumOfCurriculumProfesionListCurriculumProfesion);
                }
            }
            for (CurriculumCurso curriculumCursoListCurriculumCurso : curriculum.getCurriculumCursoList()) {
                Curriculum oldCurriculumOfCurriculumCursoListCurriculumCurso = curriculumCursoListCurriculumCurso.getCurriculum();
                curriculumCursoListCurriculumCurso.setCurriculum(curriculum);
                curriculumCursoListCurriculumCurso = em.merge(curriculumCursoListCurriculumCurso);
                if (oldCurriculumOfCurriculumCursoListCurriculumCurso != null) {
                    oldCurriculumOfCurriculumCursoListCurriculumCurso.getCurriculumCursoList().remove(curriculumCursoListCurriculumCurso);
                    oldCurriculumOfCurriculumCursoListCurriculumCurso = em.merge(oldCurriculumOfCurriculumCursoListCurriculumCurso);
                }
            }
            for (CurriculumHabilidad curriculumHabilidadListCurriculumHabilidad : curriculum.getCurriculumHabilidadList()) {
                Curriculum oldCurriculumOfCurriculumHabilidadListCurriculumHabilidad = curriculumHabilidadListCurriculumHabilidad.getCurriculum();
                curriculumHabilidadListCurriculumHabilidad.setCurriculum(curriculum);
                curriculumHabilidadListCurriculumHabilidad = em.merge(curriculumHabilidadListCurriculumHabilidad);
                if (oldCurriculumOfCurriculumHabilidadListCurriculumHabilidad != null) {
                    oldCurriculumOfCurriculumHabilidadListCurriculumHabilidad.getCurriculumHabilidadList().remove(curriculumHabilidadListCurriculumHabilidad);
                    oldCurriculumOfCurriculumHabilidadListCurriculumHabilidad = em.merge(oldCurriculumOfCurriculumHabilidadListCurriculumHabilidad);
                }
            }
            for (CurriculumIdioma curriculumIdiomaListCurriculumIdioma : curriculum.getCurriculumIdiomaList()) {
                Curriculum oldCurriculumOfCurriculumIdiomaListCurriculumIdioma = curriculumIdiomaListCurriculumIdioma.getCurriculum();
                curriculumIdiomaListCurriculumIdioma.setCurriculum(curriculum);
                curriculumIdiomaListCurriculumIdioma = em.merge(curriculumIdiomaListCurriculumIdioma);
                if (oldCurriculumOfCurriculumIdiomaListCurriculumIdioma != null) {
                    oldCurriculumOfCurriculumIdiomaListCurriculumIdioma.getCurriculumIdiomaList().remove(curriculumIdiomaListCurriculumIdioma);
                    oldCurriculumOfCurriculumIdiomaListCurriculumIdioma = em.merge(oldCurriculumOfCurriculumIdiomaListCurriculumIdioma);
                }
            }
            for (OfertalaboralCurriculum ofertalaboralCurriculumListOfertalaboralCurriculum : curriculum.getOfertalaboralCurriculumList()) {
                Curriculum oldCurriculumOfOfertalaboralCurriculumListOfertalaboralCurriculum = ofertalaboralCurriculumListOfertalaboralCurriculum.getCurriculum();
                ofertalaboralCurriculumListOfertalaboralCurriculum.setCurriculum(curriculum);
                ofertalaboralCurriculumListOfertalaboralCurriculum = em.merge(ofertalaboralCurriculumListOfertalaboralCurriculum);
                if (oldCurriculumOfOfertalaboralCurriculumListOfertalaboralCurriculum != null) {
                    oldCurriculumOfOfertalaboralCurriculumListOfertalaboralCurriculum.getOfertalaboralCurriculumList().remove(ofertalaboralCurriculumListOfertalaboralCurriculum);
                    oldCurriculumOfOfertalaboralCurriculumListOfertalaboralCurriculum = em.merge(oldCurriculumOfOfertalaboralCurriculumListOfertalaboralCurriculum);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Curriculum curriculum) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Curriculum persistentCurriculum = em.find(Curriculum.class, curriculum.getId());
            Candidato candidatoPerteneceOld = persistentCurriculum.getCandidatoPertenece();
            Candidato candidatoPerteneceNew = curriculum.getCandidatoPertenece();
            List<PropuestatrabajoCurriculum> propuestatrabajoCurriculumListOld = persistentCurriculum.getPropuestatrabajoCurriculumList();
            List<PropuestatrabajoCurriculum> propuestatrabajoCurriculumListNew = curriculum.getPropuestatrabajoCurriculumList();
            List<CurriculumProfesion> curriculumProfesionListOld = persistentCurriculum.getCurriculumProfesionList();
            List<CurriculumProfesion> curriculumProfesionListNew = curriculum.getCurriculumProfesionList();
            List<CurriculumCurso> curriculumCursoListOld = persistentCurriculum.getCurriculumCursoList();
            List<CurriculumCurso> curriculumCursoListNew = curriculum.getCurriculumCursoList();
            List<CurriculumHabilidad> curriculumHabilidadListOld = persistentCurriculum.getCurriculumHabilidadList();
            List<CurriculumHabilidad> curriculumHabilidadListNew = curriculum.getCurriculumHabilidadList();
            List<CurriculumIdioma> curriculumIdiomaListOld = persistentCurriculum.getCurriculumIdiomaList();
            List<CurriculumIdioma> curriculumIdiomaListNew = curriculum.getCurriculumIdiomaList();
            List<OfertalaboralCurriculum> ofertalaboralCurriculumListOld = persistentCurriculum.getOfertalaboralCurriculumList();
            List<OfertalaboralCurriculum> ofertalaboralCurriculumListNew = curriculum.getOfertalaboralCurriculumList();
            List<String> illegalOrphanMessages = null;
            for (PropuestatrabajoCurriculum propuestatrabajoCurriculumListOldPropuestatrabajoCurriculum : propuestatrabajoCurriculumListOld) {
                if (!propuestatrabajoCurriculumListNew.contains(propuestatrabajoCurriculumListOldPropuestatrabajoCurriculum)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain PropuestatrabajoCurriculum " + propuestatrabajoCurriculumListOldPropuestatrabajoCurriculum + " since its curriculum field is not nullable.");
                }
            }
            for (CurriculumProfesion curriculumProfesionListOldCurriculumProfesion : curriculumProfesionListOld) {
                if (!curriculumProfesionListNew.contains(curriculumProfesionListOldCurriculumProfesion)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain CurriculumProfesion " + curriculumProfesionListOldCurriculumProfesion + " since its curriculum field is not nullable.");
                }
            }
            for (CurriculumCurso curriculumCursoListOldCurriculumCurso : curriculumCursoListOld) {
                if (!curriculumCursoListNew.contains(curriculumCursoListOldCurriculumCurso)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain CurriculumCurso " + curriculumCursoListOldCurriculumCurso + " since its curriculum field is not nullable.");
                }
            }
            for (CurriculumHabilidad curriculumHabilidadListOldCurriculumHabilidad : curriculumHabilidadListOld) {
                if (!curriculumHabilidadListNew.contains(curriculumHabilidadListOldCurriculumHabilidad)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain CurriculumHabilidad " + curriculumHabilidadListOldCurriculumHabilidad + " since its curriculum field is not nullable.");
                }
            }
            for (CurriculumIdioma curriculumIdiomaListOldCurriculumIdioma : curriculumIdiomaListOld) {
                if (!curriculumIdiomaListNew.contains(curriculumIdiomaListOldCurriculumIdioma)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain CurriculumIdioma " + curriculumIdiomaListOldCurriculumIdioma + " since its curriculum field is not nullable.");
                }
            }
            for (OfertalaboralCurriculum ofertalaboralCurriculumListOldOfertalaboralCurriculum : ofertalaboralCurriculumListOld) {
                if (!ofertalaboralCurriculumListNew.contains(ofertalaboralCurriculumListOldOfertalaboralCurriculum)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain OfertalaboralCurriculum " + ofertalaboralCurriculumListOldOfertalaboralCurriculum + " since its curriculum field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (candidatoPerteneceNew != null) {
                candidatoPerteneceNew = em.getReference(candidatoPerteneceNew.getClass(), candidatoPerteneceNew.getId());
                curriculum.setCandidatoPertenece(candidatoPerteneceNew);
            }
            List<PropuestatrabajoCurriculum> attachedPropuestatrabajoCurriculumListNew = new ArrayList<PropuestatrabajoCurriculum>();
            for (PropuestatrabajoCurriculum propuestatrabajoCurriculumListNewPropuestatrabajoCurriculumToAttach : propuestatrabajoCurriculumListNew) {
                propuestatrabajoCurriculumListNewPropuestatrabajoCurriculumToAttach = em.getReference(propuestatrabajoCurriculumListNewPropuestatrabajoCurriculumToAttach.getClass(), propuestatrabajoCurriculumListNewPropuestatrabajoCurriculumToAttach.getId());
                attachedPropuestatrabajoCurriculumListNew.add(propuestatrabajoCurriculumListNewPropuestatrabajoCurriculumToAttach);
            }
            propuestatrabajoCurriculumListNew = attachedPropuestatrabajoCurriculumListNew;
            curriculum.setPropuestatrabajoCurriculumList(propuestatrabajoCurriculumListNew);
            List<CurriculumProfesion> attachedCurriculumProfesionListNew = new ArrayList<CurriculumProfesion>();
            for (CurriculumProfesion curriculumProfesionListNewCurriculumProfesionToAttach : curriculumProfesionListNew) {
                curriculumProfesionListNewCurriculumProfesionToAttach = em.getReference(curriculumProfesionListNewCurriculumProfesionToAttach.getClass(), curriculumProfesionListNewCurriculumProfesionToAttach.getId());
                attachedCurriculumProfesionListNew.add(curriculumProfesionListNewCurriculumProfesionToAttach);
            }
            curriculumProfesionListNew = attachedCurriculumProfesionListNew;
            curriculum.setCurriculumProfesionList(curriculumProfesionListNew);
            List<CurriculumCurso> attachedCurriculumCursoListNew = new ArrayList<CurriculumCurso>();
            for (CurriculumCurso curriculumCursoListNewCurriculumCursoToAttach : curriculumCursoListNew) {
                curriculumCursoListNewCurriculumCursoToAttach = em.getReference(curriculumCursoListNewCurriculumCursoToAttach.getClass(), curriculumCursoListNewCurriculumCursoToAttach.getId());
                attachedCurriculumCursoListNew.add(curriculumCursoListNewCurriculumCursoToAttach);
            }
            curriculumCursoListNew = attachedCurriculumCursoListNew;
            curriculum.setCurriculumCursoList(curriculumCursoListNew);
            List<CurriculumHabilidad> attachedCurriculumHabilidadListNew = new ArrayList<CurriculumHabilidad>();
            for (CurriculumHabilidad curriculumHabilidadListNewCurriculumHabilidadToAttach : curriculumHabilidadListNew) {
                curriculumHabilidadListNewCurriculumHabilidadToAttach = em.getReference(curriculumHabilidadListNewCurriculumHabilidadToAttach.getClass(), curriculumHabilidadListNewCurriculumHabilidadToAttach.getId());
                attachedCurriculumHabilidadListNew.add(curriculumHabilidadListNewCurriculumHabilidadToAttach);
            }
            curriculumHabilidadListNew = attachedCurriculumHabilidadListNew;
            curriculum.setCurriculumHabilidadList(curriculumHabilidadListNew);
            List<CurriculumIdioma> attachedCurriculumIdiomaListNew = new ArrayList<CurriculumIdioma>();
            for (CurriculumIdioma curriculumIdiomaListNewCurriculumIdiomaToAttach : curriculumIdiomaListNew) {
                curriculumIdiomaListNewCurriculumIdiomaToAttach = em.getReference(curriculumIdiomaListNewCurriculumIdiomaToAttach.getClass(), curriculumIdiomaListNewCurriculumIdiomaToAttach.getId());
                attachedCurriculumIdiomaListNew.add(curriculumIdiomaListNewCurriculumIdiomaToAttach);
            }
            curriculumIdiomaListNew = attachedCurriculumIdiomaListNew;
            curriculum.setCurriculumIdiomaList(curriculumIdiomaListNew);
            List<OfertalaboralCurriculum> attachedOfertalaboralCurriculumListNew = new ArrayList<OfertalaboralCurriculum>();
            for (OfertalaboralCurriculum ofertalaboralCurriculumListNewOfertalaboralCurriculumToAttach : ofertalaboralCurriculumListNew) {
                ofertalaboralCurriculumListNewOfertalaboralCurriculumToAttach = em.getReference(ofertalaboralCurriculumListNewOfertalaboralCurriculumToAttach.getClass(), ofertalaboralCurriculumListNewOfertalaboralCurriculumToAttach.getId());
                attachedOfertalaboralCurriculumListNew.add(ofertalaboralCurriculumListNewOfertalaboralCurriculumToAttach);
            }
            ofertalaboralCurriculumListNew = attachedOfertalaboralCurriculumListNew;
            curriculum.setOfertalaboralCurriculumList(ofertalaboralCurriculumListNew);
            curriculum = em.merge(curriculum);
            if (candidatoPerteneceOld != null && !candidatoPerteneceOld.equals(candidatoPerteneceNew)) {
                candidatoPerteneceOld.getCurriculumList().remove(curriculum);
                candidatoPerteneceOld = em.merge(candidatoPerteneceOld);
            }
            if (candidatoPerteneceNew != null && !candidatoPerteneceNew.equals(candidatoPerteneceOld)) {
                candidatoPerteneceNew.getCurriculumList().add(curriculum);
                candidatoPerteneceNew = em.merge(candidatoPerteneceNew);
            }
            for (PropuestatrabajoCurriculum propuestatrabajoCurriculumListNewPropuestatrabajoCurriculum : propuestatrabajoCurriculumListNew) {
                if (!propuestatrabajoCurriculumListOld.contains(propuestatrabajoCurriculumListNewPropuestatrabajoCurriculum)) {
                    Curriculum oldCurriculumOfPropuestatrabajoCurriculumListNewPropuestatrabajoCurriculum = propuestatrabajoCurriculumListNewPropuestatrabajoCurriculum.getCurriculum();
                    propuestatrabajoCurriculumListNewPropuestatrabajoCurriculum.setCurriculum(curriculum);
                    propuestatrabajoCurriculumListNewPropuestatrabajoCurriculum = em.merge(propuestatrabajoCurriculumListNewPropuestatrabajoCurriculum);
                    if (oldCurriculumOfPropuestatrabajoCurriculumListNewPropuestatrabajoCurriculum != null && !oldCurriculumOfPropuestatrabajoCurriculumListNewPropuestatrabajoCurriculum.equals(curriculum)) {
                        oldCurriculumOfPropuestatrabajoCurriculumListNewPropuestatrabajoCurriculum.getPropuestatrabajoCurriculumList().remove(propuestatrabajoCurriculumListNewPropuestatrabajoCurriculum);
                        oldCurriculumOfPropuestatrabajoCurriculumListNewPropuestatrabajoCurriculum = em.merge(oldCurriculumOfPropuestatrabajoCurriculumListNewPropuestatrabajoCurriculum);
                    }
                }
            }
            for (CurriculumProfesion curriculumProfesionListNewCurriculumProfesion : curriculumProfesionListNew) {
                if (!curriculumProfesionListOld.contains(curriculumProfesionListNewCurriculumProfesion)) {
                    Curriculum oldCurriculumOfCurriculumProfesionListNewCurriculumProfesion = curriculumProfesionListNewCurriculumProfesion.getCurriculum();
                    curriculumProfesionListNewCurriculumProfesion.setCurriculum(curriculum);
                    curriculumProfesionListNewCurriculumProfesion = em.merge(curriculumProfesionListNewCurriculumProfesion);
                    if (oldCurriculumOfCurriculumProfesionListNewCurriculumProfesion != null && !oldCurriculumOfCurriculumProfesionListNewCurriculumProfesion.equals(curriculum)) {
                        oldCurriculumOfCurriculumProfesionListNewCurriculumProfesion.getCurriculumProfesionList().remove(curriculumProfesionListNewCurriculumProfesion);
                        oldCurriculumOfCurriculumProfesionListNewCurriculumProfesion = em.merge(oldCurriculumOfCurriculumProfesionListNewCurriculumProfesion);
                    }
                }
            }
            for (CurriculumCurso curriculumCursoListNewCurriculumCurso : curriculumCursoListNew) {
                if (!curriculumCursoListOld.contains(curriculumCursoListNewCurriculumCurso)) {
                    Curriculum oldCurriculumOfCurriculumCursoListNewCurriculumCurso = curriculumCursoListNewCurriculumCurso.getCurriculum();
                    curriculumCursoListNewCurriculumCurso.setCurriculum(curriculum);
                    curriculumCursoListNewCurriculumCurso = em.merge(curriculumCursoListNewCurriculumCurso);
                    if (oldCurriculumOfCurriculumCursoListNewCurriculumCurso != null && !oldCurriculumOfCurriculumCursoListNewCurriculumCurso.equals(curriculum)) {
                        oldCurriculumOfCurriculumCursoListNewCurriculumCurso.getCurriculumCursoList().remove(curriculumCursoListNewCurriculumCurso);
                        oldCurriculumOfCurriculumCursoListNewCurriculumCurso = em.merge(oldCurriculumOfCurriculumCursoListNewCurriculumCurso);
                    }
                }
            }
            for (CurriculumHabilidad curriculumHabilidadListNewCurriculumHabilidad : curriculumHabilidadListNew) {
                if (!curriculumHabilidadListOld.contains(curriculumHabilidadListNewCurriculumHabilidad)) {
                    Curriculum oldCurriculumOfCurriculumHabilidadListNewCurriculumHabilidad = curriculumHabilidadListNewCurriculumHabilidad.getCurriculum();
                    curriculumHabilidadListNewCurriculumHabilidad.setCurriculum(curriculum);
                    curriculumHabilidadListNewCurriculumHabilidad = em.merge(curriculumHabilidadListNewCurriculumHabilidad);
                    if (oldCurriculumOfCurriculumHabilidadListNewCurriculumHabilidad != null && !oldCurriculumOfCurriculumHabilidadListNewCurriculumHabilidad.equals(curriculum)) {
                        oldCurriculumOfCurriculumHabilidadListNewCurriculumHabilidad.getCurriculumHabilidadList().remove(curriculumHabilidadListNewCurriculumHabilidad);
                        oldCurriculumOfCurriculumHabilidadListNewCurriculumHabilidad = em.merge(oldCurriculumOfCurriculumHabilidadListNewCurriculumHabilidad);
                    }
                }
            }
            for (CurriculumIdioma curriculumIdiomaListNewCurriculumIdioma : curriculumIdiomaListNew) {
                if (!curriculumIdiomaListOld.contains(curriculumIdiomaListNewCurriculumIdioma)) {
                    Curriculum oldCurriculumOfCurriculumIdiomaListNewCurriculumIdioma = curriculumIdiomaListNewCurriculumIdioma.getCurriculum();
                    curriculumIdiomaListNewCurriculumIdioma.setCurriculum(curriculum);
                    curriculumIdiomaListNewCurriculumIdioma = em.merge(curriculumIdiomaListNewCurriculumIdioma);
                    if (oldCurriculumOfCurriculumIdiomaListNewCurriculumIdioma != null && !oldCurriculumOfCurriculumIdiomaListNewCurriculumIdioma.equals(curriculum)) {
                        oldCurriculumOfCurriculumIdiomaListNewCurriculumIdioma.getCurriculumIdiomaList().remove(curriculumIdiomaListNewCurriculumIdioma);
                        oldCurriculumOfCurriculumIdiomaListNewCurriculumIdioma = em.merge(oldCurriculumOfCurriculumIdiomaListNewCurriculumIdioma);
                    }
                }
            }
            for (OfertalaboralCurriculum ofertalaboralCurriculumListNewOfertalaboralCurriculum : ofertalaboralCurriculumListNew) {
                if (!ofertalaboralCurriculumListOld.contains(ofertalaboralCurriculumListNewOfertalaboralCurriculum)) {
                    Curriculum oldCurriculumOfOfertalaboralCurriculumListNewOfertalaboralCurriculum = ofertalaboralCurriculumListNewOfertalaboralCurriculum.getCurriculum();
                    ofertalaboralCurriculumListNewOfertalaboralCurriculum.setCurriculum(curriculum);
                    ofertalaboralCurriculumListNewOfertalaboralCurriculum = em.merge(ofertalaboralCurriculumListNewOfertalaboralCurriculum);
                    if (oldCurriculumOfOfertalaboralCurriculumListNewOfertalaboralCurriculum != null && !oldCurriculumOfOfertalaboralCurriculumListNewOfertalaboralCurriculum.equals(curriculum)) {
                        oldCurriculumOfOfertalaboralCurriculumListNewOfertalaboralCurriculum.getOfertalaboralCurriculumList().remove(ofertalaboralCurriculumListNewOfertalaboralCurriculum);
                        oldCurriculumOfOfertalaboralCurriculumListNewOfertalaboralCurriculum = em.merge(oldCurriculumOfOfertalaboralCurriculumListNewOfertalaboralCurriculum);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = curriculum.getId();
                if (findCurriculum(id) == null) {
                    throw new NonexistentEntityException("The curriculum with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Curriculum curriculum;
            try {
                curriculum = em.getReference(Curriculum.class, id);
                curriculum.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The curriculum with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<PropuestatrabajoCurriculum> propuestatrabajoCurriculumListOrphanCheck = curriculum.getPropuestatrabajoCurriculumList();
            for (PropuestatrabajoCurriculum propuestatrabajoCurriculumListOrphanCheckPropuestatrabajoCurriculum : propuestatrabajoCurriculumListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Curriculum (" + curriculum + ") cannot be destroyed since the PropuestatrabajoCurriculum " + propuestatrabajoCurriculumListOrphanCheckPropuestatrabajoCurriculum + " in its propuestatrabajoCurriculumList field has a non-nullable curriculum field.");
            }
            List<CurriculumProfesion> curriculumProfesionListOrphanCheck = curriculum.getCurriculumProfesionList();
            for (CurriculumProfesion curriculumProfesionListOrphanCheckCurriculumProfesion : curriculumProfesionListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Curriculum (" + curriculum + ") cannot be destroyed since the CurriculumProfesion " + curriculumProfesionListOrphanCheckCurriculumProfesion + " in its curriculumProfesionList field has a non-nullable curriculum field.");
            }
            List<CurriculumCurso> curriculumCursoListOrphanCheck = curriculum.getCurriculumCursoList();
            for (CurriculumCurso curriculumCursoListOrphanCheckCurriculumCurso : curriculumCursoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Curriculum (" + curriculum + ") cannot be destroyed since the CurriculumCurso " + curriculumCursoListOrphanCheckCurriculumCurso + " in its curriculumCursoList field has a non-nullable curriculum field.");
            }
            List<CurriculumHabilidad> curriculumHabilidadListOrphanCheck = curriculum.getCurriculumHabilidadList();
            for (CurriculumHabilidad curriculumHabilidadListOrphanCheckCurriculumHabilidad : curriculumHabilidadListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Curriculum (" + curriculum + ") cannot be destroyed since the CurriculumHabilidad " + curriculumHabilidadListOrphanCheckCurriculumHabilidad + " in its curriculumHabilidadList field has a non-nullable curriculum field.");
            }
            List<CurriculumIdioma> curriculumIdiomaListOrphanCheck = curriculum.getCurriculumIdiomaList();
            for (CurriculumIdioma curriculumIdiomaListOrphanCheckCurriculumIdioma : curriculumIdiomaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Curriculum (" + curriculum + ") cannot be destroyed since the CurriculumIdioma " + curriculumIdiomaListOrphanCheckCurriculumIdioma + " in its curriculumIdiomaList field has a non-nullable curriculum field.");
            }
            List<OfertalaboralCurriculum> ofertalaboralCurriculumListOrphanCheck = curriculum.getOfertalaboralCurriculumList();
            for (OfertalaboralCurriculum ofertalaboralCurriculumListOrphanCheckOfertalaboralCurriculum : ofertalaboralCurriculumListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Curriculum (" + curriculum + ") cannot be destroyed since the OfertalaboralCurriculum " + ofertalaboralCurriculumListOrphanCheckOfertalaboralCurriculum + " in its ofertalaboralCurriculumList field has a non-nullable curriculum field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Candidato candidatoPertenece = curriculum.getCandidatoPertenece();
            if (candidatoPertenece != null) {
                candidatoPertenece.getCurriculumList().remove(curriculum);
                candidatoPertenece = em.merge(candidatoPertenece);
            }
            em.remove(curriculum);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Curriculum> findCurriculumEntities() {
        return findCurriculumEntities(true, -1, -1);
    }

    public List<Curriculum> findCurriculumEntities(int maxResults, int firstResult) {
        return findCurriculumEntities(false, maxResults, firstResult);
    }

    private List<Curriculum> findCurriculumEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Curriculum.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Curriculum findCurriculum(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Curriculum.class, id);
        } finally {
            em.close();
        }
    }

    public int getCurriculumCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Curriculum> rt = cq.from(Curriculum.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
