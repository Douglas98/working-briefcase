package com.workingbriefcase.persistencia.controladores;

import com.workingbriefcase.persistencia.controladores.exceptions.NonexistentEntityException;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.workingbriefcase.persistencia.entities.Cargo;
import com.workingbriefcase.persistencia.entities.Empleadoasalariado;
import com.workingbriefcase.persistencia.entities.Municipio;
import com.workingbriefcase.persistencia.entities.Pais;
import com.workingbriefcase.persistencia.entities.Profesion;
import com.workingbriefcase.persistencia.entities.Usuario;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JOptionPane;

/**
 * Nombre de la clase: EmpleadoasalariadoJpaController
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class EmpleadoasalariadoJpaController implements Serializable {

    public EmpleadoasalariadoJpaController() {
        this.emf = Persistence.createEntityManagerFactory("Working-BriefcasePU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Empleadoasalariado empleadoasalariado) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cargo cargoDesempennado = empleadoasalariado.getCargoDesempennado();
            if (cargoDesempennado != null) {
                cargoDesempennado = em.getReference(cargoDesempennado.getClass(), cargoDesempennado.getId());
                empleadoasalariado.setCargoDesempennado(cargoDesempennado);
            }
            Municipio municipioActual = empleadoasalariado.getMunicipioActual();
            if (municipioActual != null) {
                municipioActual = em.getReference(municipioActual.getClass(), municipioActual.getId());
                empleadoasalariado.setMunicipioActual(municipioActual);
            }
            Pais nacionalidad = empleadoasalariado.getNacionalidad();
            if (nacionalidad != null) {
                nacionalidad = em.getReference(nacionalidad.getClass(), nacionalidad.getId());
                empleadoasalariado.setNacionalidad(nacionalidad);
            }
            Profesion profesionEjercida = empleadoasalariado.getProfesionEjercida();
            if (profesionEjercida != null) {
                profesionEjercida = em.getReference(profesionEjercida.getClass(), profesionEjercida.getId());
                empleadoasalariado.setProfesionEjercida(profesionEjercida);
            }
            Usuario usuario = empleadoasalariado.getUsuario();
            if (usuario != null) {
                usuario = em.getReference(usuario.getClass(), usuario.getId());
                empleadoasalariado.setUsuario(usuario);
            }
            em.persist(empleadoasalariado);
            if (cargoDesempennado != null) {
                cargoDesempennado.getEmpleadoasalariadoList().add(empleadoasalariado);
                cargoDesempennado = em.merge(cargoDesempennado);
            }
            if (municipioActual != null) {
                municipioActual.getEmpleadoasalariadoList().add(empleadoasalariado);
                municipioActual = em.merge(municipioActual);
            }
            if (nacionalidad != null) {
                nacionalidad.getEmpleadoasalariadoList().add(empleadoasalariado);
                nacionalidad = em.merge(nacionalidad);
            }
            if (profesionEjercida != null) {
                profesionEjercida.getEmpleadoasalariadoList().add(empleadoasalariado);
                profesionEjercida = em.merge(profesionEjercida);
            }
            if (usuario != null) {
                usuario.getEmpleadoasalariadoList().add(empleadoasalariado);
                usuario = em.merge(usuario);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Empleadoasalariado empleadoasalariado) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Empleadoasalariado persistentEmpleadoasalariado = em.find(Empleadoasalariado.class, empleadoasalariado.getId());
            Cargo cargoDesempennadoOld = persistentEmpleadoasalariado.getCargoDesempennado();
            Cargo cargoDesempennadoNew = empleadoasalariado.getCargoDesempennado();
            Municipio municipioActualOld = persistentEmpleadoasalariado.getMunicipioActual();
            Municipio municipioActualNew = empleadoasalariado.getMunicipioActual();
            Pais nacionalidadOld = persistentEmpleadoasalariado.getNacionalidad();
            Pais nacionalidadNew = empleadoasalariado.getNacionalidad();
            Profesion profesionEjercidaOld = persistentEmpleadoasalariado.getProfesionEjercida();
            Profesion profesionEjercidaNew = empleadoasalariado.getProfesionEjercida();
            Usuario usuarioOld = persistentEmpleadoasalariado.getUsuario();
            Usuario usuarioNew = empleadoasalariado.getUsuario();
            if (cargoDesempennadoNew != null) {
                cargoDesempennadoNew = em.getReference(cargoDesempennadoNew.getClass(), cargoDesempennadoNew.getId());
                empleadoasalariado.setCargoDesempennado(cargoDesempennadoNew);
            }
            if (municipioActualNew != null) {
                municipioActualNew = em.getReference(municipioActualNew.getClass(), municipioActualNew.getId());
                empleadoasalariado.setMunicipioActual(municipioActualNew);
            }
            if (nacionalidadNew != null) {
                nacionalidadNew = em.getReference(nacionalidadNew.getClass(), nacionalidadNew.getId());
                empleadoasalariado.setNacionalidad(nacionalidadNew);
            }
            if (profesionEjercidaNew != null) {
                profesionEjercidaNew = em.getReference(profesionEjercidaNew.getClass(), profesionEjercidaNew.getId());
                empleadoasalariado.setProfesionEjercida(profesionEjercidaNew);
            }
            if (usuarioNew != null) {
                usuarioNew = em.getReference(usuarioNew.getClass(), usuarioNew.getId());
                empleadoasalariado.setUsuario(usuarioNew);
            }
            empleadoasalariado = em.merge(empleadoasalariado);
            if (cargoDesempennadoOld != null && !cargoDesempennadoOld.equals(cargoDesempennadoNew)) {
                cargoDesempennadoOld.getEmpleadoasalariadoList().remove(empleadoasalariado);
                cargoDesempennadoOld = em.merge(cargoDesempennadoOld);
            }
            if (cargoDesempennadoNew != null && !cargoDesempennadoNew.equals(cargoDesempennadoOld)) {
                cargoDesempennadoNew.getEmpleadoasalariadoList().add(empleadoasalariado);
                cargoDesempennadoNew = em.merge(cargoDesempennadoNew);
            }
            if (municipioActualOld != null && !municipioActualOld.equals(municipioActualNew)) {
                municipioActualOld.getEmpleadoasalariadoList().remove(empleadoasalariado);
                municipioActualOld = em.merge(municipioActualOld);
            }
            if (municipioActualNew != null && !municipioActualNew.equals(municipioActualOld)) {
                municipioActualNew.getEmpleadoasalariadoList().add(empleadoasalariado);
                municipioActualNew = em.merge(municipioActualNew);
            }
            if (nacionalidadOld != null && !nacionalidadOld.equals(nacionalidadNew)) {
                nacionalidadOld.getEmpleadoasalariadoList().remove(empleadoasalariado);
                nacionalidadOld = em.merge(nacionalidadOld);
            }
            if (nacionalidadNew != null && !nacionalidadNew.equals(nacionalidadOld)) {
                nacionalidadNew.getEmpleadoasalariadoList().add(empleadoasalariado);
                nacionalidadNew = em.merge(nacionalidadNew);
            }
            if (profesionEjercidaOld != null && !profesionEjercidaOld.equals(profesionEjercidaNew)) {
                profesionEjercidaOld.getEmpleadoasalariadoList().remove(empleadoasalariado);
                profesionEjercidaOld = em.merge(profesionEjercidaOld);
            }
            if (profesionEjercidaNew != null && !profesionEjercidaNew.equals(profesionEjercidaOld)) {
                profesionEjercidaNew.getEmpleadoasalariadoList().add(empleadoasalariado);
                profesionEjercidaNew = em.merge(profesionEjercidaNew);
            }
            if (usuarioOld != null && !usuarioOld.equals(usuarioNew)) {
                usuarioOld.getEmpleadoasalariadoList().remove(empleadoasalariado);
                usuarioOld = em.merge(usuarioOld);
            }
            if (usuarioNew != null && !usuarioNew.equals(usuarioOld)) {
                usuarioNew.getEmpleadoasalariadoList().add(empleadoasalariado);
                usuarioNew = em.merge(usuarioNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = empleadoasalariado.getId();
                if (findEmpleadoasalariado(id) == null) {
                    throw new NonexistentEntityException("The empleadoasalariado with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Empleadoasalariado empleadoasalariado;
            try {
                empleadoasalariado = em.getReference(Empleadoasalariado.class, id);
                empleadoasalariado.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The empleadoasalariado with id " + id + " no longer exists.", enfe);
            }
            Cargo cargoDesempennado = empleadoasalariado.getCargoDesempennado();
            if (cargoDesempennado != null) {
                cargoDesempennado.getEmpleadoasalariadoList().remove(empleadoasalariado);
                cargoDesempennado = em.merge(cargoDesempennado);
            }
            Municipio municipioActual = empleadoasalariado.getMunicipioActual();
            if (municipioActual != null) {
                municipioActual.getEmpleadoasalariadoList().remove(empleadoasalariado);
                municipioActual = em.merge(municipioActual);
            }
            Pais nacionalidad = empleadoasalariado.getNacionalidad();
            if (nacionalidad != null) {
                nacionalidad.getEmpleadoasalariadoList().remove(empleadoasalariado);
                nacionalidad = em.merge(nacionalidad);
            }
            Profesion profesionEjercida = empleadoasalariado.getProfesionEjercida();
            if (profesionEjercida != null) {
                profesionEjercida.getEmpleadoasalariadoList().remove(empleadoasalariado);
                profesionEjercida = em.merge(profesionEjercida);
            }
            Usuario usuario = empleadoasalariado.getUsuario();
            if (usuario != null) {
                usuario.getEmpleadoasalariadoList().remove(empleadoasalariado);
                usuario = em.merge(usuario);
            }
            em.remove(empleadoasalariado);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Empleadoasalariado> findEmpleadoasalariadoEntities() {
        return findEmpleadoasalariadoEntities(true, -1, -1);
    }

    public List<Empleadoasalariado> findEmpleadoasalariadoEntities(int maxResults, int firstResult) {
        return findEmpleadoasalariadoEntities(false, maxResults, firstResult);
    }

    private List<Empleadoasalariado> findEmpleadoasalariadoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Empleadoasalariado.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Empleadoasalariado findEmpleadoasalariado(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Empleadoasalariado.class, id);
        } finally {
            em.close();
        }
    }

    public int getEmpleadoasalariadoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Empleadoasalariado> rt = cq.from(Empleadoasalariado.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    
        public Usuario logIn(String usuario, String contrasenna){
        Usuario salida;
        EntityManager em;
        Query query;
        List resultado;
        
        salida = null;
        em = null;
        try {
            em = getEntityManager();
            
            //creando consulta
            query = em.createNamedQuery("Usuario.Rol");
            query.setParameter("nombreUsuario", usuario);
            query.setParameter("contrasenia", contrasenna);
            
            //"ejecutando" query
            resultado = query.getResultList();
            
            //solo debería haber 1 item en el resultado si tuvo exito sino 0
            if(resultado.size() > 0){
                salida = (Usuario) resultado.get(0);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }finally{
            //cerrar el EntityManger
            if (em != null) {
                em.close();
            }
        }
        
        return salida;
    }
    
 
}
