package com.workingbriefcase.persistencia.controladores;

import com.workingbriefcase.persistencia.controladores.exceptions.IllegalOrphanException;
import com.workingbriefcase.persistencia.controladores.exceptions.NonexistentEntityException;
import com.workingbriefcase.persistencia.entities.Arealaboral;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.workingbriefcase.persistencia.entities.Habilidadprofesional;
import java.util.ArrayList;
import java.util.List;
import com.workingbriefcase.persistencia.entities.Profesion;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Nombre de la clase: ArealaboralJpaController
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class ArealaboralJpaController implements Serializable {

       public ArealaboralJpaController() {
        this.emf = Persistence.createEntityManagerFactory("Working-BriefcasePU");
         }
    public ArealaboralJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Arealaboral arealaboral) {
        if (arealaboral.getHabilidadprofesionalList() == null) {
            arealaboral.setHabilidadprofesionalList(new ArrayList<Habilidadprofesional>());
        }
        if (arealaboral.getProfesionList() == null) {
            arealaboral.setProfesionList(new ArrayList<Profesion>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Habilidadprofesional> attachedHabilidadprofesionalList = new ArrayList<Habilidadprofesional>();
            for (Habilidadprofesional habilidadprofesionalListHabilidadprofesionalToAttach : arealaboral.getHabilidadprofesionalList()) {
                habilidadprofesionalListHabilidadprofesionalToAttach = em.getReference(habilidadprofesionalListHabilidadprofesionalToAttach.getClass(), habilidadprofesionalListHabilidadprofesionalToAttach.getId());
                attachedHabilidadprofesionalList.add(habilidadprofesionalListHabilidadprofesionalToAttach);
            }
            arealaboral.setHabilidadprofesionalList(attachedHabilidadprofesionalList);
            List<Profesion> attachedProfesionList = new ArrayList<Profesion>();
            for (Profesion profesionListProfesionToAttach : arealaboral.getProfesionList()) {
                profesionListProfesionToAttach = em.getReference(profesionListProfesionToAttach.getClass(), profesionListProfesionToAttach.getId());
                attachedProfesionList.add(profesionListProfesionToAttach);
            }
            arealaboral.setProfesionList(attachedProfesionList);
            em.persist(arealaboral);
            for (Habilidadprofesional habilidadprofesionalListHabilidadprofesional : arealaboral.getHabilidadprofesionalList()) {
                Arealaboral oldAreaLaboralOfHabilidadprofesionalListHabilidadprofesional = habilidadprofesionalListHabilidadprofesional.getAreaLaboral();
                habilidadprofesionalListHabilidadprofesional.setAreaLaboral(arealaboral);
                habilidadprofesionalListHabilidadprofesional = em.merge(habilidadprofesionalListHabilidadprofesional);
                if (oldAreaLaboralOfHabilidadprofesionalListHabilidadprofesional != null) {
                    oldAreaLaboralOfHabilidadprofesionalListHabilidadprofesional.getHabilidadprofesionalList().remove(habilidadprofesionalListHabilidadprofesional);
                    oldAreaLaboralOfHabilidadprofesionalListHabilidadprofesional = em.merge(oldAreaLaboralOfHabilidadprofesionalListHabilidadprofesional);
                }
            }
            for (Profesion profesionListProfesion : arealaboral.getProfesionList()) {
                Arealaboral oldAreaLaboralOfProfesionListProfesion = profesionListProfesion.getAreaLaboral();
                profesionListProfesion.setAreaLaboral(arealaboral);
                profesionListProfesion = em.merge(profesionListProfesion);
                if (oldAreaLaboralOfProfesionListProfesion != null) {
                    oldAreaLaboralOfProfesionListProfesion.getProfesionList().remove(profesionListProfesion);
                    oldAreaLaboralOfProfesionListProfesion = em.merge(oldAreaLaboralOfProfesionListProfesion);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Arealaboral arealaboral) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Arealaboral persistentArealaboral = em.find(Arealaboral.class, arealaboral.getId());
            List<Habilidadprofesional> habilidadprofesionalListOld = persistentArealaboral.getHabilidadprofesionalList();
            List<Habilidadprofesional> habilidadprofesionalListNew = arealaboral.getHabilidadprofesionalList();
            List<Profesion> profesionListOld = persistentArealaboral.getProfesionList();
            List<Profesion> profesionListNew = arealaboral.getProfesionList();
            List<String> illegalOrphanMessages = null;
            for (Habilidadprofesional habilidadprofesionalListOldHabilidadprofesional : habilidadprofesionalListOld) {
                if (!habilidadprofesionalListNew.contains(habilidadprofesionalListOldHabilidadprofesional)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Habilidadprofesional " + habilidadprofesionalListOldHabilidadprofesional + " since its areaLaboral field is not nullable.");
                }
            }
            for (Profesion profesionListOldProfesion : profesionListOld) {
                if (!profesionListNew.contains(profesionListOldProfesion)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Profesion " + profesionListOldProfesion + " since its areaLaboral field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Habilidadprofesional> attachedHabilidadprofesionalListNew = new ArrayList<Habilidadprofesional>();
            for (Habilidadprofesional habilidadprofesionalListNewHabilidadprofesionalToAttach : habilidadprofesionalListNew) {
                habilidadprofesionalListNewHabilidadprofesionalToAttach = em.getReference(habilidadprofesionalListNewHabilidadprofesionalToAttach.getClass(), habilidadprofesionalListNewHabilidadprofesionalToAttach.getId());
                attachedHabilidadprofesionalListNew.add(habilidadprofesionalListNewHabilidadprofesionalToAttach);
            }
            habilidadprofesionalListNew = attachedHabilidadprofesionalListNew;
            arealaboral.setHabilidadprofesionalList(habilidadprofesionalListNew);
            List<Profesion> attachedProfesionListNew = new ArrayList<Profesion>();
            for (Profesion profesionListNewProfesionToAttach : profesionListNew) {
                profesionListNewProfesionToAttach = em.getReference(profesionListNewProfesionToAttach.getClass(), profesionListNewProfesionToAttach.getId());
                attachedProfesionListNew.add(profesionListNewProfesionToAttach);
            }
            profesionListNew = attachedProfesionListNew;
            arealaboral.setProfesionList(profesionListNew);
            arealaboral = em.merge(arealaboral);
            for (Habilidadprofesional habilidadprofesionalListNewHabilidadprofesional : habilidadprofesionalListNew) {
                if (!habilidadprofesionalListOld.contains(habilidadprofesionalListNewHabilidadprofesional)) {
                    Arealaboral oldAreaLaboralOfHabilidadprofesionalListNewHabilidadprofesional = habilidadprofesionalListNewHabilidadprofesional.getAreaLaboral();
                    habilidadprofesionalListNewHabilidadprofesional.setAreaLaboral(arealaboral);
                    habilidadprofesionalListNewHabilidadprofesional = em.merge(habilidadprofesionalListNewHabilidadprofesional);
                    if (oldAreaLaboralOfHabilidadprofesionalListNewHabilidadprofesional != null && !oldAreaLaboralOfHabilidadprofesionalListNewHabilidadprofesional.equals(arealaboral)) {
                        oldAreaLaboralOfHabilidadprofesionalListNewHabilidadprofesional.getHabilidadprofesionalList().remove(habilidadprofesionalListNewHabilidadprofesional);
                        oldAreaLaboralOfHabilidadprofesionalListNewHabilidadprofesional = em.merge(oldAreaLaboralOfHabilidadprofesionalListNewHabilidadprofesional);
                    }
                }
            }
            for (Profesion profesionListNewProfesion : profesionListNew) {
                if (!profesionListOld.contains(profesionListNewProfesion)) {
                    Arealaboral oldAreaLaboralOfProfesionListNewProfesion = profesionListNewProfesion.getAreaLaboral();
                    profesionListNewProfesion.setAreaLaboral(arealaboral);
                    profesionListNewProfesion = em.merge(profesionListNewProfesion);
                    if (oldAreaLaboralOfProfesionListNewProfesion != null && !oldAreaLaboralOfProfesionListNewProfesion.equals(arealaboral)) {
                        oldAreaLaboralOfProfesionListNewProfesion.getProfesionList().remove(profesionListNewProfesion);
                        oldAreaLaboralOfProfesionListNewProfesion = em.merge(oldAreaLaboralOfProfesionListNewProfesion);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = arealaboral.getId();
                if (findArealaboral(id) == null) {
                    throw new NonexistentEntityException("The arealaboral with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Arealaboral arealaboral;
            try {
                arealaboral = em.getReference(Arealaboral.class, id);
                arealaboral.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The arealaboral with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Habilidadprofesional> habilidadprofesionalListOrphanCheck = arealaboral.getHabilidadprofesionalList();
            for (Habilidadprofesional habilidadprofesionalListOrphanCheckHabilidadprofesional : habilidadprofesionalListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Arealaboral (" + arealaboral + ") cannot be destroyed since the Habilidadprofesional " + habilidadprofesionalListOrphanCheckHabilidadprofesional + " in its habilidadprofesionalList field has a non-nullable areaLaboral field.");
            }
            List<Profesion> profesionListOrphanCheck = arealaboral.getProfesionList();
            for (Profesion profesionListOrphanCheckProfesion : profesionListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Arealaboral (" + arealaboral + ") cannot be destroyed since the Profesion " + profesionListOrphanCheckProfesion + " in its profesionList field has a non-nullable areaLaboral field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(arealaboral);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Arealaboral> findArealaboralEntities() {
        return findArealaboralEntities(true, -1, -1);
    }

    public List<Arealaboral> findArealaboralEntities(int maxResults, int firstResult) {
        return findArealaboralEntities(false, maxResults, firstResult);
    }

    private List<Arealaboral> findArealaboralEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Arealaboral.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Arealaboral findArealaboral(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Arealaboral.class, id);
        } finally {
            em.close();
        }
    }

    public int getArealaboralCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Arealaboral> rt = cq.from(Arealaboral.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
