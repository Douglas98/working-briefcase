package com.workingbriefcase.persistencia.controladores;

import com.workingbriefcase.persistencia.controladores.exceptions.IllegalOrphanException;
import com.workingbriefcase.persistencia.controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.workingbriefcase.persistencia.entities.Rol;
import com.workingbriefcase.persistencia.entities.Empleadoasalariado;
import java.util.ArrayList;
import java.util.List;
import com.workingbriefcase.persistencia.entities.Candidato;
import com.workingbriefcase.persistencia.entities.Telefono;
import com.workingbriefcase.persistencia.entities.Empleadohora;
import com.workingbriefcase.persistencia.entities.Empresa;
import com.workingbriefcase.persistencia.entities.Usuario;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Nombre de la clase: UsuarioJpaController
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class UsuarioJpaController implements Serializable {

     public UsuarioJpaController() {
        this.emf = Persistence.createEntityManagerFactory("Working-BriefcasePU");
    }
    
    public UsuarioJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Usuario usuario) {
        if (usuario.getEmpleadoasalariadoList() == null) {
            usuario.setEmpleadoasalariadoList(new ArrayList<Empleadoasalariado>());
        }
        if (usuario.getCandidatoList() == null) {
            usuario.setCandidatoList(new ArrayList<Candidato>());
        }
        if (usuario.getTelefonoList() == null) {
            usuario.setTelefonoList(new ArrayList<Telefono>());
        }
        if (usuario.getEmpleadohoraList() == null) {
            usuario.setEmpleadohoraList(new ArrayList<Empleadohora>());
        }
        if (usuario.getEmpresaList() == null) {
            usuario.setEmpresaList(new ArrayList<Empresa>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Rol rol = usuario.getRol();
            if (rol != null) {
                rol = em.getReference(rol.getClass(), rol.getId());
                usuario.setRol(rol);
            }
            List<Empleadoasalariado> attachedEmpleadoasalariadoList = new ArrayList<Empleadoasalariado>();
            for (Empleadoasalariado empleadoasalariadoListEmpleadoasalariadoToAttach : usuario.getEmpleadoasalariadoList()) {
                empleadoasalariadoListEmpleadoasalariadoToAttach = em.getReference(empleadoasalariadoListEmpleadoasalariadoToAttach.getClass(), empleadoasalariadoListEmpleadoasalariadoToAttach.getId());
                attachedEmpleadoasalariadoList.add(empleadoasalariadoListEmpleadoasalariadoToAttach);
            }
            usuario.setEmpleadoasalariadoList(attachedEmpleadoasalariadoList);
            List<Candidato> attachedCandidatoList = new ArrayList<Candidato>();
            for (Candidato candidatoListCandidatoToAttach : usuario.getCandidatoList()) {
                candidatoListCandidatoToAttach = em.getReference(candidatoListCandidatoToAttach.getClass(), candidatoListCandidatoToAttach.getId());
                attachedCandidatoList.add(candidatoListCandidatoToAttach);
            }
            usuario.setCandidatoList(attachedCandidatoList);
            List<Telefono> attachedTelefonoList = new ArrayList<Telefono>();
            for (Telefono telefonoListTelefonoToAttach : usuario.getTelefonoList()) {
                telefonoListTelefonoToAttach = em.getReference(telefonoListTelefonoToAttach.getClass(), telefonoListTelefonoToAttach.getId());
                attachedTelefonoList.add(telefonoListTelefonoToAttach);
            }
            usuario.setTelefonoList(attachedTelefonoList);
            List<Empleadohora> attachedEmpleadohoraList = new ArrayList<Empleadohora>();
            for (Empleadohora empleadohoraListEmpleadohoraToAttach : usuario.getEmpleadohoraList()) {
                empleadohoraListEmpleadohoraToAttach = em.getReference(empleadohoraListEmpleadohoraToAttach.getClass(), empleadohoraListEmpleadohoraToAttach.getId());
                attachedEmpleadohoraList.add(empleadohoraListEmpleadohoraToAttach);
            }
            usuario.setEmpleadohoraList(attachedEmpleadohoraList);
            List<Empresa> attachedEmpresaList = new ArrayList<Empresa>();
            for (Empresa empresaListEmpresaToAttach : usuario.getEmpresaList()) {
                empresaListEmpresaToAttach = em.getReference(empresaListEmpresaToAttach.getClass(), empresaListEmpresaToAttach.getId());
                attachedEmpresaList.add(empresaListEmpresaToAttach);
            }
            usuario.setEmpresaList(attachedEmpresaList);
            em.persist(usuario);
            if (rol != null) {
                rol.getUsuarioList().add(usuario);
                rol = em.merge(rol);
            }
            for (Empleadoasalariado empleadoasalariadoListEmpleadoasalariado : usuario.getEmpleadoasalariadoList()) {
                Usuario oldUsuarioOfEmpleadoasalariadoListEmpleadoasalariado = empleadoasalariadoListEmpleadoasalariado.getUsuario();
                empleadoasalariadoListEmpleadoasalariado.setUsuario(usuario);
                empleadoasalariadoListEmpleadoasalariado = em.merge(empleadoasalariadoListEmpleadoasalariado);
                if (oldUsuarioOfEmpleadoasalariadoListEmpleadoasalariado != null) {
                    oldUsuarioOfEmpleadoasalariadoListEmpleadoasalariado.getEmpleadoasalariadoList().remove(empleadoasalariadoListEmpleadoasalariado);
                    oldUsuarioOfEmpleadoasalariadoListEmpleadoasalariado = em.merge(oldUsuarioOfEmpleadoasalariadoListEmpleadoasalariado);
                }
            }
            for (Candidato candidatoListCandidato : usuario.getCandidatoList()) {
                Usuario oldUsuarioOfCandidatoListCandidato = candidatoListCandidato.getUsuario();
                candidatoListCandidato.setUsuario(usuario);
                candidatoListCandidato = em.merge(candidatoListCandidato);
                if (oldUsuarioOfCandidatoListCandidato != null) {
                    oldUsuarioOfCandidatoListCandidato.getCandidatoList().remove(candidatoListCandidato);
                    oldUsuarioOfCandidatoListCandidato = em.merge(oldUsuarioOfCandidatoListCandidato);
                }
            }
            for (Telefono telefonoListTelefono : usuario.getTelefonoList()) {
                Usuario oldUsuarioPropietarioOfTelefonoListTelefono = telefonoListTelefono.getUsuarioPropietario();
                telefonoListTelefono.setUsuarioPropietario(usuario);
                telefonoListTelefono = em.merge(telefonoListTelefono);
                if (oldUsuarioPropietarioOfTelefonoListTelefono != null) {
                    oldUsuarioPropietarioOfTelefonoListTelefono.getTelefonoList().remove(telefonoListTelefono);
                    oldUsuarioPropietarioOfTelefonoListTelefono = em.merge(oldUsuarioPropietarioOfTelefonoListTelefono);
                }
            }
            for (Empleadohora empleadohoraListEmpleadohora : usuario.getEmpleadohoraList()) {
                Usuario oldUsuarioOfEmpleadohoraListEmpleadohora = empleadohoraListEmpleadohora.getUsuario();
                empleadohoraListEmpleadohora.setUsuario(usuario);
                empleadohoraListEmpleadohora = em.merge(empleadohoraListEmpleadohora);
                if (oldUsuarioOfEmpleadohoraListEmpleadohora != null) {
                    oldUsuarioOfEmpleadohoraListEmpleadohora.getEmpleadohoraList().remove(empleadohoraListEmpleadohora);
                    oldUsuarioOfEmpleadohoraListEmpleadohora = em.merge(oldUsuarioOfEmpleadohoraListEmpleadohora);
                }
            }
            for (Empresa empresaListEmpresa : usuario.getEmpresaList()) {
                Usuario oldUsuarioOfEmpresaListEmpresa = empresaListEmpresa.getUsuario();
                empresaListEmpresa.setUsuario(usuario);
                empresaListEmpresa = em.merge(empresaListEmpresa);
                if (oldUsuarioOfEmpresaListEmpresa != null) {
                    oldUsuarioOfEmpresaListEmpresa.getEmpresaList().remove(empresaListEmpresa);
                    oldUsuarioOfEmpresaListEmpresa = em.merge(oldUsuarioOfEmpresaListEmpresa);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Usuario usuario) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario persistentUsuario = em.find(Usuario.class, usuario.getId());
            Rol rolOld = persistentUsuario.getRol();
            Rol rolNew = usuario.getRol();
            List<Empleadoasalariado> empleadoasalariadoListOld = persistentUsuario.getEmpleadoasalariadoList();
            List<Empleadoasalariado> empleadoasalariadoListNew = usuario.getEmpleadoasalariadoList();
            List<Candidato> candidatoListOld = persistentUsuario.getCandidatoList();
            List<Candidato> candidatoListNew = usuario.getCandidatoList();
            List<Telefono> telefonoListOld = persistentUsuario.getTelefonoList();
            List<Telefono> telefonoListNew = usuario.getTelefonoList();
            List<Empleadohora> empleadohoraListOld = persistentUsuario.getEmpleadohoraList();
            List<Empleadohora> empleadohoraListNew = usuario.getEmpleadohoraList();
            List<Empresa> empresaListOld = persistentUsuario.getEmpresaList();
            List<Empresa> empresaListNew = usuario.getEmpresaList();
            List<String> illegalOrphanMessages = null;
            for (Empleadoasalariado empleadoasalariadoListOldEmpleadoasalariado : empleadoasalariadoListOld) {
                if (!empleadoasalariadoListNew.contains(empleadoasalariadoListOldEmpleadoasalariado)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Empleadoasalariado " + empleadoasalariadoListOldEmpleadoasalariado + " since its usuario field is not nullable.");
                }
            }
            for (Candidato candidatoListOldCandidato : candidatoListOld) {
                if (!candidatoListNew.contains(candidatoListOldCandidato)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Candidato " + candidatoListOldCandidato + " since its usuario field is not nullable.");
                }
            }
            for (Telefono telefonoListOldTelefono : telefonoListOld) {
                if (!telefonoListNew.contains(telefonoListOldTelefono)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Telefono " + telefonoListOldTelefono + " since its usuarioPropietario field is not nullable.");
                }
            }
            for (Empleadohora empleadohoraListOldEmpleadohora : empleadohoraListOld) {
                if (!empleadohoraListNew.contains(empleadohoraListOldEmpleadohora)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Empleadohora " + empleadohoraListOldEmpleadohora + " since its usuario field is not nullable.");
                }
            }
            for (Empresa empresaListOldEmpresa : empresaListOld) {
                if (!empresaListNew.contains(empresaListOldEmpresa)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Empresa " + empresaListOldEmpresa + " since its usuario field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (rolNew != null) {
                rolNew = em.getReference(rolNew.getClass(), rolNew.getId());
                usuario.setRol(rolNew);
            }
            List<Empleadoasalariado> attachedEmpleadoasalariadoListNew = new ArrayList<Empleadoasalariado>();
            for (Empleadoasalariado empleadoasalariadoListNewEmpleadoasalariadoToAttach : empleadoasalariadoListNew) {
                empleadoasalariadoListNewEmpleadoasalariadoToAttach = em.getReference(empleadoasalariadoListNewEmpleadoasalariadoToAttach.getClass(), empleadoasalariadoListNewEmpleadoasalariadoToAttach.getId());
                attachedEmpleadoasalariadoListNew.add(empleadoasalariadoListNewEmpleadoasalariadoToAttach);
            }
            empleadoasalariadoListNew = attachedEmpleadoasalariadoListNew;
            usuario.setEmpleadoasalariadoList(empleadoasalariadoListNew);
            List<Candidato> attachedCandidatoListNew = new ArrayList<Candidato>();
            for (Candidato candidatoListNewCandidatoToAttach : candidatoListNew) {
                candidatoListNewCandidatoToAttach = em.getReference(candidatoListNewCandidatoToAttach.getClass(), candidatoListNewCandidatoToAttach.getId());
                attachedCandidatoListNew.add(candidatoListNewCandidatoToAttach);
            }
            candidatoListNew = attachedCandidatoListNew;
            usuario.setCandidatoList(candidatoListNew);
            List<Telefono> attachedTelefonoListNew = new ArrayList<Telefono>();
            for (Telefono telefonoListNewTelefonoToAttach : telefonoListNew) {
                telefonoListNewTelefonoToAttach = em.getReference(telefonoListNewTelefonoToAttach.getClass(), telefonoListNewTelefonoToAttach.getId());
                attachedTelefonoListNew.add(telefonoListNewTelefonoToAttach);
            }
            telefonoListNew = attachedTelefonoListNew;
            usuario.setTelefonoList(telefonoListNew);
            List<Empleadohora> attachedEmpleadohoraListNew = new ArrayList<Empleadohora>();
            for (Empleadohora empleadohoraListNewEmpleadohoraToAttach : empleadohoraListNew) {
                empleadohoraListNewEmpleadohoraToAttach = em.getReference(empleadohoraListNewEmpleadohoraToAttach.getClass(), empleadohoraListNewEmpleadohoraToAttach.getId());
                attachedEmpleadohoraListNew.add(empleadohoraListNewEmpleadohoraToAttach);
            }
            empleadohoraListNew = attachedEmpleadohoraListNew;
            usuario.setEmpleadohoraList(empleadohoraListNew);
            List<Empresa> attachedEmpresaListNew = new ArrayList<Empresa>();
            for (Empresa empresaListNewEmpresaToAttach : empresaListNew) {
                empresaListNewEmpresaToAttach = em.getReference(empresaListNewEmpresaToAttach.getClass(), empresaListNewEmpresaToAttach.getId());
                attachedEmpresaListNew.add(empresaListNewEmpresaToAttach);
            }
            empresaListNew = attachedEmpresaListNew;
            usuario.setEmpresaList(empresaListNew);
            usuario = em.merge(usuario);
            if (rolOld != null && !rolOld.equals(rolNew)) {
                rolOld.getUsuarioList().remove(usuario);
                rolOld = em.merge(rolOld);
            }
            if (rolNew != null && !rolNew.equals(rolOld)) {
                rolNew.getUsuarioList().add(usuario);
                rolNew = em.merge(rolNew);
            }
            for (Empleadoasalariado empleadoasalariadoListNewEmpleadoasalariado : empleadoasalariadoListNew) {
                if (!empleadoasalariadoListOld.contains(empleadoasalariadoListNewEmpleadoasalariado)) {
                    Usuario oldUsuarioOfEmpleadoasalariadoListNewEmpleadoasalariado = empleadoasalariadoListNewEmpleadoasalariado.getUsuario();
                    empleadoasalariadoListNewEmpleadoasalariado.setUsuario(usuario);
                    empleadoasalariadoListNewEmpleadoasalariado = em.merge(empleadoasalariadoListNewEmpleadoasalariado);
                    if (oldUsuarioOfEmpleadoasalariadoListNewEmpleadoasalariado != null && !oldUsuarioOfEmpleadoasalariadoListNewEmpleadoasalariado.equals(usuario)) {
                        oldUsuarioOfEmpleadoasalariadoListNewEmpleadoasalariado.getEmpleadoasalariadoList().remove(empleadoasalariadoListNewEmpleadoasalariado);
                        oldUsuarioOfEmpleadoasalariadoListNewEmpleadoasalariado = em.merge(oldUsuarioOfEmpleadoasalariadoListNewEmpleadoasalariado);
                    }
                }
            }
            for (Candidato candidatoListNewCandidato : candidatoListNew) {
                if (!candidatoListOld.contains(candidatoListNewCandidato)) {
                    Usuario oldUsuarioOfCandidatoListNewCandidato = candidatoListNewCandidato.getUsuario();
                    candidatoListNewCandidato.setUsuario(usuario);
                    candidatoListNewCandidato = em.merge(candidatoListNewCandidato);
                    if (oldUsuarioOfCandidatoListNewCandidato != null && !oldUsuarioOfCandidatoListNewCandidato.equals(usuario)) {
                        oldUsuarioOfCandidatoListNewCandidato.getCandidatoList().remove(candidatoListNewCandidato);
                        oldUsuarioOfCandidatoListNewCandidato = em.merge(oldUsuarioOfCandidatoListNewCandidato);
                    }
                }
            }
            for (Telefono telefonoListNewTelefono : telefonoListNew) {
                if (!telefonoListOld.contains(telefonoListNewTelefono)) {
                    Usuario oldUsuarioPropietarioOfTelefonoListNewTelefono = telefonoListNewTelefono.getUsuarioPropietario();
                    telefonoListNewTelefono.setUsuarioPropietario(usuario);
                    telefonoListNewTelefono = em.merge(telefonoListNewTelefono);
                    if (oldUsuarioPropietarioOfTelefonoListNewTelefono != null && !oldUsuarioPropietarioOfTelefonoListNewTelefono.equals(usuario)) {
                        oldUsuarioPropietarioOfTelefonoListNewTelefono.getTelefonoList().remove(telefonoListNewTelefono);
                        oldUsuarioPropietarioOfTelefonoListNewTelefono = em.merge(oldUsuarioPropietarioOfTelefonoListNewTelefono);
                    }
                }
            }
            for (Empleadohora empleadohoraListNewEmpleadohora : empleadohoraListNew) {
                if (!empleadohoraListOld.contains(empleadohoraListNewEmpleadohora)) {
                    Usuario oldUsuarioOfEmpleadohoraListNewEmpleadohora = empleadohoraListNewEmpleadohora.getUsuario();
                    empleadohoraListNewEmpleadohora.setUsuario(usuario);
                    empleadohoraListNewEmpleadohora = em.merge(empleadohoraListNewEmpleadohora);
                    if (oldUsuarioOfEmpleadohoraListNewEmpleadohora != null && !oldUsuarioOfEmpleadohoraListNewEmpleadohora.equals(usuario)) {
                        oldUsuarioOfEmpleadohoraListNewEmpleadohora.getEmpleadohoraList().remove(empleadohoraListNewEmpleadohora);
                        oldUsuarioOfEmpleadohoraListNewEmpleadohora = em.merge(oldUsuarioOfEmpleadohoraListNewEmpleadohora);
                    }
                }
            }
            for (Empresa empresaListNewEmpresa : empresaListNew) {
                if (!empresaListOld.contains(empresaListNewEmpresa)) {
                    Usuario oldUsuarioOfEmpresaListNewEmpresa = empresaListNewEmpresa.getUsuario();
                    empresaListNewEmpresa.setUsuario(usuario);
                    empresaListNewEmpresa = em.merge(empresaListNewEmpresa);
                    if (oldUsuarioOfEmpresaListNewEmpresa != null && !oldUsuarioOfEmpresaListNewEmpresa.equals(usuario)) {
                        oldUsuarioOfEmpresaListNewEmpresa.getEmpresaList().remove(empresaListNewEmpresa);
                        oldUsuarioOfEmpresaListNewEmpresa = em.merge(oldUsuarioOfEmpresaListNewEmpresa);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = usuario.getId();
                if (findUsuario(id) == null) {
                    throw new NonexistentEntityException("The usuario with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario usuario;
            try {
                usuario = em.getReference(Usuario.class, id);
                usuario.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The usuario with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Empleadoasalariado> empleadoasalariadoListOrphanCheck = usuario.getEmpleadoasalariadoList();
            for (Empleadoasalariado empleadoasalariadoListOrphanCheckEmpleadoasalariado : empleadoasalariadoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Empleadoasalariado " + empleadoasalariadoListOrphanCheckEmpleadoasalariado + " in its empleadoasalariadoList field has a non-nullable usuario field.");
            }
            List<Candidato> candidatoListOrphanCheck = usuario.getCandidatoList();
            for (Candidato candidatoListOrphanCheckCandidato : candidatoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Candidato " + candidatoListOrphanCheckCandidato + " in its candidatoList field has a non-nullable usuario field.");
            }
            List<Telefono> telefonoListOrphanCheck = usuario.getTelefonoList();
            for (Telefono telefonoListOrphanCheckTelefono : telefonoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Telefono " + telefonoListOrphanCheckTelefono + " in its telefonoList field has a non-nullable usuarioPropietario field.");
            }
            List<Empleadohora> empleadohoraListOrphanCheck = usuario.getEmpleadohoraList();
            for (Empleadohora empleadohoraListOrphanCheckEmpleadohora : empleadohoraListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Empleadohora " + empleadohoraListOrphanCheckEmpleadohora + " in its empleadohoraList field has a non-nullable usuario field.");
            }
            List<Empresa> empresaListOrphanCheck = usuario.getEmpresaList();
            for (Empresa empresaListOrphanCheckEmpresa : empresaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Empresa " + empresaListOrphanCheckEmpresa + " in its empresaList field has a non-nullable usuario field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Rol rol = usuario.getRol();
            if (rol != null) {
                rol.getUsuarioList().remove(usuario);
                rol = em.merge(rol);
            }
            em.remove(usuario);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Usuario> findUsuarioEntities() {
        return findUsuarioEntities(true, -1, -1);
    }

    public List<Usuario> findUsuarioEntities(int maxResults, int firstResult) {
        return findUsuarioEntities(false, maxResults, firstResult);
    }

    private List<Usuario> findUsuarioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Usuario.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Usuario findUsuario(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Usuario.class, id);
        } finally {
            em.close();
        }
    }

    public int getUsuarioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Usuario> rt = cq.from(Usuario.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    public void modificar(Usuario us){
            EntityManager em = getEntityManager();
            try {
               em.getTransaction().begin();
               String queryupdate = "UPDATE Usuario SET nombre=?1, correoElectronico = ?2  WHERE id=?3"; 
               em.createQuery(queryupdate).setParameter(1, us.getNombre()).setParameter(2, us.getCorreoElectronico()).setParameter(3, us.getId()).executeUpdate();
               em.getTransaction().commit();

        } finally {
              em.close();
        }
 
    }
        public void modificarContra(Usuario us){
            EntityManager em = getEntityManager();
            try {
               em.getTransaction().begin();
               String queryupdate = "UPDATE Usuario SET contrasenna=?1  WHERE id=?2"; 
               em.createQuery(queryupdate).setParameter(1, us.getContrasenna()).setParameter(2, us.getId()).executeUpdate();
               em.getTransaction().commit();

        } finally {
              em.close();
     
        }
     }
       public int lastID(){
        int salida = 0;
        EntityManager em;
        Query query;
        List resultado;
        em = null;
        try {
            em = getEntityManager();
            
            //creando consulta
            query = em.createNamedQuery("Usuario.LastId");
            
            //"ejecutando" query
            resultado = query.getResultList();
            
            //solo debería haber 1 item en el resultado si tuvo exito sino 0
            if(resultado.size() > 0){
                salida = (int) resultado.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();      
        }finally{
            //cerrar el EntityManger
            if (em != null) {
                em.close();
            }
        }
        return salida;
    }
    
}
