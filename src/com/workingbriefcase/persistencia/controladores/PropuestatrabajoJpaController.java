package com.workingbriefcase.persistencia.controladores;

import com.workingbriefcase.persistencia.controladores.exceptions.IllegalOrphanException;
import com.workingbriefcase.persistencia.controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.workingbriefcase.persistencia.entities.Ofertalaboral;
import com.workingbriefcase.persistencia.entities.Propuestatrabajo;
import com.workingbriefcase.persistencia.entities.PropuestatrabajoCurriculum;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Nombre de la clase: PropuestatrabajoJpaController
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class PropuestatrabajoJpaController implements Serializable {

      public PropuestatrabajoJpaController() {
        this.emf = Persistence.createEntityManagerFactory("Working-BriefcasePU");
    }
    public PropuestatrabajoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

 
    public int lastID(){
        int salida = 0;
        EntityManager em;
        Query query;
        List resultado;
        em = null;
        try {
            em = getEntityManager();
            
            //creando consulta
            query = em.createNamedQuery("Propuestatrabajo.lastId");
            
            //"ejecutando" query
            resultado = query.getResultList();
            
            //solo debería haber 1 item en el resultado si tuvo exito sino 0
            if(resultado.size() > 0){
                salida = (int) resultado.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();      
        }finally{
            //cerrar el EntityManger
            if (em != null) {
                em.close();
            }
        }
        return salida;
    }
    
    public void create(Propuestatrabajo propuestatrabajo) {
        if (propuestatrabajo.getPropuestatrabajoCurriculumList() == null) {
            propuestatrabajo.setPropuestatrabajoCurriculumList(new ArrayList<PropuestatrabajoCurriculum>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ofertalaboral ofertaLaboral = propuestatrabajo.getOfertaLaboral();
            if (ofertaLaboral != null) {
                ofertaLaboral = em.getReference(ofertaLaboral.getClass(), ofertaLaboral.getId());
                propuestatrabajo.setOfertaLaboral(ofertaLaboral);
            }
            List<PropuestatrabajoCurriculum> attachedPropuestatrabajoCurriculumList = new ArrayList<PropuestatrabajoCurriculum>();
            for (PropuestatrabajoCurriculum propuestatrabajoCurriculumListPropuestatrabajoCurriculumToAttach : propuestatrabajo.getPropuestatrabajoCurriculumList()) {
                propuestatrabajoCurriculumListPropuestatrabajoCurriculumToAttach = em.getReference(propuestatrabajoCurriculumListPropuestatrabajoCurriculumToAttach.getClass(), propuestatrabajoCurriculumListPropuestatrabajoCurriculumToAttach.getId());
                attachedPropuestatrabajoCurriculumList.add(propuestatrabajoCurriculumListPropuestatrabajoCurriculumToAttach);
            }
            propuestatrabajo.setPropuestatrabajoCurriculumList(attachedPropuestatrabajoCurriculumList);
            em.persist(propuestatrabajo);
            if (ofertaLaboral != null) {
                ofertaLaboral.getPropuestatrabajoList().add(propuestatrabajo);
                ofertaLaboral = em.merge(ofertaLaboral);
            }
            for (PropuestatrabajoCurriculum propuestatrabajoCurriculumListPropuestatrabajoCurriculum : propuestatrabajo.getPropuestatrabajoCurriculumList()) {
                Propuestatrabajo oldPropuestaTrabajoOfPropuestatrabajoCurriculumListPropuestatrabajoCurriculum = propuestatrabajoCurriculumListPropuestatrabajoCurriculum.getPropuestaTrabajo();
                propuestatrabajoCurriculumListPropuestatrabajoCurriculum.setPropuestaTrabajo(propuestatrabajo);
                propuestatrabajoCurriculumListPropuestatrabajoCurriculum = em.merge(propuestatrabajoCurriculumListPropuestatrabajoCurriculum);
                if (oldPropuestaTrabajoOfPropuestatrabajoCurriculumListPropuestatrabajoCurriculum != null) {
                    oldPropuestaTrabajoOfPropuestatrabajoCurriculumListPropuestatrabajoCurriculum.getPropuestatrabajoCurriculumList().remove(propuestatrabajoCurriculumListPropuestatrabajoCurriculum);
                    oldPropuestaTrabajoOfPropuestatrabajoCurriculumListPropuestatrabajoCurriculum = em.merge(oldPropuestaTrabajoOfPropuestatrabajoCurriculumListPropuestatrabajoCurriculum);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Propuestatrabajo propuestatrabajo) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Propuestatrabajo persistentPropuestatrabajo = em.find(Propuestatrabajo.class, propuestatrabajo.getId());
            Ofertalaboral ofertaLaboralOld = persistentPropuestatrabajo.getOfertaLaboral();
            Ofertalaboral ofertaLaboralNew = propuestatrabajo.getOfertaLaboral();
            List<PropuestatrabajoCurriculum> propuestatrabajoCurriculumListOld = persistentPropuestatrabajo.getPropuestatrabajoCurriculumList();
            List<PropuestatrabajoCurriculum> propuestatrabajoCurriculumListNew = propuestatrabajo.getPropuestatrabajoCurriculumList();
            List<String> illegalOrphanMessages = null;
            for (PropuestatrabajoCurriculum propuestatrabajoCurriculumListOldPropuestatrabajoCurriculum : propuestatrabajoCurriculumListOld) {
                if (!propuestatrabajoCurriculumListNew.contains(propuestatrabajoCurriculumListOldPropuestatrabajoCurriculum)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain PropuestatrabajoCurriculum " + propuestatrabajoCurriculumListOldPropuestatrabajoCurriculum + " since its propuestaTrabajo field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (ofertaLaboralNew != null) {
                ofertaLaboralNew = em.getReference(ofertaLaboralNew.getClass(), ofertaLaboralNew.getId());
                propuestatrabajo.setOfertaLaboral(ofertaLaboralNew);
            }
            List<PropuestatrabajoCurriculum> attachedPropuestatrabajoCurriculumListNew = new ArrayList<PropuestatrabajoCurriculum>();
            for (PropuestatrabajoCurriculum propuestatrabajoCurriculumListNewPropuestatrabajoCurriculumToAttach : propuestatrabajoCurriculumListNew) {
                propuestatrabajoCurriculumListNewPropuestatrabajoCurriculumToAttach = em.getReference(propuestatrabajoCurriculumListNewPropuestatrabajoCurriculumToAttach.getClass(), propuestatrabajoCurriculumListNewPropuestatrabajoCurriculumToAttach.getId());
                attachedPropuestatrabajoCurriculumListNew.add(propuestatrabajoCurriculumListNewPropuestatrabajoCurriculumToAttach);
            }
            propuestatrabajoCurriculumListNew = attachedPropuestatrabajoCurriculumListNew;
            propuestatrabajo.setPropuestatrabajoCurriculumList(propuestatrabajoCurriculumListNew);
            propuestatrabajo = em.merge(propuestatrabajo);
            if (ofertaLaboralOld != null && !ofertaLaboralOld.equals(ofertaLaboralNew)) {
                ofertaLaboralOld.getPropuestatrabajoList().remove(propuestatrabajo);
                ofertaLaboralOld = em.merge(ofertaLaboralOld);
            }
            if (ofertaLaboralNew != null && !ofertaLaboralNew.equals(ofertaLaboralOld)) {
                ofertaLaboralNew.getPropuestatrabajoList().add(propuestatrabajo);
                ofertaLaboralNew = em.merge(ofertaLaboralNew);
            }
            for (PropuestatrabajoCurriculum propuestatrabajoCurriculumListNewPropuestatrabajoCurriculum : propuestatrabajoCurriculumListNew) {
                if (!propuestatrabajoCurriculumListOld.contains(propuestatrabajoCurriculumListNewPropuestatrabajoCurriculum)) {
                    Propuestatrabajo oldPropuestaTrabajoOfPropuestatrabajoCurriculumListNewPropuestatrabajoCurriculum = propuestatrabajoCurriculumListNewPropuestatrabajoCurriculum.getPropuestaTrabajo();
                    propuestatrabajoCurriculumListNewPropuestatrabajoCurriculum.setPropuestaTrabajo(propuestatrabajo);
                    propuestatrabajoCurriculumListNewPropuestatrabajoCurriculum = em.merge(propuestatrabajoCurriculumListNewPropuestatrabajoCurriculum);
                    if (oldPropuestaTrabajoOfPropuestatrabajoCurriculumListNewPropuestatrabajoCurriculum != null && !oldPropuestaTrabajoOfPropuestatrabajoCurriculumListNewPropuestatrabajoCurriculum.equals(propuestatrabajo)) {
                        oldPropuestaTrabajoOfPropuestatrabajoCurriculumListNewPropuestatrabajoCurriculum.getPropuestatrabajoCurriculumList().remove(propuestatrabajoCurriculumListNewPropuestatrabajoCurriculum);
                        oldPropuestaTrabajoOfPropuestatrabajoCurriculumListNewPropuestatrabajoCurriculum = em.merge(oldPropuestaTrabajoOfPropuestatrabajoCurriculumListNewPropuestatrabajoCurriculum);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = propuestatrabajo.getId();
                if (findPropuestatrabajo(id) == null) {
                    throw new NonexistentEntityException("The propuestatrabajo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Propuestatrabajo propuestatrabajo;
            try {
                propuestatrabajo = em.getReference(Propuestatrabajo.class, id);
                propuestatrabajo.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The propuestatrabajo with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<PropuestatrabajoCurriculum> propuestatrabajoCurriculumListOrphanCheck = propuestatrabajo.getPropuestatrabajoCurriculumList();
            for (PropuestatrabajoCurriculum propuestatrabajoCurriculumListOrphanCheckPropuestatrabajoCurriculum : propuestatrabajoCurriculumListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Propuestatrabajo (" + propuestatrabajo + ") cannot be destroyed since the PropuestatrabajoCurriculum " + propuestatrabajoCurriculumListOrphanCheckPropuestatrabajoCurriculum + " in its propuestatrabajoCurriculumList field has a non-nullable propuestaTrabajo field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Ofertalaboral ofertaLaboral = propuestatrabajo.getOfertaLaboral();
            if (ofertaLaboral != null) {
                ofertaLaboral.getPropuestatrabajoList().remove(propuestatrabajo);
                ofertaLaboral = em.merge(ofertaLaboral);
            }
            em.remove(propuestatrabajo);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Propuestatrabajo> findPropuestatrabajoEntities() {
        return findPropuestatrabajoEntities(true, -1, -1);
    }

    public List<Propuestatrabajo> findPropuestatrabajoEntities(int maxResults, int firstResult) {
        return findPropuestatrabajoEntities(false, maxResults, firstResult);
    }

    private List<Propuestatrabajo> findPropuestatrabajoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Propuestatrabajo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Propuestatrabajo findPropuestatrabajo(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Propuestatrabajo.class, id);
        } finally {
            em.close();
        }
    }

    public int getPropuestatrabajoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Propuestatrabajo> rt = cq.from(Propuestatrabajo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
