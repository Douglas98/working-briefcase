package com.workingbriefcase.persistencia.controladores;

import com.workingbriefcase.persistencia.controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.workingbriefcase.persistencia.entities.Curriculum;
import com.workingbriefcase.persistencia.entities.CurriculumHabilidad;
import com.workingbriefcase.persistencia.entities.Habilidadprofesional;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Nombre de la clase: CurriculumHabilidadJpaController
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class CurriculumHabilidadJpaController implements Serializable {

    public CurriculumHabilidadJpaController() {
        this.emf = emf = Persistence.createEntityManagerFactory("Working-BriefcasePU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CurriculumHabilidad curriculumHabilidad) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Curriculum curriculum = curriculumHabilidad.getCurriculum();
            if (curriculum != null) {
                curriculum = em.getReference(curriculum.getClass(), curriculum.getId());
                curriculumHabilidad.setCurriculum(curriculum);
            }
            Habilidadprofesional habilidadProfesional = curriculumHabilidad.getHabilidadProfesional();
            if (habilidadProfesional != null) {
                habilidadProfesional = em.getReference(habilidadProfesional.getClass(), habilidadProfesional.getId());
                curriculumHabilidad.setHabilidadProfesional(habilidadProfesional);
            }
            em.persist(curriculumHabilidad);
            if (curriculum != null) {
                curriculum.getCurriculumHabilidadList().add(curriculumHabilidad);
                curriculum = em.merge(curriculum);
            }
            if (habilidadProfesional != null) {
                habilidadProfesional.getCurriculumHabilidadList().add(curriculumHabilidad);
                habilidadProfesional = em.merge(habilidadProfesional);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CurriculumHabilidad curriculumHabilidad) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CurriculumHabilidad persistentCurriculumHabilidad = em.find(CurriculumHabilidad.class, curriculumHabilidad.getId());
            Curriculum curriculumOld = persistentCurriculumHabilidad.getCurriculum();
            Curriculum curriculumNew = curriculumHabilidad.getCurriculum();
            Habilidadprofesional habilidadProfesionalOld = persistentCurriculumHabilidad.getHabilidadProfesional();
            Habilidadprofesional habilidadProfesionalNew = curriculumHabilidad.getHabilidadProfesional();
            if (curriculumNew != null) {
                curriculumNew = em.getReference(curriculumNew.getClass(), curriculumNew.getId());
                curriculumHabilidad.setCurriculum(curriculumNew);
            }
            if (habilidadProfesionalNew != null) {
                habilidadProfesionalNew = em.getReference(habilidadProfesionalNew.getClass(), habilidadProfesionalNew.getId());
                curriculumHabilidad.setHabilidadProfesional(habilidadProfesionalNew);
            }
            curriculumHabilidad = em.merge(curriculumHabilidad);
            if (curriculumOld != null && !curriculumOld.equals(curriculumNew)) {
                curriculumOld.getCurriculumHabilidadList().remove(curriculumHabilidad);
                curriculumOld = em.merge(curriculumOld);
            }
            if (curriculumNew != null && !curriculumNew.equals(curriculumOld)) {
                curriculumNew.getCurriculumHabilidadList().add(curriculumHabilidad);
                curriculumNew = em.merge(curriculumNew);
            }
            if (habilidadProfesionalOld != null && !habilidadProfesionalOld.equals(habilidadProfesionalNew)) {
                habilidadProfesionalOld.getCurriculumHabilidadList().remove(curriculumHabilidad);
                habilidadProfesionalOld = em.merge(habilidadProfesionalOld);
            }
            if (habilidadProfesionalNew != null && !habilidadProfesionalNew.equals(habilidadProfesionalOld)) {
                habilidadProfesionalNew.getCurriculumHabilidadList().add(curriculumHabilidad);
                habilidadProfesionalNew = em.merge(habilidadProfesionalNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = curriculumHabilidad.getId();
                if (findCurriculumHabilidad(id) == null) {
                    throw new NonexistentEntityException("The curriculumHabilidad with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CurriculumHabilidad curriculumHabilidad;
            try {
                curriculumHabilidad = em.getReference(CurriculumHabilidad.class, id);
                curriculumHabilidad.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The curriculumHabilidad with id " + id + " no longer exists.", enfe);
            }
            Curriculum curriculum = curriculumHabilidad.getCurriculum();
            if (curriculum != null) {
                curriculum.getCurriculumHabilidadList().remove(curriculumHabilidad);
                curriculum = em.merge(curriculum);
            }
            Habilidadprofesional habilidadProfesional = curriculumHabilidad.getHabilidadProfesional();
            if (habilidadProfesional != null) {
                habilidadProfesional.getCurriculumHabilidadList().remove(curriculumHabilidad);
                habilidadProfesional = em.merge(habilidadProfesional);
            }
            em.remove(curriculumHabilidad);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CurriculumHabilidad> findCurriculumHabilidadEntities() {
        return findCurriculumHabilidadEntities(true, -1, -1);
    }

    public List<CurriculumHabilidad> findCurriculumHabilidadEntities(int maxResults, int firstResult) {
        return findCurriculumHabilidadEntities(false, maxResults, firstResult);
    }

    private List<CurriculumHabilidad> findCurriculumHabilidadEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CurriculumHabilidad.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CurriculumHabilidad findCurriculumHabilidad(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CurriculumHabilidad.class, id);
        } finally {
            em.close();
        }
    }

    public int getCurriculumHabilidadCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CurriculumHabilidad> rt = cq.from(CurriculumHabilidad.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
