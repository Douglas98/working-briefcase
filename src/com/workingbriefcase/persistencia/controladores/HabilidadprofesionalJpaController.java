package com.workingbriefcase.persistencia.controladores;

import com.workingbriefcase.persistencia.controladores.exceptions.IllegalOrphanException;
import com.workingbriefcase.persistencia.controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.workingbriefcase.persistencia.entities.Arealaboral;
import com.workingbriefcase.persistencia.entities.OfertalaboralHabilidad;
import java.util.ArrayList;
import java.util.List;
import com.workingbriefcase.persistencia.entities.CurriculumHabilidad;
import com.workingbriefcase.persistencia.entities.Habilidadprofesional;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Nombre de la clase: HabilidadprofesionalJpaController
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class HabilidadprofesionalJpaController implements Serializable {

     public HabilidadprofesionalJpaController() {
        this.emf = Persistence.createEntityManagerFactory("Working-BriefcasePU");
    }
    public HabilidadprofesionalJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Habilidadprofesional habilidadprofesional) {
        if (habilidadprofesional.getOfertalaboralHabilidadList() == null) {
            habilidadprofesional.setOfertalaboralHabilidadList(new ArrayList<OfertalaboralHabilidad>());
        }
        if (habilidadprofesional.getCurriculumHabilidadList() == null) {
            habilidadprofesional.setCurriculumHabilidadList(new ArrayList<CurriculumHabilidad>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Arealaboral areaLaboral = habilidadprofesional.getAreaLaboral();
            if (areaLaboral != null) {
                areaLaboral = em.getReference(areaLaboral.getClass(), areaLaboral.getId());
                habilidadprofesional.setAreaLaboral(areaLaboral);
            }
            List<OfertalaboralHabilidad> attachedOfertalaboralHabilidadList = new ArrayList<OfertalaboralHabilidad>();
            for (OfertalaboralHabilidad ofertalaboralHabilidadListOfertalaboralHabilidadToAttach : habilidadprofesional.getOfertalaboralHabilidadList()) {
                ofertalaboralHabilidadListOfertalaboralHabilidadToAttach = em.getReference(ofertalaboralHabilidadListOfertalaboralHabilidadToAttach.getClass(), ofertalaboralHabilidadListOfertalaboralHabilidadToAttach.getId());
                attachedOfertalaboralHabilidadList.add(ofertalaboralHabilidadListOfertalaboralHabilidadToAttach);
            }
            habilidadprofesional.setOfertalaboralHabilidadList(attachedOfertalaboralHabilidadList);
            List<CurriculumHabilidad> attachedCurriculumHabilidadList = new ArrayList<CurriculumHabilidad>();
            for (CurriculumHabilidad curriculumHabilidadListCurriculumHabilidadToAttach : habilidadprofesional.getCurriculumHabilidadList()) {
                curriculumHabilidadListCurriculumHabilidadToAttach = em.getReference(curriculumHabilidadListCurriculumHabilidadToAttach.getClass(), curriculumHabilidadListCurriculumHabilidadToAttach.getId());
                attachedCurriculumHabilidadList.add(curriculumHabilidadListCurriculumHabilidadToAttach);
            }
            habilidadprofesional.setCurriculumHabilidadList(attachedCurriculumHabilidadList);
            em.persist(habilidadprofesional);
            if (areaLaboral != null) {
                areaLaboral.getHabilidadprofesionalList().add(habilidadprofesional);
                areaLaboral = em.merge(areaLaboral);
            }
            for (OfertalaboralHabilidad ofertalaboralHabilidadListOfertalaboralHabilidad : habilidadprofesional.getOfertalaboralHabilidadList()) {
                Habilidadprofesional oldHabilidadProfesionalOfOfertalaboralHabilidadListOfertalaboralHabilidad = ofertalaboralHabilidadListOfertalaboralHabilidad.getHabilidadProfesional();
                ofertalaboralHabilidadListOfertalaboralHabilidad.setHabilidadProfesional(habilidadprofesional);
                ofertalaboralHabilidadListOfertalaboralHabilidad = em.merge(ofertalaboralHabilidadListOfertalaboralHabilidad);
                if (oldHabilidadProfesionalOfOfertalaboralHabilidadListOfertalaboralHabilidad != null) {
                    oldHabilidadProfesionalOfOfertalaboralHabilidadListOfertalaboralHabilidad.getOfertalaboralHabilidadList().remove(ofertalaboralHabilidadListOfertalaboralHabilidad);
                    oldHabilidadProfesionalOfOfertalaboralHabilidadListOfertalaboralHabilidad = em.merge(oldHabilidadProfesionalOfOfertalaboralHabilidadListOfertalaboralHabilidad);
                }
            }
            for (CurriculumHabilidad curriculumHabilidadListCurriculumHabilidad : habilidadprofesional.getCurriculumHabilidadList()) {
                Habilidadprofesional oldHabilidadProfesionalOfCurriculumHabilidadListCurriculumHabilidad = curriculumHabilidadListCurriculumHabilidad.getHabilidadProfesional();
                curriculumHabilidadListCurriculumHabilidad.setHabilidadProfesional(habilidadprofesional);
                curriculumHabilidadListCurriculumHabilidad = em.merge(curriculumHabilidadListCurriculumHabilidad);
                if (oldHabilidadProfesionalOfCurriculumHabilidadListCurriculumHabilidad != null) {
                    oldHabilidadProfesionalOfCurriculumHabilidadListCurriculumHabilidad.getCurriculumHabilidadList().remove(curriculumHabilidadListCurriculumHabilidad);
                    oldHabilidadProfesionalOfCurriculumHabilidadListCurriculumHabilidad = em.merge(oldHabilidadProfesionalOfCurriculumHabilidadListCurriculumHabilidad);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Habilidadprofesional habilidadprofesional) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Habilidadprofesional persistentHabilidadprofesional = em.find(Habilidadprofesional.class, habilidadprofesional.getId());
            Arealaboral areaLaboralOld = persistentHabilidadprofesional.getAreaLaboral();
            Arealaboral areaLaboralNew = habilidadprofesional.getAreaLaboral();
            List<OfertalaboralHabilidad> ofertalaboralHabilidadListOld = persistentHabilidadprofesional.getOfertalaboralHabilidadList();
            List<OfertalaboralHabilidad> ofertalaboralHabilidadListNew = habilidadprofesional.getOfertalaboralHabilidadList();
            List<CurriculumHabilidad> curriculumHabilidadListOld = persistentHabilidadprofesional.getCurriculumHabilidadList();
            List<CurriculumHabilidad> curriculumHabilidadListNew = habilidadprofesional.getCurriculumHabilidadList();
            List<String> illegalOrphanMessages = null;
            for (OfertalaboralHabilidad ofertalaboralHabilidadListOldOfertalaboralHabilidad : ofertalaboralHabilidadListOld) {
                if (!ofertalaboralHabilidadListNew.contains(ofertalaboralHabilidadListOldOfertalaboralHabilidad)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain OfertalaboralHabilidad " + ofertalaboralHabilidadListOldOfertalaboralHabilidad + " since its habilidadProfesional field is not nullable.");
                }
            }
            for (CurriculumHabilidad curriculumHabilidadListOldCurriculumHabilidad : curriculumHabilidadListOld) {
                if (!curriculumHabilidadListNew.contains(curriculumHabilidadListOldCurriculumHabilidad)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain CurriculumHabilidad " + curriculumHabilidadListOldCurriculumHabilidad + " since its habilidadProfesional field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (areaLaboralNew != null) {
                areaLaboralNew = em.getReference(areaLaboralNew.getClass(), areaLaboralNew.getId());
                habilidadprofesional.setAreaLaboral(areaLaboralNew);
            }
            List<OfertalaboralHabilidad> attachedOfertalaboralHabilidadListNew = new ArrayList<OfertalaboralHabilidad>();
            for (OfertalaboralHabilidad ofertalaboralHabilidadListNewOfertalaboralHabilidadToAttach : ofertalaboralHabilidadListNew) {
                ofertalaboralHabilidadListNewOfertalaboralHabilidadToAttach = em.getReference(ofertalaboralHabilidadListNewOfertalaboralHabilidadToAttach.getClass(), ofertalaboralHabilidadListNewOfertalaboralHabilidadToAttach.getId());
                attachedOfertalaboralHabilidadListNew.add(ofertalaboralHabilidadListNewOfertalaboralHabilidadToAttach);
            }
            ofertalaboralHabilidadListNew = attachedOfertalaboralHabilidadListNew;
            habilidadprofesional.setOfertalaboralHabilidadList(ofertalaboralHabilidadListNew);
            List<CurriculumHabilidad> attachedCurriculumHabilidadListNew = new ArrayList<CurriculumHabilidad>();
            for (CurriculumHabilidad curriculumHabilidadListNewCurriculumHabilidadToAttach : curriculumHabilidadListNew) {
                curriculumHabilidadListNewCurriculumHabilidadToAttach = em.getReference(curriculumHabilidadListNewCurriculumHabilidadToAttach.getClass(), curriculumHabilidadListNewCurriculumHabilidadToAttach.getId());
                attachedCurriculumHabilidadListNew.add(curriculumHabilidadListNewCurriculumHabilidadToAttach);
            }
            curriculumHabilidadListNew = attachedCurriculumHabilidadListNew;
            habilidadprofesional.setCurriculumHabilidadList(curriculumHabilidadListNew);
            habilidadprofesional = em.merge(habilidadprofesional);
            if (areaLaboralOld != null && !areaLaboralOld.equals(areaLaboralNew)) {
                areaLaboralOld.getHabilidadprofesionalList().remove(habilidadprofesional);
                areaLaboralOld = em.merge(areaLaboralOld);
            }
            if (areaLaboralNew != null && !areaLaboralNew.equals(areaLaboralOld)) {
                areaLaboralNew.getHabilidadprofesionalList().add(habilidadprofesional);
                areaLaboralNew = em.merge(areaLaboralNew);
            }
            for (OfertalaboralHabilidad ofertalaboralHabilidadListNewOfertalaboralHabilidad : ofertalaboralHabilidadListNew) {
                if (!ofertalaboralHabilidadListOld.contains(ofertalaboralHabilidadListNewOfertalaboralHabilidad)) {
                    Habilidadprofesional oldHabilidadProfesionalOfOfertalaboralHabilidadListNewOfertalaboralHabilidad = ofertalaboralHabilidadListNewOfertalaboralHabilidad.getHabilidadProfesional();
                    ofertalaboralHabilidadListNewOfertalaboralHabilidad.setHabilidadProfesional(habilidadprofesional);
                    ofertalaboralHabilidadListNewOfertalaboralHabilidad = em.merge(ofertalaboralHabilidadListNewOfertalaboralHabilidad);
                    if (oldHabilidadProfesionalOfOfertalaboralHabilidadListNewOfertalaboralHabilidad != null && !oldHabilidadProfesionalOfOfertalaboralHabilidadListNewOfertalaboralHabilidad.equals(habilidadprofesional)) {
                        oldHabilidadProfesionalOfOfertalaboralHabilidadListNewOfertalaboralHabilidad.getOfertalaboralHabilidadList().remove(ofertalaboralHabilidadListNewOfertalaboralHabilidad);
                        oldHabilidadProfesionalOfOfertalaboralHabilidadListNewOfertalaboralHabilidad = em.merge(oldHabilidadProfesionalOfOfertalaboralHabilidadListNewOfertalaboralHabilidad);
                    }
                }
            }
            for (CurriculumHabilidad curriculumHabilidadListNewCurriculumHabilidad : curriculumHabilidadListNew) {
                if (!curriculumHabilidadListOld.contains(curriculumHabilidadListNewCurriculumHabilidad)) {
                    Habilidadprofesional oldHabilidadProfesionalOfCurriculumHabilidadListNewCurriculumHabilidad = curriculumHabilidadListNewCurriculumHabilidad.getHabilidadProfesional();
                    curriculumHabilidadListNewCurriculumHabilidad.setHabilidadProfesional(habilidadprofesional);
                    curriculumHabilidadListNewCurriculumHabilidad = em.merge(curriculumHabilidadListNewCurriculumHabilidad);
                    if (oldHabilidadProfesionalOfCurriculumHabilidadListNewCurriculumHabilidad != null && !oldHabilidadProfesionalOfCurriculumHabilidadListNewCurriculumHabilidad.equals(habilidadprofesional)) {
                        oldHabilidadProfesionalOfCurriculumHabilidadListNewCurriculumHabilidad.getCurriculumHabilidadList().remove(curriculumHabilidadListNewCurriculumHabilidad);
                        oldHabilidadProfesionalOfCurriculumHabilidadListNewCurriculumHabilidad = em.merge(oldHabilidadProfesionalOfCurriculumHabilidadListNewCurriculumHabilidad);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = habilidadprofesional.getId();
                if (findHabilidadprofesional(id) == null) {
                    throw new NonexistentEntityException("The habilidadprofesional with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Habilidadprofesional habilidadprofesional;
            try {
                habilidadprofesional = em.getReference(Habilidadprofesional.class, id);
                habilidadprofesional.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The habilidadprofesional with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<OfertalaboralHabilidad> ofertalaboralHabilidadListOrphanCheck = habilidadprofesional.getOfertalaboralHabilidadList();
            for (OfertalaboralHabilidad ofertalaboralHabilidadListOrphanCheckOfertalaboralHabilidad : ofertalaboralHabilidadListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Habilidadprofesional (" + habilidadprofesional + ") cannot be destroyed since the OfertalaboralHabilidad " + ofertalaboralHabilidadListOrphanCheckOfertalaboralHabilidad + " in its ofertalaboralHabilidadList field has a non-nullable habilidadProfesional field.");
            }
            List<CurriculumHabilidad> curriculumHabilidadListOrphanCheck = habilidadprofesional.getCurriculumHabilidadList();
            for (CurriculumHabilidad curriculumHabilidadListOrphanCheckCurriculumHabilidad : curriculumHabilidadListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Habilidadprofesional (" + habilidadprofesional + ") cannot be destroyed since the CurriculumHabilidad " + curriculumHabilidadListOrphanCheckCurriculumHabilidad + " in its curriculumHabilidadList field has a non-nullable habilidadProfesional field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Arealaboral areaLaboral = habilidadprofesional.getAreaLaboral();
            if (areaLaboral != null) {
                areaLaboral.getHabilidadprofesionalList().remove(habilidadprofesional);
                areaLaboral = em.merge(areaLaboral);
            }
            em.remove(habilidadprofesional);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Habilidadprofesional> findHabilidadprofesionalEntities() {
        return findHabilidadprofesionalEntities(true, -1, -1);
    }

    public List<Habilidadprofesional> findHabilidadprofesionalEntities(int maxResults, int firstResult) {
        return findHabilidadprofesionalEntities(false, maxResults, firstResult);
    }

    private List<Habilidadprofesional> findHabilidadprofesionalEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Habilidadprofesional.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Habilidadprofesional findHabilidadprofesional(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Habilidadprofesional.class, id);
        } finally {
            em.close();
        }
    }

    public int getHabilidadprofesionalCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Habilidadprofesional> rt = cq.from(Habilidadprofesional.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
