package com.workingbriefcase.persistencia.controladores;

import com.workingbriefcase.persistencia.controladores.exceptions.IllegalOrphanException;
import com.workingbriefcase.persistencia.controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.workingbriefcase.persistencia.entities.Empresa;
import com.workingbriefcase.persistencia.entities.Sectorempresarial;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Nombre de la clase: SectorempresarialJpaController
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class SectorempresarialJpaController implements Serializable {

    
    
    public SectorempresarialJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    public SectorempresarialJpaController() {
        this.emf = Persistence.createEntityManagerFactory("Working-BriefcasePU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Sectorempresarial sectorempresarial) {
        if (sectorempresarial.getEmpresaList() == null) {
            sectorempresarial.setEmpresaList(new ArrayList<Empresa>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Empresa> attachedEmpresaList = new ArrayList<Empresa>();
            for (Empresa empresaListEmpresaToAttach : sectorempresarial.getEmpresaList()) {
                empresaListEmpresaToAttach = em.getReference(empresaListEmpresaToAttach.getClass(), empresaListEmpresaToAttach.getId());
                attachedEmpresaList.add(empresaListEmpresaToAttach);
            }
            sectorempresarial.setEmpresaList(attachedEmpresaList);
            em.persist(sectorempresarial);
            for (Empresa empresaListEmpresa : sectorempresarial.getEmpresaList()) {
                Sectorempresarial oldSectorEmpresarialOfEmpresaListEmpresa = empresaListEmpresa.getSectorEmpresarial();
                empresaListEmpresa.setSectorEmpresarial(sectorempresarial);
                empresaListEmpresa = em.merge(empresaListEmpresa);
                if (oldSectorEmpresarialOfEmpresaListEmpresa != null) {
                    oldSectorEmpresarialOfEmpresaListEmpresa.getEmpresaList().remove(empresaListEmpresa);
                    oldSectorEmpresarialOfEmpresaListEmpresa = em.merge(oldSectorEmpresarialOfEmpresaListEmpresa);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Sectorempresarial sectorempresarial) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sectorempresarial persistentSectorempresarial = em.find(Sectorempresarial.class, sectorempresarial.getId());
            List<Empresa> empresaListOld = persistentSectorempresarial.getEmpresaList();
            List<Empresa> empresaListNew = sectorempresarial.getEmpresaList();
            List<String> illegalOrphanMessages = null;
            for (Empresa empresaListOldEmpresa : empresaListOld) {
                if (!empresaListNew.contains(empresaListOldEmpresa)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Empresa " + empresaListOldEmpresa + " since its sectorEmpresarial field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Empresa> attachedEmpresaListNew = new ArrayList<Empresa>();
            for (Empresa empresaListNewEmpresaToAttach : empresaListNew) {
                empresaListNewEmpresaToAttach = em.getReference(empresaListNewEmpresaToAttach.getClass(), empresaListNewEmpresaToAttach.getId());
                attachedEmpresaListNew.add(empresaListNewEmpresaToAttach);
            }
            empresaListNew = attachedEmpresaListNew;
            sectorempresarial.setEmpresaList(empresaListNew);
            sectorempresarial = em.merge(sectorempresarial);
            for (Empresa empresaListNewEmpresa : empresaListNew) {
                if (!empresaListOld.contains(empresaListNewEmpresa)) {
                    Sectorempresarial oldSectorEmpresarialOfEmpresaListNewEmpresa = empresaListNewEmpresa.getSectorEmpresarial();
                    empresaListNewEmpresa.setSectorEmpresarial(sectorempresarial);
                    empresaListNewEmpresa = em.merge(empresaListNewEmpresa);
                    if (oldSectorEmpresarialOfEmpresaListNewEmpresa != null && !oldSectorEmpresarialOfEmpresaListNewEmpresa.equals(sectorempresarial)) {
                        oldSectorEmpresarialOfEmpresaListNewEmpresa.getEmpresaList().remove(empresaListNewEmpresa);
                        oldSectorEmpresarialOfEmpresaListNewEmpresa = em.merge(oldSectorEmpresarialOfEmpresaListNewEmpresa);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = sectorempresarial.getId();
                if (findSectorempresarial(id) == null) {
                    throw new NonexistentEntityException("The sectorempresarial with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sectorempresarial sectorempresarial;
            try {
                sectorempresarial = em.getReference(Sectorempresarial.class, id);
                sectorempresarial.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The sectorempresarial with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Empresa> empresaListOrphanCheck = sectorempresarial.getEmpresaList();
            for (Empresa empresaListOrphanCheckEmpresa : empresaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Sectorempresarial (" + sectorempresarial + ") cannot be destroyed since the Empresa " + empresaListOrphanCheckEmpresa + " in its empresaList field has a non-nullable sectorEmpresarial field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(sectorempresarial);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Sectorempresarial> findSectorempresarialEntities() {
        return findSectorempresarialEntities(true, -1, -1);
    }

    public List<Sectorempresarial> findSectorempresarialEntities(int maxResults, int firstResult) {
        return findSectorempresarialEntities(false, maxResults, firstResult);
    }

    private List<Sectorempresarial> findSectorempresarialEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Sectorempresarial.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Sectorempresarial findSectorempresarial(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Sectorempresarial.class, id);
        } finally {
            em.close();
        }
    }

    public int getSectorempresarialCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Sectorempresarial> rt = cq.from(Sectorempresarial.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
