package com.workingbriefcase.persistencia.controladores;

import com.workingbriefcase.persistencia.controladores.exceptions.IllegalOrphanException;
import com.workingbriefcase.persistencia.controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.workingbriefcase.persistencia.entities.Departamento;
import com.workingbriefcase.persistencia.entities.Empleadoasalariado;
import java.util.ArrayList;
import java.util.List;
import com.workingbriefcase.persistencia.entities.Ofertalaboral;
import com.workingbriefcase.persistencia.entities.Candidato;
import com.workingbriefcase.persistencia.entities.Empleadohora;
import com.workingbriefcase.persistencia.entities.Empresa;
import com.workingbriefcase.persistencia.entities.Municipio;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Nombre de la clase: MunicipioJpaController
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class MunicipioJpaController implements Serializable {

    public MunicipioJpaController() {
         this.emf = Persistence.createEntityManagerFactory("Working-BriefcasePU");
    }

    
    //metodos personalizados
     public List<Municipio> findMunicipioEntitiesByDepartamento(int codigoDepartamento) {
        EntityManager em = getEntityManager();
            Query query;
       
        try {
            
            query =em.createNamedQuery("Municipio.findByDepartamento");
                    query.setParameter("id", codigoDepartamento);
            return query.getResultList();
        } finally {
            em.close();
        }
    }
    
    public MunicipioJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Municipio municipio) {
        if (municipio.getEmpleadoasalariadoList() == null) {
            municipio.setEmpleadoasalariadoList(new ArrayList<Empleadoasalariado>());
        }
        if (municipio.getOfertalaboralList() == null) {
            municipio.setOfertalaboralList(new ArrayList<Ofertalaboral>());
        }
        if (municipio.getCandidatoList() == null) {
            municipio.setCandidatoList(new ArrayList<Candidato>());
        }
        if (municipio.getEmpleadohoraList() == null) {
            municipio.setEmpleadohoraList(new ArrayList<Empleadohora>());
        }
        if (municipio.getEmpresaList() == null) {
            municipio.setEmpresaList(new ArrayList<Empresa>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Departamento departamentoPertenece = municipio.getDepartamentoPertenece();
            if (departamentoPertenece != null) {
                departamentoPertenece = em.getReference(departamentoPertenece.getClass(), departamentoPertenece.getId());
                municipio.setDepartamentoPertenece(departamentoPertenece);
            }
            List<Empleadoasalariado> attachedEmpleadoasalariadoList = new ArrayList<Empleadoasalariado>();
            for (Empleadoasalariado empleadoasalariadoListEmpleadoasalariadoToAttach : municipio.getEmpleadoasalariadoList()) {
                empleadoasalariadoListEmpleadoasalariadoToAttach = em.getReference(empleadoasalariadoListEmpleadoasalariadoToAttach.getClass(), empleadoasalariadoListEmpleadoasalariadoToAttach.getId());
                attachedEmpleadoasalariadoList.add(empleadoasalariadoListEmpleadoasalariadoToAttach);
            }
            municipio.setEmpleadoasalariadoList(attachedEmpleadoasalariadoList);
            List<Ofertalaboral> attachedOfertalaboralList = new ArrayList<Ofertalaboral>();
            for (Ofertalaboral ofertalaboralListOfertalaboralToAttach : municipio.getOfertalaboralList()) {
                ofertalaboralListOfertalaboralToAttach = em.getReference(ofertalaboralListOfertalaboralToAttach.getClass(), ofertalaboralListOfertalaboralToAttach.getId());
                attachedOfertalaboralList.add(ofertalaboralListOfertalaboralToAttach);
            }
            municipio.setOfertalaboralList(attachedOfertalaboralList);
            List<Candidato> attachedCandidatoList = new ArrayList<Candidato>();
            for (Candidato candidatoListCandidatoToAttach : municipio.getCandidatoList()) {
                candidatoListCandidatoToAttach = em.getReference(candidatoListCandidatoToAttach.getClass(), candidatoListCandidatoToAttach.getId());
                attachedCandidatoList.add(candidatoListCandidatoToAttach);
            }
            municipio.setCandidatoList(attachedCandidatoList);
            List<Empleadohora> attachedEmpleadohoraList = new ArrayList<Empleadohora>();
            for (Empleadohora empleadohoraListEmpleadohoraToAttach : municipio.getEmpleadohoraList()) {
                empleadohoraListEmpleadohoraToAttach = em.getReference(empleadohoraListEmpleadohoraToAttach.getClass(), empleadohoraListEmpleadohoraToAttach.getId());
                attachedEmpleadohoraList.add(empleadohoraListEmpleadohoraToAttach);
            }
            municipio.setEmpleadohoraList(attachedEmpleadohoraList);
            List<Empresa> attachedEmpresaList = new ArrayList<Empresa>();
            for (Empresa empresaListEmpresaToAttach : municipio.getEmpresaList()) {
                empresaListEmpresaToAttach = em.getReference(empresaListEmpresaToAttach.getClass(), empresaListEmpresaToAttach.getId());
                attachedEmpresaList.add(empresaListEmpresaToAttach);
            }
            municipio.setEmpresaList(attachedEmpresaList);
            em.persist(municipio);
            if (departamentoPertenece != null) {
                departamentoPertenece.getMunicipioList().add(municipio);
                departamentoPertenece = em.merge(departamentoPertenece);
            }
            for (Empleadoasalariado empleadoasalariadoListEmpleadoasalariado : municipio.getEmpleadoasalariadoList()) {
                Municipio oldMunicipioActualOfEmpleadoasalariadoListEmpleadoasalariado = empleadoasalariadoListEmpleadoasalariado.getMunicipioActual();
                empleadoasalariadoListEmpleadoasalariado.setMunicipioActual(municipio);
                empleadoasalariadoListEmpleadoasalariado = em.merge(empleadoasalariadoListEmpleadoasalariado);
                if (oldMunicipioActualOfEmpleadoasalariadoListEmpleadoasalariado != null) {
                    oldMunicipioActualOfEmpleadoasalariadoListEmpleadoasalariado.getEmpleadoasalariadoList().remove(empleadoasalariadoListEmpleadoasalariado);
                    oldMunicipioActualOfEmpleadoasalariadoListEmpleadoasalariado = em.merge(oldMunicipioActualOfEmpleadoasalariadoListEmpleadoasalariado);
                }
            }
            for (Ofertalaboral ofertalaboralListOfertalaboral : municipio.getOfertalaboralList()) {
                Municipio oldMunicipioEmpleoOfOfertalaboralListOfertalaboral = ofertalaboralListOfertalaboral.getMunicipioEmpleo();
                ofertalaboralListOfertalaboral.setMunicipioEmpleo(municipio);
                ofertalaboralListOfertalaboral = em.merge(ofertalaboralListOfertalaboral);
                if (oldMunicipioEmpleoOfOfertalaboralListOfertalaboral != null) {
                    oldMunicipioEmpleoOfOfertalaboralListOfertalaboral.getOfertalaboralList().remove(ofertalaboralListOfertalaboral);
                    oldMunicipioEmpleoOfOfertalaboralListOfertalaboral = em.merge(oldMunicipioEmpleoOfOfertalaboralListOfertalaboral);
                }
            }
            for (Candidato candidatoListCandidato : municipio.getCandidatoList()) {
                Municipio oldMunicipioActualOfCandidatoListCandidato = candidatoListCandidato.getMunicipioActual();
                candidatoListCandidato.setMunicipioActual(municipio);
                candidatoListCandidato = em.merge(candidatoListCandidato);
                if (oldMunicipioActualOfCandidatoListCandidato != null) {
                    oldMunicipioActualOfCandidatoListCandidato.getCandidatoList().remove(candidatoListCandidato);
                    oldMunicipioActualOfCandidatoListCandidato = em.merge(oldMunicipioActualOfCandidatoListCandidato);
                }
            }
            for (Empleadohora empleadohoraListEmpleadohora : municipio.getEmpleadohoraList()) {
                Municipio oldMunicipioActualOfEmpleadohoraListEmpleadohora = empleadohoraListEmpleadohora.getMunicipioActual();
                empleadohoraListEmpleadohora.setMunicipioActual(municipio);
                empleadohoraListEmpleadohora = em.merge(empleadohoraListEmpleadohora);
                if (oldMunicipioActualOfEmpleadohoraListEmpleadohora != null) {
                    oldMunicipioActualOfEmpleadohoraListEmpleadohora.getEmpleadohoraList().remove(empleadohoraListEmpleadohora);
                    oldMunicipioActualOfEmpleadohoraListEmpleadohora = em.merge(oldMunicipioActualOfEmpleadohoraListEmpleadohora);
                }
            }
            for (Empresa empresaListEmpresa : municipio.getEmpresaList()) {
                Municipio oldMunicipioActualOfEmpresaListEmpresa = empresaListEmpresa.getMunicipioActual();
                empresaListEmpresa.setMunicipioActual(municipio);
                empresaListEmpresa = em.merge(empresaListEmpresa);
                if (oldMunicipioActualOfEmpresaListEmpresa != null) {
                    oldMunicipioActualOfEmpresaListEmpresa.getEmpresaList().remove(empresaListEmpresa);
                    oldMunicipioActualOfEmpresaListEmpresa = em.merge(oldMunicipioActualOfEmpresaListEmpresa);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Municipio municipio) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Municipio persistentMunicipio = em.find(Municipio.class, municipio.getId());
            Departamento departamentoPerteneceOld = persistentMunicipio.getDepartamentoPertenece();
            Departamento departamentoPerteneceNew = municipio.getDepartamentoPertenece();
            List<Empleadoasalariado> empleadoasalariadoListOld = persistentMunicipio.getEmpleadoasalariadoList();
            List<Empleadoasalariado> empleadoasalariadoListNew = municipio.getEmpleadoasalariadoList();
            List<Ofertalaboral> ofertalaboralListOld = persistentMunicipio.getOfertalaboralList();
            List<Ofertalaboral> ofertalaboralListNew = municipio.getOfertalaboralList();
            List<Candidato> candidatoListOld = persistentMunicipio.getCandidatoList();
            List<Candidato> candidatoListNew = municipio.getCandidatoList();
            List<Empleadohora> empleadohoraListOld = persistentMunicipio.getEmpleadohoraList();
            List<Empleadohora> empleadohoraListNew = municipio.getEmpleadohoraList();
            List<Empresa> empresaListOld = persistentMunicipio.getEmpresaList();
            List<Empresa> empresaListNew = municipio.getEmpresaList();
            List<String> illegalOrphanMessages = null;
            for (Empleadoasalariado empleadoasalariadoListOldEmpleadoasalariado : empleadoasalariadoListOld) {
                if (!empleadoasalariadoListNew.contains(empleadoasalariadoListOldEmpleadoasalariado)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Empleadoasalariado " + empleadoasalariadoListOldEmpleadoasalariado + " since its municipioActual field is not nullable.");
                }
            }
            for (Ofertalaboral ofertalaboralListOldOfertalaboral : ofertalaboralListOld) {
                if (!ofertalaboralListNew.contains(ofertalaboralListOldOfertalaboral)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Ofertalaboral " + ofertalaboralListOldOfertalaboral + " since its municipioEmpleo field is not nullable.");
                }
            }
            for (Candidato candidatoListOldCandidato : candidatoListOld) {
                if (!candidatoListNew.contains(candidatoListOldCandidato)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Candidato " + candidatoListOldCandidato + " since its municipioActual field is not nullable.");
                }
            }
            for (Empleadohora empleadohoraListOldEmpleadohora : empleadohoraListOld) {
                if (!empleadohoraListNew.contains(empleadohoraListOldEmpleadohora)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Empleadohora " + empleadohoraListOldEmpleadohora + " since its municipioActual field is not nullable.");
                }
            }
            for (Empresa empresaListOldEmpresa : empresaListOld) {
                if (!empresaListNew.contains(empresaListOldEmpresa)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Empresa " + empresaListOldEmpresa + " since its municipioActual field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (departamentoPerteneceNew != null) {
                departamentoPerteneceNew = em.getReference(departamentoPerteneceNew.getClass(), departamentoPerteneceNew.getId());
                municipio.setDepartamentoPertenece(departamentoPerteneceNew);
            }
            List<Empleadoasalariado> attachedEmpleadoasalariadoListNew = new ArrayList<Empleadoasalariado>();
            for (Empleadoasalariado empleadoasalariadoListNewEmpleadoasalariadoToAttach : empleadoasalariadoListNew) {
                empleadoasalariadoListNewEmpleadoasalariadoToAttach = em.getReference(empleadoasalariadoListNewEmpleadoasalariadoToAttach.getClass(), empleadoasalariadoListNewEmpleadoasalariadoToAttach.getId());
                attachedEmpleadoasalariadoListNew.add(empleadoasalariadoListNewEmpleadoasalariadoToAttach);
            }
            empleadoasalariadoListNew = attachedEmpleadoasalariadoListNew;
            municipio.setEmpleadoasalariadoList(empleadoasalariadoListNew);
            List<Ofertalaboral> attachedOfertalaboralListNew = new ArrayList<Ofertalaboral>();
            for (Ofertalaboral ofertalaboralListNewOfertalaboralToAttach : ofertalaboralListNew) {
                ofertalaboralListNewOfertalaboralToAttach = em.getReference(ofertalaboralListNewOfertalaboralToAttach.getClass(), ofertalaboralListNewOfertalaboralToAttach.getId());
                attachedOfertalaboralListNew.add(ofertalaboralListNewOfertalaboralToAttach);
            }
            ofertalaboralListNew = attachedOfertalaboralListNew;
            municipio.setOfertalaboralList(ofertalaboralListNew);
            List<Candidato> attachedCandidatoListNew = new ArrayList<Candidato>();
            for (Candidato candidatoListNewCandidatoToAttach : candidatoListNew) {
                candidatoListNewCandidatoToAttach = em.getReference(candidatoListNewCandidatoToAttach.getClass(), candidatoListNewCandidatoToAttach.getId());
                attachedCandidatoListNew.add(candidatoListNewCandidatoToAttach);
            }
            candidatoListNew = attachedCandidatoListNew;
            municipio.setCandidatoList(candidatoListNew);
            List<Empleadohora> attachedEmpleadohoraListNew = new ArrayList<Empleadohora>();
            for (Empleadohora empleadohoraListNewEmpleadohoraToAttach : empleadohoraListNew) {
                empleadohoraListNewEmpleadohoraToAttach = em.getReference(empleadohoraListNewEmpleadohoraToAttach.getClass(), empleadohoraListNewEmpleadohoraToAttach.getId());
                attachedEmpleadohoraListNew.add(empleadohoraListNewEmpleadohoraToAttach);
            }
            empleadohoraListNew = attachedEmpleadohoraListNew;
            municipio.setEmpleadohoraList(empleadohoraListNew);
            List<Empresa> attachedEmpresaListNew = new ArrayList<Empresa>();
            for (Empresa empresaListNewEmpresaToAttach : empresaListNew) {
                empresaListNewEmpresaToAttach = em.getReference(empresaListNewEmpresaToAttach.getClass(), empresaListNewEmpresaToAttach.getId());
                attachedEmpresaListNew.add(empresaListNewEmpresaToAttach);
            }
            empresaListNew = attachedEmpresaListNew;
            municipio.setEmpresaList(empresaListNew);
            municipio = em.merge(municipio);
            if (departamentoPerteneceOld != null && !departamentoPerteneceOld.equals(departamentoPerteneceNew)) {
                departamentoPerteneceOld.getMunicipioList().remove(municipio);
                departamentoPerteneceOld = em.merge(departamentoPerteneceOld);
            }
            if (departamentoPerteneceNew != null && !departamentoPerteneceNew.equals(departamentoPerteneceOld)) {
                departamentoPerteneceNew.getMunicipioList().add(municipio);
                departamentoPerteneceNew = em.merge(departamentoPerteneceNew);
            }
            for (Empleadoasalariado empleadoasalariadoListNewEmpleadoasalariado : empleadoasalariadoListNew) {
                if (!empleadoasalariadoListOld.contains(empleadoasalariadoListNewEmpleadoasalariado)) {
                    Municipio oldMunicipioActualOfEmpleadoasalariadoListNewEmpleadoasalariado = empleadoasalariadoListNewEmpleadoasalariado.getMunicipioActual();
                    empleadoasalariadoListNewEmpleadoasalariado.setMunicipioActual(municipio);
                    empleadoasalariadoListNewEmpleadoasalariado = em.merge(empleadoasalariadoListNewEmpleadoasalariado);
                    if (oldMunicipioActualOfEmpleadoasalariadoListNewEmpleadoasalariado != null && !oldMunicipioActualOfEmpleadoasalariadoListNewEmpleadoasalariado.equals(municipio)) {
                        oldMunicipioActualOfEmpleadoasalariadoListNewEmpleadoasalariado.getEmpleadoasalariadoList().remove(empleadoasalariadoListNewEmpleadoasalariado);
                        oldMunicipioActualOfEmpleadoasalariadoListNewEmpleadoasalariado = em.merge(oldMunicipioActualOfEmpleadoasalariadoListNewEmpleadoasalariado);
                    }
                }
            }
            for (Ofertalaboral ofertalaboralListNewOfertalaboral : ofertalaboralListNew) {
                if (!ofertalaboralListOld.contains(ofertalaboralListNewOfertalaboral)) {
                    Municipio oldMunicipioEmpleoOfOfertalaboralListNewOfertalaboral = ofertalaboralListNewOfertalaboral.getMunicipioEmpleo();
                    ofertalaboralListNewOfertalaboral.setMunicipioEmpleo(municipio);
                    ofertalaboralListNewOfertalaboral = em.merge(ofertalaboralListNewOfertalaboral);
                    if (oldMunicipioEmpleoOfOfertalaboralListNewOfertalaboral != null && !oldMunicipioEmpleoOfOfertalaboralListNewOfertalaboral.equals(municipio)) {
                        oldMunicipioEmpleoOfOfertalaboralListNewOfertalaboral.getOfertalaboralList().remove(ofertalaboralListNewOfertalaboral);
                        oldMunicipioEmpleoOfOfertalaboralListNewOfertalaboral = em.merge(oldMunicipioEmpleoOfOfertalaboralListNewOfertalaboral);
                    }
                }
            }
            for (Candidato candidatoListNewCandidato : candidatoListNew) {
                if (!candidatoListOld.contains(candidatoListNewCandidato)) {
                    Municipio oldMunicipioActualOfCandidatoListNewCandidato = candidatoListNewCandidato.getMunicipioActual();
                    candidatoListNewCandidato.setMunicipioActual(municipio);
                    candidatoListNewCandidato = em.merge(candidatoListNewCandidato);
                    if (oldMunicipioActualOfCandidatoListNewCandidato != null && !oldMunicipioActualOfCandidatoListNewCandidato.equals(municipio)) {
                        oldMunicipioActualOfCandidatoListNewCandidato.getCandidatoList().remove(candidatoListNewCandidato);
                        oldMunicipioActualOfCandidatoListNewCandidato = em.merge(oldMunicipioActualOfCandidatoListNewCandidato);
                    }
                }
            }
            for (Empleadohora empleadohoraListNewEmpleadohora : empleadohoraListNew) {
                if (!empleadohoraListOld.contains(empleadohoraListNewEmpleadohora)) {
                    Municipio oldMunicipioActualOfEmpleadohoraListNewEmpleadohora = empleadohoraListNewEmpleadohora.getMunicipioActual();
                    empleadohoraListNewEmpleadohora.setMunicipioActual(municipio);
                    empleadohoraListNewEmpleadohora = em.merge(empleadohoraListNewEmpleadohora);
                    if (oldMunicipioActualOfEmpleadohoraListNewEmpleadohora != null && !oldMunicipioActualOfEmpleadohoraListNewEmpleadohora.equals(municipio)) {
                        oldMunicipioActualOfEmpleadohoraListNewEmpleadohora.getEmpleadohoraList().remove(empleadohoraListNewEmpleadohora);
                        oldMunicipioActualOfEmpleadohoraListNewEmpleadohora = em.merge(oldMunicipioActualOfEmpleadohoraListNewEmpleadohora);
                    }
                }
            }
            for (Empresa empresaListNewEmpresa : empresaListNew) {
                if (!empresaListOld.contains(empresaListNewEmpresa)) {
                    Municipio oldMunicipioActualOfEmpresaListNewEmpresa = empresaListNewEmpresa.getMunicipioActual();
                    empresaListNewEmpresa.setMunicipioActual(municipio);
                    empresaListNewEmpresa = em.merge(empresaListNewEmpresa);
                    if (oldMunicipioActualOfEmpresaListNewEmpresa != null && !oldMunicipioActualOfEmpresaListNewEmpresa.equals(municipio)) {
                        oldMunicipioActualOfEmpresaListNewEmpresa.getEmpresaList().remove(empresaListNewEmpresa);
                        oldMunicipioActualOfEmpresaListNewEmpresa = em.merge(oldMunicipioActualOfEmpresaListNewEmpresa);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = municipio.getId();
                if (findMunicipio(id) == null) {
                    throw new NonexistentEntityException("The municipio with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Municipio municipio;
            try {
                municipio = em.getReference(Municipio.class, id);
                municipio.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The municipio with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Empleadoasalariado> empleadoasalariadoListOrphanCheck = municipio.getEmpleadoasalariadoList();
            for (Empleadoasalariado empleadoasalariadoListOrphanCheckEmpleadoasalariado : empleadoasalariadoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Municipio (" + municipio + ") cannot be destroyed since the Empleadoasalariado " + empleadoasalariadoListOrphanCheckEmpleadoasalariado + " in its empleadoasalariadoList field has a non-nullable municipioActual field.");
            }
            List<Ofertalaboral> ofertalaboralListOrphanCheck = municipio.getOfertalaboralList();
            for (Ofertalaboral ofertalaboralListOrphanCheckOfertalaboral : ofertalaboralListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Municipio (" + municipio + ") cannot be destroyed since the Ofertalaboral " + ofertalaboralListOrphanCheckOfertalaboral + " in its ofertalaboralList field has a non-nullable municipioEmpleo field.");
            }
            List<Candidato> candidatoListOrphanCheck = municipio.getCandidatoList();
            for (Candidato candidatoListOrphanCheckCandidato : candidatoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Municipio (" + municipio + ") cannot be destroyed since the Candidato " + candidatoListOrphanCheckCandidato + " in its candidatoList field has a non-nullable municipioActual field.");
            }
            List<Empleadohora> empleadohoraListOrphanCheck = municipio.getEmpleadohoraList();
            for (Empleadohora empleadohoraListOrphanCheckEmpleadohora : empleadohoraListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Municipio (" + municipio + ") cannot be destroyed since the Empleadohora " + empleadohoraListOrphanCheckEmpleadohora + " in its empleadohoraList field has a non-nullable municipioActual field.");
            }
            List<Empresa> empresaListOrphanCheck = municipio.getEmpresaList();
            for (Empresa empresaListOrphanCheckEmpresa : empresaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Municipio (" + municipio + ") cannot be destroyed since the Empresa " + empresaListOrphanCheckEmpresa + " in its empresaList field has a non-nullable municipioActual field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Departamento departamentoPertenece = municipio.getDepartamentoPertenece();
            if (departamentoPertenece != null) {
                departamentoPertenece.getMunicipioList().remove(municipio);
                departamentoPertenece = em.merge(departamentoPertenece);
            }
            em.remove(municipio);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Municipio> findMunicipioEntities() {
        return findMunicipioEntities(true, -1, -1);
    }

    public List<Municipio> findMunicipioEntities(int maxResults, int firstResult) {
        return findMunicipioEntities(false, maxResults, firstResult);
    }

    private List<Municipio> findMunicipioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Municipio.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    
    public Municipio findMunicipio(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Municipio.class, id);
        } finally {
            em.close();
        }
    }

    public int getMunicipioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Municipio> rt = cq.from(Municipio.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
