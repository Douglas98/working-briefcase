package com.workingbriefcase.persistencia.controladores;

import com.workingbriefcase.persistencia.controladores.exceptions.IllegalOrphanException;
import com.workingbriefcase.persistencia.controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.workingbriefcase.persistencia.entities.Candidato;
import com.workingbriefcase.persistencia.entities.CurriculumCurso;
import com.workingbriefcase.persistencia.entities.Curso;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Nombre de la clase: CursoJpaController
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class CursoJpaController implements Serializable {

    public CursoJpaController() {
        this.emf = emf = Persistence.createEntityManagerFactory("Working-BriefcasePU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Curso curso) {
        if (curso.getCurriculumCursoList() == null) {
            curso.setCurriculumCursoList(new ArrayList<CurriculumCurso>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Candidato candidatoPertenece = curso.getCandidatoPertenece();
            if (candidatoPertenece != null) {
                candidatoPertenece = em.getReference(candidatoPertenece.getClass(), candidatoPertenece.getId());
                curso.setCandidatoPertenece(candidatoPertenece);
            }
            List<CurriculumCurso> attachedCurriculumCursoList = new ArrayList<CurriculumCurso>();
            for (CurriculumCurso curriculumCursoListCurriculumCursoToAttach : curso.getCurriculumCursoList()) {
                curriculumCursoListCurriculumCursoToAttach = em.getReference(curriculumCursoListCurriculumCursoToAttach.getClass(), curriculumCursoListCurriculumCursoToAttach.getId());
                attachedCurriculumCursoList.add(curriculumCursoListCurriculumCursoToAttach);
            }
            curso.setCurriculumCursoList(attachedCurriculumCursoList);
            em.persist(curso);
            if (candidatoPertenece != null) {
                candidatoPertenece.getCursoList().add(curso);
                candidatoPertenece = em.merge(candidatoPertenece);
            }
            for (CurriculumCurso curriculumCursoListCurriculumCurso : curso.getCurriculumCursoList()) {
                Curso oldCursoOfCurriculumCursoListCurriculumCurso = curriculumCursoListCurriculumCurso.getCurso();
                curriculumCursoListCurriculumCurso.setCurso(curso);
                curriculumCursoListCurriculumCurso = em.merge(curriculumCursoListCurriculumCurso);
                if (oldCursoOfCurriculumCursoListCurriculumCurso != null) {
                    oldCursoOfCurriculumCursoListCurriculumCurso.getCurriculumCursoList().remove(curriculumCursoListCurriculumCurso);
                    oldCursoOfCurriculumCursoListCurriculumCurso = em.merge(oldCursoOfCurriculumCursoListCurriculumCurso);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Curso curso) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Curso persistentCurso = em.find(Curso.class, curso.getId());
            Candidato candidatoPerteneceOld = persistentCurso.getCandidatoPertenece();
            Candidato candidatoPerteneceNew = curso.getCandidatoPertenece();
            List<CurriculumCurso> curriculumCursoListOld = persistentCurso.getCurriculumCursoList();
            List<CurriculumCurso> curriculumCursoListNew = curso.getCurriculumCursoList();
            List<String> illegalOrphanMessages = null;
            for (CurriculumCurso curriculumCursoListOldCurriculumCurso : curriculumCursoListOld) {
                if (!curriculumCursoListNew.contains(curriculumCursoListOldCurriculumCurso)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain CurriculumCurso " + curriculumCursoListOldCurriculumCurso + " since its curso field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (candidatoPerteneceNew != null) {
                candidatoPerteneceNew = em.getReference(candidatoPerteneceNew.getClass(), candidatoPerteneceNew.getId());
                curso.setCandidatoPertenece(candidatoPerteneceNew);
            }
            List<CurriculumCurso> attachedCurriculumCursoListNew = new ArrayList<CurriculumCurso>();
            for (CurriculumCurso curriculumCursoListNewCurriculumCursoToAttach : curriculumCursoListNew) {
                curriculumCursoListNewCurriculumCursoToAttach = em.getReference(curriculumCursoListNewCurriculumCursoToAttach.getClass(), curriculumCursoListNewCurriculumCursoToAttach.getId());
                attachedCurriculumCursoListNew.add(curriculumCursoListNewCurriculumCursoToAttach);
            }
            curriculumCursoListNew = attachedCurriculumCursoListNew;
            curso.setCurriculumCursoList(curriculumCursoListNew);
            curso = em.merge(curso);
            if (candidatoPerteneceOld != null && !candidatoPerteneceOld.equals(candidatoPerteneceNew)) {
                candidatoPerteneceOld.getCursoList().remove(curso);
                candidatoPerteneceOld = em.merge(candidatoPerteneceOld);
            }
            if (candidatoPerteneceNew != null && !candidatoPerteneceNew.equals(candidatoPerteneceOld)) {
                candidatoPerteneceNew.getCursoList().add(curso);
                candidatoPerteneceNew = em.merge(candidatoPerteneceNew);
            }
            for (CurriculumCurso curriculumCursoListNewCurriculumCurso : curriculumCursoListNew) {
                if (!curriculumCursoListOld.contains(curriculumCursoListNewCurriculumCurso)) {
                    Curso oldCursoOfCurriculumCursoListNewCurriculumCurso = curriculumCursoListNewCurriculumCurso.getCurso();
                    curriculumCursoListNewCurriculumCurso.setCurso(curso);
                    curriculumCursoListNewCurriculumCurso = em.merge(curriculumCursoListNewCurriculumCurso);
                    if (oldCursoOfCurriculumCursoListNewCurriculumCurso != null && !oldCursoOfCurriculumCursoListNewCurriculumCurso.equals(curso)) {
                        oldCursoOfCurriculumCursoListNewCurriculumCurso.getCurriculumCursoList().remove(curriculumCursoListNewCurriculumCurso);
                        oldCursoOfCurriculumCursoListNewCurriculumCurso = em.merge(oldCursoOfCurriculumCursoListNewCurriculumCurso);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = curso.getId();
                if (findCurso(id) == null) {
                    throw new NonexistentEntityException("The curso with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Curso curso;
            try {
                curso = em.getReference(Curso.class, id);
                curso.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The curso with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<CurriculumCurso> curriculumCursoListOrphanCheck = curso.getCurriculumCursoList();
            for (CurriculumCurso curriculumCursoListOrphanCheckCurriculumCurso : curriculumCursoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Curso (" + curso + ") cannot be destroyed since the CurriculumCurso " + curriculumCursoListOrphanCheckCurriculumCurso + " in its curriculumCursoList field has a non-nullable curso field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Candidato candidatoPertenece = curso.getCandidatoPertenece();
            if (candidatoPertenece != null) {
                candidatoPertenece.getCursoList().remove(curso);
                candidatoPertenece = em.merge(candidatoPertenece);
            }
            em.remove(curso);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Curso> findCursoEntities() {
        return findCursoEntities(true, -1, -1);
    }

    public List<Curso> findCursoEntities(int maxResults, int firstResult) {
        return findCursoEntities(false, maxResults, firstResult);
    }

    private List<Curso> findCursoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Curso.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Curso findCurso(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Curso.class, id);
        } finally {
            em.close();
        }
    }

    public int getCursoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Curso> rt = cq.from(Curso.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
