package com.workingbriefcase.persistencia.controladores;

import com.workingbriefcase.persistencia.controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.workingbriefcase.persistencia.entities.Curriculum;
import com.workingbriefcase.persistencia.entities.Propuestatrabajo;
import com.workingbriefcase.persistencia.entities.PropuestatrabajoCurriculum;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Nombre de la clase: PropuestatrabajoCurriculumJpaController
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class PropuestatrabajoCurriculumJpaController implements Serializable {

     public PropuestatrabajoCurriculumJpaController() {
        this.emf = Persistence.createEntityManagerFactory("Working-BriefcasePU");
    }
    public PropuestatrabajoCurriculumJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(PropuestatrabajoCurriculum propuestatrabajoCurriculum) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Curriculum curriculum = propuestatrabajoCurriculum.getCurriculum();
            if (curriculum != null) {
                curriculum = em.getReference(curriculum.getClass(), curriculum.getId());
                propuestatrabajoCurriculum.setCurriculum(curriculum);
            }
            Propuestatrabajo propuestaTrabajo = propuestatrabajoCurriculum.getPropuestaTrabajo();
            if (propuestaTrabajo != null) {
                propuestaTrabajo = em.getReference(propuestaTrabajo.getClass(), propuestaTrabajo.getId());
                propuestatrabajoCurriculum.setPropuestaTrabajo(propuestaTrabajo);
            }
            em.persist(propuestatrabajoCurriculum);
            if (curriculum != null) {
                curriculum.getPropuestatrabajoCurriculumList().add(propuestatrabajoCurriculum);
                curriculum = em.merge(curriculum);
            }
            if (propuestaTrabajo != null) {
                propuestaTrabajo.getPropuestatrabajoCurriculumList().add(propuestatrabajoCurriculum);
                propuestaTrabajo = em.merge(propuestaTrabajo);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PropuestatrabajoCurriculum propuestatrabajoCurriculum) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PropuestatrabajoCurriculum persistentPropuestatrabajoCurriculum = em.find(PropuestatrabajoCurriculum.class, propuestatrabajoCurriculum.getId());
            Curriculum curriculumOld = persistentPropuestatrabajoCurriculum.getCurriculum();
            Curriculum curriculumNew = propuestatrabajoCurriculum.getCurriculum();
            Propuestatrabajo propuestaTrabajoOld = persistentPropuestatrabajoCurriculum.getPropuestaTrabajo();
            Propuestatrabajo propuestaTrabajoNew = propuestatrabajoCurriculum.getPropuestaTrabajo();
            if (curriculumNew != null) {
                curriculumNew = em.getReference(curriculumNew.getClass(), curriculumNew.getId());
                propuestatrabajoCurriculum.setCurriculum(curriculumNew);
            }
            if (propuestaTrabajoNew != null) {
                propuestaTrabajoNew = em.getReference(propuestaTrabajoNew.getClass(), propuestaTrabajoNew.getId());
                propuestatrabajoCurriculum.setPropuestaTrabajo(propuestaTrabajoNew);
            }
            propuestatrabajoCurriculum = em.merge(propuestatrabajoCurriculum);
            if (curriculumOld != null && !curriculumOld.equals(curriculumNew)) {
                curriculumOld.getPropuestatrabajoCurriculumList().remove(propuestatrabajoCurriculum);
                curriculumOld = em.merge(curriculumOld);
            }
            if (curriculumNew != null && !curriculumNew.equals(curriculumOld)) {
                curriculumNew.getPropuestatrabajoCurriculumList().add(propuestatrabajoCurriculum);
                curriculumNew = em.merge(curriculumNew);
            }
            if (propuestaTrabajoOld != null && !propuestaTrabajoOld.equals(propuestaTrabajoNew)) {
                propuestaTrabajoOld.getPropuestatrabajoCurriculumList().remove(propuestatrabajoCurriculum);
                propuestaTrabajoOld = em.merge(propuestaTrabajoOld);
            }
            if (propuestaTrabajoNew != null && !propuestaTrabajoNew.equals(propuestaTrabajoOld)) {
                propuestaTrabajoNew.getPropuestatrabajoCurriculumList().add(propuestatrabajoCurriculum);
                propuestaTrabajoNew = em.merge(propuestaTrabajoNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = propuestatrabajoCurriculum.getId();
                if (findPropuestatrabajoCurriculum(id) == null) {
                    throw new NonexistentEntityException("The propuestatrabajoCurriculum with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PropuestatrabajoCurriculum propuestatrabajoCurriculum;
            try {
                propuestatrabajoCurriculum = em.getReference(PropuestatrabajoCurriculum.class, id);
                propuestatrabajoCurriculum.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The propuestatrabajoCurriculum with id " + id + " no longer exists.", enfe);
            }
            Curriculum curriculum = propuestatrabajoCurriculum.getCurriculum();
            if (curriculum != null) {
                curriculum.getPropuestatrabajoCurriculumList().remove(propuestatrabajoCurriculum);
                curriculum = em.merge(curriculum);
            }
            Propuestatrabajo propuestaTrabajo = propuestatrabajoCurriculum.getPropuestaTrabajo();
            if (propuestaTrabajo != null) {
                propuestaTrabajo.getPropuestatrabajoCurriculumList().remove(propuestatrabajoCurriculum);
                propuestaTrabajo = em.merge(propuestaTrabajo);
            }
            em.remove(propuestatrabajoCurriculum);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PropuestatrabajoCurriculum> findPropuestatrabajoCurriculumEntities() {
        return findPropuestatrabajoCurriculumEntities(true, -1, -1);
    }

    public List<PropuestatrabajoCurriculum> findPropuestatrabajoCurriculumEntities(int maxResults, int firstResult) {
        return findPropuestatrabajoCurriculumEntities(false, maxResults, firstResult);
    }

    private List<PropuestatrabajoCurriculum> findPropuestatrabajoCurriculumEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(PropuestatrabajoCurriculum.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PropuestatrabajoCurriculum findPropuestatrabajoCurriculum(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(PropuestatrabajoCurriculum.class, id);
        } finally {
            em.close();
        }
    }

    public int getPropuestatrabajoCurriculumCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<PropuestatrabajoCurriculum> rt = cq.from(PropuestatrabajoCurriculum.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
