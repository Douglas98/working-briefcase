/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.workingbriefcase.persistencia.controladores;

import com.workingbriefcase.persistencia.controladores.exceptions.IllegalOrphanException;
import com.workingbriefcase.persistencia.controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.workingbriefcase.persistencia.entities.Cargo;
import com.workingbriefcase.persistencia.entities.Municipio;
import com.workingbriefcase.persistencia.entities.Ofertalaboral;
import com.workingbriefcase.persistencia.entities.Profesion;
import com.workingbriefcase.persistencia.entities.OfertalaboralHabilidad;
import java.util.ArrayList;
import java.util.List;
import com.workingbriefcase.persistencia.entities.Propuestatrabajo;
import com.workingbriefcase.persistencia.entities.OfertalaboralIdioma;
import com.workingbriefcase.persistencia.entities.OfertalaboralCurriculum;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author hola
 */
public class OfertalaboralJpaController implements Serializable {

        public OfertalaboralJpaController() {
        this.emf = Persistence.createEntityManagerFactory("Working-BriefcasePU");
    }
    public OfertalaboralJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

     public List obtenerByEmpresa(int empresa){

        EntityManager em;
        Query query;
        List resultado = null;
        
       
        em = null;
        try {
            em = getEntityManager();
            
            //creando consulta
            query =em.createNamedQuery("Ofertalaboral.findByEmpresa");
            query.setParameter("idEmpresa", empresa);

            
            //"ejecutando" query
            
            resultado = query.getResultList();
            //solo debería haber 1 item en el resultado si tuvo exito sino 0
            if(resultado.size() > 0){
               return resultado ;
            }
        } catch (Exception e) {
            e.printStackTrace();      
        }finally{
            //cerrar el EntityManger
            if (em != null) {
                em.close();
            }
        }
        
        return resultado;
    }
    
    
    public void create(Ofertalaboral ofertalaboral) {
        if (ofertalaboral.getOfertalaboralHabilidadList() == null) {
            ofertalaboral.setOfertalaboralHabilidadList(new ArrayList<OfertalaboralHabilidad>());
        }
        if (ofertalaboral.getPropuestatrabajoList() == null) {
            ofertalaboral.setPropuestatrabajoList(new ArrayList<Propuestatrabajo>());
        }
        if (ofertalaboral.getOfertalaboralIdiomaList() == null) {
            ofertalaboral.setOfertalaboralIdiomaList(new ArrayList<OfertalaboralIdioma>());
        }
        if (ofertalaboral.getOfertalaboralCurriculumList() == null) {
            ofertalaboral.setOfertalaboralCurriculumList(new ArrayList<OfertalaboralCurriculum>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cargo cargoSolicitado = ofertalaboral.getCargoSolicitado();
            if (cargoSolicitado != null) {
                cargoSolicitado = em.getReference(cargoSolicitado.getClass(), cargoSolicitado.getId());
                ofertalaboral.setCargoSolicitado(cargoSolicitado);
            }
            Municipio municipioEmpleo = ofertalaboral.getMunicipioEmpleo();
            if (municipioEmpleo != null) {
                municipioEmpleo = em.getReference(municipioEmpleo.getClass(), municipioEmpleo.getId());
                ofertalaboral.setMunicipioEmpleo(municipioEmpleo);
            }
            Profesion profesionRequerida = ofertalaboral.getProfesionRequerida();
            if (profesionRequerida != null) {
                profesionRequerida = em.getReference(profesionRequerida.getClass(), profesionRequerida.getId());
                ofertalaboral.setProfesionRequerida(profesionRequerida);
            }
            List<OfertalaboralHabilidad> attachedOfertalaboralHabilidadList = new ArrayList<OfertalaboralHabilidad>();
            for (OfertalaboralHabilidad ofertalaboralHabilidadListOfertalaboralHabilidadToAttach : ofertalaboral.getOfertalaboralHabilidadList()) {
                ofertalaboralHabilidadListOfertalaboralHabilidadToAttach = em.getReference(ofertalaboralHabilidadListOfertalaboralHabilidadToAttach.getClass(), ofertalaboralHabilidadListOfertalaboralHabilidadToAttach.getId());
                attachedOfertalaboralHabilidadList.add(ofertalaboralHabilidadListOfertalaboralHabilidadToAttach);
            }
            ofertalaboral.setOfertalaboralHabilidadList(attachedOfertalaboralHabilidadList);
            List<Propuestatrabajo> attachedPropuestatrabajoList = new ArrayList<Propuestatrabajo>();
            for (Propuestatrabajo propuestatrabajoListPropuestatrabajoToAttach : ofertalaboral.getPropuestatrabajoList()) {
                propuestatrabajoListPropuestatrabajoToAttach = em.getReference(propuestatrabajoListPropuestatrabajoToAttach.getClass(), propuestatrabajoListPropuestatrabajoToAttach.getId());
                attachedPropuestatrabajoList.add(propuestatrabajoListPropuestatrabajoToAttach);
            }
            ofertalaboral.setPropuestatrabajoList(attachedPropuestatrabajoList);
            List<OfertalaboralIdioma> attachedOfertalaboralIdiomaList = new ArrayList<OfertalaboralIdioma>();
            for (OfertalaboralIdioma ofertalaboralIdiomaListOfertalaboralIdiomaToAttach : ofertalaboral.getOfertalaboralIdiomaList()) {
                ofertalaboralIdiomaListOfertalaboralIdiomaToAttach = em.getReference(ofertalaboralIdiomaListOfertalaboralIdiomaToAttach.getClass(), ofertalaboralIdiomaListOfertalaboralIdiomaToAttach.getId());
                attachedOfertalaboralIdiomaList.add(ofertalaboralIdiomaListOfertalaboralIdiomaToAttach);
            }
            ofertalaboral.setOfertalaboralIdiomaList(attachedOfertalaboralIdiomaList);
            List<OfertalaboralCurriculum> attachedOfertalaboralCurriculumList = new ArrayList<OfertalaboralCurriculum>();
            for (OfertalaboralCurriculum ofertalaboralCurriculumListOfertalaboralCurriculumToAttach : ofertalaboral.getOfertalaboralCurriculumList()) {
                ofertalaboralCurriculumListOfertalaboralCurriculumToAttach = em.getReference(ofertalaboralCurriculumListOfertalaboralCurriculumToAttach.getClass(), ofertalaboralCurriculumListOfertalaboralCurriculumToAttach.getId());
                attachedOfertalaboralCurriculumList.add(ofertalaboralCurriculumListOfertalaboralCurriculumToAttach);
            }
            ofertalaboral.setOfertalaboralCurriculumList(attachedOfertalaboralCurriculumList);
            em.persist(ofertalaboral);
            if (cargoSolicitado != null) {
                cargoSolicitado.getOfertalaboralList().add(ofertalaboral);
                cargoSolicitado = em.merge(cargoSolicitado);
            }
            if (municipioEmpleo != null) {
                municipioEmpleo.getOfertalaboralList().add(ofertalaboral);
                municipioEmpleo = em.merge(municipioEmpleo);
            }
            if (profesionRequerida != null) {
                profesionRequerida.getOfertalaboralList().add(ofertalaboral);
                profesionRequerida = em.merge(profesionRequerida);
            }
            for (OfertalaboralHabilidad ofertalaboralHabilidadListOfertalaboralHabilidad : ofertalaboral.getOfertalaboralHabilidadList()) {
                Ofertalaboral oldOfertaLaboralOfOfertalaboralHabilidadListOfertalaboralHabilidad = ofertalaboralHabilidadListOfertalaboralHabilidad.getOfertaLaboral();
                ofertalaboralHabilidadListOfertalaboralHabilidad.setOfertaLaboral(ofertalaboral);
                ofertalaboralHabilidadListOfertalaboralHabilidad = em.merge(ofertalaboralHabilidadListOfertalaboralHabilidad);
                if (oldOfertaLaboralOfOfertalaboralHabilidadListOfertalaboralHabilidad != null) {
                    oldOfertaLaboralOfOfertalaboralHabilidadListOfertalaboralHabilidad.getOfertalaboralHabilidadList().remove(ofertalaboralHabilidadListOfertalaboralHabilidad);
                    oldOfertaLaboralOfOfertalaboralHabilidadListOfertalaboralHabilidad = em.merge(oldOfertaLaboralOfOfertalaboralHabilidadListOfertalaboralHabilidad);
                }
            }
            for (Propuestatrabajo propuestatrabajoListPropuestatrabajo : ofertalaboral.getPropuestatrabajoList()) {
                Ofertalaboral oldOfertaLaboralOfPropuestatrabajoListPropuestatrabajo = propuestatrabajoListPropuestatrabajo.getOfertaLaboral();
                propuestatrabajoListPropuestatrabajo.setOfertaLaboral(ofertalaboral);
                propuestatrabajoListPropuestatrabajo = em.merge(propuestatrabajoListPropuestatrabajo);
                if (oldOfertaLaboralOfPropuestatrabajoListPropuestatrabajo != null) {
                    oldOfertaLaboralOfPropuestatrabajoListPropuestatrabajo.getPropuestatrabajoList().remove(propuestatrabajoListPropuestatrabajo);
                    oldOfertaLaboralOfPropuestatrabajoListPropuestatrabajo = em.merge(oldOfertaLaboralOfPropuestatrabajoListPropuestatrabajo);
                }
            }
            for (OfertalaboralIdioma ofertalaboralIdiomaListOfertalaboralIdioma : ofertalaboral.getOfertalaboralIdiomaList()) {
                Ofertalaboral oldOfertaLaboralOfOfertalaboralIdiomaListOfertalaboralIdioma = ofertalaboralIdiomaListOfertalaboralIdioma.getOfertaLaboral();
                ofertalaboralIdiomaListOfertalaboralIdioma.setOfertaLaboral(ofertalaboral);
                ofertalaboralIdiomaListOfertalaboralIdioma = em.merge(ofertalaboralIdiomaListOfertalaboralIdioma);
                if (oldOfertaLaboralOfOfertalaboralIdiomaListOfertalaboralIdioma != null) {
                    oldOfertaLaboralOfOfertalaboralIdiomaListOfertalaboralIdioma.getOfertalaboralIdiomaList().remove(ofertalaboralIdiomaListOfertalaboralIdioma);
                    oldOfertaLaboralOfOfertalaboralIdiomaListOfertalaboralIdioma = em.merge(oldOfertaLaboralOfOfertalaboralIdiomaListOfertalaboralIdioma);
                }
            }
            for (OfertalaboralCurriculum ofertalaboralCurriculumListOfertalaboralCurriculum : ofertalaboral.getOfertalaboralCurriculumList()) {
                Ofertalaboral oldOfertaLaboralOfOfertalaboralCurriculumListOfertalaboralCurriculum = ofertalaboralCurriculumListOfertalaboralCurriculum.getOfertaLaboral();
                ofertalaboralCurriculumListOfertalaboralCurriculum.setOfertaLaboral(ofertalaboral);
                ofertalaboralCurriculumListOfertalaboralCurriculum = em.merge(ofertalaboralCurriculumListOfertalaboralCurriculum);
                if (oldOfertaLaboralOfOfertalaboralCurriculumListOfertalaboralCurriculum != null) {
                    oldOfertaLaboralOfOfertalaboralCurriculumListOfertalaboralCurriculum.getOfertalaboralCurriculumList().remove(ofertalaboralCurriculumListOfertalaboralCurriculum);
                    oldOfertaLaboralOfOfertalaboralCurriculumListOfertalaboralCurriculum = em.merge(oldOfertaLaboralOfOfertalaboralCurriculumListOfertalaboralCurriculum);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Ofertalaboral ofertalaboral) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ofertalaboral persistentOfertalaboral = em.find(Ofertalaboral.class, ofertalaboral.getId());
            Cargo cargoSolicitadoOld = persistentOfertalaboral.getCargoSolicitado();
            Cargo cargoSolicitadoNew = ofertalaboral.getCargoSolicitado();
            Municipio municipioEmpleoOld = persistentOfertalaboral.getMunicipioEmpleo();
            Municipio municipioEmpleoNew = ofertalaboral.getMunicipioEmpleo();
            Profesion profesionRequeridaOld = persistentOfertalaboral.getProfesionRequerida();
            Profesion profesionRequeridaNew = ofertalaboral.getProfesionRequerida();
            List<OfertalaboralHabilidad> ofertalaboralHabilidadListOld = persistentOfertalaboral.getOfertalaboralHabilidadList();
            List<OfertalaboralHabilidad> ofertalaboralHabilidadListNew = ofertalaboral.getOfertalaboralHabilidadList();
            List<Propuestatrabajo> propuestatrabajoListOld = persistentOfertalaboral.getPropuestatrabajoList();
            List<Propuestatrabajo> propuestatrabajoListNew = ofertalaboral.getPropuestatrabajoList();
            List<OfertalaboralIdioma> ofertalaboralIdiomaListOld = persistentOfertalaboral.getOfertalaboralIdiomaList();
            List<OfertalaboralIdioma> ofertalaboralIdiomaListNew = ofertalaboral.getOfertalaboralIdiomaList();
            List<OfertalaboralCurriculum> ofertalaboralCurriculumListOld = persistentOfertalaboral.getOfertalaboralCurriculumList();
            List<OfertalaboralCurriculum> ofertalaboralCurriculumListNew = ofertalaboral.getOfertalaboralCurriculumList();
            List<String> illegalOrphanMessages = null;
            for (OfertalaboralHabilidad ofertalaboralHabilidadListOldOfertalaboralHabilidad : ofertalaboralHabilidadListOld) {
                if (!ofertalaboralHabilidadListNew.contains(ofertalaboralHabilidadListOldOfertalaboralHabilidad)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain OfertalaboralHabilidad " + ofertalaboralHabilidadListOldOfertalaboralHabilidad + " since its ofertaLaboral field is not nullable.");
                }
            }
            for (Propuestatrabajo propuestatrabajoListOldPropuestatrabajo : propuestatrabajoListOld) {
                if (!propuestatrabajoListNew.contains(propuestatrabajoListOldPropuestatrabajo)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Propuestatrabajo " + propuestatrabajoListOldPropuestatrabajo + " since its ofertaLaboral field is not nullable.");
                }
            }
            for (OfertalaboralIdioma ofertalaboralIdiomaListOldOfertalaboralIdioma : ofertalaboralIdiomaListOld) {
                if (!ofertalaboralIdiomaListNew.contains(ofertalaboralIdiomaListOldOfertalaboralIdioma)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain OfertalaboralIdioma " + ofertalaboralIdiomaListOldOfertalaboralIdioma + " since its ofertaLaboral field is not nullable.");
                }
            }
            for (OfertalaboralCurriculum ofertalaboralCurriculumListOldOfertalaboralCurriculum : ofertalaboralCurriculumListOld) {
                if (!ofertalaboralCurriculumListNew.contains(ofertalaboralCurriculumListOldOfertalaboralCurriculum)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain OfertalaboralCurriculum " + ofertalaboralCurriculumListOldOfertalaboralCurriculum + " since its ofertaLaboral field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (cargoSolicitadoNew != null) {
                cargoSolicitadoNew = em.getReference(cargoSolicitadoNew.getClass(), cargoSolicitadoNew.getId());
                ofertalaboral.setCargoSolicitado(cargoSolicitadoNew);
            }
            if (municipioEmpleoNew != null) {
                municipioEmpleoNew = em.getReference(municipioEmpleoNew.getClass(), municipioEmpleoNew.getId());
                ofertalaboral.setMunicipioEmpleo(municipioEmpleoNew);
            }
            if (profesionRequeridaNew != null) {
                profesionRequeridaNew = em.getReference(profesionRequeridaNew.getClass(), profesionRequeridaNew.getId());
                ofertalaboral.setProfesionRequerida(profesionRequeridaNew);
            }
            List<OfertalaboralHabilidad> attachedOfertalaboralHabilidadListNew = new ArrayList<OfertalaboralHabilidad>();
            for (OfertalaboralHabilidad ofertalaboralHabilidadListNewOfertalaboralHabilidadToAttach : ofertalaboralHabilidadListNew) {
                ofertalaboralHabilidadListNewOfertalaboralHabilidadToAttach = em.getReference(ofertalaboralHabilidadListNewOfertalaboralHabilidadToAttach.getClass(), ofertalaboralHabilidadListNewOfertalaboralHabilidadToAttach.getId());
                attachedOfertalaboralHabilidadListNew.add(ofertalaboralHabilidadListNewOfertalaboralHabilidadToAttach);
            }
            ofertalaboralHabilidadListNew = attachedOfertalaboralHabilidadListNew;
            ofertalaboral.setOfertalaboralHabilidadList(ofertalaboralHabilidadListNew);
            List<Propuestatrabajo> attachedPropuestatrabajoListNew = new ArrayList<Propuestatrabajo>();
            for (Propuestatrabajo propuestatrabajoListNewPropuestatrabajoToAttach : propuestatrabajoListNew) {
                propuestatrabajoListNewPropuestatrabajoToAttach = em.getReference(propuestatrabajoListNewPropuestatrabajoToAttach.getClass(), propuestatrabajoListNewPropuestatrabajoToAttach.getId());
                attachedPropuestatrabajoListNew.add(propuestatrabajoListNewPropuestatrabajoToAttach);
            }
            propuestatrabajoListNew = attachedPropuestatrabajoListNew;
            ofertalaboral.setPropuestatrabajoList(propuestatrabajoListNew);
            List<OfertalaboralIdioma> attachedOfertalaboralIdiomaListNew = new ArrayList<OfertalaboralIdioma>();
            for (OfertalaboralIdioma ofertalaboralIdiomaListNewOfertalaboralIdiomaToAttach : ofertalaboralIdiomaListNew) {
                ofertalaboralIdiomaListNewOfertalaboralIdiomaToAttach = em.getReference(ofertalaboralIdiomaListNewOfertalaboralIdiomaToAttach.getClass(), ofertalaboralIdiomaListNewOfertalaboralIdiomaToAttach.getId());
                attachedOfertalaboralIdiomaListNew.add(ofertalaboralIdiomaListNewOfertalaboralIdiomaToAttach);
            }
            ofertalaboralIdiomaListNew = attachedOfertalaboralIdiomaListNew;
            ofertalaboral.setOfertalaboralIdiomaList(ofertalaboralIdiomaListNew);
            List<OfertalaboralCurriculum> attachedOfertalaboralCurriculumListNew = new ArrayList<OfertalaboralCurriculum>();
            for (OfertalaboralCurriculum ofertalaboralCurriculumListNewOfertalaboralCurriculumToAttach : ofertalaboralCurriculumListNew) {
                ofertalaboralCurriculumListNewOfertalaboralCurriculumToAttach = em.getReference(ofertalaboralCurriculumListNewOfertalaboralCurriculumToAttach.getClass(), ofertalaboralCurriculumListNewOfertalaboralCurriculumToAttach.getId());
                attachedOfertalaboralCurriculumListNew.add(ofertalaboralCurriculumListNewOfertalaboralCurriculumToAttach);
            }
            ofertalaboralCurriculumListNew = attachedOfertalaboralCurriculumListNew;
            ofertalaboral.setOfertalaboralCurriculumList(ofertalaboralCurriculumListNew);
            ofertalaboral = em.merge(ofertalaboral);
            if (cargoSolicitadoOld != null && !cargoSolicitadoOld.equals(cargoSolicitadoNew)) {
                cargoSolicitadoOld.getOfertalaboralList().remove(ofertalaboral);
                cargoSolicitadoOld = em.merge(cargoSolicitadoOld);
            }
            if (cargoSolicitadoNew != null && !cargoSolicitadoNew.equals(cargoSolicitadoOld)) {
                cargoSolicitadoNew.getOfertalaboralList().add(ofertalaboral);
                cargoSolicitadoNew = em.merge(cargoSolicitadoNew);
            }
            if (municipioEmpleoOld != null && !municipioEmpleoOld.equals(municipioEmpleoNew)) {
                municipioEmpleoOld.getOfertalaboralList().remove(ofertalaboral);
                municipioEmpleoOld = em.merge(municipioEmpleoOld);
            }
            if (municipioEmpleoNew != null && !municipioEmpleoNew.equals(municipioEmpleoOld)) {
                municipioEmpleoNew.getOfertalaboralList().add(ofertalaboral);
                municipioEmpleoNew = em.merge(municipioEmpleoNew);
            }
            if (profesionRequeridaOld != null && !profesionRequeridaOld.equals(profesionRequeridaNew)) {
                profesionRequeridaOld.getOfertalaboralList().remove(ofertalaboral);
                profesionRequeridaOld = em.merge(profesionRequeridaOld);
            }
            if (profesionRequeridaNew != null && !profesionRequeridaNew.equals(profesionRequeridaOld)) {
                profesionRequeridaNew.getOfertalaboralList().add(ofertalaboral);
                profesionRequeridaNew = em.merge(profesionRequeridaNew);
            }
            for (OfertalaboralHabilidad ofertalaboralHabilidadListNewOfertalaboralHabilidad : ofertalaboralHabilidadListNew) {
                if (!ofertalaboralHabilidadListOld.contains(ofertalaboralHabilidadListNewOfertalaboralHabilidad)) {
                    Ofertalaboral oldOfertaLaboralOfOfertalaboralHabilidadListNewOfertalaboralHabilidad = ofertalaboralHabilidadListNewOfertalaboralHabilidad.getOfertaLaboral();
                    ofertalaboralHabilidadListNewOfertalaboralHabilidad.setOfertaLaboral(ofertalaboral);
                    ofertalaboralHabilidadListNewOfertalaboralHabilidad = em.merge(ofertalaboralHabilidadListNewOfertalaboralHabilidad);
                    if (oldOfertaLaboralOfOfertalaboralHabilidadListNewOfertalaboralHabilidad != null && !oldOfertaLaboralOfOfertalaboralHabilidadListNewOfertalaboralHabilidad.equals(ofertalaboral)) {
                        oldOfertaLaboralOfOfertalaboralHabilidadListNewOfertalaboralHabilidad.getOfertalaboralHabilidadList().remove(ofertalaboralHabilidadListNewOfertalaboralHabilidad);
                        oldOfertaLaboralOfOfertalaboralHabilidadListNewOfertalaboralHabilidad = em.merge(oldOfertaLaboralOfOfertalaboralHabilidadListNewOfertalaboralHabilidad);
                    }
                }
            }
            for (Propuestatrabajo propuestatrabajoListNewPropuestatrabajo : propuestatrabajoListNew) {
                if (!propuestatrabajoListOld.contains(propuestatrabajoListNewPropuestatrabajo)) {
                    Ofertalaboral oldOfertaLaboralOfPropuestatrabajoListNewPropuestatrabajo = propuestatrabajoListNewPropuestatrabajo.getOfertaLaboral();
                    propuestatrabajoListNewPropuestatrabajo.setOfertaLaboral(ofertalaboral);
                    propuestatrabajoListNewPropuestatrabajo = em.merge(propuestatrabajoListNewPropuestatrabajo);
                    if (oldOfertaLaboralOfPropuestatrabajoListNewPropuestatrabajo != null && !oldOfertaLaboralOfPropuestatrabajoListNewPropuestatrabajo.equals(ofertalaboral)) {
                        oldOfertaLaboralOfPropuestatrabajoListNewPropuestatrabajo.getPropuestatrabajoList().remove(propuestatrabajoListNewPropuestatrabajo);
                        oldOfertaLaboralOfPropuestatrabajoListNewPropuestatrabajo = em.merge(oldOfertaLaboralOfPropuestatrabajoListNewPropuestatrabajo);
                    }
                }
            }
            for (OfertalaboralIdioma ofertalaboralIdiomaListNewOfertalaboralIdioma : ofertalaboralIdiomaListNew) {
                if (!ofertalaboralIdiomaListOld.contains(ofertalaboralIdiomaListNewOfertalaboralIdioma)) {
                    Ofertalaboral oldOfertaLaboralOfOfertalaboralIdiomaListNewOfertalaboralIdioma = ofertalaboralIdiomaListNewOfertalaboralIdioma.getOfertaLaboral();
                    ofertalaboralIdiomaListNewOfertalaboralIdioma.setOfertaLaboral(ofertalaboral);
                    ofertalaboralIdiomaListNewOfertalaboralIdioma = em.merge(ofertalaboralIdiomaListNewOfertalaboralIdioma);
                    if (oldOfertaLaboralOfOfertalaboralIdiomaListNewOfertalaboralIdioma != null && !oldOfertaLaboralOfOfertalaboralIdiomaListNewOfertalaboralIdioma.equals(ofertalaboral)) {
                        oldOfertaLaboralOfOfertalaboralIdiomaListNewOfertalaboralIdioma.getOfertalaboralIdiomaList().remove(ofertalaboralIdiomaListNewOfertalaboralIdioma);
                        oldOfertaLaboralOfOfertalaboralIdiomaListNewOfertalaboralIdioma = em.merge(oldOfertaLaboralOfOfertalaboralIdiomaListNewOfertalaboralIdioma);
                    }
                }
            }
            for (OfertalaboralCurriculum ofertalaboralCurriculumListNewOfertalaboralCurriculum : ofertalaboralCurriculumListNew) {
                if (!ofertalaboralCurriculumListOld.contains(ofertalaboralCurriculumListNewOfertalaboralCurriculum)) {
                    Ofertalaboral oldOfertaLaboralOfOfertalaboralCurriculumListNewOfertalaboralCurriculum = ofertalaboralCurriculumListNewOfertalaboralCurriculum.getOfertaLaboral();
                    ofertalaboralCurriculumListNewOfertalaboralCurriculum.setOfertaLaboral(ofertalaboral);
                    ofertalaboralCurriculumListNewOfertalaboralCurriculum = em.merge(ofertalaboralCurriculumListNewOfertalaboralCurriculum);
                    if (oldOfertaLaboralOfOfertalaboralCurriculumListNewOfertalaboralCurriculum != null && !oldOfertaLaboralOfOfertalaboralCurriculumListNewOfertalaboralCurriculum.equals(ofertalaboral)) {
                        oldOfertaLaboralOfOfertalaboralCurriculumListNewOfertalaboralCurriculum.getOfertalaboralCurriculumList().remove(ofertalaboralCurriculumListNewOfertalaboralCurriculum);
                        oldOfertaLaboralOfOfertalaboralCurriculumListNewOfertalaboralCurriculum = em.merge(oldOfertaLaboralOfOfertalaboralCurriculumListNewOfertalaboralCurriculum);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = ofertalaboral.getId();
                if (findOfertalaboral(id) == null) {
                    throw new NonexistentEntityException("The ofertalaboral with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ofertalaboral ofertalaboral;
            try {
                ofertalaboral = em.getReference(Ofertalaboral.class, id);
                ofertalaboral.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ofertalaboral with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<OfertalaboralHabilidad> ofertalaboralHabilidadListOrphanCheck = ofertalaboral.getOfertalaboralHabilidadList();
            for (OfertalaboralHabilidad ofertalaboralHabilidadListOrphanCheckOfertalaboralHabilidad : ofertalaboralHabilidadListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Ofertalaboral (" + ofertalaboral + ") cannot be destroyed since the OfertalaboralHabilidad " + ofertalaboralHabilidadListOrphanCheckOfertalaboralHabilidad + " in its ofertalaboralHabilidadList field has a non-nullable ofertaLaboral field.");
            }
            List<Propuestatrabajo> propuestatrabajoListOrphanCheck = ofertalaboral.getPropuestatrabajoList();
            for (Propuestatrabajo propuestatrabajoListOrphanCheckPropuestatrabajo : propuestatrabajoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Ofertalaboral (" + ofertalaboral + ") cannot be destroyed since the Propuestatrabajo " + propuestatrabajoListOrphanCheckPropuestatrabajo + " in its propuestatrabajoList field has a non-nullable ofertaLaboral field.");
            }
            List<OfertalaboralIdioma> ofertalaboralIdiomaListOrphanCheck = ofertalaboral.getOfertalaboralIdiomaList();
            for (OfertalaboralIdioma ofertalaboralIdiomaListOrphanCheckOfertalaboralIdioma : ofertalaboralIdiomaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Ofertalaboral (" + ofertalaboral + ") cannot be destroyed since the OfertalaboralIdioma " + ofertalaboralIdiomaListOrphanCheckOfertalaboralIdioma + " in its ofertalaboralIdiomaList field has a non-nullable ofertaLaboral field.");
            }
            List<OfertalaboralCurriculum> ofertalaboralCurriculumListOrphanCheck = ofertalaboral.getOfertalaboralCurriculumList();
            for (OfertalaboralCurriculum ofertalaboralCurriculumListOrphanCheckOfertalaboralCurriculum : ofertalaboralCurriculumListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Ofertalaboral (" + ofertalaboral + ") cannot be destroyed since the OfertalaboralCurriculum " + ofertalaboralCurriculumListOrphanCheckOfertalaboralCurriculum + " in its ofertalaboralCurriculumList field has a non-nullable ofertaLaboral field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Cargo cargoSolicitado = ofertalaboral.getCargoSolicitado();
            if (cargoSolicitado != null) {
                cargoSolicitado.getOfertalaboralList().remove(ofertalaboral);
                cargoSolicitado = em.merge(cargoSolicitado);
            }
            Municipio municipioEmpleo = ofertalaboral.getMunicipioEmpleo();
            if (municipioEmpleo != null) {
                municipioEmpleo.getOfertalaboralList().remove(ofertalaboral);
                municipioEmpleo = em.merge(municipioEmpleo);
            }
            Profesion profesionRequerida = ofertalaboral.getProfesionRequerida();
            if (profesionRequerida != null) {
                profesionRequerida.getOfertalaboralList().remove(ofertalaboral);
                profesionRequerida = em.merge(profesionRequerida);
            }
            em.remove(ofertalaboral);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Ofertalaboral> findOfertalaboralEntities() {
        return findOfertalaboralEntities(true, -1, -1);
    }

    public List<Ofertalaboral> findOfertalaboralEntities(int maxResults, int firstResult) {
        return findOfertalaboralEntities(false, maxResults, firstResult);
    }

    private List<Ofertalaboral> findOfertalaboralEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Ofertalaboral.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Ofertalaboral findOfertalaboral(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Ofertalaboral.class, id);
        } finally {
            em.close();
        }
    }

    public int getOfertalaboralCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Ofertalaboral> rt = cq.from(Ofertalaboral.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
