package com.workingbriefcase.persistencia.controladores;

import com.workingbriefcase.persistencia.controladores.exceptions.IllegalOrphanException;
import com.workingbriefcase.persistencia.controladores.exceptions.NonexistentEntityException;
import com.workingbriefcase.persistencia.controladores.exceptions.PreexistingEntityException;
import com.workingbriefcase.persistencia.entities.Candidato;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.workingbriefcase.persistencia.entities.Municipio;
import com.workingbriefcase.persistencia.entities.Pais;
import com.workingbriefcase.persistencia.entities.Usuario;
import com.workingbriefcase.persistencia.entities.Curriculum;
import java.util.ArrayList;
import java.util.List;
import com.workingbriefcase.persistencia.entities.Curso;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Nombre de la clase: CandidatoJpaController
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class CandidatoJpaController implements Serializable {

    public CandidatoJpaController() {
        this.emf = Persistence.createEntityManagerFactory("Working-BriefcasePU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    //<editor-fold defaultstate="collapsed" desc="funciones personalizadas">
    /**
    * Verifica que un usuario exista y que su contraseña sea valida
    * @param usuario
    * @param contrasenna
    * @return instancia de Candidato con los datos del Candidato logeado o 
    * null si la operación falló
    */
    public Candidato logIn(String usuario, String contrasenna){
        Candidato salida;
        EntityManager em;
        Query query;
        List resultado;
        
        salida = null;
        em = null;
        try {
            em = getEntityManager();
            
            //creando consulta
            query =em.createNamedQuery("Candidato.logIn");
            query.setParameter("nombreUsuario", usuario);
            query.setParameter("contrasenna", contrasenna);
            
            //"ejecutando" query
            resultado = query.getResultList();
            
            //solo debería haber 1 item en el resultado si tuvo exito sino 0
            if(resultado.size() > 0){
                salida = (Candidato) resultado.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();      
        }finally{
            //cerrar el EntityManger
            if (em != null) {
                em.close();
            }
        }
        
        return salida;
    }
    
   
//</editor-fold>
public void create(Candidato candidato) throws PreexistingEntityException, Exception {
        if (candidato.getCursoList() == null) {
            candidato.setCursoList(new ArrayList<Curso>());
        }
        if (candidato.getCurriculumList() == null) {
            candidato.setCurriculumList(new ArrayList<Curriculum>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Municipio municipioActual = candidato.getMunicipioActual();
            if (municipioActual != null) {
                municipioActual = em.getReference(municipioActual.getClass(), municipioActual.getId());
                candidato.setMunicipioActual(municipioActual);
            }
            Pais nacionalidad = candidato.getNacionalidad();
            if (nacionalidad != null) {
                nacionalidad = em.getReference(nacionalidad.getClass(), nacionalidad.getId());
                candidato.setNacionalidad(nacionalidad);
            }
            Usuario usuario = candidato.getUsuario();
            if (usuario != null) {
                usuario = em.getReference(usuario.getClass(), usuario.getId());
                candidato.setUsuario(usuario);
            }
            List<Curso> attachedCursoList = new ArrayList<Curso>();
            for (Curso cursoListCursoToAttach : candidato.getCursoList()) {
                cursoListCursoToAttach = em.getReference(cursoListCursoToAttach.getClass(), cursoListCursoToAttach.getId());
                attachedCursoList.add(cursoListCursoToAttach);
            }
            candidato.setCursoList(attachedCursoList);
            List<Curriculum> attachedCurriculumList = new ArrayList<Curriculum>();
            for (Curriculum curriculumListCurriculumToAttach : candidato.getCurriculumList()) {
                curriculumListCurriculumToAttach = em.getReference(curriculumListCurriculumToAttach.getClass(), curriculumListCurriculumToAttach.getId());
                attachedCurriculumList.add(curriculumListCurriculumToAttach);
            }
            candidato.setCurriculumList(attachedCurriculumList);
            em.persist(candidato);
            if (municipioActual != null) {
                municipioActual.getCandidatoList().add(candidato);
                municipioActual = em.merge(municipioActual);
            }
            if (nacionalidad != null) {
                nacionalidad.getCandidatoList().add(candidato);
                nacionalidad = em.merge(nacionalidad);
            }
            if (usuario != null) {
                usuario.getCandidatoList().add(candidato);
                usuario = em.merge(usuario);
            }
            for (Curso cursoListCurso : candidato.getCursoList()) {
                Candidato oldCandidatoPerteneceOfCursoListCurso = cursoListCurso.getCandidatoPertenece();
                cursoListCurso.setCandidatoPertenece(candidato);
                cursoListCurso = em.merge(cursoListCurso);
                if (oldCandidatoPerteneceOfCursoListCurso != null) {
                    oldCandidatoPerteneceOfCursoListCurso.getCursoList().remove(cursoListCurso);
                    oldCandidatoPerteneceOfCursoListCurso = em.merge(oldCandidatoPerteneceOfCursoListCurso);
                }
            }
            for (Curriculum curriculumListCurriculum : candidato.getCurriculumList()) {
                Candidato oldCandidatoPerteneceOfCurriculumListCurriculum = curriculumListCurriculum.getCandidatoPertenece();
                curriculumListCurriculum.setCandidatoPertenece(candidato);
                curriculumListCurriculum = em.merge(curriculumListCurriculum);
                if (oldCandidatoPerteneceOfCurriculumListCurriculum != null) {
                    oldCandidatoPerteneceOfCurriculumListCurriculum.getCurriculumList().remove(curriculumListCurriculum);
                    oldCandidatoPerteneceOfCurriculumListCurriculum = em.merge(oldCandidatoPerteneceOfCurriculumListCurriculum);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCandidato(candidato.getId()) != null) {
                throw new PreexistingEntityException("Candidato " + candidato + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }


    public void findCon(Candidato candidato) {
        if (candidato.getCurriculumList() == null) {
            candidato.setCurriculumList(new ArrayList<Curriculum>());
        }
        if (candidato.getCursoList() == null) {
            candidato.setCursoList(new ArrayList<Curso>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Municipio municipioActual = candidato.getMunicipioActual();
            if (municipioActual != null) {
                municipioActual = em.getReference(municipioActual.getClass(), municipioActual.getId());
                candidato.setMunicipioActual(municipioActual);
            }
            Pais nacionalidad = candidato.getNacionalidad();
            if (nacionalidad != null) {
                nacionalidad = em.getReference(nacionalidad.getClass(), nacionalidad.getId());
                candidato.setNacionalidad(nacionalidad);
            }
            /*Usuario usuario = candidato.getUsuario();
            if (usuario != null) {
                usuario = em.getReference(usuario.getClass(), usuario.getId());
                candidato.setUsuario(usuario);
            }*/
            List<Curriculum> attachedCurriculumList = new ArrayList<Curriculum>();
            for (Curriculum curriculumListCurriculumToAttach : candidato.getCurriculumList()) {
                curriculumListCurriculumToAttach = em.getReference(curriculumListCurriculumToAttach.getClass(), curriculumListCurriculumToAttach.getId());
                attachedCurriculumList.add(curriculumListCurriculumToAttach);
            }
            candidato.setCurriculumList(attachedCurriculumList);
            List<Curso> attachedCursoList = new ArrayList<Curso>();
            for (Curso cursoListCursoToAttach : candidato.getCursoList()) {
                cursoListCursoToAttach = em.getReference(cursoListCursoToAttach.getClass(), cursoListCursoToAttach.getId());
                attachedCursoList.add(cursoListCursoToAttach);
            }
            candidato.setCursoList(attachedCursoList);
            em.persist(candidato);
            if (municipioActual != null) {
                municipioActual.getCandidatoList().add(candidato);
                municipioActual = em.merge(municipioActual);
            }
            if (nacionalidad != null) {
                nacionalidad.getCandidatoList().add(candidato);
                nacionalidad = em.merge(nacionalidad);
            }
            /*if (usuario != null) {
                usuario.getCandidatoList().add(candidato);
                usuario = em.merge(usuario);
            }*/
            for (Curriculum curriculumListCurriculum : candidato.getCurriculumList()) {
                Candidato oldCandidatoPerteneceOfCurriculumListCurriculum = curriculumListCurriculum.getCandidatoPertenece();
                curriculumListCurriculum.setCandidatoPertenece(candidato);
                curriculumListCurriculum = em.merge(curriculumListCurriculum);
                if (oldCandidatoPerteneceOfCurriculumListCurriculum != null) {
                    oldCandidatoPerteneceOfCurriculumListCurriculum.getCurriculumList().remove(curriculumListCurriculum);
                    oldCandidatoPerteneceOfCurriculumListCurriculum = em.merge(oldCandidatoPerteneceOfCurriculumListCurriculum);
                }
            }
            for (Curso cursoListCurso : candidato.getCursoList()) {
                Candidato oldCandidatoPerteneceOfCursoListCurso = cursoListCurso.getCandidatoPertenece();
                cursoListCurso.setCandidatoPertenece(candidato);
                cursoListCurso = em.merge(cursoListCurso);
                if (oldCandidatoPerteneceOfCursoListCurso != null) {
                    oldCandidatoPerteneceOfCursoListCurso.getCursoList().remove(cursoListCurso);
                    oldCandidatoPerteneceOfCursoListCurso = em.merge(oldCandidatoPerteneceOfCursoListCurso);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Candidato candidato) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Candidato persistentCandidato = em.find(Candidato.class, candidato.getId());
            Municipio municipioActualOld = persistentCandidato.getMunicipioActual();
            Municipio municipioActualNew = candidato.getMunicipioActual();
            Pais nacionalidadOld = persistentCandidato.getNacionalidad();
            Pais nacionalidadNew = candidato.getNacionalidad();
            Usuario usuarioOld = persistentCandidato.getUsuario();
            Usuario usuarioNew = candidato.getUsuario();
            List<Curriculum> curriculumListOld = persistentCandidato.getCurriculumList();
            List<Curriculum> curriculumListNew = candidato.getCurriculumList();
            List<Curso> cursoListOld = persistentCandidato.getCursoList();
            List<Curso> cursoListNew = candidato.getCursoList();
            List<String> illegalOrphanMessages = null;
            for (Curriculum curriculumListOldCurriculum : curriculumListOld) {
                if (!curriculumListNew.contains(curriculumListOldCurriculum)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Curriculum " + curriculumListOldCurriculum + " since its candidatoPertenece field is not nullable.");
                }
            }
            for (Curso cursoListOldCurso : cursoListOld) {
                if (!cursoListNew.contains(cursoListOldCurso)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Curso " + cursoListOldCurso + " since its candidatoPertenece field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (municipioActualNew != null) {
                municipioActualNew = em.getReference(municipioActualNew.getClass(), municipioActualNew.getId());
                candidato.setMunicipioActual(municipioActualNew);
            }
            if (nacionalidadNew != null) {
                nacionalidadNew = em.getReference(nacionalidadNew.getClass(), nacionalidadNew.getId());
                candidato.setNacionalidad(nacionalidadNew);
            }
            if (usuarioNew != null) {
                usuarioNew = em.getReference(usuarioNew.getClass(), usuarioNew.getId());
                candidato.setUsuario(usuarioNew);
            }
            List<Curriculum> attachedCurriculumListNew = new ArrayList<Curriculum>();
            for (Curriculum curriculumListNewCurriculumToAttach : curriculumListNew) {
                curriculumListNewCurriculumToAttach = em.getReference(curriculumListNewCurriculumToAttach.getClass(), curriculumListNewCurriculumToAttach.getId());
                attachedCurriculumListNew.add(curriculumListNewCurriculumToAttach);
            }
            curriculumListNew = attachedCurriculumListNew;
            candidato.setCurriculumList(curriculumListNew);
            List<Curso> attachedCursoListNew = new ArrayList<Curso>();
            for (Curso cursoListNewCursoToAttach : cursoListNew) {
                cursoListNewCursoToAttach = em.getReference(cursoListNewCursoToAttach.getClass(), cursoListNewCursoToAttach.getId());
                attachedCursoListNew.add(cursoListNewCursoToAttach);
            }
            cursoListNew = attachedCursoListNew;
            candidato.setCursoList(cursoListNew);
            candidato = em.merge(candidato);
            if (municipioActualOld != null && !municipioActualOld.equals(municipioActualNew)) {
                municipioActualOld.getCandidatoList().remove(candidato);
                municipioActualOld = em.merge(municipioActualOld);
            }
            if (municipioActualNew != null && !municipioActualNew.equals(municipioActualOld)) {
                municipioActualNew.getCandidatoList().add(candidato);
                municipioActualNew = em.merge(municipioActualNew);
            }
            if (nacionalidadOld != null && !nacionalidadOld.equals(nacionalidadNew)) {
                nacionalidadOld.getCandidatoList().remove(candidato);
                nacionalidadOld = em.merge(nacionalidadOld);
            }
            if (nacionalidadNew != null && !nacionalidadNew.equals(nacionalidadOld)) {
                nacionalidadNew.getCandidatoList().add(candidato);
                nacionalidadNew = em.merge(nacionalidadNew);
            }
            if (usuarioOld != null && !usuarioOld.equals(usuarioNew)) {
                usuarioOld.getCandidatoList().remove(candidato);
                usuarioOld = em.merge(usuarioOld);
            }
            if (usuarioNew != null && !usuarioNew.equals(usuarioOld)) {
                usuarioNew.getCandidatoList().add(candidato);
                usuarioNew = em.merge(usuarioNew);
            }
            for (Curriculum curriculumListNewCurriculum : curriculumListNew) {
                if (!curriculumListOld.contains(curriculumListNewCurriculum)) {
                    Candidato oldCandidatoPerteneceOfCurriculumListNewCurriculum = curriculumListNewCurriculum.getCandidatoPertenece();
                    curriculumListNewCurriculum.setCandidatoPertenece(candidato);
                    curriculumListNewCurriculum = em.merge(curriculumListNewCurriculum);
                    if (oldCandidatoPerteneceOfCurriculumListNewCurriculum != null && !oldCandidatoPerteneceOfCurriculumListNewCurriculum.equals(candidato)) {
                        oldCandidatoPerteneceOfCurriculumListNewCurriculum.getCurriculumList().remove(curriculumListNewCurriculum);
                        oldCandidatoPerteneceOfCurriculumListNewCurriculum = em.merge(oldCandidatoPerteneceOfCurriculumListNewCurriculum);
                    }
                }
            }
            for (Curso cursoListNewCurso : cursoListNew) {
                if (!cursoListOld.contains(cursoListNewCurso)) {
                    Candidato oldCandidatoPerteneceOfCursoListNewCurso = cursoListNewCurso.getCandidatoPertenece();
                    cursoListNewCurso.setCandidatoPertenece(candidato);
                    cursoListNewCurso = em.merge(cursoListNewCurso);
                    if (oldCandidatoPerteneceOfCursoListNewCurso != null && !oldCandidatoPerteneceOfCursoListNewCurso.equals(candidato)) {
                        oldCandidatoPerteneceOfCursoListNewCurso.getCursoList().remove(cursoListNewCurso);
                        oldCandidatoPerteneceOfCursoListNewCurso = em.merge(oldCandidatoPerteneceOfCursoListNewCurso);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = candidato.getId();
                if (findCandidato(id) == null) {
                    throw new NonexistentEntityException("The candidato with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Candidato candidato;
            try {
                candidato = em.getReference(Candidato.class, id);
                candidato.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The candidato with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Curriculum> curriculumListOrphanCheck = candidato.getCurriculumList();
            for (Curriculum curriculumListOrphanCheckCurriculum : curriculumListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Candidato (" + candidato + ") cannot be destroyed since the Curriculum " + curriculumListOrphanCheckCurriculum + " in its curriculumList field has a non-nullable candidatoPertenece field.");
            }
            List<Curso> cursoListOrphanCheck = candidato.getCursoList();
            for (Curso cursoListOrphanCheckCurso : cursoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Candidato (" + candidato + ") cannot be destroyed since the Curso " + cursoListOrphanCheckCurso + " in its cursoList field has a non-nullable candidatoPertenece field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Municipio municipioActual = candidato.getMunicipioActual();
            if (municipioActual != null) {
                municipioActual.getCandidatoList().remove(candidato);
                municipioActual = em.merge(municipioActual);
            }
            Pais nacionalidad = candidato.getNacionalidad();
            if (nacionalidad != null) {
                nacionalidad.getCandidatoList().remove(candidato);
                nacionalidad = em.merge(nacionalidad);
            }
            Usuario usuario = candidato.getUsuario();
            if (usuario != null) {
                usuario.getCandidatoList().remove(candidato);
                usuario = em.merge(usuario);
            }
            em.remove(candidato);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Candidato> findCandidatoEntities() {
        return findCandidatoEntities(true, -1, -1);
    }

    public List<Candidato> findCandidatoEntities(int maxResults, int firstResult) {
        return findCandidatoEntities(false, maxResults, firstResult);
    }

    private List<Candidato> findCandidatoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Candidato.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Candidato findCandidato(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Candidato.class, id);
        } finally {
            em.close();
        }
    }

    public int getCandidatoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Candidato> rt = cq.from(Candidato.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
