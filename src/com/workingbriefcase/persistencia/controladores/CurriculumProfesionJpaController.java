package com.workingbriefcase.persistencia.controladores;

import com.workingbriefcase.persistencia.controladores.exceptions.NonexistentEntityException;
import com.workingbriefcase.persistencia.entities.Candidato;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.workingbriefcase.persistencia.entities.Curriculum;
import com.workingbriefcase.persistencia.entities.CurriculumProfesion;
import com.workingbriefcase.persistencia.entities.Profesion;
import java.sql.ResultSet;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Nombre de la clase: CurriculumProfesionJpaController
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class CurriculumProfesionJpaController implements Serializable {

      public CurriculumProfesionJpaController() {
        this.emf = Persistence.createEntityManagerFactory("Working-BriefcasePU");
    }
    public CurriculumProfesionJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
     public List findByAreaLaboralAndAnios(int areaLaboral, int anios){
  
        EntityManager em;
        Query query;
        List resultado =  null;
       
     
        em = null;
        try {
            em = getEntityManager();
           
            //creando consulta
           query = em.createNamedQuery("CurriculumProfesion.findByAniosAndAreaLaboral");
           query.setParameter("anios", anios);
           query.setParameter("idAreaLaboral", areaLaboral);
            //"ejecutando" query
            resultado = query.getResultList();
      
        } catch (Exception e) {
            e.printStackTrace();      
        }finally{
            //cerrar el EntityManger
            if (em != null) {
                em.close();
            }
        }
        
        return resultado;
    }
    
    public void create(CurriculumProfesion curriculumProfesion) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Curriculum curriculum = curriculumProfesion.getCurriculum();
            if (curriculum != null) {
                curriculum = em.getReference(curriculum.getClass(), curriculum.getId());
                curriculumProfesion.setCurriculum(curriculum);
            }
            Profesion profesion = curriculumProfesion.getProfesion();
            if (profesion != null) {
                profesion = em.getReference(profesion.getClass(), profesion.getId());
                curriculumProfesion.setProfesion(profesion);
            }
            em.persist(curriculumProfesion);
            if (curriculum != null) {
                curriculum.getCurriculumProfesionList().add(curriculumProfesion);
                curriculum = em.merge(curriculum);
            }
            if (profesion != null) {
                profesion.getCurriculumProfesionList().add(curriculumProfesion);
                profesion = em.merge(profesion);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CurriculumProfesion curriculumProfesion) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CurriculumProfesion persistentCurriculumProfesion = em.find(CurriculumProfesion.class, curriculumProfesion.getId());
            Curriculum curriculumOld = persistentCurriculumProfesion.getCurriculum();
            Curriculum curriculumNew = curriculumProfesion.getCurriculum();
            Profesion profesionOld = persistentCurriculumProfesion.getProfesion();
            Profesion profesionNew = curriculumProfesion.getProfesion();
            if (curriculumNew != null) {
                curriculumNew = em.getReference(curriculumNew.getClass(), curriculumNew.getId());
                curriculumProfesion.setCurriculum(curriculumNew);
            }
            if (profesionNew != null) {
                profesionNew = em.getReference(profesionNew.getClass(), profesionNew.getId());
                curriculumProfesion.setProfesion(profesionNew);
            }
            curriculumProfesion = em.merge(curriculumProfesion);
            if (curriculumOld != null && !curriculumOld.equals(curriculumNew)) {
                curriculumOld.getCurriculumProfesionList().remove(curriculumProfesion);
                curriculumOld = em.merge(curriculumOld);
            }
            if (curriculumNew != null && !curriculumNew.equals(curriculumOld)) {
                curriculumNew.getCurriculumProfesionList().add(curriculumProfesion);
                curriculumNew = em.merge(curriculumNew);
            }
            if (profesionOld != null && !profesionOld.equals(profesionNew)) {
                profesionOld.getCurriculumProfesionList().remove(curriculumProfesion);
                profesionOld = em.merge(profesionOld);
            }
            if (profesionNew != null && !profesionNew.equals(profesionOld)) {
                profesionNew.getCurriculumProfesionList().add(curriculumProfesion);
                profesionNew = em.merge(profesionNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = curriculumProfesion.getId();
                if (findCurriculumProfesion(id) == null) {
                    throw new NonexistentEntityException("The curriculumProfesion with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CurriculumProfesion curriculumProfesion;
            try {
                curriculumProfesion = em.getReference(CurriculumProfesion.class, id);
                curriculumProfesion.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The curriculumProfesion with id " + id + " no longer exists.", enfe);
            }
            Curriculum curriculum = curriculumProfesion.getCurriculum();
            if (curriculum != null) {
                curriculum.getCurriculumProfesionList().remove(curriculumProfesion);
                curriculum = em.merge(curriculum);
            }
            Profesion profesion = curriculumProfesion.getProfesion();
            if (profesion != null) {
                profesion.getCurriculumProfesionList().remove(curriculumProfesion);
                profesion = em.merge(profesion);
            }
            em.remove(curriculumProfesion);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CurriculumProfesion> findCurriculumProfesionEntities() {
        return findCurriculumProfesionEntities(true, -1, -1);
    }

    public List<CurriculumProfesion> findCurriculumProfesionEntities(int maxResults, int firstResult) {
        return findCurriculumProfesionEntities(false, maxResults, firstResult);
    }

    private List<CurriculumProfesion> findCurriculumProfesionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CurriculumProfesion.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CurriculumProfesion findCurriculumProfesion(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CurriculumProfesion.class, id);
        } finally {
            em.close();
        }
    }

    public int getCurriculumProfesionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CurriculumProfesion> rt = cq.from(CurriculumProfesion.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
