package com.workingbriefcase.persistencia.controladores;

import com.workingbriefcase.persistencia.controladores.exceptions.IllegalOrphanException;
import com.workingbriefcase.persistencia.controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.workingbriefcase.persistencia.entities.CurriculumIdioma;
import com.workingbriefcase.persistencia.entities.Idioma;
import java.util.ArrayList;
import java.util.List;
import com.workingbriefcase.persistencia.entities.OfertalaboralIdioma;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Nombre de la clase: IdiomaJpaController
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class IdiomaJpaController implements Serializable {

    public IdiomaJpaController() {
        this.emf = emf = Persistence.createEntityManagerFactory("Working-BriefcasePU");
    }
 
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Idioma idioma) {
        if (idioma.getCurriculumIdiomaList() == null) {
            idioma.setCurriculumIdiomaList(new ArrayList<CurriculumIdioma>());
        }
        if (idioma.getOfertalaboralIdiomaList() == null) {
            idioma.setOfertalaboralIdiomaList(new ArrayList<OfertalaboralIdioma>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<CurriculumIdioma> attachedCurriculumIdiomaList = new ArrayList<CurriculumIdioma>();
            for (CurriculumIdioma curriculumIdiomaListCurriculumIdiomaToAttach : idioma.getCurriculumIdiomaList()) {
                curriculumIdiomaListCurriculumIdiomaToAttach = em.getReference(curriculumIdiomaListCurriculumIdiomaToAttach.getClass(), curriculumIdiomaListCurriculumIdiomaToAttach.getId());
                attachedCurriculumIdiomaList.add(curriculumIdiomaListCurriculumIdiomaToAttach);
            }
            idioma.setCurriculumIdiomaList(attachedCurriculumIdiomaList);
            List<OfertalaboralIdioma> attachedOfertalaboralIdiomaList = new ArrayList<OfertalaboralIdioma>();
            for (OfertalaboralIdioma ofertalaboralIdiomaListOfertalaboralIdiomaToAttach : idioma.getOfertalaboralIdiomaList()) {
                ofertalaboralIdiomaListOfertalaboralIdiomaToAttach = em.getReference(ofertalaboralIdiomaListOfertalaboralIdiomaToAttach.getClass(), ofertalaboralIdiomaListOfertalaboralIdiomaToAttach.getId());
                attachedOfertalaboralIdiomaList.add(ofertalaboralIdiomaListOfertalaboralIdiomaToAttach);
            }
            idioma.setOfertalaboralIdiomaList(attachedOfertalaboralIdiomaList);
            em.persist(idioma);
            for (CurriculumIdioma curriculumIdiomaListCurriculumIdioma : idioma.getCurriculumIdiomaList()) {
                Idioma oldIdiomaOfCurriculumIdiomaListCurriculumIdioma = curriculumIdiomaListCurriculumIdioma.getIdioma();
                curriculumIdiomaListCurriculumIdioma.setIdioma(idioma);
                curriculumIdiomaListCurriculumIdioma = em.merge(curriculumIdiomaListCurriculumIdioma);
                if (oldIdiomaOfCurriculumIdiomaListCurriculumIdioma != null) {
                    oldIdiomaOfCurriculumIdiomaListCurriculumIdioma.getCurriculumIdiomaList().remove(curriculumIdiomaListCurriculumIdioma);
                    oldIdiomaOfCurriculumIdiomaListCurriculumIdioma = em.merge(oldIdiomaOfCurriculumIdiomaListCurriculumIdioma);
                }
            }
            for (OfertalaboralIdioma ofertalaboralIdiomaListOfertalaboralIdioma : idioma.getOfertalaboralIdiomaList()) {
                Idioma oldIdiomaOfOfertalaboralIdiomaListOfertalaboralIdioma = ofertalaboralIdiomaListOfertalaboralIdioma.getIdioma();
                ofertalaboralIdiomaListOfertalaboralIdioma.setIdioma(idioma);
                ofertalaboralIdiomaListOfertalaboralIdioma = em.merge(ofertalaboralIdiomaListOfertalaboralIdioma);
                if (oldIdiomaOfOfertalaboralIdiomaListOfertalaboralIdioma != null) {
                    oldIdiomaOfOfertalaboralIdiomaListOfertalaboralIdioma.getOfertalaboralIdiomaList().remove(ofertalaboralIdiomaListOfertalaboralIdioma);
                    oldIdiomaOfOfertalaboralIdiomaListOfertalaboralIdioma = em.merge(oldIdiomaOfOfertalaboralIdiomaListOfertalaboralIdioma);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Idioma idioma) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Idioma persistentIdioma = em.find(Idioma.class, idioma.getId());
            List<CurriculumIdioma> curriculumIdiomaListOld = persistentIdioma.getCurriculumIdiomaList();
            List<CurriculumIdioma> curriculumIdiomaListNew = idioma.getCurriculumIdiomaList();
            List<OfertalaboralIdioma> ofertalaboralIdiomaListOld = persistentIdioma.getOfertalaboralIdiomaList();
            List<OfertalaboralIdioma> ofertalaboralIdiomaListNew = idioma.getOfertalaboralIdiomaList();
            List<String> illegalOrphanMessages = null;
            for (CurriculumIdioma curriculumIdiomaListOldCurriculumIdioma : curriculumIdiomaListOld) {
                if (!curriculumIdiomaListNew.contains(curriculumIdiomaListOldCurriculumIdioma)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain CurriculumIdioma " + curriculumIdiomaListOldCurriculumIdioma + " since its idioma field is not nullable.");
                }
            }
            for (OfertalaboralIdioma ofertalaboralIdiomaListOldOfertalaboralIdioma : ofertalaboralIdiomaListOld) {
                if (!ofertalaboralIdiomaListNew.contains(ofertalaboralIdiomaListOldOfertalaboralIdioma)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain OfertalaboralIdioma " + ofertalaboralIdiomaListOldOfertalaboralIdioma + " since its idioma field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<CurriculumIdioma> attachedCurriculumIdiomaListNew = new ArrayList<CurriculumIdioma>();
            for (CurriculumIdioma curriculumIdiomaListNewCurriculumIdiomaToAttach : curriculumIdiomaListNew) {
                curriculumIdiomaListNewCurriculumIdiomaToAttach = em.getReference(curriculumIdiomaListNewCurriculumIdiomaToAttach.getClass(), curriculumIdiomaListNewCurriculumIdiomaToAttach.getId());
                attachedCurriculumIdiomaListNew.add(curriculumIdiomaListNewCurriculumIdiomaToAttach);
            }
            curriculumIdiomaListNew = attachedCurriculumIdiomaListNew;
            idioma.setCurriculumIdiomaList(curriculumIdiomaListNew);
            List<OfertalaboralIdioma> attachedOfertalaboralIdiomaListNew = new ArrayList<OfertalaboralIdioma>();
            for (OfertalaboralIdioma ofertalaboralIdiomaListNewOfertalaboralIdiomaToAttach : ofertalaboralIdiomaListNew) {
                ofertalaboralIdiomaListNewOfertalaboralIdiomaToAttach = em.getReference(ofertalaboralIdiomaListNewOfertalaboralIdiomaToAttach.getClass(), ofertalaboralIdiomaListNewOfertalaboralIdiomaToAttach.getId());
                attachedOfertalaboralIdiomaListNew.add(ofertalaboralIdiomaListNewOfertalaboralIdiomaToAttach);
            }
            ofertalaboralIdiomaListNew = attachedOfertalaboralIdiomaListNew;
            idioma.setOfertalaboralIdiomaList(ofertalaboralIdiomaListNew);
            idioma = em.merge(idioma);
            for (CurriculumIdioma curriculumIdiomaListNewCurriculumIdioma : curriculumIdiomaListNew) {
                if (!curriculumIdiomaListOld.contains(curriculumIdiomaListNewCurriculumIdioma)) {
                    Idioma oldIdiomaOfCurriculumIdiomaListNewCurriculumIdioma = curriculumIdiomaListNewCurriculumIdioma.getIdioma();
                    curriculumIdiomaListNewCurriculumIdioma.setIdioma(idioma);
                    curriculumIdiomaListNewCurriculumIdioma = em.merge(curriculumIdiomaListNewCurriculumIdioma);
                    if (oldIdiomaOfCurriculumIdiomaListNewCurriculumIdioma != null && !oldIdiomaOfCurriculumIdiomaListNewCurriculumIdioma.equals(idioma)) {
                        oldIdiomaOfCurriculumIdiomaListNewCurriculumIdioma.getCurriculumIdiomaList().remove(curriculumIdiomaListNewCurriculumIdioma);
                        oldIdiomaOfCurriculumIdiomaListNewCurriculumIdioma = em.merge(oldIdiomaOfCurriculumIdiomaListNewCurriculumIdioma);
                    }
                }
            }
            for (OfertalaboralIdioma ofertalaboralIdiomaListNewOfertalaboralIdioma : ofertalaboralIdiomaListNew) {
                if (!ofertalaboralIdiomaListOld.contains(ofertalaboralIdiomaListNewOfertalaboralIdioma)) {
                    Idioma oldIdiomaOfOfertalaboralIdiomaListNewOfertalaboralIdioma = ofertalaboralIdiomaListNewOfertalaboralIdioma.getIdioma();
                    ofertalaboralIdiomaListNewOfertalaboralIdioma.setIdioma(idioma);
                    ofertalaboralIdiomaListNewOfertalaboralIdioma = em.merge(ofertalaboralIdiomaListNewOfertalaboralIdioma);
                    if (oldIdiomaOfOfertalaboralIdiomaListNewOfertalaboralIdioma != null && !oldIdiomaOfOfertalaboralIdiomaListNewOfertalaboralIdioma.equals(idioma)) {
                        oldIdiomaOfOfertalaboralIdiomaListNewOfertalaboralIdioma.getOfertalaboralIdiomaList().remove(ofertalaboralIdiomaListNewOfertalaboralIdioma);
                        oldIdiomaOfOfertalaboralIdiomaListNewOfertalaboralIdioma = em.merge(oldIdiomaOfOfertalaboralIdiomaListNewOfertalaboralIdioma);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = idioma.getId();
                if (findIdioma(id) == null) {
                    throw new NonexistentEntityException("The idioma with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Idioma idioma;
            try {
                idioma = em.getReference(Idioma.class, id);
                idioma.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The idioma with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<CurriculumIdioma> curriculumIdiomaListOrphanCheck = idioma.getCurriculumIdiomaList();
            for (CurriculumIdioma curriculumIdiomaListOrphanCheckCurriculumIdioma : curriculumIdiomaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Idioma (" + idioma + ") cannot be destroyed since the CurriculumIdioma " + curriculumIdiomaListOrphanCheckCurriculumIdioma + " in its curriculumIdiomaList field has a non-nullable idioma field.");
            }
            List<OfertalaboralIdioma> ofertalaboralIdiomaListOrphanCheck = idioma.getOfertalaboralIdiomaList();
            for (OfertalaboralIdioma ofertalaboralIdiomaListOrphanCheckOfertalaboralIdioma : ofertalaboralIdiomaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Idioma (" + idioma + ") cannot be destroyed since the OfertalaboralIdioma " + ofertalaboralIdiomaListOrphanCheckOfertalaboralIdioma + " in its ofertalaboralIdiomaList field has a non-nullable idioma field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(idioma);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Idioma> findIdiomaEntities() {
        return findIdiomaEntities(true, -1, -1);
    }

    public List<Idioma> findIdiomaEntities(int maxResults, int firstResult) {
        return findIdiomaEntities(false, maxResults, firstResult);
    }

    private List<Idioma> findIdiomaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Idioma.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Idioma findIdioma(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Idioma.class, id);
        } finally {
            em.close();
        }
    }

    public int getIdiomaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Idioma> rt = cq.from(Idioma.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
