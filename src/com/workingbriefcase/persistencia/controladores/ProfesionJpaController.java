package com.workingbriefcase.persistencia.controladores;

import com.workingbriefcase.persistencia.controladores.exceptions.IllegalOrphanException;
import com.workingbriefcase.persistencia.controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.workingbriefcase.persistencia.entities.Arealaboral;
import com.workingbriefcase.persistencia.entities.Empleadoasalariado;
import java.util.ArrayList;
import java.util.List;
import com.workingbriefcase.persistencia.entities.CurriculumProfesion;
import com.workingbriefcase.persistencia.entities.Ofertalaboral;
import com.workingbriefcase.persistencia.entities.Encargado;
import com.workingbriefcase.persistencia.entities.Empleadohora;
import com.workingbriefcase.persistencia.entities.Profesion;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Nombre de la clase: ProfesionJpaController
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class ProfesionJpaController implements Serializable {
       public ProfesionJpaController() {
        this.emf = Persistence.createEntityManagerFactory("Working-BriefcasePU");
    }

 
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    
    //metodos personalizados
     public List<Profesion> findProfesionByAreaLaboral(int codigoAreaLaboral) {
        EntityManager em = getEntityManager();
            Query query;
        
        try {
            
            query =em.createNamedQuery("Profesion.findByAreaLaboral");
                    query.setParameter("idAreaLaboral", codigoAreaLaboral);
            return query.getResultList();
        } finally {
            em.close();
        }
    }
    

    public void create(Profesion profesion) {
        if (profesion.getEmpleadoasalariadoList() == null) {
            profesion.setEmpleadoasalariadoList(new ArrayList<Empleadoasalariado>());
        }
        if (profesion.getCurriculumProfesionList() == null) {
            profesion.setCurriculumProfesionList(new ArrayList<CurriculumProfesion>());
        }
        if (profesion.getOfertalaboralList() == null) {
            profesion.setOfertalaboralList(new ArrayList<Ofertalaboral>());
        }
        if (profesion.getEncargadoList() == null) {
            profesion.setEncargadoList(new ArrayList<Encargado>());
        }
        if (profesion.getEmpleadohoraList() == null) {
            profesion.setEmpleadohoraList(new ArrayList<Empleadohora>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Arealaboral areaLaboral = profesion.getAreaLaboral();
            if (areaLaboral != null) {
                areaLaboral = em.getReference(areaLaboral.getClass(), areaLaboral.getId());
                profesion.setAreaLaboral(areaLaboral);
            }
            List<Empleadoasalariado> attachedEmpleadoasalariadoList = new ArrayList<Empleadoasalariado>();
            for (Empleadoasalariado empleadoasalariadoListEmpleadoasalariadoToAttach : profesion.getEmpleadoasalariadoList()) {
                empleadoasalariadoListEmpleadoasalariadoToAttach = em.getReference(empleadoasalariadoListEmpleadoasalariadoToAttach.getClass(), empleadoasalariadoListEmpleadoasalariadoToAttach.getId());
                attachedEmpleadoasalariadoList.add(empleadoasalariadoListEmpleadoasalariadoToAttach);
            }
            profesion.setEmpleadoasalariadoList(attachedEmpleadoasalariadoList);
            List<CurriculumProfesion> attachedCurriculumProfesionList = new ArrayList<CurriculumProfesion>();
            for (CurriculumProfesion curriculumProfesionListCurriculumProfesionToAttach : profesion.getCurriculumProfesionList()) {
                curriculumProfesionListCurriculumProfesionToAttach = em.getReference(curriculumProfesionListCurriculumProfesionToAttach.getClass(), curriculumProfesionListCurriculumProfesionToAttach.getId());
                attachedCurriculumProfesionList.add(curriculumProfesionListCurriculumProfesionToAttach);
            }
            profesion.setCurriculumProfesionList(attachedCurriculumProfesionList);
            List<Ofertalaboral> attachedOfertalaboralList = new ArrayList<Ofertalaboral>();
            for (Ofertalaboral ofertalaboralListOfertalaboralToAttach : profesion.getOfertalaboralList()) {
                ofertalaboralListOfertalaboralToAttach = em.getReference(ofertalaboralListOfertalaboralToAttach.getClass(), ofertalaboralListOfertalaboralToAttach.getId());
                attachedOfertalaboralList.add(ofertalaboralListOfertalaboralToAttach);
            }
            profesion.setOfertalaboralList(attachedOfertalaboralList);
            List<Encargado> attachedEncargadoList = new ArrayList<Encargado>();
            for (Encargado encargadoListEncargadoToAttach : profesion.getEncargadoList()) {
                encargadoListEncargadoToAttach = em.getReference(encargadoListEncargadoToAttach.getClass(), encargadoListEncargadoToAttach.getId());
                attachedEncargadoList.add(encargadoListEncargadoToAttach);
            }
            profesion.setEncargadoList(attachedEncargadoList);
            List<Empleadohora> attachedEmpleadohoraList = new ArrayList<Empleadohora>();
            for (Empleadohora empleadohoraListEmpleadohoraToAttach : profesion.getEmpleadohoraList()) {
                empleadohoraListEmpleadohoraToAttach = em.getReference(empleadohoraListEmpleadohoraToAttach.getClass(), empleadohoraListEmpleadohoraToAttach.getId());
                attachedEmpleadohoraList.add(empleadohoraListEmpleadohoraToAttach);
            }
            profesion.setEmpleadohoraList(attachedEmpleadohoraList);
            em.persist(profesion);
            if (areaLaboral != null) {
                areaLaboral.getProfesionList().add(profesion);
                areaLaboral = em.merge(areaLaboral);
            }
            for (Empleadoasalariado empleadoasalariadoListEmpleadoasalariado : profesion.getEmpleadoasalariadoList()) {
                Profesion oldProfesionEjercidaOfEmpleadoasalariadoListEmpleadoasalariado = empleadoasalariadoListEmpleadoasalariado.getProfesionEjercida();
                empleadoasalariadoListEmpleadoasalariado.setProfesionEjercida(profesion);
                empleadoasalariadoListEmpleadoasalariado = em.merge(empleadoasalariadoListEmpleadoasalariado);
                if (oldProfesionEjercidaOfEmpleadoasalariadoListEmpleadoasalariado != null) {
                    oldProfesionEjercidaOfEmpleadoasalariadoListEmpleadoasalariado.getEmpleadoasalariadoList().remove(empleadoasalariadoListEmpleadoasalariado);
                    oldProfesionEjercidaOfEmpleadoasalariadoListEmpleadoasalariado = em.merge(oldProfesionEjercidaOfEmpleadoasalariadoListEmpleadoasalariado);
                }
            }
            for (CurriculumProfesion curriculumProfesionListCurriculumProfesion : profesion.getCurriculumProfesionList()) {
                Profesion oldProfesionOfCurriculumProfesionListCurriculumProfesion = curriculumProfesionListCurriculumProfesion.getProfesion();
                curriculumProfesionListCurriculumProfesion.setProfesion(profesion);
                curriculumProfesionListCurriculumProfesion = em.merge(curriculumProfesionListCurriculumProfesion);
                if (oldProfesionOfCurriculumProfesionListCurriculumProfesion != null) {
                    oldProfesionOfCurriculumProfesionListCurriculumProfesion.getCurriculumProfesionList().remove(curriculumProfesionListCurriculumProfesion);
                    oldProfesionOfCurriculumProfesionListCurriculumProfesion = em.merge(oldProfesionOfCurriculumProfesionListCurriculumProfesion);
                }
            }
            for (Ofertalaboral ofertalaboralListOfertalaboral : profesion.getOfertalaboralList()) {
                Profesion oldProfesionRequeridaOfOfertalaboralListOfertalaboral = ofertalaboralListOfertalaboral.getProfesionRequerida();
                ofertalaboralListOfertalaboral.setProfesionRequerida(profesion);
                ofertalaboralListOfertalaboral = em.merge(ofertalaboralListOfertalaboral);
                if (oldProfesionRequeridaOfOfertalaboralListOfertalaboral != null) {
                    oldProfesionRequeridaOfOfertalaboralListOfertalaboral.getOfertalaboralList().remove(ofertalaboralListOfertalaboral);
                    oldProfesionRequeridaOfOfertalaboralListOfertalaboral = em.merge(oldProfesionRequeridaOfOfertalaboralListOfertalaboral);
                }
            }
            for (Encargado encargadoListEncargado : profesion.getEncargadoList()) {
                Profesion oldProfesionEjercidaOfEncargadoListEncargado = encargadoListEncargado.getProfesionEjercida();
                encargadoListEncargado.setProfesionEjercida(profesion);
                encargadoListEncargado = em.merge(encargadoListEncargado);
                if (oldProfesionEjercidaOfEncargadoListEncargado != null) {
                    oldProfesionEjercidaOfEncargadoListEncargado.getEncargadoList().remove(encargadoListEncargado);
                    oldProfesionEjercidaOfEncargadoListEncargado = em.merge(oldProfesionEjercidaOfEncargadoListEncargado);
                }
            }
            for (Empleadohora empleadohoraListEmpleadohora : profesion.getEmpleadohoraList()) {
                Profesion oldProfesionEjercidaOfEmpleadohoraListEmpleadohora = empleadohoraListEmpleadohora.getProfesionEjercida();
                empleadohoraListEmpleadohora.setProfesionEjercida(profesion);
                empleadohoraListEmpleadohora = em.merge(empleadohoraListEmpleadohora);
                if (oldProfesionEjercidaOfEmpleadohoraListEmpleadohora != null) {
                    oldProfesionEjercidaOfEmpleadohoraListEmpleadohora.getEmpleadohoraList().remove(empleadohoraListEmpleadohora);
                    oldProfesionEjercidaOfEmpleadohoraListEmpleadohora = em.merge(oldProfesionEjercidaOfEmpleadohoraListEmpleadohora);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Profesion profesion) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Profesion persistentProfesion = em.find(Profesion.class, profesion.getId());
            Arealaboral areaLaboralOld = persistentProfesion.getAreaLaboral();
            Arealaboral areaLaboralNew = profesion.getAreaLaboral();
            List<Empleadoasalariado> empleadoasalariadoListOld = persistentProfesion.getEmpleadoasalariadoList();
            List<Empleadoasalariado> empleadoasalariadoListNew = profesion.getEmpleadoasalariadoList();
            List<CurriculumProfesion> curriculumProfesionListOld = persistentProfesion.getCurriculumProfesionList();
            List<CurriculumProfesion> curriculumProfesionListNew = profesion.getCurriculumProfesionList();
            List<Ofertalaboral> ofertalaboralListOld = persistentProfesion.getOfertalaboralList();
            List<Ofertalaboral> ofertalaboralListNew = profesion.getOfertalaboralList();
            List<Encargado> encargadoListOld = persistentProfesion.getEncargadoList();
            List<Encargado> encargadoListNew = profesion.getEncargadoList();
            List<Empleadohora> empleadohoraListOld = persistentProfesion.getEmpleadohoraList();
            List<Empleadohora> empleadohoraListNew = profesion.getEmpleadohoraList();
            List<String> illegalOrphanMessages = null;
            for (Empleadoasalariado empleadoasalariadoListOldEmpleadoasalariado : empleadoasalariadoListOld) {
                if (!empleadoasalariadoListNew.contains(empleadoasalariadoListOldEmpleadoasalariado)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Empleadoasalariado " + empleadoasalariadoListOldEmpleadoasalariado + " since its profesionEjercida field is not nullable.");
                }
            }
            for (CurriculumProfesion curriculumProfesionListOldCurriculumProfesion : curriculumProfesionListOld) {
                if (!curriculumProfesionListNew.contains(curriculumProfesionListOldCurriculumProfesion)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain CurriculumProfesion " + curriculumProfesionListOldCurriculumProfesion + " since its profesion field is not nullable.");
                }
            }
            for (Ofertalaboral ofertalaboralListOldOfertalaboral : ofertalaboralListOld) {
                if (!ofertalaboralListNew.contains(ofertalaboralListOldOfertalaboral)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Ofertalaboral " + ofertalaboralListOldOfertalaboral + " since its profesionRequerida field is not nullable.");
                }
            }
            for (Encargado encargadoListOldEncargado : encargadoListOld) {
                if (!encargadoListNew.contains(encargadoListOldEncargado)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Encargado " + encargadoListOldEncargado + " since its profesionEjercida field is not nullable.");
                }
            }
            for (Empleadohora empleadohoraListOldEmpleadohora : empleadohoraListOld) {
                if (!empleadohoraListNew.contains(empleadohoraListOldEmpleadohora)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Empleadohora " + empleadohoraListOldEmpleadohora + " since its profesionEjercida field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (areaLaboralNew != null) {
                areaLaboralNew = em.getReference(areaLaboralNew.getClass(), areaLaboralNew.getId());
                profesion.setAreaLaboral(areaLaboralNew);
            }
            List<Empleadoasalariado> attachedEmpleadoasalariadoListNew = new ArrayList<Empleadoasalariado>();
            for (Empleadoasalariado empleadoasalariadoListNewEmpleadoasalariadoToAttach : empleadoasalariadoListNew) {
                empleadoasalariadoListNewEmpleadoasalariadoToAttach = em.getReference(empleadoasalariadoListNewEmpleadoasalariadoToAttach.getClass(), empleadoasalariadoListNewEmpleadoasalariadoToAttach.getId());
                attachedEmpleadoasalariadoListNew.add(empleadoasalariadoListNewEmpleadoasalariadoToAttach);
            }
            empleadoasalariadoListNew = attachedEmpleadoasalariadoListNew;
            profesion.setEmpleadoasalariadoList(empleadoasalariadoListNew);
            List<CurriculumProfesion> attachedCurriculumProfesionListNew = new ArrayList<CurriculumProfesion>();
            for (CurriculumProfesion curriculumProfesionListNewCurriculumProfesionToAttach : curriculumProfesionListNew) {
                curriculumProfesionListNewCurriculumProfesionToAttach = em.getReference(curriculumProfesionListNewCurriculumProfesionToAttach.getClass(), curriculumProfesionListNewCurriculumProfesionToAttach.getId());
                attachedCurriculumProfesionListNew.add(curriculumProfesionListNewCurriculumProfesionToAttach);
            }
            curriculumProfesionListNew = attachedCurriculumProfesionListNew;
            profesion.setCurriculumProfesionList(curriculumProfesionListNew);
            List<Ofertalaboral> attachedOfertalaboralListNew = new ArrayList<Ofertalaboral>();
            for (Ofertalaboral ofertalaboralListNewOfertalaboralToAttach : ofertalaboralListNew) {
                ofertalaboralListNewOfertalaboralToAttach = em.getReference(ofertalaboralListNewOfertalaboralToAttach.getClass(), ofertalaboralListNewOfertalaboralToAttach.getId());
                attachedOfertalaboralListNew.add(ofertalaboralListNewOfertalaboralToAttach);
            }
            ofertalaboralListNew = attachedOfertalaboralListNew;
            profesion.setOfertalaboralList(ofertalaboralListNew);
            List<Encargado> attachedEncargadoListNew = new ArrayList<Encargado>();
            for (Encargado encargadoListNewEncargadoToAttach : encargadoListNew) {
                encargadoListNewEncargadoToAttach = em.getReference(encargadoListNewEncargadoToAttach.getClass(), encargadoListNewEncargadoToAttach.getId());
                attachedEncargadoListNew.add(encargadoListNewEncargadoToAttach);
            }
            encargadoListNew = attachedEncargadoListNew;
            profesion.setEncargadoList(encargadoListNew);
            List<Empleadohora> attachedEmpleadohoraListNew = new ArrayList<Empleadohora>();
            for (Empleadohora empleadohoraListNewEmpleadohoraToAttach : empleadohoraListNew) {
                empleadohoraListNewEmpleadohoraToAttach = em.getReference(empleadohoraListNewEmpleadohoraToAttach.getClass(), empleadohoraListNewEmpleadohoraToAttach.getId());
                attachedEmpleadohoraListNew.add(empleadohoraListNewEmpleadohoraToAttach);
            }
            empleadohoraListNew = attachedEmpleadohoraListNew;
            profesion.setEmpleadohoraList(empleadohoraListNew);
            profesion = em.merge(profesion);
            if (areaLaboralOld != null && !areaLaboralOld.equals(areaLaboralNew)) {
                areaLaboralOld.getProfesionList().remove(profesion);
                areaLaboralOld = em.merge(areaLaboralOld);
            }
            if (areaLaboralNew != null && !areaLaboralNew.equals(areaLaboralOld)) {
                areaLaboralNew.getProfesionList().add(profesion);
                areaLaboralNew = em.merge(areaLaboralNew);
            }
            for (Empleadoasalariado empleadoasalariadoListNewEmpleadoasalariado : empleadoasalariadoListNew) {
                if (!empleadoasalariadoListOld.contains(empleadoasalariadoListNewEmpleadoasalariado)) {
                    Profesion oldProfesionEjercidaOfEmpleadoasalariadoListNewEmpleadoasalariado = empleadoasalariadoListNewEmpleadoasalariado.getProfesionEjercida();
                    empleadoasalariadoListNewEmpleadoasalariado.setProfesionEjercida(profesion);
                    empleadoasalariadoListNewEmpleadoasalariado = em.merge(empleadoasalariadoListNewEmpleadoasalariado);
                    if (oldProfesionEjercidaOfEmpleadoasalariadoListNewEmpleadoasalariado != null && !oldProfesionEjercidaOfEmpleadoasalariadoListNewEmpleadoasalariado.equals(profesion)) {
                        oldProfesionEjercidaOfEmpleadoasalariadoListNewEmpleadoasalariado.getEmpleadoasalariadoList().remove(empleadoasalariadoListNewEmpleadoasalariado);
                        oldProfesionEjercidaOfEmpleadoasalariadoListNewEmpleadoasalariado = em.merge(oldProfesionEjercidaOfEmpleadoasalariadoListNewEmpleadoasalariado);
                    }
                }
            }
            for (CurriculumProfesion curriculumProfesionListNewCurriculumProfesion : curriculumProfesionListNew) {
                if (!curriculumProfesionListOld.contains(curriculumProfesionListNewCurriculumProfesion)) {
                    Profesion oldProfesionOfCurriculumProfesionListNewCurriculumProfesion = curriculumProfesionListNewCurriculumProfesion.getProfesion();
                    curriculumProfesionListNewCurriculumProfesion.setProfesion(profesion);
                    curriculumProfesionListNewCurriculumProfesion = em.merge(curriculumProfesionListNewCurriculumProfesion);
                    if (oldProfesionOfCurriculumProfesionListNewCurriculumProfesion != null && !oldProfesionOfCurriculumProfesionListNewCurriculumProfesion.equals(profesion)) {
                        oldProfesionOfCurriculumProfesionListNewCurriculumProfesion.getCurriculumProfesionList().remove(curriculumProfesionListNewCurriculumProfesion);
                        oldProfesionOfCurriculumProfesionListNewCurriculumProfesion = em.merge(oldProfesionOfCurriculumProfesionListNewCurriculumProfesion);
                    }
                }
            }
            for (Ofertalaboral ofertalaboralListNewOfertalaboral : ofertalaboralListNew) {
                if (!ofertalaboralListOld.contains(ofertalaboralListNewOfertalaboral)) {
                    Profesion oldProfesionRequeridaOfOfertalaboralListNewOfertalaboral = ofertalaboralListNewOfertalaboral.getProfesionRequerida();
                    ofertalaboralListNewOfertalaboral.setProfesionRequerida(profesion);
                    ofertalaboralListNewOfertalaboral = em.merge(ofertalaboralListNewOfertalaboral);
                    if (oldProfesionRequeridaOfOfertalaboralListNewOfertalaboral != null && !oldProfesionRequeridaOfOfertalaboralListNewOfertalaboral.equals(profesion)) {
                        oldProfesionRequeridaOfOfertalaboralListNewOfertalaboral.getOfertalaboralList().remove(ofertalaboralListNewOfertalaboral);
                        oldProfesionRequeridaOfOfertalaboralListNewOfertalaboral = em.merge(oldProfesionRequeridaOfOfertalaboralListNewOfertalaboral);
                    }
                }
            }
            for (Encargado encargadoListNewEncargado : encargadoListNew) {
                if (!encargadoListOld.contains(encargadoListNewEncargado)) {
                    Profesion oldProfesionEjercidaOfEncargadoListNewEncargado = encargadoListNewEncargado.getProfesionEjercida();
                    encargadoListNewEncargado.setProfesionEjercida(profesion);
                    encargadoListNewEncargado = em.merge(encargadoListNewEncargado);
                    if (oldProfesionEjercidaOfEncargadoListNewEncargado != null && !oldProfesionEjercidaOfEncargadoListNewEncargado.equals(profesion)) {
                        oldProfesionEjercidaOfEncargadoListNewEncargado.getEncargadoList().remove(encargadoListNewEncargado);
                        oldProfesionEjercidaOfEncargadoListNewEncargado = em.merge(oldProfesionEjercidaOfEncargadoListNewEncargado);
                    }
                }
            }
            for (Empleadohora empleadohoraListNewEmpleadohora : empleadohoraListNew) {
                if (!empleadohoraListOld.contains(empleadohoraListNewEmpleadohora)) {
                    Profesion oldProfesionEjercidaOfEmpleadohoraListNewEmpleadohora = empleadohoraListNewEmpleadohora.getProfesionEjercida();
                    empleadohoraListNewEmpleadohora.setProfesionEjercida(profesion);
                    empleadohoraListNewEmpleadohora = em.merge(empleadohoraListNewEmpleadohora);
                    if (oldProfesionEjercidaOfEmpleadohoraListNewEmpleadohora != null && !oldProfesionEjercidaOfEmpleadohoraListNewEmpleadohora.equals(profesion)) {
                        oldProfesionEjercidaOfEmpleadohoraListNewEmpleadohora.getEmpleadohoraList().remove(empleadohoraListNewEmpleadohora);
                        oldProfesionEjercidaOfEmpleadohoraListNewEmpleadohora = em.merge(oldProfesionEjercidaOfEmpleadohoraListNewEmpleadohora);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = profesion.getId();
                if (findProfesion(id) == null) {
                    throw new NonexistentEntityException("The profesion with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Profesion profesion;
            try {
                profesion = em.getReference(Profesion.class, id);
                profesion.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The profesion with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Empleadoasalariado> empleadoasalariadoListOrphanCheck = profesion.getEmpleadoasalariadoList();
            for (Empleadoasalariado empleadoasalariadoListOrphanCheckEmpleadoasalariado : empleadoasalariadoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Profesion (" + profesion + ") cannot be destroyed since the Empleadoasalariado " + empleadoasalariadoListOrphanCheckEmpleadoasalariado + " in its empleadoasalariadoList field has a non-nullable profesionEjercida field.");
            }
            List<CurriculumProfesion> curriculumProfesionListOrphanCheck = profesion.getCurriculumProfesionList();
            for (CurriculumProfesion curriculumProfesionListOrphanCheckCurriculumProfesion : curriculumProfesionListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Profesion (" + profesion + ") cannot be destroyed since the CurriculumProfesion " + curriculumProfesionListOrphanCheckCurriculumProfesion + " in its curriculumProfesionList field has a non-nullable profesion field.");
            }
            List<Ofertalaboral> ofertalaboralListOrphanCheck = profesion.getOfertalaboralList();
            for (Ofertalaboral ofertalaboralListOrphanCheckOfertalaboral : ofertalaboralListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Profesion (" + profesion + ") cannot be destroyed since the Ofertalaboral " + ofertalaboralListOrphanCheckOfertalaboral + " in its ofertalaboralList field has a non-nullable profesionRequerida field.");
            }
            List<Encargado> encargadoListOrphanCheck = profesion.getEncargadoList();
            for (Encargado encargadoListOrphanCheckEncargado : encargadoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Profesion (" + profesion + ") cannot be destroyed since the Encargado " + encargadoListOrphanCheckEncargado + " in its encargadoList field has a non-nullable profesionEjercida field.");
            }
            List<Empleadohora> empleadohoraListOrphanCheck = profesion.getEmpleadohoraList();
            for (Empleadohora empleadohoraListOrphanCheckEmpleadohora : empleadohoraListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Profesion (" + profesion + ") cannot be destroyed since the Empleadohora " + empleadohoraListOrphanCheckEmpleadohora + " in its empleadohoraList field has a non-nullable profesionEjercida field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Arealaboral areaLaboral = profesion.getAreaLaboral();
            if (areaLaboral != null) {
                areaLaboral.getProfesionList().remove(profesion);
                areaLaboral = em.merge(areaLaboral);
            }
            em.remove(profesion);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Profesion> findProfesionEntities() {
        return findProfesionEntities(true, -1, -1);
    }

    public List<Profesion> findProfesionEntities(int maxResults, int firstResult) {
        return findProfesionEntities(false, maxResults, firstResult);
    }

    private List<Profesion> findProfesionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Profesion.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Profesion findProfesion(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Profesion.class, id);
        } finally {
            em.close();
        }
    }

    public int getProfesionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Profesion> rt = cq.from(Profesion.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
