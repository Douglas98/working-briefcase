package com.workingbriefcase.persistencia.controladores;

import com.workingbriefcase.persistencia.controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.workingbriefcase.persistencia.entities.Idioma;
import com.workingbriefcase.persistencia.entities.Ofertalaboral;
import com.workingbriefcase.persistencia.entities.OfertalaboralIdioma;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * Nombre de la clase: OfertalaboralIdiomaJpaController
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class OfertalaboralIdiomaJpaController implements Serializable {

    public OfertalaboralIdiomaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(OfertalaboralIdioma ofertalaboralIdioma) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Idioma idioma = ofertalaboralIdioma.getIdioma();
            if (idioma != null) {
                idioma = em.getReference(idioma.getClass(), idioma.getId());
                ofertalaboralIdioma.setIdioma(idioma);
            }
            Ofertalaboral ofertaLaboral = ofertalaboralIdioma.getOfertaLaboral();
            if (ofertaLaboral != null) {
                ofertaLaboral = em.getReference(ofertaLaboral.getClass(), ofertaLaboral.getId());
                ofertalaboralIdioma.setOfertaLaboral(ofertaLaboral);
            }
            em.persist(ofertalaboralIdioma);
            if (idioma != null) {
                idioma.getOfertalaboralIdiomaList().add(ofertalaboralIdioma);
                idioma = em.merge(idioma);
            }
            if (ofertaLaboral != null) {
                ofertaLaboral.getOfertalaboralIdiomaList().add(ofertalaboralIdioma);
                ofertaLaboral = em.merge(ofertaLaboral);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(OfertalaboralIdioma ofertalaboralIdioma) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            OfertalaboralIdioma persistentOfertalaboralIdioma = em.find(OfertalaboralIdioma.class, ofertalaboralIdioma.getId());
            Idioma idiomaOld = persistentOfertalaboralIdioma.getIdioma();
            Idioma idiomaNew = ofertalaboralIdioma.getIdioma();
            Ofertalaboral ofertaLaboralOld = persistentOfertalaboralIdioma.getOfertaLaboral();
            Ofertalaboral ofertaLaboralNew = ofertalaboralIdioma.getOfertaLaboral();
            if (idiomaNew != null) {
                idiomaNew = em.getReference(idiomaNew.getClass(), idiomaNew.getId());
                ofertalaboralIdioma.setIdioma(idiomaNew);
            }
            if (ofertaLaboralNew != null) {
                ofertaLaboralNew = em.getReference(ofertaLaboralNew.getClass(), ofertaLaboralNew.getId());
                ofertalaboralIdioma.setOfertaLaboral(ofertaLaboralNew);
            }
            ofertalaboralIdioma = em.merge(ofertalaboralIdioma);
            if (idiomaOld != null && !idiomaOld.equals(idiomaNew)) {
                idiomaOld.getOfertalaboralIdiomaList().remove(ofertalaboralIdioma);
                idiomaOld = em.merge(idiomaOld);
            }
            if (idiomaNew != null && !idiomaNew.equals(idiomaOld)) {
                idiomaNew.getOfertalaboralIdiomaList().add(ofertalaboralIdioma);
                idiomaNew = em.merge(idiomaNew);
            }
            if (ofertaLaboralOld != null && !ofertaLaboralOld.equals(ofertaLaboralNew)) {
                ofertaLaboralOld.getOfertalaboralIdiomaList().remove(ofertalaboralIdioma);
                ofertaLaboralOld = em.merge(ofertaLaboralOld);
            }
            if (ofertaLaboralNew != null && !ofertaLaboralNew.equals(ofertaLaboralOld)) {
                ofertaLaboralNew.getOfertalaboralIdiomaList().add(ofertalaboralIdioma);
                ofertaLaboralNew = em.merge(ofertaLaboralNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = ofertalaboralIdioma.getId();
                if (findOfertalaboralIdioma(id) == null) {
                    throw new NonexistentEntityException("The ofertalaboralIdioma with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            OfertalaboralIdioma ofertalaboralIdioma;
            try {
                ofertalaboralIdioma = em.getReference(OfertalaboralIdioma.class, id);
                ofertalaboralIdioma.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ofertalaboralIdioma with id " + id + " no longer exists.", enfe);
            }
            Idioma idioma = ofertalaboralIdioma.getIdioma();
            if (idioma != null) {
                idioma.getOfertalaboralIdiomaList().remove(ofertalaboralIdioma);
                idioma = em.merge(idioma);
            }
            Ofertalaboral ofertaLaboral = ofertalaboralIdioma.getOfertaLaboral();
            if (ofertaLaboral != null) {
                ofertaLaboral.getOfertalaboralIdiomaList().remove(ofertalaboralIdioma);
                ofertaLaboral = em.merge(ofertaLaboral);
            }
            em.remove(ofertalaboralIdioma);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<OfertalaboralIdioma> findOfertalaboralIdiomaEntities() {
        return findOfertalaboralIdiomaEntities(true, -1, -1);
    }

    public List<OfertalaboralIdioma> findOfertalaboralIdiomaEntities(int maxResults, int firstResult) {
        return findOfertalaboralIdiomaEntities(false, maxResults, firstResult);
    }

    private List<OfertalaboralIdioma> findOfertalaboralIdiomaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(OfertalaboralIdioma.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public OfertalaboralIdioma findOfertalaboralIdioma(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(OfertalaboralIdioma.class, id);
        } finally {
            em.close();
        }
    }

    public int getOfertalaboralIdiomaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<OfertalaboralIdioma> rt = cq.from(OfertalaboralIdioma.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
