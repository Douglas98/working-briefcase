package com.workingbriefcase.persistencia.controladores;

import com.workingbriefcase.persistencia.controladores.exceptions.IllegalOrphanException;
import com.workingbriefcase.persistencia.controladores.exceptions.NonexistentEntityException;
import com.workingbriefcase.persistencia.entities.Cargo;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.workingbriefcase.persistencia.entities.Empleadoasalariado;
import java.util.ArrayList;
import java.util.List;
import com.workingbriefcase.persistencia.entities.Ofertalaboral;
import com.workingbriefcase.persistencia.entities.Encargado;
import com.workingbriefcase.persistencia.entities.Empleadohora;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Nombre de la clase: CargoJpaController
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class CargoJpaController implements Serializable {

    public CargoJpaController() {
        this.emf = Persistence.createEntityManagerFactory("Working-BriefcasePU");
    }
    public CargoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Cargo cargo) {
        if (cargo.getEmpleadoasalariadoList() == null) {
            cargo.setEmpleadoasalariadoList(new ArrayList<Empleadoasalariado>());
        }
        if (cargo.getOfertalaboralList() == null) {
            cargo.setOfertalaboralList(new ArrayList<Ofertalaboral>());
        }
        if (cargo.getEncargadoList() == null) {
            cargo.setEncargadoList(new ArrayList<Encargado>());
        }
        if (cargo.getEmpleadohoraList() == null) {
            cargo.setEmpleadohoraList(new ArrayList<Empleadohora>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Empleadoasalariado> attachedEmpleadoasalariadoList = new ArrayList<Empleadoasalariado>();
            for (Empleadoasalariado empleadoasalariadoListEmpleadoasalariadoToAttach : cargo.getEmpleadoasalariadoList()) {
                empleadoasalariadoListEmpleadoasalariadoToAttach = em.getReference(empleadoasalariadoListEmpleadoasalariadoToAttach.getClass(), empleadoasalariadoListEmpleadoasalariadoToAttach.getId());
                attachedEmpleadoasalariadoList.add(empleadoasalariadoListEmpleadoasalariadoToAttach);
            }
            cargo.setEmpleadoasalariadoList(attachedEmpleadoasalariadoList);
            List<Ofertalaboral> attachedOfertalaboralList = new ArrayList<Ofertalaboral>();
            for (Ofertalaboral ofertalaboralListOfertalaboralToAttach : cargo.getOfertalaboralList()) {
                ofertalaboralListOfertalaboralToAttach = em.getReference(ofertalaboralListOfertalaboralToAttach.getClass(), ofertalaboralListOfertalaboralToAttach.getId());
                attachedOfertalaboralList.add(ofertalaboralListOfertalaboralToAttach);
            }
            cargo.setOfertalaboralList(attachedOfertalaboralList);
            List<Encargado> attachedEncargadoList = new ArrayList<Encargado>();
            for (Encargado encargadoListEncargadoToAttach : cargo.getEncargadoList()) {
                encargadoListEncargadoToAttach = em.getReference(encargadoListEncargadoToAttach.getClass(), encargadoListEncargadoToAttach.getId());
                attachedEncargadoList.add(encargadoListEncargadoToAttach);
            }
            cargo.setEncargadoList(attachedEncargadoList);
            List<Empleadohora> attachedEmpleadohoraList = new ArrayList<Empleadohora>();
            for (Empleadohora empleadohoraListEmpleadohoraToAttach : cargo.getEmpleadohoraList()) {
                empleadohoraListEmpleadohoraToAttach = em.getReference(empleadohoraListEmpleadohoraToAttach.getClass(), empleadohoraListEmpleadohoraToAttach.getId());
                attachedEmpleadohoraList.add(empleadohoraListEmpleadohoraToAttach);
            }
            cargo.setEmpleadohoraList(attachedEmpleadohoraList);
            em.persist(cargo);
            for (Empleadoasalariado empleadoasalariadoListEmpleadoasalariado : cargo.getEmpleadoasalariadoList()) {
                Cargo oldCargoDesempennadoOfEmpleadoasalariadoListEmpleadoasalariado = empleadoasalariadoListEmpleadoasalariado.getCargoDesempennado();
                empleadoasalariadoListEmpleadoasalariado.setCargoDesempennado(cargo);
                empleadoasalariadoListEmpleadoasalariado = em.merge(empleadoasalariadoListEmpleadoasalariado);
                if (oldCargoDesempennadoOfEmpleadoasalariadoListEmpleadoasalariado != null) {
                    oldCargoDesempennadoOfEmpleadoasalariadoListEmpleadoasalariado.getEmpleadoasalariadoList().remove(empleadoasalariadoListEmpleadoasalariado);
                    oldCargoDesempennadoOfEmpleadoasalariadoListEmpleadoasalariado = em.merge(oldCargoDesempennadoOfEmpleadoasalariadoListEmpleadoasalariado);
                }
            }
            for (Ofertalaboral ofertalaboralListOfertalaboral : cargo.getOfertalaboralList()) {
                Cargo oldCargoSolicitadoOfOfertalaboralListOfertalaboral = ofertalaboralListOfertalaboral.getCargoSolicitado();
                ofertalaboralListOfertalaboral.setCargoSolicitado(cargo);
                ofertalaboralListOfertalaboral = em.merge(ofertalaboralListOfertalaboral);
                if (oldCargoSolicitadoOfOfertalaboralListOfertalaboral != null) {
                    oldCargoSolicitadoOfOfertalaboralListOfertalaboral.getOfertalaboralList().remove(ofertalaboralListOfertalaboral);
                    oldCargoSolicitadoOfOfertalaboralListOfertalaboral = em.merge(oldCargoSolicitadoOfOfertalaboralListOfertalaboral);
                }
            }
            for (Encargado encargadoListEncargado : cargo.getEncargadoList()) {
                Cargo oldCargoDesepennadoOfEncargadoListEncargado = encargadoListEncargado.getCargoDesepennado();
                encargadoListEncargado.setCargoDesepennado(cargo);
                encargadoListEncargado = em.merge(encargadoListEncargado);
                if (oldCargoDesepennadoOfEncargadoListEncargado != null) {
                    oldCargoDesepennadoOfEncargadoListEncargado.getEncargadoList().remove(encargadoListEncargado);
                    oldCargoDesepennadoOfEncargadoListEncargado = em.merge(oldCargoDesepennadoOfEncargadoListEncargado);
                }
            }
            for (Empleadohora empleadohoraListEmpleadohora : cargo.getEmpleadohoraList()) {
                Cargo oldCargoDesempennadoOfEmpleadohoraListEmpleadohora = empleadohoraListEmpleadohora.getCargoDesempennado();
                empleadohoraListEmpleadohora.setCargoDesempennado(cargo);
                empleadohoraListEmpleadohora = em.merge(empleadohoraListEmpleadohora);
                if (oldCargoDesempennadoOfEmpleadohoraListEmpleadohora != null) {
                    oldCargoDesempennadoOfEmpleadohoraListEmpleadohora.getEmpleadohoraList().remove(empleadohoraListEmpleadohora);
                    oldCargoDesempennadoOfEmpleadohoraListEmpleadohora = em.merge(oldCargoDesempennadoOfEmpleadohoraListEmpleadohora);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Cargo cargo) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
                 if (cargo.getEmpleadoasalariadoList() == null) {
            cargo.setEmpleadoasalariadoList(new ArrayList<Empleadoasalariado>());
        }
        if (cargo.getOfertalaboralList() == null) {
            cargo.setOfertalaboralList(new ArrayList<Ofertalaboral>());
        }
        if (cargo.getEncargadoList() == null) {
            cargo.setEncargadoList(new ArrayList<Encargado>());
        }
        if (cargo.getEmpleadohoraList() == null) {
            cargo.setEmpleadohoraList(new ArrayList<Empleadohora>());
        }
            em = getEntityManager();
            em.getTransaction().begin();
            Cargo persistentCargo = em.find(Cargo.class, cargo.getId());
            List<Empleadoasalariado> empleadoasalariadoListOld = persistentCargo.getEmpleadoasalariadoList();
            List<Empleadoasalariado> empleadoasalariadoListNew = cargo.getEmpleadoasalariadoList();
            List<Ofertalaboral> ofertalaboralListOld = persistentCargo.getOfertalaboralList();
            List<Ofertalaboral> ofertalaboralListNew = cargo.getOfertalaboralList();
            List<Encargado> encargadoListOld = persistentCargo.getEncargadoList();
            List<Encargado> encargadoListNew = cargo.getEncargadoList();
            List<Empleadohora> empleadohoraListOld = persistentCargo.getEmpleadohoraList();
            List<Empleadohora> empleadohoraListNew = cargo.getEmpleadohoraList();
            List<String> illegalOrphanMessages = null;
            for (Empleadoasalariado empleadoasalariadoListOldEmpleadoasalariado : empleadoasalariadoListOld) {
                if (!empleadoasalariadoListNew.contains(empleadoasalariadoListOldEmpleadoasalariado)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Empleadoasalariado " + empleadoasalariadoListOldEmpleadoasalariado + " since its cargoDesempennado field is not nullable.");
                }
            }
            for (Ofertalaboral ofertalaboralListOldOfertalaboral : ofertalaboralListOld) {
                if (!ofertalaboralListNew.contains(ofertalaboralListOldOfertalaboral)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Ofertalaboral " + ofertalaboralListOldOfertalaboral + " since its cargoSolicitado field is not nullable.");
                }
            }
            for (Encargado encargadoListOldEncargado : encargadoListOld) {
                if (!encargadoListNew.contains(encargadoListOldEncargado)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Encargado " + encargadoListOldEncargado + " since its cargoDesepennado field is not nullable.");
                }
            }
            for (Empleadohora empleadohoraListOldEmpleadohora : empleadohoraListOld) {
                if (!empleadohoraListNew.contains(empleadohoraListOldEmpleadohora)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Empleadohora " + empleadohoraListOldEmpleadohora + " since its cargoDesempennado field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Empleadoasalariado> attachedEmpleadoasalariadoListNew = new ArrayList<Empleadoasalariado>();
            for (Empleadoasalariado empleadoasalariadoListNewEmpleadoasalariadoToAttach : empleadoasalariadoListNew) {
                empleadoasalariadoListNewEmpleadoasalariadoToAttach = em.getReference(empleadoasalariadoListNewEmpleadoasalariadoToAttach.getClass(), empleadoasalariadoListNewEmpleadoasalariadoToAttach.getId());
                attachedEmpleadoasalariadoListNew.add(empleadoasalariadoListNewEmpleadoasalariadoToAttach);
            }
            empleadoasalariadoListNew = attachedEmpleadoasalariadoListNew;
            cargo.setEmpleadoasalariadoList(empleadoasalariadoListNew);
            List<Ofertalaboral> attachedOfertalaboralListNew = new ArrayList<Ofertalaboral>();
            for (Ofertalaboral ofertalaboralListNewOfertalaboralToAttach : ofertalaboralListNew) {
                ofertalaboralListNewOfertalaboralToAttach = em.getReference(ofertalaboralListNewOfertalaboralToAttach.getClass(), ofertalaboralListNewOfertalaboralToAttach.getId());
                attachedOfertalaboralListNew.add(ofertalaboralListNewOfertalaboralToAttach);
            }
            ofertalaboralListNew = attachedOfertalaboralListNew;
            cargo.setOfertalaboralList(ofertalaboralListNew);
            List<Encargado> attachedEncargadoListNew = new ArrayList<Encargado>();
            for (Encargado encargadoListNewEncargadoToAttach : encargadoListNew) {
                encargadoListNewEncargadoToAttach = em.getReference(encargadoListNewEncargadoToAttach.getClass(), encargadoListNewEncargadoToAttach.getId());
                attachedEncargadoListNew.add(encargadoListNewEncargadoToAttach);
            }
            encargadoListNew = attachedEncargadoListNew;
            cargo.setEncargadoList(encargadoListNew);
            List<Empleadohora> attachedEmpleadohoraListNew = new ArrayList<Empleadohora>();
            for (Empleadohora empleadohoraListNewEmpleadohoraToAttach : empleadohoraListNew) {
                empleadohoraListNewEmpleadohoraToAttach = em.getReference(empleadohoraListNewEmpleadohoraToAttach.getClass(), empleadohoraListNewEmpleadohoraToAttach.getId());
                attachedEmpleadohoraListNew.add(empleadohoraListNewEmpleadohoraToAttach);
            }
            empleadohoraListNew = attachedEmpleadohoraListNew;
            cargo.setEmpleadohoraList(empleadohoraListNew);
            cargo = em.merge(cargo);
            for (Empleadoasalariado empleadoasalariadoListNewEmpleadoasalariado : empleadoasalariadoListNew) {
                if (!empleadoasalariadoListOld.contains(empleadoasalariadoListNewEmpleadoasalariado)) {
                    Cargo oldCargoDesempennadoOfEmpleadoasalariadoListNewEmpleadoasalariado = empleadoasalariadoListNewEmpleadoasalariado.getCargoDesempennado();
                    empleadoasalariadoListNewEmpleadoasalariado.setCargoDesempennado(cargo);
                    empleadoasalariadoListNewEmpleadoasalariado = em.merge(empleadoasalariadoListNewEmpleadoasalariado);
                    if (oldCargoDesempennadoOfEmpleadoasalariadoListNewEmpleadoasalariado != null && !oldCargoDesempennadoOfEmpleadoasalariadoListNewEmpleadoasalariado.equals(cargo)) {
                        oldCargoDesempennadoOfEmpleadoasalariadoListNewEmpleadoasalariado.getEmpleadoasalariadoList().remove(empleadoasalariadoListNewEmpleadoasalariado);
                        oldCargoDesempennadoOfEmpleadoasalariadoListNewEmpleadoasalariado = em.merge(oldCargoDesempennadoOfEmpleadoasalariadoListNewEmpleadoasalariado);
                    }
                }
            }
            for (Ofertalaboral ofertalaboralListNewOfertalaboral : ofertalaboralListNew) {
                if (!ofertalaboralListOld.contains(ofertalaboralListNewOfertalaboral)) {
                    Cargo oldCargoSolicitadoOfOfertalaboralListNewOfertalaboral = ofertalaboralListNewOfertalaboral.getCargoSolicitado();
                    ofertalaboralListNewOfertalaboral.setCargoSolicitado(cargo);
                    ofertalaboralListNewOfertalaboral = em.merge(ofertalaboralListNewOfertalaboral);
                    if (oldCargoSolicitadoOfOfertalaboralListNewOfertalaboral != null && !oldCargoSolicitadoOfOfertalaboralListNewOfertalaboral.equals(cargo)) {
                        oldCargoSolicitadoOfOfertalaboralListNewOfertalaboral.getOfertalaboralList().remove(ofertalaboralListNewOfertalaboral);
                        oldCargoSolicitadoOfOfertalaboralListNewOfertalaboral = em.merge(oldCargoSolicitadoOfOfertalaboralListNewOfertalaboral);
                    }
                }
            }
            for (Encargado encargadoListNewEncargado : encargadoListNew) {
                if (!encargadoListOld.contains(encargadoListNewEncargado)) {
                    Cargo oldCargoDesepennadoOfEncargadoListNewEncargado = encargadoListNewEncargado.getCargoDesepennado();
                    encargadoListNewEncargado.setCargoDesepennado(cargo);
                    encargadoListNewEncargado = em.merge(encargadoListNewEncargado);
                    if (oldCargoDesepennadoOfEncargadoListNewEncargado != null && !oldCargoDesepennadoOfEncargadoListNewEncargado.equals(cargo)) {
                        oldCargoDesepennadoOfEncargadoListNewEncargado.getEncargadoList().remove(encargadoListNewEncargado);
                        oldCargoDesepennadoOfEncargadoListNewEncargado = em.merge(oldCargoDesepennadoOfEncargadoListNewEncargado);
                    }
                }
            }
            for (Empleadohora empleadohoraListNewEmpleadohora : empleadohoraListNew) {
                if (!empleadohoraListOld.contains(empleadohoraListNewEmpleadohora)) {
                    Cargo oldCargoDesempennadoOfEmpleadohoraListNewEmpleadohora = empleadohoraListNewEmpleadohora.getCargoDesempennado();
                    empleadohoraListNewEmpleadohora.setCargoDesempennado(cargo);
                    empleadohoraListNewEmpleadohora = em.merge(empleadohoraListNewEmpleadohora);
                    if (oldCargoDesempennadoOfEmpleadohoraListNewEmpleadohora != null && !oldCargoDesempennadoOfEmpleadohoraListNewEmpleadohora.equals(cargo)) {
                        oldCargoDesempennadoOfEmpleadohoraListNewEmpleadohora.getEmpleadohoraList().remove(empleadohoraListNewEmpleadohora);
                        oldCargoDesempennadoOfEmpleadohoraListNewEmpleadohora = em.merge(oldCargoDesempennadoOfEmpleadohoraListNewEmpleadohora);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = cargo.getId();
                if (findCargo(id) == null) {
                    throw new NonexistentEntityException("The cargo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cargo cargo;
            try {
                cargo = em.getReference(Cargo.class, id);
                cargo.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cargo with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Empleadoasalariado> empleadoasalariadoListOrphanCheck = cargo.getEmpleadoasalariadoList();
            for (Empleadoasalariado empleadoasalariadoListOrphanCheckEmpleadoasalariado : empleadoasalariadoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Cargo (" + cargo + ") cannot be destroyed since the Empleadoasalariado " + empleadoasalariadoListOrphanCheckEmpleadoasalariado + " in its empleadoasalariadoList field has a non-nullable cargoDesempennado field.");
            }
            List<Ofertalaboral> ofertalaboralListOrphanCheck = cargo.getOfertalaboralList();
            for (Ofertalaboral ofertalaboralListOrphanCheckOfertalaboral : ofertalaboralListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Cargo (" + cargo + ") cannot be destroyed since the Ofertalaboral " + ofertalaboralListOrphanCheckOfertalaboral + " in its ofertalaboralList field has a non-nullable cargoSolicitado field.");
            }
            List<Encargado> encargadoListOrphanCheck = cargo.getEncargadoList();
            for (Encargado encargadoListOrphanCheckEncargado : encargadoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Cargo (" + cargo + ") cannot be destroyed since the Encargado " + encargadoListOrphanCheckEncargado + " in its encargadoList field has a non-nullable cargoDesepennado field.");
            }
            List<Empleadohora> empleadohoraListOrphanCheck = cargo.getEmpleadohoraList();
            for (Empleadohora empleadohoraListOrphanCheckEmpleadohora : empleadohoraListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Cargo (" + cargo + ") cannot be destroyed since the Empleadohora " + empleadohoraListOrphanCheckEmpleadohora + " in its empleadohoraList field has a non-nullable cargoDesempennado field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(cargo);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Cargo> findCargoEntities() {
        return findCargoEntities(true, -1, -1);
    }

    public List<Cargo> findCargoEntities(int maxResults, int firstResult) {
        return findCargoEntities(false, maxResults, firstResult);
    }

    private List<Cargo> findCargoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Cargo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Cargo findCargo(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Cargo.class, id);
        } finally {
            em.close();
        }
    }

    public int getCargoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Cargo> rt = cq.from(Cargo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
