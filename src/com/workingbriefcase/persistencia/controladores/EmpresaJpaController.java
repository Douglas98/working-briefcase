package com.workingbriefcase.persistencia.controladores;

import com.workingbriefcase.persistencia.controladores.exceptions.IllegalOrphanException;
import com.workingbriefcase.persistencia.controladores.exceptions.NonexistentEntityException;
import com.workingbriefcase.persistencia.entities.Empresa;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.workingbriefcase.persistencia.entities.Encargado;
import com.workingbriefcase.persistencia.entities.Municipio;
import com.workingbriefcase.persistencia.entities.Pais;
import com.workingbriefcase.persistencia.entities.Sectorempresarial;
import com.workingbriefcase.persistencia.entities.Usuario;
import com.workingbriefcase.persistencia.entities.Ofertalaboral;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Nombre de la clase: EmpresaJpaController
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class EmpresaJpaController implements Serializable {

     public EmpresaJpaController() {
        this.emf = Persistence.createEntityManagerFactory("Working-BriefcasePU");
    }
    public EmpresaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
 public Usuario logIn(String usuario, String contrasenna, int rol){
        Usuario salida;
        EntityManager em;
        Query query;
        List resultado;
        
        salida = null;
        em = null;
        try {
            em = getEntityManager();
            
            //creando consulta
            query =em.createNamedQuery("Usuario.LogIn");
            query.setParameter("nombreUsuario", usuario);
            query.setParameter("contrasenia", contrasenna);
            query.setParameter("rol", rol);
            
            //"ejecutando" query
            resultado = query.getResultList();
            
            //solo debería haber 1 item en el resultado si tuvo exito sino 0
            if(resultado.size() > 0){
                salida = (Usuario) resultado.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();      
        }finally{
            //cerrar el EntityManger
            if (em != null) {
                em.close();
            }
        }
        
        return salida;
    }
 
 public Empresa obtenerEmpresaByIdUsuario(int idUsuario){
        Empresa salida;
        EntityManager em;
        Query query;
        List resultado;
        
        salida = null;
        em = null;
        try {
            em = getEntityManager();
            
            //creando consulta
            query =em.createNamedQuery("Empresa.findEmpresaIdByUsuarioId");
            query.setParameter("UsuarioId", idUsuario);
         
            //"ejecutando" query
            resultado = query.getResultList();
            
            //solo debería haber 1 item en el resultado si tuvo exito sino 0
            if(resultado.size() > 0){
                salida = (Empresa) resultado.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();      
        }finally{
            //cerrar el EntityManger
            if (em != null) {
                em.close();
            }
        }
        
        return salida;
    }

    public void create(Empresa empresa) {
        if (empresa.getOfertalaboralList() == null) {
            empresa.setOfertalaboralList(new ArrayList<Ofertalaboral>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Encargado encargado = empresa.getEncargado();
            if (encargado != null) {
                encargado = em.getReference(encargado.getClass(), encargado.getId());
                empresa.setEncargado(encargado);
            }
            Municipio municipioActual = empresa.getMunicipioActual();
            if (municipioActual != null) {
                municipioActual = em.getReference(municipioActual.getClass(), municipioActual.getId());
                empresa.setMunicipioActual(municipioActual);
            }
            Pais nacionalidad = empresa.getNacionalidad();
            if (nacionalidad != null) {
                nacionalidad = em.getReference(nacionalidad.getClass(), nacionalidad.getId());
                empresa.setNacionalidad(nacionalidad);
            }
            Sectorempresarial sectorEmpresarial = empresa.getSectorEmpresarial();
            if (sectorEmpresarial != null) {
                sectorEmpresarial = em.getReference(sectorEmpresarial.getClass(), sectorEmpresarial.getId());
                empresa.setSectorEmpresarial(sectorEmpresarial);
            }
            Usuario usuario = empresa.getUsuario();
            if (usuario != null) {
                usuario = em.getReference(usuario.getClass(), usuario.getId());
                empresa.setUsuario(usuario);
            }
            List<Ofertalaboral> attachedOfertalaboralList = new ArrayList<Ofertalaboral>();
            for (Ofertalaboral ofertalaboralListOfertalaboralToAttach : empresa.getOfertalaboralList()) {
                ofertalaboralListOfertalaboralToAttach = em.getReference(ofertalaboralListOfertalaboralToAttach.getClass(), ofertalaboralListOfertalaboralToAttach.getId());
                attachedOfertalaboralList.add(ofertalaboralListOfertalaboralToAttach);
            }
            empresa.setOfertalaboralList(attachedOfertalaboralList);
            em.persist(empresa);
            if (encargado != null) {
                encargado.getEmpresaList().add(empresa);
                encargado = em.merge(encargado);
            }
            if (municipioActual != null) {
                municipioActual.getEmpresaList().add(empresa);
                municipioActual = em.merge(municipioActual);
            }
            if (nacionalidad != null) {
                nacionalidad.getEmpresaList().add(empresa);
                nacionalidad = em.merge(nacionalidad);
            }
            if (sectorEmpresarial != null) {
                sectorEmpresarial.getEmpresaList().add(empresa);
                sectorEmpresarial = em.merge(sectorEmpresarial);
            }
            if (usuario != null) {
                usuario.getEmpresaList().add(empresa);
                usuario = em.merge(usuario);
            }
            for (Ofertalaboral ofertalaboralListOfertalaboral : empresa.getOfertalaboralList()) {
                Empresa oldEmpresaPublicadoraOfOfertalaboralListOfertalaboral = ofertalaboralListOfertalaboral.getEmpresaPublicadora();
                ofertalaboralListOfertalaboral.setEmpresaPublicadora(empresa);
                ofertalaboralListOfertalaboral = em.merge(ofertalaboralListOfertalaboral);
                if (oldEmpresaPublicadoraOfOfertalaboralListOfertalaboral != null) {
                    oldEmpresaPublicadoraOfOfertalaboralListOfertalaboral.getOfertalaboralList().remove(ofertalaboralListOfertalaboral);
                    oldEmpresaPublicadoraOfOfertalaboralListOfertalaboral = em.merge(oldEmpresaPublicadoraOfOfertalaboralListOfertalaboral);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Empresa empresa) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Empresa persistentEmpresa = em.find(Empresa.class, empresa.getId());
            Encargado encargadoOld = persistentEmpresa.getEncargado();
            Encargado encargadoNew = empresa.getEncargado();
            Municipio municipioActualOld = persistentEmpresa.getMunicipioActual();
            Municipio municipioActualNew = empresa.getMunicipioActual();
            Pais nacionalidadOld = persistentEmpresa.getNacionalidad();
            Pais nacionalidadNew = empresa.getNacionalidad();
            Sectorempresarial sectorEmpresarialOld = persistentEmpresa.getSectorEmpresarial();
            Sectorempresarial sectorEmpresarialNew = empresa.getSectorEmpresarial();
            Usuario usuarioOld = persistentEmpresa.getUsuario();
            Usuario usuarioNew = empresa.getUsuario();
            List<Ofertalaboral> ofertalaboralListOld = persistentEmpresa.getOfertalaboralList();
            List<Ofertalaboral> ofertalaboralListNew = empresa.getOfertalaboralList();
            List<String> illegalOrphanMessages = null;
            for (Ofertalaboral ofertalaboralListOldOfertalaboral : ofertalaboralListOld) {
                if (!ofertalaboralListNew.contains(ofertalaboralListOldOfertalaboral)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Ofertalaboral " + ofertalaboralListOldOfertalaboral + " since its empresaPublicadora field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (encargadoNew != null) {
                encargadoNew = em.getReference(encargadoNew.getClass(), encargadoNew.getId());
                empresa.setEncargado(encargadoNew);
            }
            if (municipioActualNew != null) {
                municipioActualNew = em.getReference(municipioActualNew.getClass(), municipioActualNew.getId());
                empresa.setMunicipioActual(municipioActualNew);
            }
            if (nacionalidadNew != null) {
                nacionalidadNew = em.getReference(nacionalidadNew.getClass(), nacionalidadNew.getId());
                empresa.setNacionalidad(nacionalidadNew);
            }
            if (sectorEmpresarialNew != null) {
                sectorEmpresarialNew = em.getReference(sectorEmpresarialNew.getClass(), sectorEmpresarialNew.getId());
                empresa.setSectorEmpresarial(sectorEmpresarialNew);
            }
            if (usuarioNew != null) {
                usuarioNew = em.getReference(usuarioNew.getClass(), usuarioNew.getId());
                empresa.setUsuario(usuarioNew);
            }
            List<Ofertalaboral> attachedOfertalaboralListNew = new ArrayList<Ofertalaboral>();
            for (Ofertalaboral ofertalaboralListNewOfertalaboralToAttach : ofertalaboralListNew) {
                ofertalaboralListNewOfertalaboralToAttach = em.getReference(ofertalaboralListNewOfertalaboralToAttach.getClass(), ofertalaboralListNewOfertalaboralToAttach.getId());
                attachedOfertalaboralListNew.add(ofertalaboralListNewOfertalaboralToAttach);
            }
            ofertalaboralListNew = attachedOfertalaboralListNew;
            empresa.setOfertalaboralList(ofertalaboralListNew);
            empresa = em.merge(empresa);
            if (encargadoOld != null && !encargadoOld.equals(encargadoNew)) {
                encargadoOld.getEmpresaList().remove(empresa);
                encargadoOld = em.merge(encargadoOld);
            }
            if (encargadoNew != null && !encargadoNew.equals(encargadoOld)) {
                encargadoNew.getEmpresaList().add(empresa);
                encargadoNew = em.merge(encargadoNew);
            }
            if (municipioActualOld != null && !municipioActualOld.equals(municipioActualNew)) {
                municipioActualOld.getEmpresaList().remove(empresa);
                municipioActualOld = em.merge(municipioActualOld);
            }
            if (municipioActualNew != null && !municipioActualNew.equals(municipioActualOld)) {
                municipioActualNew.getEmpresaList().add(empresa);
                municipioActualNew = em.merge(municipioActualNew);
            }
            if (nacionalidadOld != null && !nacionalidadOld.equals(nacionalidadNew)) {
                nacionalidadOld.getEmpresaList().remove(empresa);
                nacionalidadOld = em.merge(nacionalidadOld);
            }
            if (nacionalidadNew != null && !nacionalidadNew.equals(nacionalidadOld)) {
                nacionalidadNew.getEmpresaList().add(empresa);
                nacionalidadNew = em.merge(nacionalidadNew);
            }
            if (sectorEmpresarialOld != null && !sectorEmpresarialOld.equals(sectorEmpresarialNew)) {
                sectorEmpresarialOld.getEmpresaList().remove(empresa);
                sectorEmpresarialOld = em.merge(sectorEmpresarialOld);
            }
            if (sectorEmpresarialNew != null && !sectorEmpresarialNew.equals(sectorEmpresarialOld)) {
                sectorEmpresarialNew.getEmpresaList().add(empresa);
                sectorEmpresarialNew = em.merge(sectorEmpresarialNew);
            }
            if (usuarioOld != null && !usuarioOld.equals(usuarioNew)) {
                usuarioOld.getEmpresaList().remove(empresa);
                usuarioOld = em.merge(usuarioOld);
            }
            if (usuarioNew != null && !usuarioNew.equals(usuarioOld)) {
                usuarioNew.getEmpresaList().add(empresa);
                usuarioNew = em.merge(usuarioNew);
            }
            for (Ofertalaboral ofertalaboralListNewOfertalaboral : ofertalaboralListNew) {
                if (!ofertalaboralListOld.contains(ofertalaboralListNewOfertalaboral)) {
                    Empresa oldEmpresaPublicadoraOfOfertalaboralListNewOfertalaboral = ofertalaboralListNewOfertalaboral.getEmpresaPublicadora();
                    ofertalaboralListNewOfertalaboral.setEmpresaPublicadora(empresa);
                    ofertalaboralListNewOfertalaboral = em.merge(ofertalaboralListNewOfertalaboral);
                    if (oldEmpresaPublicadoraOfOfertalaboralListNewOfertalaboral != null && !oldEmpresaPublicadoraOfOfertalaboralListNewOfertalaboral.equals(empresa)) {
                        oldEmpresaPublicadoraOfOfertalaboralListNewOfertalaboral.getOfertalaboralList().remove(ofertalaboralListNewOfertalaboral);
                        oldEmpresaPublicadoraOfOfertalaboralListNewOfertalaboral = em.merge(oldEmpresaPublicadoraOfOfertalaboralListNewOfertalaboral);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = empresa.getId();
                if (findEmpresa(id) == null) {
                    throw new NonexistentEntityException("The empresa with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Empresa empresa;
            try {
                empresa = em.getReference(Empresa.class, id);
                empresa.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The empresa with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Ofertalaboral> ofertalaboralListOrphanCheck = empresa.getOfertalaboralList();
            for (Ofertalaboral ofertalaboralListOrphanCheckOfertalaboral : ofertalaboralListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Empresa (" + empresa + ") cannot be destroyed since the Ofertalaboral " + ofertalaboralListOrphanCheckOfertalaboral + " in its ofertalaboralList field has a non-nullable empresaPublicadora field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Encargado encargado = empresa.getEncargado();
            if (encargado != null) {
                encargado.getEmpresaList().remove(empresa);
                encargado = em.merge(encargado);
            }
            Municipio municipioActual = empresa.getMunicipioActual();
            if (municipioActual != null) {
                municipioActual.getEmpresaList().remove(empresa);
                municipioActual = em.merge(municipioActual);
            }
            Pais nacionalidad = empresa.getNacionalidad();
            if (nacionalidad != null) {
                nacionalidad.getEmpresaList().remove(empresa);
                nacionalidad = em.merge(nacionalidad);
            }
            Sectorempresarial sectorEmpresarial = empresa.getSectorEmpresarial();
            if (sectorEmpresarial != null) {
                sectorEmpresarial.getEmpresaList().remove(empresa);
                sectorEmpresarial = em.merge(sectorEmpresarial);
            }
            Usuario usuario = empresa.getUsuario();
            if (usuario != null) {
                usuario.getEmpresaList().remove(empresa);
                usuario = em.merge(usuario);
            }
            em.remove(empresa);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Empresa> findEmpresaEntities() {
        return findEmpresaEntities(true, -1, -1);
    }

    public List<Empresa> findEmpresaEntities(int maxResults, int firstResult) {
        return findEmpresaEntities(false, maxResults, firstResult);
    }

    private List<Empresa> findEmpresaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Empresa.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Empresa findEmpresa(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Empresa.class, id);
        } finally {
            em.close();
        }
    }

    public int getEmpresaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Empresa> rt = cq.from(Empresa.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
